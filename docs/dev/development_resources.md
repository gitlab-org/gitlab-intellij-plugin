# Development resources

In this doc, you can find a list of useful resources related with building plugins for the JetBrains IDEs as well as references for learning Kotlin.

## Plugin basics

- [IntelliJ Platform Plugin SDK: Developing a Plugin](https://plugins.jetbrains.com/docs/intellij/creating-plugin-project.html)
- [IntelliJ Platform Plugin SDK: Plugin Structure](https://plugins.jetbrains.com/docs/intellij/plugin-structure.html)
- [YouTube: Busy Plugin Developers Playlist](https://www.youtube.com/playlist?list=PLQ176FUIyIUZRWGCFY7G9V5zaM00THymY)
- [Youtube: Working with Gradle in IntelliJ IDEA](https://www.youtube.com/watch?v=6V6G3RyxEMk)
- [IntelliJ IDEA Open API and Plugin Development (Forum)](https://intellij-support.jetbrains.com/hc/en-us/community/topics/200366979-IntelliJ-IDEA-Open-API-and-Plugin-Development)
- [Plugin Dev: IntelliJ (blog)](https://www.plugin-dev.com/intellij/)

### UI Elements

- [IntelliJ UI Platform Guidelines](https://jetbrains.design/intellij/)
- [IntelliJ Platform Plugin SDK: Swing Components](https://plugins.jetbrains.com/docs/intellij/misc-swing-components.html)
- [IntelliJ Platform Plugin SDK: UI Tests](https://plugins.jetbrains.com/docs/intellij/testing-plugins.html#ui-tests)

### Reference documentations

- [IntelliJ Platform Plugin SDK: Persisting Sensitive Data](https://plugins.jetbrains.com/docs/intellij/persisting-sensitive-data.html)
- [IntelliJ Platform Plugin SDK: Persistent State Component](https://plugins.jetbrains.com/docs/intellij/persisting-state-of-components.html#using-persistentstatecomponent)
- [IntelliJ Platform Plugin SDK: Declaring Application Settings](https://plugins.jetbrains.com/docs/intellij/settings-guide.html#declaring-application-settings)
- [Kotlin UI DSL Version 2](https://plugins.jetbrains.com/docs/intellij/kotlin-ui-dsl-version-2.html)
- [Ghost Text API code base](https://github.com/JetBrains/intellij-community/tree/master/platform/platform-impl/src/com/intellij/codeInsight/inline/completion)
- [IntelliJ Platform Plugin SDK: Services](https://plugins.jetbrains.com/docs/intellij/plugin-services.html)
- [IntelliJ Platform Plugin SDK: Messaging Infrastructure](https://plugins.jetbrains.com/docs/intellij/messaging-infrastructure.html)
- [IntelliJ Platform Plugin SDK: General Threading Rules](https://plugins.jetbrains.com/docs/intellij/general-threading-rules.html)
- [IntelliJ Platform Plugin SDK: Disposer and Disposable](https://plugins.jetbrains.com/docs/intellij/disposers.html)
- [IntelliJ Platform Plugin SDK: Actions](https://plugins.jetbrains.com/docs/intellij/basic-action-system.html)

### Example codes

- [IntelliJ Platform Plugin SDK: Gradle IntelliJ Plugin - Usage Examples](https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin-examples.html) \
  _(list of opensource plugins using Gradle Project method)_
- [Intellij Platform Explorer](https://plugins.jetbrains.com/intellij-platform-explorer/extensions) _(search for plugins using specific extension points or listeners)_
- [IntelliJ Platform Plugin SDK: Example Plugins Implemented in Kotlin](https://plugins.jetbrains.com/docs/intellij/using-kotlin.html#example-plugins-implemented-in-kotlin)
- [GitHub: IntelliJ IDEA Community Edition & IntelliJ Platform](https://github.com/JetBrains/intellij-community/)
- [GitHub: IntelliJ SDK Docs Code Samples](https://github.com/JetBrains/intellij-sdk-code-samples/tree/main)
- [Demo Dialogs using Kotlin UI DSL Version 2](https://github.com/JetBrains/intellij-community/tree/idea/231.9011.34/platform/platform-impl/src/com/intellij/internal/ui/uiDslShowcase)

## Learning Kotlin

- [Kotlin Documentation: Get Started with Kotlin](https://kotlinlang.org/docs/getting-started.html)
- [Atomic Kotlin](https://www.atomickotlin.com/atomickotlin/) (Online Course + Ebook)
- [DZone: Mockito](https://dzone.com/refcardz/mockito)
- [Gradle Tips & Tricks](https://docs.google.com/presentation/d/10-px53GqXE3iM4hKeWbRk7Y5Hf6XFy_JjA-LJGIWZjg/edit#slide=id.g1300c038e5_1_70)

## Code Suggestions API

- [API reference](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist)

## Writing tests

- Test framework and assertions: [Kotest](https://kotest.io/)
- Mocking: [Mockk](https://mockk.io/)
- [Best Practices for Unit Testing in Kotlin](https://phauer.com/2018/best-practices-unit-testing-kotlin/)
