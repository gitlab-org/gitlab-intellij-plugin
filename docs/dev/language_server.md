# Language Server

This document explains how the GitLab Duo plugin and the
[GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp)
are integrated. It's intended to help you understand how features can be integrated.

## LSP

The GitLab Language server is based on the established
[Language Server Protocol Specification](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/).
Some additional messages supported by our Language Server are documented in
[Supported messages](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/blob/main/docs/supported_messages.md).

## LSP4J

The [LSP4J library](https://github.com/eclipse-lsp4j/lsp4j) implements a
[server](#server) and a [client](#client) to interface with our Language Server integration.

> [!note]
> We decided to use this approach, instead of the
> [native solution in the JetBrains SDK documentation](https://plugins.jetbrains.com/docs/intellij/language-server-protocol.html),
> because we want to support both community and paid editions. That is not possible with the native plugin LSP integration.

### Client

Our `GitLabLanguageServerClient` implements the `LanguageClient` interface from LSP4J.
It handles [notifications](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#notificationMessage)
sent from the Language Server to our extension.

```mermaid
sequenceDiagram
  participant Language Server
  participant GitLab Duo
  Language Server-->>GitLab Duo: notification (ex: $/gitlab/codeSuggestions/stateChange)
  Note right of GitLab Duo: Notification messages <br>don't result in responses.
```

### Server

Our `GitLabLanguageServer` implements the `LanguageServer` interface from LSP4J.
It sends [notifications](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#notificationMessage)
and [requests](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#requestMessage)
to the Language Server.

```mermaid
sequenceDiagram
  participant GitLab Duo
  participant Language Server
  GitLab Duo-->>Language Server: notification (ex: $/gitlab/didChangeDocumentInActiveEditor)
  Note right of Language Server: Notification messages <br>don't result in responses.

  GitLab Duo-->>Language Server: request (ex: textDocument/inlineCompletion)
  Note right of Language Server: Request messages <br>always result in responses.
  Language Server-->>GitLab Duo: response
```

## How to integrate a new Language Server feature

1. Determine the messages to support.

   - If the communication is from the **client to the Language Server**, add the messages in the [server](#server).
   - If the communication is from the **Language Server to the client**, add the messages in the [client](#client).

1. Make sure the messages sent and received are POJOs (plain old Java objects).
   Use the data class to ensure correct serialization and deserialization.
1. Write the required tests to ensure that the integration of the new message works as expected.

## Debugging

By default, the plugin starts its Language Server from a binary file downloaded from the latest release in
the [GitLab Language Server Package Registry](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/packages/).

When debugging a feature that integrates with the Language Server, it may be helpful instead to run the plugin with your
local clone of the Language Server project. This will allow you to more immediately see the effects of changes, and, if
you start an inspector client, set breakpoints in the Language Server code.

To do this, follow these steps:

1. Run the `watch` script in the directory for your local clone of the
   [GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp) project.

   ```shell
   npm run watch -- --editor jetbrains
   ```

   This will automatically rebuild
   the `gitlab-lsp` project on every change and copy the build artifacts to `tmp/language-server` in this project.
   For more details, see the [documentation on watch mode](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp#watch-mode).
1. Run the plugin with the environment variable `USE_LOCAL_LSP` set to `true`. The `Run Plugin with Local LSP` run
   configuration will do this for you. Note that you will need to restart the plugin after each change to the Language Server.
1. Start an inspector client.
   1. In a Chrome browser, open [`chrome://inspect`](chrome://inspect).
   1. Select **Configure**, and ensure your target host and port are listed. Use `localhost:6010`.
   1. Select **Open dedicated DevTools for Node**.
   1. Select the **Sources** tab.
      - Nested under `file://` you will find the `gitlab-lsp` directory structure.
      - Navigate through it to relevant files to set breakpoints.
   1. Debug! Breakpoints set by clicking line numbers in the **Sources** files in the Chrome inspector will stop when
      reached by your locally running plugin.
