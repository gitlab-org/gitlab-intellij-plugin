# Jetbrains plugin architecture

This document gives you a high-level overview of the main components of the plugin.

## What does the plugin do?

The Jetbrains plugin (developed using [IntelliJ Platform Plugin SDK](https://plugins.jetbrains.com/docs/intellij/developing-plugins.html)) is meant to provide
functionality to the developers working with IntelliJ IDEA and other JetBrains IDEs. It provides a set of features that enhance the development experience and productivity, such as:

### Authentication/Authorization

- Authenticates through personal access token
- Detects current GitLab project
- Confirms access to Code Suggestions and GitLab Duo Chat features.

<details><summary>Authentication diagram</summary>

```mermaid
sequenceDiagram
    actor User
    User->>IntelliJ: Open IntelliJ project
    IntelliJ->>+GitLab Duo for JetBrains: GitLabProjectStartup.execute()

    %% Document each request sent to the API.
    GitLab Duo for JetBrains->>+GitLab API: GET /personal_access_tokens/self
    alt Valid personal access token
        GitLab API->>GitLab Duo for JetBrains: 200 OK
    else
        break Invalid personal access token
            GitLab API->>-GitLab Duo for JetBrains: 401 Unauthorized
            GitLab Duo for JetBrains->>IntelliJ: Show Notification
            note over GitLab Duo for JetBrains,IntelliJ: [GitLab Duo] You need to configure your<br/> GitLab credentials first.
        end
    end
```

</details>

### Code Suggestions

- Enables you to write code more efficiently by displaying code suggestions as you type and generating code based on comments.

<details><summary>Code suggestions diagram</summary>

```mermaid
sequenceDiagram
  actor User

  User->>+IntelliJ: Opens a file
  User->>+IntelliJ: Starts editing file
  IntelliJ->>+GitLab Duo for JetBrains: SuggestionsProvider.getSuggestionDebounced()
  GitLab Duo for JetBrains->>+GitLab API: POST /api/v4/code_suggestions/completions
  GitLab API->>+AI Gateway: POST /v2/code/completions
  AI Gateway->>-GitLab API: 200 OK
  GitLab API->>-GitLab Duo for JetBrains: 200 OK

  GitLab Duo for JetBrains->>-IntelliJ: [InlineCompletionItem]
```

</details>

- Code Suggestions through the [Inline Completion Provider](https://plugins.jetbrains.com/docs/intellij/intellij-platform-extension-point-list.html#editorextensionpointsxml) EP.
  - Code Completion
  - Code Generation

### GitLab Duo Chat

- General Chat
- Code Selection context-aware
  - Explain Code
  - Generate Tests
  - Refactor Code
  - Fix Code

### Duo Quick Chat

Enhances the developer experience in JetBrains by integrating the chat functionality within the editor window.

- General Chat
- Code Selection context-aware
  - Explain Code
  - Fix Code
  - Generate Tests
  - Refactor Code

<details>
<summary>Architecture Diagram</summary>

![Duo Quick Chat Architecture](assets/quick_chat_architecture.png)

</details>

<details>
<summary>High-level diagram of interactions (sending a message)</summary>

```mermaid
sequenceDiagram
   participant User
   participant QuickChatView
   participant ChatController
   participant GitLab
   User->>QuickChatView: Send prompt
   QuickChatView->>ChatController: NewPromptMessage
   ChatController->>ChatController: handleViewOnMessage(NewPromptMessage)
   ChatController->>GitLab: ChatMutation
   GitLab->>ChatController: requestId
   ChatController->>QuickChatView: addRecord(ChatRecord)
   ChatController->>GitLab: Subscribe to message update
   GitLab->>ChatController: chunk
   ChatController->>QuickChatView: updateRecord(ChatRecord)
```

</details>

## Components

1. **IntelliJ Platform Integration**: The plugin leverages the IntelliJ Platform SDK to integrate into the JetBrains IDEs. This allows the plugin to access the IDE's features, APIs, and user interface.

1. **Code Suggestions Feature**: This component is responsible for providing relevant code suggestions by using advanced natural language processing to generate high-quality code completions.

1. **GitLab Duo Chat Feature**: The GitLab Duo Chat Assistant is an AI-powered conversational interface that assists developers with a wide range of tasks, such as answering questions, generating tests, and explaining or refactoring code. It utilizes large language models and integrates with the plugin's other components to provide a comprehensive development assistance experience.

1. **User Interface**: The plugin's UI is designed to be intuitive and user-friendly, providing easy access to the various features and functionalities. This includes UI elements for invoking the code suggestions, interacting with the GitLab Duo Chat Assistant, and configuring the plugin's settings.

1. **Configuration and Settings**: The plugin allows users to customize various settings, such as enabling/disabling code suggestions and GitLab Duo Chat, GitLab instance configuration, token configuration.

![Plugin key components](assets/architecture.png)
