# JetBrains GitLab Duo Plugin releases

This process describes the steps for a successful release for the JetBrains GitLab Duo plugin. If any questions come
up during this process, reach out in the `#f_jetbrains_plugin` Slack channel.

1. [Determine the patch type](#determine-the-patch-type)
1. [Create the branch and merge request](#create-the-branch-and-merge-request).
   1. [Release a hotfix](#release-a-hotfix)
1. [Releasing to alpha channel](#release-to-alpha-channel)
1. [Releasing to stable channel](#release-to-stable-channel)

## Determine the patch type

1. Determine if the release is a `major`, `minor`, or `patch` release. Use this command
   to check all commit titles after the last release:

   ```shell
   git log --format='%s' $(git describe --abbrev=0 --tags HEAD)..HEAD | grep -v 'Merge branch'
   ```

   - Releasing a new major feature like Code Suggestions or GitLab Duo Chat? Perform a `major` version increment.
   - Do any commit messages contain `feat:`? Perform a `minor` version increment.
   - If none of the above, perform a `patch` version increment.

## Create the branch and merge request

Create a branch with a name like `yourname/prepare-0.5.5-release`. It should contain these changes:

1. [ ] In `gradle.properties`, update the `plugin.version` number above the last stable version according to the [Semantic Version 2.0](https://semver.org/) guidelines.
1. [ ] Sync the project version update to `package.json` using this command:

   ```shell
   npm run version:sync
   ```

   > [!note]
   > This command requires `jq` to be installed. If that is not installed on your machine,
   > refer to the [installation instructions](https://jqlang.github.io/jq/download/).

1. [ ] Move the changes in `CHANGELOG.md` from the `[Unreleased]` section to
   the section for the new version with this command:

   ```shell
   ./gradlew patchChangelog
   ```

1. [ ] Review the changes to `CHANGELOG.md`.

   - Do the changes look sensible?
   - Are words spelled correctly?
   - Do the links work?
   - Is the versioning correct for the intended release. See
     [Keep a Changelog](https://keepachangelog.com/en/1.1.0/) for format examples.

1. [ ] Ensure the Version Compatibility table in `README.md` is up-to-date.
1. [ ] Review the plugin's marketplace description in `src/main/resources/META-INF/plugin.xml`.
1. [ ] Were any major features released that you should feature in the release?
1. [ ] Create a merge request containing these changes, get it reviewed, and merge it.

Optional:

1. [ ] Ask the Editor Extensions PM if you should include anything else in the changelog.
1. [ ] Request a documentation review from the Editor Extensions Tech Writing
   [stable counterpart](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/#stable-counterparts). If they're unavailable, request a review in the `#docs` Slack channel.

### Release a hotfix

If `main` contains changes that cannot be released but a `patch` release is pending you should use the hotfix process.

1. Ask a maintainer to create a hotfix branch for you.
   1. Checkout the [![Marketplace Version](https://img.shields.io/jetbrains/plugin/v/22325-gitlab-duo)](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/versions) branch.
   1. Create a branch using the `hotfix/` prefix followed by the `MAJOR.MINOR.PATCH` numbers of the current release, e.g. `hotfix/2.10.1`.
   1. Push up the branch.
1. Create a merge request against this hotfix branch.
   1. Cherry-pick or rebase the minimal required changes.
   1. Increment patch version of the plugin.
1. Optional. Request a reviewer review for this hotfix.
1. Request a maintainer review for this hotfix.
1. Once the changes are reviewed trigger the manual `plugin:publish` job for the changes you would like to release.

For security releases confirm with our Application Security stable counterpart if the fix is suitable to "fix in public" before pushing any changes to the public project.

## Release to alpha channel

The following checklist triggers the job that builds the plugin, then publishes it to the
[`Alpha` release channel](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions/alpha) in JetBrains Marketplace.

1. [ ] In GitLab, on the left sidebar, select **Search or go to** and find your project.
1. [ ] Select **Build > Pipelines**.
1. [ ] In **Filter pipelines**, filter by `Branch name`, and find the latest pipeline for the `main` branch.
1. [ ] On that pipeline, select **Run manual or delayed jobs**, then select the `plugin:publish-alpha` job.

## Release to stable channel

To complete these steps, you must be a code owner or maintainer for the
[GitLab Duo Plugin for JetBrains](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin) project. If you are not, ask in `#f_jetbrains_plugin` for help.

This process has two main steps:

1. [Tag the new version](#tag-the-new-version).
1. [Upload the plugin](#upload-the-plugin).

### Tag the new version

After the merge request merges:

1. [ ] Tag the merged commit with the new version:

   ```shell
   # Tag the release version with the same version as in the gradle.properties file
   git tag v0.5.5 <COMMIT-HASH>

   # Push tags:
   git push origin v0.5.5
   ```

These commands send a Slack message to the `#f_jetbrains_plugin` channel, saying that the release
is now in progress.

1. [ ] In the GitLab UI, confirm the tag was added correctly:

   1. On the left sidebar, select **Search or go to** and find this project.
   1. Select **Code > Tags**, and confirm your new tag exists.
   1. Confirm that your tag has a pipeline in progress, or completed.

1. [ ] After the tag pipeline finishes, select the pipeline status icon to view the jobs
   in this pipeline.
1. [ ] After the preliminary pipeline steps finish successfully, select **Play** on
   the `plugin:publish` job in the `release` stage to start it manually.
1. [ ] After the tag pipeline finishes, on the left sidebar, select **Deploy > Releases** and find your release.
1. [ ] From the release, download `gitlab-jetbrains-plugin-X.X.X.zip` under `Assets > Packages`. Where `X.X.X` is the
   version associated with the merge request.
1. [ ] Install the plugin locally and test to ensure it works properly.

### Upload the plugin

To complete these steps, you must have permission to upload the new version of the plugin to the
JetBrains marketplace. If you do not, ask in `#f_jetbrains_plugin` for help.

If the plugin works as expected:

1. [ ] Go to the [JetBrains marketplace](https://plugins.jetbrains.com/) and search for `GitLab Duo`.
1. [ ] In the upper right corner of the page, select **Upload Update** and upload the `.zip` file from the GitLab release.
1. [ ] In Slack, go to the `#f_jetbrains_plugin` channel, write a release message using this template:

``` plaintext
# Update the URL below with this release's URL
[GitLab Duo Plugin](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions/stable/612226) `<version number>` for :jetbrains: has been released :rocket:

# Copy the preview version of the latest CHANGELOG.md release entry.
```

**💡Tip:** For less link-formatting pain, switch to using markup to write messages in Slack.
Do this by clicking your profile picture in the sidebar, navigating to `Preferences > Advanced`,
and checking `Format messages with markup`.

1. [ ] Remove all preview links from the Slack message before posting.
1. [ ] Update the release schedule with the results ([example](https://gitlab.com/gitlab-org/editor-extensions/meta/-/issues/146#jetbrains)).

These steps submit the new version for moderation, which takes up to two business days. During this
time, the version is not available for users to download. The newly uploaded version is shown in the
[versions tab of the JetBrains Marketplace listing](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions).
The compatibility verification results populate after 5-10 minutes.

### Smoke test strategy for stable releases

1. Check Duo Chat interactions:
   1. Ask Duo Chat a question.
   1. Test the `/clear` slash command.
   1. Test the `/explain` slash command.
1. Check Duo Code suggestions: comment with a simple request
1. Access Duo Settings and make a change in the Authentication methods (example: revoke OAuth and login again)
1. Access Duo Settings and try to visually inspect some features (example: check/uncheck additional languages)
1. Open the Quick Chat and try out at least one prompt
1. After playing around with a couple of changes, go to your idea.log logs and check for exceptions
