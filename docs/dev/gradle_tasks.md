# Gradle tasks used in this project

| Name                    | Description                                                                                                                                                                              |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `test`                  | Run tests. Set project property `-Pe2eTests` to run end-to-end tests. For information on running end-to-end tests, see [end-to-end testing docs](end_to_end_testing.md).                 |
| `detektMain detektTest` | Run the detekt linter for the `main` and `test` source sets using the `detekt.yml` configuration. Use the `--auto-correct` option to allow rules to autocorrect code if they support it. |
| `assemble`              | Build the plugin into a `zip` file that can be installed into the IDE. The file is written to `build/distributons/`.                                                                     |
| `runIde`                | Build the plugin and open a separate IDE to run it. Useful for debugging the plugin with breakpoints.                                                                                    |
| `runIdeForUiTests`      | Runs plugin in separate IDE with robot-server installed and specific configuration. Used for automated end-to-end testing.                                                               |
| `patchChangelog`        | Update the `Unreleased` section of `Changelog.md` to the version (`plugin.version`) specified in `gradle.properties`.                                                                    |
| `npmRunBuild`           | Depends on `npmInstall`. Run `npm run build` in the `webview` directory.                                                                                                                 |
| `copyWebviewAssets`     | Depends on `npmRunBuild`. Copy the contents of `webview/dist` to `src/main/resources/webview`.                                                                                           |
