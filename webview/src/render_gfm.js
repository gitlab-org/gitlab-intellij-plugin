import hljs from 'highlight.js';
import 'highlight.js/styles/atom-one-dark.css';
import {
  InsertCodeSnippetElement
} from "@gitlab/duo-ui/dist/components/chat/components/duo_chat_message/insert_code_snippet_element";

hljs.configure({
  // we highlight only elements with `js-syntax-highlight` class and
  // this class marks code blocks that are already highlighted by the GitLab instance
  // If we ever remove the GitLab post-processing of the LLM response, we can remove this ignore
  ignoreUnescapedHTML: true,
});

function syntaxHighlight(els) {
  if (!els || els.length === 0) return;

  els.forEach(el => {
    if (el.classList && el.classList.contains('js-syntax-highlight') && !el.dataset.highlighted) {
      hljs.highlightElement(el);

      // This is how the dom elements are designed to be manipulated
      // eslint-disable-next-line no-param-reassign
      el.dataset.highlighted = 'true';
    }
  });
}

// this is a partial implementation of `renderGFM` concerned only with syntax highlighting.
// for all possible renderers, check
// https://gitlab.com/gitlab-org/gitlab/-/blob/774ecc1f2b15a581e8eab6441de33585c9691c82/app/assets/javascripts/behaviors/markdown/render_gfm.js#L18-40

function renderGFM(element) {
  if (!element) {
    return;
  }

  const highlightEls = Array.from(element.querySelectorAll('.js-syntax-highlight'));
  syntaxHighlight(highlightEls);

  /*
   * This is a workaround to ensure that users with a GitLab version lower than 17.4.0 can use the feature.
   */
  const codeBlocks = Array.from(element.querySelectorAll('.markdown-code-block'))
  codeBlocks.forEach((block) => {
    // Ensure that we do not render the button twice once the changes are deployed.
    if(block?.querySelector('insert-code-snippet') == null) {
      block?.appendChild(new InsertCodeSnippetElement(block));
    }
  });
}

export default renderGFM;
