# What does this MR do and why?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{first_multiline_commit}

# MR acceptance checklist

- [ ] The changes have been tested manually and are working as specified.
- [ ] The correct [type labels](https://handbook.gitlab.com/handbook/engineering/metrics/#work-type-classification) have been applied to this MR.
- [ ] This MR has been made [as small as possible](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#keep-it-simple), to improve review efficiency and code quality.
- [ ] If this is a user-facing change, an [entry](https://keepachangelog.com/en/) has been added to the `Unreleased` section of [`CHANGELOG.md`](../../CHANGELOG.md).
- [ ] If this is a user-facing change, update the [smoke test strategy](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/docs/dev/releases/release_process.md?ref_type=heads#smoke-test-strategy-for-stable-releases) if applicable.

/label ~"Editor Extensions::JetBrains" ~"Category:Editor Extensions" ~"devops::create" ~"group::editor extensions" ~"section::dev"

/assign me
