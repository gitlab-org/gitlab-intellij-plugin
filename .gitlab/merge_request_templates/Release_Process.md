<!--
Please fill out the version changes below.
Then, check off the list as items are completed to ensure all steps are completed correctly.
-->

This merge request updates the plugin version from `vX.X.X` to `vX.X.X`.

It should contain these changes:

1. [ ] In `gradle.properties`, update the `plugin.version` number above the last stable version
   according to the [Semantic Version 2.0](https://semver.org/) guidelines.
1. [ ] Sync the project version update to `package.json` using this command:

   ```shell
   npm run version:sync
   ```

   **Note:** This command requires `jq` to be installed. If it isn't, see its
   [installation instructions](https://jqlang.github.io/jq/download/).

1. [ ] Move the changes in `CHANGELOG.md` from the `[Unreleased]` section, to
   the section for the new version, with this command:

   ```shell
   ./gradlew patchChangelog
   ```

1. [ ] Review the changes to `CHANGELOG.md`.

   - Do the changes look sensible?
   - Are words spelled correctly?
   - Do the links work?
   - Is the versioning correct for the intended release. See
     [Keep a Changelog](https://keepachangelog.com/en/1.1.0/) for format examples.

1. [ ] Ensure the Version Compatibility table in `README.md` is up-to-date.
1. [ ] Review the plugin's marketplace description in `src/main/resources/META-INF/plugin.xml`.
1. [ ] Were any major features released that you should feature in the release?
1. [ ] Create a merge request containing these changes, get it reviewed, and merge it.

Optional:

1. [ ] Ask the Editor Extensions PM if you should include anything else in the changelog.
1. [ ] Request a documentation review from the Editor Extensions Tech Writing
   [stable counterpart](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/#stable-counterparts). If they're unavailable, request a review in the `#docs` Slack channel.

## Release to alpha channel

This checklist triggers the job that builds the plugin, then publishes the plugin to the
[`Alpha` release channel](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions/alpha) in JetBrains Marketplace.

1. [ ] In GitLab, on the left sidebar, select **Search or go to** and find your project.
1. [ ] Select **Build > Pipelines**.
1. [ ] In **Filter pipelines**, filter by `Branch name`, and find the latest pipeline for the `main` branch.
1. [ ] On that pipeline, select **Run manual or delayed jobs**, then select the `plugin:publish-alpha` job.

## Release to stable channel

To complete these steps, you must be a code owner or maintainer for the
[GitLab Duo Plugin for JetBrains](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin)
project. If you are not, ask in `#f_jetbrains_plugin` for help.

This process has two main steps:

1. [Tag the new version](#tag-the-new-version).
1. [Upload the plugin](#upload-the-plugin).

### Tag the new version

After the merge request merges:

1. [ ] Tag the merged commit with the new version:

   ```shell
   # Tag the release version with the same version as in the gradle.properties file
   git tag v0.5.5 <COMMIT-HASH>

   # Push tags:
   git push origin v0.5.5
   ```

These commands send a Slack message to the `#f_jetbrains_plugin` channel, saying that the release
is now in progress.

1. [ ] In the GitLab UI, confirm the tag was added correctly:

   1. On the left sidebar, select **Search or go to** and find this project.
   1. Select **Code > Tags**, and confirm your new tag exists.
   1. Confirm that your tag has a pipeline in progress, or completed.

1. [ ] After the tag pipeline finishes, select the pipeline status icon to view the jobs
   in this pipeline.
1. [ ] After the preliminary pipeline steps finish successfully, select **Play** on
   the `plugin:publish` job in the `release` stage to start it manually.
1. [ ] After the tag pipeline finishes, on the left sidebar, select **Deploy > Releases** and find your release.
1. [ ] From the release, download `gitlab-jetbrains-plugin-X.X.X.zip` under `Assets > Packages`. Where `X.X.X` is the
   version associated with the merge request.
1. [ ] Install the plugin locally and test to ensure it works properly.

### Upload the plugin

To complete these steps, you must have permission to upload the new version of the plugin to the
JetBrains marketplace. If you do not, ask in `#f_jetbrains_plugin` for help.

If the plugin works as expected:

1. [ ] Go to the [JetBrains marketplace](https://plugins.jetbrains.com/) and search for `GitLab Duo`.
1. [ ] In the upper right corner of the page, select **Upload Update** and upload the `.zip`
   file from the GitLab release.
1. [ ] In the JetBrains marketplace, take a screenshot of the release information.
1. [ ] In Slack, go to the `#f_jetbrains_plugin` channel, and find the Slack thread
   containing the auto-generated release message.
1. [ ] In the thread, include:
   - A link to the GitLab release.
   - The release notes.
   - The screenshot of the release information from the JetBrains marketplace.

These steps submit the new version for moderation, which takes up to two business days. During this
time, the version is not available for users to download. The newly uploaded version is shown in the
[versions tab of the JetBrains Marketplace listing](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions).
The compatibility verification results populate after 5-10 minutes.

<!-- Do not edit below this line -->

/label ~"Editor Extensions::JetBrains" ~"Category:Editor Extensions" ~"devops::create" ~"group::editor extensions" ~"section::dev"

/label ~"type::maintenance" ~"maintenance::release"

/assign me
