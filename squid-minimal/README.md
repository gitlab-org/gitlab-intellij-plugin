# Minimal Squid

## Available proxies

### Authenticated squid proxy server

1. Set `http_proxy=127.0.0.1:3128`
1. Set `HTTPS_PROXY=127.0.0.1:3128`
1. Review the default credentials in [entrypoint.sh](./entrypoint.sh).
1. Test credentials against `https://gitlab.com` or another host in test:

   ```shell
   curl -v --proxy-basic --proxy-user "$PROXY_USERNAME:$PROXY_PASSWORD" --proxy http://127.0.0.1:3128 https://gitlab.com
   ```

### Unauthenticated squid proxy server

1. Set `http_proxy=127.0.0.1:3129`
1. Set `HTTPS_PROXY=127.0.0.1:3129`
1. Test the proxy connect to `https://gitlab.com` or another host in test:

   ```shell
   curl -v --proxy http://127.0.0.1:3128 https://gitlab.com
   ```

## Usage

The project is designed to run locally or as a GitLab CI/CD service container for integration testing.

### Build container image

```shell
docker build -t squid-minimal:latest .
```

### Run minimal squid container

Run the image in the foreground using:

```shell
docker run -it --name squid-minimal -p 3128:3128 -p 3129:3129 squid-minimal:latest
```

Run the container in the background using:

```shell
docker run -d -it --name squid-minimal -p 3128:3128 -p 3129:3129 squid-minimal:latest
```

To overwrite the default credentials set `PROXY_USERNAME` and `PROXY_PASSWORD`:

```shell
docker run -it --name squid-minimal -p 3128:3128 -p 3129:3129 -e PROXY_USERNAME=your_user -e PROXY_PASSWORD=your_password squid-minimal:latest
```

### Stop the container

Use `docker rm -f squid-minimal` to stop the image.
