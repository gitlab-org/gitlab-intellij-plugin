package com.gitlab.plugin.codesuggestions

enum class SwitchInlineCompletionSuggestionMode {
  NEXT,
  PREVIOUS
}
