package com.gitlab.plugin.codesuggestions.actions

import com.intellij.codeInsight.inline.completion.session.InlineCompletionContext
import com.intellij.openapi.actionSystem.ActionPromoter
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.DataContext

internal class SwitchInlineCompletionSuggestionActionsPromoter : ActionPromoter {
  override fun promote(actions: List<AnAction>, context: DataContext): List<AnAction> {
    val editor = CommonDataKeys.EDITOR.getData(context) ?: return emptyList()

    if (InlineCompletionContext.getOrNull(editor) == null) {
      return emptyList()
    }

    return actions.filterIsInstance<SwitchInlineCompletionSuggestionAction>()
  }
}
