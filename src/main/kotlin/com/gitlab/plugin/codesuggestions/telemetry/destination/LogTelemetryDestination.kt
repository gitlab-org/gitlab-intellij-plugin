package com.gitlab.plugin.codesuggestions.telemetry.destination

import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.intellij.openapi.diagnostic.Logger

class LogTelemetryDestination(private val logger: Logger = Logger.getInstance(LogTelemetryDestination::class.java)) :
  TelemetryDestination {
  override fun receive(event: Event) {
    logger.info("Action: ${event.type.action}, Context: ${event.context}")
  }
}
