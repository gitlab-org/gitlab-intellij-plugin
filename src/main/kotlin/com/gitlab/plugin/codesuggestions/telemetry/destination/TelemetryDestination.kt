package com.gitlab.plugin.codesuggestions.telemetry.destination

import com.gitlab.plugin.codesuggestions.telemetry.Event

interface TelemetryDestination {
  fun receive(event: Event)
}
