package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.codesuggestions.telemetry.destination.LanguageServerTelemetryDestination
import com.gitlab.plugin.codesuggestions.telemetry.destination.LogTelemetryDestination
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import java.util.*

@Suppress("TooManyFunctions")
@Service(Service.Level.PROJECT)
class TelemetryService(private val project: Project) : SwitchInlineCompletionEventListener {
  init {
    project
      .messageBus
      .connect()
      .subscribe(SwitchInlineCompletionEventListener.SWITCH_INLINE_COMPLETION_TOPIC, this)
  }

  private val destinations by lazy {
    listOf(
      LogTelemetryDestination(),
      LanguageServerTelemetryDestination(project)
    )
  }

  override fun onSwitchInlineCompletion(suggestionsTracker: SuggestionsTracker) {
    val currentChoice = suggestionsTracker.getCurrentChoice() ?: return
    update(
      currentContext.copy(
        trackingId = currentChoice.uniqueTrackingId ?: currentContext.trackingId,
        optionId = currentChoice.optionId
      )
    )
    shown()
  }

  var currentContext: Event.Context = newContext()
    private set

  fun update(context: Event.Context) {
    currentContext = context
  }

  fun accepted() = broadcast(Event.Type.ACCEPTED, currentContext)
  fun shown() = broadcast(Event.Type.SHOWN, currentContext)
  fun rejected() = broadcast(Event.Type.REJECTED, currentContext)

  fun newContext() = Event.Context(trackingId = UUID.randomUUID().toString())

  private fun broadcast(type: Event.Type, context: Event.Context) {
    if (!service<DuoPersistentSettings>().telemetryEnabled) return
    destinations.forEach {
      it.receive(Event(type, context))
    }
  }
}
