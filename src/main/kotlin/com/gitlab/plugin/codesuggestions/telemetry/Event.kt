package com.gitlab.plugin.codesuggestions.telemetry

data class Event(val type: Type, val context: Context) {
  val action by type::action

  enum class Type(val action: String) {
    ACCEPTED("suggestion_accepted"),
    SHOWN("suggestion_shown"),
    REJECTED("suggestion_rejected")
  }

  data class Context(
    val trackingId: String,
    val optionId: Int? = null,
    val requestId: String? = null,
    val modelEngine: String? = null,
    val modelName: String? = null,
    val language: String? = null,
    val prefixLength: Int? = null,
    val suffixLength: Int? = null,
    val apiStatusCode: Int? = null
  )
}
