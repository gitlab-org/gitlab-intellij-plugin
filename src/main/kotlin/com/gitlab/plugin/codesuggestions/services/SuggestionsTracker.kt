package com.gitlab.plugin.codesuggestions.services

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.SwitchInlineCompletionSuggestionMode
import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.intellij.openapi.application.runInEdt

class SuggestionsTracker(
  val context: SuggestionContext,
  val element: UpdatableInlineCompletionGrayElement,
  val streamId: String? = null,
) {
  val isStreaming = streamId != null
  val editor = element.editor
  var response: Completion.Response? = null
  var isCancelled = false

  private var currentIndex = 0
    set(value) {
      val response = response ?: return

      field = when {
        value > response.choices.size - 1 -> 0
        value < 0 -> response.choices.size - 1
        else -> value
      }
    }

  fun switch(mode: SwitchInlineCompletionSuggestionMode) {
    if (element.isPartiallyAccepted || response?.choices?.size == 1) {
      return
    }

    when (mode) {
      SwitchInlineCompletionSuggestionMode.NEXT -> currentIndex++
      SwitchInlineCompletionSuggestionMode.PREVIOUS -> currentIndex--
    }

    val newChoice = response?.choices?.getOrNull(currentIndex)
      ?: return

    runInEdt { element.update(newChoice.text) }
  }

  fun getIndexInformation(): String {
    return "${currentIndex + 1}/${response?.choices?.size ?: 1}"
  }

  fun getCurrentChoice(): Completion.Response.Choice? {
    return response?.choices?.getOrNull(currentIndex)
  }

  fun cancel() {
    isCancelled = true
    element.cancel()
  }
}
