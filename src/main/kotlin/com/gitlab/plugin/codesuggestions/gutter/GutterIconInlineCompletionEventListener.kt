package com.gitlab.plugin.codesuggestions.gutter

import com.gitlab.plugin.codesuggestions.listeners.StreamingInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.gitlab.plugin.codesuggestions.services.SUGGESTION_TRACKER_KEY
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.intellij.codeInsight.inline.completion.InlineCompletionEventAdapter
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.markup.HighlighterLayer
import com.intellij.openapi.editor.markup.RangeHighlighter
import com.intellij.openapi.util.Key
import com.intellij.openapi.util.removeUserData
import com.intellij.util.application

val CODE_SUGGESTIONS_GUTTER_ICON_KEY = Key<RangeHighlighter>("CODE_SUGGESTIONS_GUTTER_ICON_KEY")

@Suppress("UnstableApiUsage")
class GutterIconInlineCompletionEventListener(
  private val editor: Editor
) : InlineCompletionEventAdapter, SwitchInlineCompletionEventListener, StreamingInlineCompletionEventListener {
  private val exceptionTracker by lazy { application.service<ExceptionTracker>() }

  private var highlighter
    get() = editor.getUserData(CODE_SUGGESTIONS_GUTTER_ICON_KEY)
    set(value) = editor.putUserData(CODE_SUGGESTIONS_GUTTER_ICON_KEY, value)

  @Suppress("ReturnCount")
  override fun onShow(event: InlineCompletionEventType.Show) = handleExceptions {
    val element = event.element

    if (element is UpdatableInlineCompletionGrayElement) {
      val suggestion = editor.getUserData(SUGGESTION_TRACKER_KEY)
        ?: return@handleExceptions

      highlighter?.gutterIconRenderer = when {
        element.isPartiallyAccepted -> GutterIcons.Ready
        suggestion.isStreaming -> GutterIcons.Loading
        else -> GutterIcons.Ready
      }

      return@handleExceptions
    }

    highlighter?.gutterIconRenderer = GutterIcons.Ready
  }

  override fun onHide(event: InlineCompletionEventType.Hide) = handleExceptions {
    cleanup()
  }

  override fun onRequest(event: InlineCompletionEventType.Request) = handleExceptions {
    cleanup()

    highlighter = event
      .request
      .editor
      .markupModel
      .addLineHighlighter(event.request.currentLine, HighlighterLayer.CARET_ROW, null)
      .apply { gutterIconRenderer = GutterIcons.Loading }
  }

  @Suppress("TooGenericExceptionCaught")
  private fun handleExceptions(block: () -> Unit) = try {
    block()
  } catch (e: Exception) {
    exceptionTracker.handle(e)
  }

  override fun onRequestMultipleSuggestions() = runInEdt {
    highlighter?.gutterIconRenderer = GutterIcons.Loading
  }

  override fun onReceivedMultipleSuggestions(suggestionsTracker: SuggestionsTracker) = runInEdt {
    if (!suggestionsTracker.isCancelled) {
      highlighter?.gutterIconRenderer = GutterIcons.Ready
    }
  }

  override fun onStreamingInlineCompletionCompletion() {
    highlighter?.gutterIconRenderer = GutterIcons.Ready
  }

  fun cleanup() {
    editor.removeUserData(CODE_SUGGESTIONS_GUTTER_ICON_KEY)?.dispose()

    editor.markupModel.allHighlighters.forEach { highlighter ->
      val renderer = highlighter.gutterIconRenderer ?: return@forEach

      if (renderer == GutterIcons.Loading || renderer == GutterIcons.Ready) {
        highlighter.dispose()
      }
    }
  }

  private val InlineCompletionRequest.currentLine: Int
    get() = document.getLineNumber(endOffset)
}
