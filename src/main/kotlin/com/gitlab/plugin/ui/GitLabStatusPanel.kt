package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.actions.status.StatusActions.Companion.STATUS_ACTIONS_GROUP_ID
import com.gitlab.plugin.services.Status
import com.intellij.openapi.actionSystem.ActionGroup
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.DataContext
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.openapi.ui.popup.ListPopup
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.wm.impl.status.EditorBasedStatusBarPopup
import com.intellij.ui.AnimatedIcon
import com.intellij.util.messages.MessageBusConnection
import kotlinx.coroutines.CoroutineScope
import javax.swing.Icon

class GitLabStatusPanel(project: Project, scope: CoroutineScope) : EditorBasedStatusBarPopup(
  isWriteableFileRequired = true,
  project = project,
  scope = scope,
) {
  private var state: WidgetState = LOADING

  init {
    registerCustomListeners(myConnection)
  }

  override fun ID(): String = WIDGET_ID

  override fun createInstance(project: Project): GitLabStatusPanel {
    return GitLabStatusPanel(project, scope)
  }

  override fun createPopup(context: DataContext): ListPopup? {
    return ActionManager.getInstance().getAction(STATUS_ACTIONS_GROUP_ID).let {
      val group = it as? ActionGroup ?: return@let null
      val showDisabledActions = true

      JBPopupFactory.getInstance().createActionGroupPopup(
        group.templateText,
        group,
        context,
        JBPopupFactory.ActionSelectionAid.SPEEDSEARCH,
        showDisabledActions,
      )
    }
  }

  override fun getWidgetState(file: VirtualFile?): WidgetState = state

  override fun registerCustomListeners(connection: MessageBusConnection) {
    connection.subscribe(
      GitLabSettingsListener.SETTINGS_CHANGED,
      object : GitLabSettingsListener {
        override fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent) {
          val nextState = when (event.status) {
            Status.ENABLED -> ENABLED
            Status.DISABLED -> DISABLED
            Status.ERROR -> ERROR
            Status.LOADING -> LOADING
            Status.NOT_LICENSED -> NOT_LICENSED
            Status.GITLAB_VERSION_UNSUPPORTED -> GITLAB_VERSION_UNSUPPORTED
          }

          if (nextState != state) {
            state = nextState
          }

          runInEdt {
            updateComponent(state)
          }
        }
      }
    )
  }

  companion object {
    @JvmField
    val WIDGET_ID: String = GitLabStatusPanel::class.java.name

    private class GitLabPluginWidgetState(toolTip: String?, val widgetIcon: Icon) :
      WidgetState(toolTip, "Duo", true) { init {
      icon = widgetIcon
    }
    }

    private val DISABLED = GitLabPluginWidgetState(
      toolTip = GitLabBundle.message("status-icon.tooltip.disabled"),
      widgetIcon = GitLabIcons.StatusWidget.CodeSuggestionsDisabled,
    )

    private val ENABLED = GitLabPluginWidgetState(
      toolTip = GitLabBundle.message("status-icon.tooltip.enabled"),
      widgetIcon = GitLabIcons.StatusWidget.CodeSuggestionsEnabled,
    )

    private val ERROR = GitLabPluginWidgetState(
      toolTip = GitLabBundle.message("status-icon.tooltip.error"),
      widgetIcon = GitLabIcons.StatusWidget.CodeSuggestionsError,
    )

    private val GITLAB_VERSION_UNSUPPORTED = GitLabPluginWidgetState(
      toolTip = GitLabBundle.message("code-suggestions.gitlab-version-unsupported"),
      widgetIcon = GitLabIcons.StatusWidget.CodeSuggestionsDisabled,
    )

    private val LOADING = GitLabPluginWidgetState(
      toolTip = GitLabBundle.message("status-icon.tooltip.loading"),
      widgetIcon = AnimatedIcon.Default.INSTANCE,
    )

    private val NOT_LICENSED = GitLabPluginWidgetState(
      toolTip = GitLabBundle.message("code-suggestions.not-licensed"),
      widgetIcon = GitLabIcons.StatusWidget.CodeSuggestionsDisabled,
    )
  }
}
