package com.gitlab.plugin.ui

import com.intellij.openapi.util.IconLoader

object GitLabIcons {
  /**
   * Icons assigned to different actions.
   */
  object Actions {
    @JvmField
    val CodeSuggestionsDisabled = IconLoader.getIcon("/icons/actions/code-suggestions-disabled.svg", javaClass)

    @JvmField
    val CodeSuggestionsEnabled = IconLoader.getIcon("/icons/actions/code-suggestions-enabled.svg", javaClass)

    @JvmField
    val DuoChatDisabled = IconLoader.getIcon("/icons/actions/duo-chat-off.svg", javaClass)

    @JvmField
    val DuoChatEnabled = IconLoader.getIcon("/icons/actions/duo-chat.svg", javaClass)
  }

  /**
   * Icons used by the status bar widget.
   */
  object StatusWidget {
    @JvmField
    val CodeSuggestionsDisabled = IconLoader.getIcon("/icons/gitlab-code-suggestions-disabled.svg", javaClass)

    @JvmField
    val CodeSuggestionsEnabled = IconLoader.getIcon("/icons/gitlab-code-suggestions-enabled.svg", javaClass)

    @JvmField
    val CodeSuggestionsError = IconLoader.getIcon("/icons/gitlab-code-suggestions-error.svg", javaClass)
  }

  object Status {
    @JvmField
    val Success = IconLoader.getIcon("/icons/status/success.svg", javaClass)
  }

  @JvmField
  val NotificationIcon = IconLoader.getIcon("/icons/notificationIcon.svg", javaClass)
}
