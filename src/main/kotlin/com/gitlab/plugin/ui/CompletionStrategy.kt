package com.gitlab.plugin.ui

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.SuggestionContext

interface CompletionStrategy {
  suspend fun generateCompletions(context: SuggestionContext): Completion.Response?
}
