package com.gitlab.plugin.ui.designsystem.components

import com.intellij.collaboration.ui.CollaborationToolsUIUtil.defaultButton
import javax.swing.JButton

class PrimaryButton(text: String) : JButton(text) {

  init {
    @Suppress("UnstableApiUsage")
    defaultButton()
  }
}
