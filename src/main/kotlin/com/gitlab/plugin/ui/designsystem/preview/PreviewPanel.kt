package com.gitlab.plugin.ui.designsystem.preview

import com.gitlab.plugin.ui.designsystem.components.InlineBanner
import com.gitlab.plugin.ui.designsystem.components.PrimaryButton
import com.gitlab.plugin.ui.designsystem.components.SecondaryButton
import com.intellij.ui.EditorNotificationPanel
import com.intellij.util.ui.JBFont
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*

@Suppress("MagicNumber")
class PreviewPanel : JPanel() {
  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)
    border = BorderFactory.createEmptyBorder(16, 16, 16, 16)

    // Banners
    addLeftAlignedComponent(JLabel("Inline Banners").apply { font = JBFont.h1().asBold() })
    addWithWrappingPanel(InlineBanner("This is an info inline banner", EditorNotificationPanel.Status.Info))
    addWithWrappingPanel(InlineBanner("This is an error inline banner", EditorNotificationPanel.Status.Error))
    addWithWrappingPanel(InlineBanner("This is a success inline banner", EditorNotificationPanel.Status.Success))
    addWithWrappingPanel(InlineBanner("This is a warning inline banner", EditorNotificationPanel.Status.Warning))

    add(Box.createRigidArea(Dimension(0, 16)))

    // Buttons
    addLeftAlignedComponent(JLabel("Buttons").apply { font = JBFont.h1().asBold() })
    addLeftAlignedComponent(PrimaryButton("Primary Button"))
    addLeftAlignedComponent(PrimaryButton("Primary Button (disabled)").apply { isEnabled = false })
    addLeftAlignedComponent(SecondaryButton("Secondary Button"))
    addLeftAlignedComponent(SecondaryButton("Secondary Button (disabled)").apply { isEnabled = false })
  }

  private fun addLeftAlignedComponent(component: JComponent) {
    add(component.apply { alignmentX = LEFT_ALIGNMENT })
  }

  private fun addWithWrappingPanel(component: JComponent) {
    val panel = object : JPanel(FlowLayout(FlowLayout.LEFT)) {
      init {
        add(component)
        alignmentX = LEFT_ALIGNMENT
      }

      override fun getMaximumSize(): Dimension {
        return Dimension(this@PreviewPanel.width, preferredSize.height)
      }
    }

    add(panel)
  }
}
