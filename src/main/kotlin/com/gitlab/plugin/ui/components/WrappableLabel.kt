package com.gitlab.plugin.ui.components

import javax.swing.JLabel

class WrappableLabel(
  text: String,
  config: JLabel.() -> Unit
) :
  JLabel("<html><body style=\"text-justify: inter-word;\">$text</body></html>") {
  init {
    config()
  }
}
