package com.gitlab.plugin.ui.components

import com.intellij.ui.JBColor
import com.intellij.util.ui.JBUI
import java.awt.*
import java.awt.geom.RoundRectangle2D
import javax.swing.JComponent

@Suppress("MagicNumber")
class Shortcut(private val text: String) : JComponent() {
  private val padding = JBUI.insets(2, 4)
  private val backgroundColor = JBColor(Color(0xDFE1E5), Color(0x4E5157))
  private val textColor = JBColor.BLACK

  init {
    isOpaque = false
  }

  override fun paintComponent(g: Graphics) {
    val g2 = g.create() as Graphics2D
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

    // Draw background
    g2.color = backgroundColor
    g2.fill(RoundRectangle2D.Float(0f, 0f, width.toFloat(), height.toFloat(), 8f, 8f))

    // Draw text
    g2.color = textColor
    g2.font = font
    val fm = g2.fontMetrics
    val x = padding.left
    val y = ((height - fm.height) / 2) + fm.ascent
    g2.drawString(text, x, y)

    g2.dispose()
  }

  override fun getPreferredSize(): Dimension {
    val fm = getFontMetrics(font)
    return Dimension(
      fm.stringWidth(text) + padding.left + padding.right,
      fm.height + padding.top + padding.bottom
    )
  }

  override fun getMinimumSize(): Dimension = preferredSize
  override fun getMaximumSize(): Dimension = preferredSize
}
