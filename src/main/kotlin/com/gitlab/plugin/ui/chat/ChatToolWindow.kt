package com.gitlab.plugin.ui.chat

import com.gitlab.plugin.chat.ui.JCEFNotAvailablePanelFactory
import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory

internal class ChatToolWindow : ToolWindowFactory {
  override fun shouldBeAvailable(project: Project): Boolean {
    return project.service<DuoChatStateService>().duoChatEnabled
  }

  override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
    val factory = toolWindow.contentManager.factory

    val content = try {
      val chatService = project.service<ChatService>()
      val chatBrowserComponent = chatService.browserComponent

      factory
        .createContent(chatBrowserComponent, null, false)
        .apply { setDisposer(chatService) }
    } catch (e: ExceptionInInitializerError) {
      if (e.cause?.message?.contains("JCEF is not supported") == true) {
        factory.createContent(JCEFNotAvailablePanelFactory.create(), null, false)
      } else {
        throw e
      }
    }

    toolWindow.contentManager.addContent(content)
  }
}
