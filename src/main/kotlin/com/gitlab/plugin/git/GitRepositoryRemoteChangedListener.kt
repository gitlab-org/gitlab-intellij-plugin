package com.gitlab.plugin.git

import com.gitlab.plugin.services.DuoEnabledForGitLabProjectService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.repo.GitConfigListener
import git4idea.repo.GitRepository

class GitRepositoryRemoteChangedListener(private val project: Project) : GitConfigListener {
  private lateinit var lastRemotes: Array<String>

  override fun notifyConfigChanged(repository: GitRepository) {
    val currentRemotes = repository.remotes.mapNotNull { it.firstUrl }.toTypedArray()

    if (!::lastRemotes.isInitialized || !lastRemotes.contentEquals(currentRemotes)) {
      lastRemotes = currentRemotes

      project.service<DuoEnabledForGitLabProjectService>().isDuoEnabledAtGitLabProjectLevel(true)
    }
  }
}
