package com.gitlab.plugin.editor

import com.gitlab.plugin.chat.listeners.SelectionChangedDuoChatListener
import com.gitlab.plugin.codesuggestions.gutter.GutterIconInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.listeners.StreamingInlineCompletionEventListener.Companion.STREAMING_INLINE_COMPLETION_TOPIC
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener.Companion.SWITCH_INLINE_COMPLETION_TOPIC
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryService
import com.intellij.codeInsight.inline.completion.InlineCompletion
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Key
import com.intellij.openapi.util.removeUserData

val EDITOR_LISTENERS_KEY = Key<EditorListeners>("EDITOR_LISTENERS_KEY")

@Service(Service.Level.PROJECT)
class EditorRegistryService(private val project: Project) {
  fun registerAllEditors() {
    EditorFactory
      .getInstance()
      .allEditors
      .forEach { registerEditor(it) }
  }

  fun registerEditor(editor: Editor) {
    if (isRegistered(editor)) return

    val gutterIconInlineCompletionEventListener = GutterIconInlineCompletionEventListener(editor)
    val telemetryInlineCompletionEventListener =
      TelemetryInlineCompletionEventListener(project.service<TelemetryService>())
    val selectionChangedDuoChatListener = SelectionChangedDuoChatListener()

    editor
      .project
      ?.messageBus
      ?.connect()
      ?.subscribe(SWITCH_INLINE_COMPLETION_TOPIC, gutterIconInlineCompletionEventListener)

    editor
      .project
      ?.messageBus
      ?.connect()
      ?.subscribe(STREAMING_INLINE_COMPLETION_TOPIC, gutterIconInlineCompletionEventListener)

    InlineCompletion.getHandlerOrNull(editor)?.run {
      addEventListener(telemetryInlineCompletionEventListener)
      addEventListener(gutterIconInlineCompletionEventListener)
    }

    editor.selectionModel.addSelectionListener(selectionChangedDuoChatListener)

    editor.putUserData(
      EDITOR_LISTENERS_KEY,
      EditorListeners(
        gutterIconInlineCompletionEventListener,
        telemetryInlineCompletionEventListener,
        selectionChangedDuoChatListener
      )
    )
  }

  fun unregisterEditor(editor: Editor) {
    if (!isRegistered(editor)) return

    editor.removeUserData(EDITOR_LISTENERS_KEY)?.let { listeners ->
      InlineCompletion
        .getHandlerOrNull(editor)
        ?.run {
          removeEventListener(listeners.telemetryInlineCompletionEventListener)
          removeEventListener(listeners.gutterIconInlineCompletionEventListener)
        }

      editor.selectionModel.removeSelectionListener(listeners.selectionChangedDuoChatListener)

      listeners.gutterIconInlineCompletionEventListener.cleanup()
    }
  }

  private fun isRegistered(editor: Editor): Boolean {
    return editor.getUserData(EDITOR_LISTENERS_KEY) != null
  }
}
