package com.gitlab.plugin.editor

import com.gitlab.plugin.util.project.tryGetService
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.event.EditorFactoryEvent
import com.intellij.openapi.editor.event.EditorFactoryListener

internal class GitLabEditorFactoryListener : EditorFactoryListener {
  override fun editorCreated(event: EditorFactoryEvent) {
    event.editor.let {
      it.project?.service<EditorRegistryService>()?.registerEditor(it)
    }
  }

  override fun editorReleased(event: EditorFactoryEvent) {
    event.editor.let {
      it.project?.tryGetService<EditorRegistryService>()?.unregisterEditor(it)
    }
  }
}
