package com.gitlab.plugin.api.graphql.actioncable

import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import kotlinx.coroutines.delay
import kotlin.math.pow

class ActionCableWebSocketReopenStrategy(
  config: Config = Config()
) {
  companion object {
    private const val DEFAULT_INITIAL_WEB_SOCKET_REOPEN_DELAY_MILLISECONDS = 100L
    private const val DEFAULT_MAX_WEB_SOCKET_REOPEN_DELAY_MILLISECONDS = 15_000L

    internal val logger: Logger = logger<ActionCableWebSocketReopenStrategy>()
  }

  private val initialWebSocketReopenDelayMilliseconds = config.initialWebSocketReopenDelayMilliseconds
  private val maxWebSocketReopenDelayMilliseconds = config.maxWebSocketReopenDelayMilliseconds

  /**
   * Handles network errors for WebSocket connections.
   *
   * @param throwable The exception that caused the WebSocket to close.
   * @param attempt The number of times we have tried to reopen the WebSocket, 0-indexed.
   * @return Boolean indicating whether to attempt reopening the WebSocket.
   */
  suspend fun handleNetworkError(
    throwable: Throwable,
    attempt: Long
  ): Boolean {
    if (!shouldReopen(attempt)) return false

    val currentDelayMilliseconds = calculateCurrentDelayMilliseconds(attempt)
    logger.warn(
      """
        Error: ${throwable.message}. Cause: ${throwable.cause}. WebSocket reopen attempts: $attempt. Delay before next attempt: $currentDelayMilliseconds ms
      """.trimIndent()
    )

    delay(currentDelayMilliseconds)

    return true
  }

  private fun shouldReopen(attempt: Long): Boolean =
    calculateCurrentDelayMilliseconds(attempt) > 0

  private fun calculateCurrentDelayMilliseconds(
    attempt: Long
  ): Long {
    val previousTotal = calculateTotalDelayMilliseconds(attempt)
    return calculateNextBackoffDelayMilliseconds(attempt)
      .coerceAtMost(maxWebSocketReopenDelayMilliseconds - previousTotal)
  }

  private fun calculateNextBackoffDelayMilliseconds(attempt: Long): Long {
    return initialWebSocketReopenDelayMilliseconds * 2.0.pow(attempt.toInt()).toLong()
  }

  private fun calculateTotalDelayMilliseconds(attempt: Long): Long {
    return (0..(attempt - 1).toInt()).sumOf { calculateNextBackoffDelayMilliseconds(it.toLong()) }
  }

  data class Config(
    val initialWebSocketReopenDelayMilliseconds: Long = DEFAULT_INITIAL_WEB_SOCKET_REOPEN_DELAY_MILLISECONDS,
    val maxWebSocketReopenDelayMilliseconds: Long = DEFAULT_MAX_WEB_SOCKET_REOPEN_DELAY_MILLISECONDS
  )
}
