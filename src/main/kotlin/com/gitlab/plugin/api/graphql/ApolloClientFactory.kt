package com.gitlab.plugin.api.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.http.DefaultHttpEngine
import com.apollographql.apollo3.network.ws.WebSocketNetworkTransport
import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.api.duo.configureSslSocketFactory
import com.gitlab.plugin.api.graphql.actioncable.ActionCableWebSocketReopenStrategy
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWebSocketEngine
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWsProtocolFactory
import com.gitlab.plugin.api.okhttp.OkHttpAuthorizationInterceptor
import com.gitlab.plugin.api.utils.getOrigin
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.collaboration.util.resolveRelative
import com.intellij.openapi.components.Service
import com.intellij.util.net.HttpConfigurable
import io.ktor.http.HttpHeaders.Authorization
import io.ktor.http.HttpHeaders.Origin
import okhttp3.OkHttpClient
import org.jetbrains.annotations.VisibleForTesting
import java.net.URI

@Service(Service.Level.APP)
class ApolloClientFactory {
  private val okHttpClient: OkHttpClient by lazy {
    OkHttpClient.Builder().apply {
      configureSslSocketFactory()
      followRedirects(true)

      val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
      proxyInfo?.let {
        ProxyManager(it) { url -> HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(url) }.let { proxyManager ->
          proxySelector(proxyManager)
          proxyAuthenticator(proxyManager)
        }
      }

      addInterceptor(OkHttpAuthorizationInterceptor())
    }.build()
  }

  private lateinit var webSocketEngine: ActionCableWebSocketEngine

  fun create(): ApolloClient {
    val url = DuoPersistentSettings.getInstance().url

    return create(url)
  }

  fun create(url: String, token: String? = null): ApolloClient {
    val instanceUri = URI(url)
    val bearerToken = token?.let { "Bearer $it" }

    webSocketEngine = ActionCableWebSocketEngine(okHttpClient)

    val wsTransport = WebSocketNetworkTransport.Builder()
      .serverUrl(createSubscriptionUrl(instanceUri))
      .addHeader(Origin, instanceUri.getOrigin())
      .webSocketEngine(webSocketEngine)
      .protocol(ActionCableWsProtocolFactory())
      .apply { if (bearerToken != null) addHeader(Authorization, bearerToken) }
      .reopenWhen(ActionCableWebSocketReopenStrategy()::handleNetworkError)
      .build()

    return ApolloClient.Builder()
      .httpEngine(DefaultHttpEngine(okHttpClient))
      .serverUrl(createServerUrl(instanceUri))
      .subscriptionNetworkTransport(wsTransport)
      .apply { if (bearerToken != null) addHttpHeader(Authorization, bearerToken) }
      .build()
  }

  @VisibleForTesting
  internal fun createServerUrl(uri: URI) = uri
    .resolveRelative("api/graphql")
    .toString()

  @VisibleForTesting
  internal fun createSubscriptionUrl(uri: URI) = uri
    .resolveRelative("-/cable")
    .toString()
    .replace("https://", "wss://")
    .replace("http://", "ws://")

  fun getWebSocketEngine(): ActionCableWebSocketEngine? {
    if (!::webSocketEngine.isInitialized) {
      return null
    }

    return webSocketEngine
  }
}
