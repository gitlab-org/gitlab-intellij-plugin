package com.gitlab.plugin.api

import io.ktor.client.call.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.utils.io.errors.*
import java.nio.channels.UnresolvedAddressException

@Suppress("ThrowsCount")
object HttpExceptionHandler {
  suspend fun handle(cause: Throwable, request: HttpRequest) {
    when (cause) {
      is ClientRequestException -> {
        val exceptionResponse = cause.response
        val exceptionResponseText = exceptionResponse.body<String>()

        when (exceptionResponse.status) {
          HttpStatusCode.Unauthorized -> throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
          HttpStatusCode.Forbidden -> throw GitLabForbiddenException(exceptionResponse, exceptionResponseText)
          HttpStatusCode.ProxyAuthenticationRequired -> throw GitLabProxyException(cause)
          HttpStatusCode.PayloadTooLarge -> throw GitLabProxyException(cause)
          else -> throw GitLabResponseException(exceptionResponse, exceptionResponseText)
        }
      }
      is UnresolvedAddressException -> throw GitLabOfflineException(request, cause)
      is ConnectTimeoutException -> throw GitLabOfflineException(request, cause)
      is HttpRequestTimeoutException -> throw GitLabOfflineException(request, cause)
      is IOException -> if (cause.message?.contains("431") == true) {
        throw GitLabProxyException(cause)
      } else {
        throw cause
      }

      else -> throw cause
    }
  }
}
