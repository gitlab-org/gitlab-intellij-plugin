package com.gitlab.plugin.telemetry.tracked.actions

import com.gitlab.plugin.authentication.TokenProviderType
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.gitlab.plugin.telemetry.StandardContext
import com.intellij.openapi.components.service
import com.snowplowanalytics.snowplow.tracker.events.Structured

fun trackAuthenticationType(authenticationProviderType: TokenProviderType) {
  val tracker = service<GitLabApplicationService>().snowplowTracker

  val event = Structured.builder()
    .category(SnowplowTracker.Category.GITLAB_AUTHENTICATION)
    .action(authenticationProviderType.name)
    .label("selected_authentication_type")
    .customContext(
      listOf(
        StandardContext.build(),
        SnowplowTracker.IDE_EXTENSION_VERSION_CONTEXT
      )
    )
    .build()

  tracker.track(event)
}
