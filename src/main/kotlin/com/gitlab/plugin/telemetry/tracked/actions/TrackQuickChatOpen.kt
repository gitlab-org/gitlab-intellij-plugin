package com.gitlab.plugin.telemetry.tracked.actions

import com.gitlab.plugin.chat.quickchat.QuickChatOpenMethod
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.gitlab.plugin.telemetry.StandardContext
import com.snowplowanalytics.snowplow.tracker.events.Structured

fun trackQuickChatOpen(openMethod: QuickChatOpenMethod) {
  val tracker = GitLabApplicationService.getInstance().snowplowTracker

  val action = when (openMethod) {
    QuickChatOpenMethod.SHORTCUT -> "shortcut"
    QuickChatOpenMethod.CLICK_BUTTON -> "click_button"
  }

  val event = Structured.builder()
    .category(SnowplowTracker.Category.GITLAB_QUICK_CHAT)
    .action(action)
    .label("open_quick_chat")
    .customContext(
      listOf(
        StandardContext.build(),
        SnowplowTracker.IDE_EXTENSION_VERSION_CONTEXT
      )
    )
    .build()

  tracker.track(event)
}
