package com.gitlab.plugin.lsp

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.codesnippets.InsertCodeSnippetService
import com.gitlab.plugin.chat.view.model.InsertCodeSnippetMessage
import com.gitlab.plugin.git.GitDiffProvider
import com.gitlab.plugin.lsp.capabilities.LanguageServerCapabilityManager
import com.gitlab.plugin.lsp.listeners.CompletionListener.Companion.CODE_COMPLETION_TOPIC
import com.gitlab.plugin.lsp.params.*
import com.gitlab.plugin.lsp.webview.LanguageServerDuoChatWebViewService
import com.gitlab.plugin.services.AuthenticationStateService
import com.gitlab.plugin.services.CodeSuggestionsStateService
import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.ui.NotificationGroupType
import com.google.gson.JsonObject
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.future.asCompletableFuture
import kotlinx.coroutines.launch
import kotlinx.coroutines.time.withTimeout
import kotlinx.serialization.json.Json
import org.eclipse.lsp4j.*
import org.eclipse.lsp4j.jsonrpc.services.JsonNotification
import org.eclipse.lsp4j.jsonrpc.services.JsonRequest
import org.eclipse.lsp4j.services.LanguageClient
import java.time.Duration
import java.util.concurrent.CompletableFuture

@Suppress("MagicNumber")
private val TIMEOUT = Duration.ofSeconds(10L)

@Suppress("ForbiddenComment", "TooManyFunctions", "TooGenericExceptionCaught", "ForbiddenVoid")
class GitLabLanguageServerClient(
  private val project: Project,
  private val coroutineScope: CoroutineScope,
  private val notificationManager: GitLabNotificationManager = GitLabNotificationManager(),
  private val capabilityManager: LanguageServerCapabilityManager = LanguageServerCapabilityManager(project)
) : LanguageClient {
  private val logger: Logger = logger<GitLabLanguageServerClient>()
  private val jsonSerializer = Json { ignoreUnknownKeys = true }

  @JsonNotification("$/gitlab/plugin/notification")
  fun handleLSNotification(notification: LSNotificationParams) = coroutineScope.async {
    logger.debug("insertCodeSnippet: ${notification.pluginId}; ${notification.type}; ${notification.payload}.")

    if (notification.pluginId == LSRequestResponseParams.WEBVIEW_PLUGIN_ID) {
      when (notification.type) {
        LSRequestType.APP_READY.requestType -> project.service<LanguageServerDuoChatWebViewService>()
          .markLSCommunicationAsReady()

        LSRequestType.INSERT_CODE_SNIPPET.requestType -> {
          try {
            val deserializedParam =
              jsonSerializer.decodeFromString<InsertCodeSnippetMessage>(notification.payload.toString())

            project.service<InsertCodeSnippetService>().insertCodeSnippet(deserializedParam)
          } catch (e: Throwable) {
            logger.warn("Exception when calling insertCodeSnippet.", e)
          }
        }

        LSRequestType.SHOW_MESSAGE.requestType -> {
          try {
            val messageParams = jsonSerializer.decodeFromString<ViewMessagePayload>(notification.payload.toString())
            val (group, type) = extractGroupAndType(messageParams.type)

            if (group != null && type != null) {
              notificationManager.sendNotification(
                notificationData = Notification(
                  title = GitLabBundle.message("settings.ui.group.language-server.name"),
                  message = messageParams.message
                ),
                notificationGroupType = group,
                notificationType = type,
              )
            }
          } catch (e: Throwable) {
            logger.warn("Exception when handling showMessage.", e)
          }
        }
      }
    }
  }.asCompletableFuture()

  private fun extractGroupAndType(messageParamsType: String?) = when (messageParamsType) {
    ViewMessageType.ERROR.messageType -> Pair(NotificationGroupType.IMPORTANT, NotificationType.ERROR)
    ViewMessageType.WARNING.messageType -> Pair(NotificationGroupType.IMPORTANT, NotificationType.WARNING)
    ViewMessageType.INFO.messageType -> Pair(NotificationGroupType.GENERAL, NotificationType.INFORMATION)
    else -> Pair(null, null)
  }

  @JsonRequest("$/gitlab/plugin/request")
  fun getCurrentFileContext(request: CurrentFileContextRequestParams) = coroutineScope.async {
    logger.debug("getCurrentFileContext: ${request.pluginId}; ${request.type}; ${request.payload}.")

    if (request.pluginId == LSRequestResponseParams.WEBVIEW_PLUGIN_ID && request.type == LSRequestType.CURRENT_FILE_CONTEXT.requestType) {
      try {
        val chatRecordContext = project.service<ChatRecordContextService>().getChatRecordContextOrNull()
        return@async CurrentFileContextResponseParams(
          chatRecordContext?.currentFile?.selectedText.orEmpty(),
          chatRecordContext?.currentFile?.fileName.orEmpty(),
          chatRecordContext?.currentFile?.contentAboveCursor.orEmpty(),
          chatRecordContext?.currentFile?.contentBelowCursor.orEmpty()
        )
      } catch (e: Throwable) {
        logger.warn("Exception when calling getCurrentFileContext.", e)
      }
    }
    return@async null
  }.asCompletableFuture()

  @JsonRequest("$/gitlab/ai-context/git-diff")
  fun getGitDiff(request: GitDiffParams) = coroutineScope.async {
    logger.debug("Retrieving Git Diff for ${request.repositoryUri} and branch ${request.branch}.")

    try {
      val diffProvider = project.service<GitDiffProvider>()

      when {
        request.branch != null -> diffProvider.getDiffWithBranch(request.repositoryUri, request.branch)
        else -> diffProvider.getDiffWithHead(request.repositoryUri)
      }
    } catch (e: Throwable) {
      logger.warn("Exception when retrieving git diff.", e)
      null
    }
  }.asCompletableFuture()

  @JsonNotification("$/gitlab/featureStateChange")
  fun gitlabFeatureStateChange(params: Array<FeatureStateParams>) = coroutineScope.launchWithTimeout(TIMEOUT) {
    params.forEach { featureState ->
      logger.debug("Processing feature state change for: ${featureState.featureId}")

      when (featureState.featureId) {
        FeatureId.CODE_SUGGESTIONS -> project.service<CodeSuggestionsStateService>().update(featureState)
        FeatureId.CHAT -> project.service<DuoChatStateService>().update(featureState)
        FeatureId.AUTHENTICATION -> project.service<AuthenticationStateService>().update(featureState)
      }
    }
  }.asCompletableFuture()

  @JsonNotification("streamingCompletionResponse")
  fun streamingCompletionResponse(params: StreamingCompletionResponse) = coroutineScope.launchWithTimeout(TIMEOUT) {
    project.messageBus.syncPublisher(CODE_COMPLETION_TOPIC).update(params)
  }.asCompletableFuture()

  @JsonNotification("cancelStreaming")
  fun cancelStreaming(params: StreamWithId) = coroutineScope.launchWithTimeout(TIMEOUT) {
    project.messageBus.syncPublisher(CODE_COMPLETION_TOPIC).cancel(params.id)
  }.asCompletableFuture()

  override fun logMessage(message: MessageParams?) {
    coroutineScope.launchWithTimeout(TIMEOUT) {
      message?.also { logger.info(message.message) }
    }
  }

  // TODO: Implement LSP diagnostic handler
  override fun publishDiagnostics(diagnostics: PublishDiagnosticsParams) {
    coroutineScope.launchWithTimeout(TIMEOUT) {
      logger.info("publishDiagnostics: $diagnostics")
    }
  }

  override fun showMessage(messageParams: MessageParams?) {
    coroutineScope.launchWithTimeout(TIMEOUT) {
      val request = messageParams?.let {
        ShowMessageRequestParams().apply {
          message = it.message
          type = it.type
          actions = emptyList()
        }
      }

      showMessageRequest(request)
    }
  }

  override fun showMessageRequest(
    messageParams: ShowMessageRequestParams?
  ): CompletableFuture<MessageActionItem?> = coroutineScope.asyncWithTimeout(TIMEOUT) {
    val (group, type) = when (messageParams?.type) {
      MessageType.Error -> Pair(NotificationGroupType.IMPORTANT, NotificationType.ERROR)
      MessageType.Warning -> Pair(NotificationGroupType.IMPORTANT, NotificationType.WARNING)
      MessageType.Info -> Pair(NotificationGroupType.GENERAL, NotificationType.INFORMATION)
      MessageType.Log -> Pair(null, null).also { logMessage(messageParams) }
      else -> Pair(null, null)
    }

    var selected: MessageActionItem? = null
    if (messageParams != null && group != null && type != null) {
      notificationManager.sendNotification(
        notificationData = Notification(
          title = GitLabBundle.message("settings.ui.group.language-server.name"),
          message = messageParams.message,
          actions = messageParams.actions.map { action ->
            NotificationAction(action.title) { selected = action }
          }
        ),
        notificationGroupType = group,
        notificationType = type,
      )
    }

    selected
  }.asCompletableFuture()

  @Suppress("ForbiddenVoid")
  override fun registerCapability(params: RegistrationParams): CompletableFuture<Void?> {
    return coroutineScope.asyncWithTimeout(TIMEOUT) {
      params.registrations.forEach { request ->
        capabilityManager.register(
          request.id,
          request.method,
          request.registerOptions as JsonObject
        )
      }

      null
    }.asCompletableFuture()
  }

  @Suppress("ForbiddenVoid")
  override fun unregisterCapability(params: UnregistrationParams): CompletableFuture<Void?> {
    return coroutineScope.asyncWithTimeout(TIMEOUT) {
      params.unregisterations.forEach { request ->
        capabilityManager.unregister(request.id, request.method)
      }

      null
    }.asCompletableFuture()
  }

  // TODO: Implement Telemetry event handler
  override fun telemetryEvent(event: Any) {
    logger.info("telemetryEvent: $event")
  }

  private fun CoroutineScope.launchWithTimeout(timeout: Duration, block: () -> Unit) = launch {
    withTimeout(timeout) {
      try {
        block()
      } catch (e: Exception) {
        logger.warn("Error processing a Language Server client message.", e)
      }
    }
  }

  private fun <T> CoroutineScope.asyncWithTimeout(timeout: Duration, block: () -> T) = async {
    withTimeout(timeout) {
      try {
        block()
      } catch (e: Exception) {
        logger.warn("Error processing a Language Server client message.", e)
        null
      }
    }
  }
}
