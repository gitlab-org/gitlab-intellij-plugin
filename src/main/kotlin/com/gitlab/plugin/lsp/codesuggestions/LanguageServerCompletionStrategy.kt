package com.gitlab.plugin.lsp.codesuggestions

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryService
import com.gitlab.plugin.lsp.params.InlineCompletionContext
import com.gitlab.plugin.lsp.params.InlineCompletionParams
import com.gitlab.plugin.lsp.params.InlineCompletionTriggerKind
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.util.code.CodeFormatter
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.future.asDeferred
import org.eclipse.lsp4j.CompletionItem
import org.eclipse.lsp4j.CompletionList
import org.eclipse.lsp4j.Position
import org.eclipse.lsp4j.TextDocumentIdentifier
import org.eclipse.lsp4j.jsonrpc.messages.Either
import java.util.concurrent.CompletableFuture

@Service(Service.Level.PROJECT)
class LanguageServerCompletionStrategy(
  private val project: Project,
  private val coroutineScope: CoroutineScope
) : CompletionStrategy {
  private val logger = logger<LanguageServerCompletionStrategy>()

  private var ongoingRequest: CompletableFuture<Either<List<CompletionItem>, CompletionList>>? = null

  private val codeFormatter by lazy { CodeFormatter() }
  private val languageServerService by lazy { project.service<GitLabLanguageServerService>() }

  @Suppress("TooGenericExceptionCaught")
  override suspend fun generateCompletions(context: SuggestionContext): Completion.Response? {
    ongoingRequest?.cancel(true)

    val lspServer = languageServerService.lspServer
      ?: return null

    val payload = context.toRequestPayload()
    return coroutineScope.async {
      val request = lspServer.inlineCompletion(
        InlineCompletionParams(
          textDocumentIdentifier = TextDocumentIdentifier(payload.currentFile.fileUri),
          cursorPosition = Position(payload.currentFile.cursorLine, payload.currentFile.cursorColumn),
          context = InlineCompletionContext(context.triggerKind)
        )
      ).also { ongoingRequest = it }

      try {
        val result = request.asDeferred().await()
        logger.info("Received completion from GitLab Language Server. result=$result")

        val choices = (result.left ?: result.right?.items)
          ?.map { it.toChoice(context) }
          ?.distinctBy { it.text }
          .orEmpty()

        val telemetryService = project.service<TelemetryService>()
        choices.firstOrNull()?.uniqueTrackingId?.let { uniqueTrackingId ->
          telemetryService.newContext().copy(trackingId = uniqueTrackingId).run(telemetryService::update)
        }

        Completion.Response(choices)
      } catch (_: CancellationException) {
        logger.debug("Cancelled completions request from language server.")
        null
      } catch (e: Throwable) {
        logger.warn("Failed to get completions from language server", e)
        null
      }
    }.await()
  }

  /**
   * For details on response model see:
   * [completionChoiceMapper](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/blob/e79169228/src/common/utils/suggestion_mappers.ts)
   */
  private suspend fun CompletionItem.toChoice(context: SuggestionContext): Completion.Response.Choice {
    return Completion.Response.Choice(
      codeFormatter.format(insertText, context.toFormattingContext()),
      command
    )
  }

  private val SuggestionContext.triggerKind: InlineCompletionTriggerKind
    get() = when {
      isInvoked -> InlineCompletionTriggerKind.INVOKED
      else -> InlineCompletionTriggerKind.AUTOMATIC
    }
}
