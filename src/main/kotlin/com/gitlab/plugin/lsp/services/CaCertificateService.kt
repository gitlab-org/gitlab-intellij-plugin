package com.gitlab.plugin.lsp.services

import com.intellij.openapi.components.Service
import java.io.File
import java.security.cert.X509Certificate
import java.util.*

@Service(Service.Level.APP)
class CaCertificateService {
  var caCertificateFile: File? = null
  private var caCertificate: X509Certificate? = null

  fun updateCaCertificate(caCertificate: X509Certificate) {
    if (caCertificate.serialNumber == this.caCertificate?.serialNumber) {
      return
    }

    this.caCertificate = caCertificate
    caCertificateFile = saveAsPemToDisk()
  }

  private fun saveAsPemToDisk(): File? {
    val caCertificate = this.caCertificate ?: return null

    val pemFile = File.createTempFile("ca_certificate", ".pem")
    pemFile.bufferedWriter().use { writer ->
      writer.write("-----BEGIN CERTIFICATE-----\n")
      writer.write(Base64.getEncoder().encodeToString(caCertificate.encoded))
      writer.write("\n-----END CERTIFICATE-----")
    }

    return pemFile
  }
}
