package com.gitlab.plugin.lsp.webview

import com.gitlab.plugin.chat.view.ThemeInfoPayload
import com.intellij.openapi.components.Service
import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.util.FontUtil

@Service(Service.Level.APP)
class ThemeProvider {
  // The name HAS TO be styles so that is maps to the correct JSON key
  data class Theme(val styles: Map<String, String>)

  fun getCurrentTheme(): Theme {
    val themeInfo = ThemeInfoPayload()
    val themeColors = mutableMapOf(
      "--editor-foreground" to themeInfo.jetBrainsForeground,
      "--editor-foreground--muted" to themeInfo.jetBrainsInactiveForeground,
      "--editor-foreground--disabled" to themeInfo.jetBrainsInactiveForeground,
      "--editor-background" to themeInfo.jetBrainsBackground,
      "--editor-background-alternative" to themeInfo.jetBrainsBackground,

      "--editor-heading-foreground" to themeInfo.pluginHeaderForeground,
      "--editor-border-color" to themeInfo.jetBrainsBorder,

      "--editor-alert-foreground" to themeInfo.notificationForeground,
      "--editor-alert-background" to themeInfo.notificationBackground,
      "--editor-alert-border-color" to themeInfo.notificationBorder,

      "--editor-token-foreground" to themeInfo.jetBrainsBadgeForeground,
      "--editor-token-background" to themeInfo.jetBrainsBadgeBackground,

      "--editor-icon-foreground" to themeInfo.iconForeground,

      "--editor-textLink-foreground" to themeInfo.jetBrainsActiveHyperlink,
      "--editor-textLink-foreground-active" to themeInfo.jetBrainsPressedHyperlink,
      "--editor-textPreformat-foreground" to themeInfo.jetBrainsTextFieldForeground,
      "--editor-textPreformat-background" to themeInfo.jetBrainsCodeBlockEditorPaneBackground,

      "--editor-input-border" to themeInfo.textFieldBorder,
      "--editor-input-background" to themeInfo.jetBrainsTextFieldBackground,
      "--editor-input-foreground" to themeInfo.jetBrainsTextFieldForeground,
      "--editor-input-placeholder-foreground" to themeInfo.editorPlaceholderForeground,
      "--editor-input-background--focus" to themeInfo.jetBrainsTextFieldSelectionBackground,
      "--editor-input-foreground--focus" to themeInfo.jetBrainsTextFieldSelectionForeground,

      "--editor-checkbox-background" to themeInfo.jetBrainsCheckboxBackground,
      "--editor-checkbox-border" to themeInfo.jetBrainsCheckboxBorder,
      "--editor-checkbox-background-selected" to themeInfo.jetBrainsCheckboxMenuItemSelectionBackground,
      "--editor-checkbox-border-selected" to themeInfo.jetBrainsCheckboxBorder,

      "--editor-button-background" to themeInfo.jetBrainsButtonDefaultButton,
      "--editor-button-foreground" to themeInfo.jetBrainsButtonForeground,
      "--editor-button-border" to themeInfo.jetBrainsButtonShadow,
      "--editor-button-background--hover" to themeInfo.jetBrainsButtonDefaultButton,
      "--editor-button-foreground--hover" to themeInfo.jetBrainsButtonForeground,

      "--editor-textCodeBlock-background" to themeInfo.jetBrainsCodeBlockEditorPaneBackground,
      "--editor-widget-shadow" to themeInfo.jetBrainsButtonShadow,
      "--editor-error-foreground" to themeInfo.jetBrainsErrorForeground,

      "--editor-dropdown-background" to themeInfo.jetBrainsCheckboxMenuItemSelectionBackground,
      "--editor-dropdown-foreground" to themeInfo.jetBrainsButtonForeground,
      "--editor-dropdown-border" to themeInfo.jetBrainsButtonForeground,

      "--editor-button-foreground--hover" to themeInfo.jetBrainsButtonForeground,
    )

    val outputThemesMap = themeColors.map { (key, value) ->
      key to rgbListToHex(value)
    }.toMap().toMutableMap()

    outputThemesMap["--editor-font-family"] = EditorColorsManager.getInstance().globalScheme.editorFontName
    outputThemesMap["--editor-font-size"] = EditorColorsManager.getInstance().globalScheme.editorFontSize.toString()

    outputThemesMap["--editor-code-font-family"] = FontUtil.getMenuFont().family
    outputThemesMap["--editor-code-font-size"] = FontUtil.getMenuFont().size.toString()
    outputThemesMap["--editor-code-font-weight"] = FontUtil.getMenuFont().size.toString()

    return Theme(
      outputThemesMap
    )
  }

  // Using this conversion method to be able to re-use the ThemeInfoPayload info
  private fun rgbListToHex(rgbList: List<Int>): String {
    val r = rgbList[0]
    val g = rgbList[1]
    val b = rgbList[2]
    return "#%02X%02X%02X".format(r, g, b)
  }
}
