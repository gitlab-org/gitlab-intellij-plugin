package com.gitlab.plugin.lsp.messages

data class CodeSuggestionsTelemetryContext(val trackingId: String, val optionId: Int?)

class CodeSuggestionsTelemetryParams(
  val action: String,
  val context: CodeSuggestionsTelemetryContext,
) {
  val category: String = "code_suggestions"

  override fun toString(): String {
    return "CodeSuggestionsTelemetryParams(category=$category, action=$action, context=$context)"
  }
}
