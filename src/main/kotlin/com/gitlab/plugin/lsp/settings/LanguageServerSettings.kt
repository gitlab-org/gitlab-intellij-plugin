package com.gitlab.plugin.lsp.settings

import com.intellij.openapi.components.*
import com.intellij.openapi.components.Service.Level.PROJECT

/**
 * The properties defined inside objects extending BaseState must be mutable (var) to be tracked properly.
 * The delegates provided by BaseState (like list<T>()) automatically track changes to mutable collections
 * and mark the state as modified, even when the collection is modified (add/remove items). This ensures
 * the state is persisted correctly across IDE restarts without needing to call incrementModificationCount().
 */

class FeatureFlags : BaseState() {
  var streamCodeGenerations by property(true)
}

class HttpAgentOptions : BaseState() {
  var pass by property(false)
  var ca by string()
  var cert by string()
  var certKey by string()
}

/**
 * Using an immutable reference to a mutable list (val list<>) followed by calling incrementModificationCount()
 * doesn't persist the state. Neither having a mutable reference to an immutable collection (var listOf<>). The only
 * way to get the state persisted correctly by the BaseState is by having double mutability.
 */

@Suppress("DoubleMutabilityForCollection")
class CodeCompletion : BaseState() {
  /*
   * Use the helper functions in this class to modify these stringSets, as incrementModificationCount needs to be
   * triggered in every modification.
   */
  var disabledSupportedLanguages by stringSet()
  var additionalLanguages by stringSet()

  var enableOpenTabsContext by property(true)

  fun disableSupportedLanguage(languageId: String): Boolean {
    val isAdded = disabledSupportedLanguages.add(languageId)
    incrementModificationCount()
    return isAdded
  }

  fun enableSupportedLanguage(languageId: String) {
    disabledSupportedLanguages.remove(languageId)
    incrementModificationCount()
  }

  fun addAdditionalLanguage(languageId: String): Boolean {
    val isAdded = additionalLanguages.add(languageId)
    incrementModificationCount()
    return isAdded
  }

  fun removeAdditionalLanguage(languageId: String) {
    additionalLanguages.remove(languageId)
    incrementModificationCount()
  }

  fun removeAllAdditionalLanguages() {
    additionalLanguages.clear()
    incrementModificationCount()
  }

  fun getDisabledLanguages(): Set<String> {
    return disabledSupportedLanguages.filterNot { additionalLanguages.contains(it) }.toSet()
  }
}

class WorkspaceSettings : BaseState() {
  var codeCompletion by property(CodeCompletion())
  var httpAgentOptions by property(HttpAgentOptions())
  var logLevel by string("info")
  var featureFlags by property(FeatureFlags())
}

@Suppress("DoubleMutabilityForCollection")
class LanguageServerSettingsState : BaseState() {
  var workspaceSettings by property(WorkspaceSettings())
}

@Service(PROJECT)
@State(name = "LanguageServerSettings", storages = [Storage(value = "gitlab.xml")], reportStatistic = false)
class LanguageServerSettings : SimplePersistentStateComponent<LanguageServerSettingsState>(
  LanguageServerSettingsState()
)
