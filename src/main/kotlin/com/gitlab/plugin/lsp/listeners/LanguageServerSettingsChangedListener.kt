package com.gitlab.plugin.lsp.listeners

import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.ProjectLevel

interface LanguageServerSettingsChangedListener {
  companion object {
    @ProjectLevel
    val LANGUAGE_SERVER_SETTINGS_CHANGED_TOPIC = Topic(
      LanguageServerSettingsChangedListener::class.java.name,
      LanguageServerSettingsChangedListener::class.java
    )
  }

  fun onLanguageServerSettingsChanged()
}
