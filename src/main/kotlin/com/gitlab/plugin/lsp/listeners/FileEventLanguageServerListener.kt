package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.language.Language
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.uri
import com.intellij.openapi.application.runReadAction
import com.intellij.openapi.components.service
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.fileEditor.FileEditorManagerEvent
import com.intellij.openapi.fileEditor.FileEditorManagerListener
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.eclipse.lsp4j.DidCloseTextDocumentParams
import org.eclipse.lsp4j.DidOpenTextDocumentParams
import org.eclipse.lsp4j.TextDocumentIdentifier
import org.eclipse.lsp4j.TextDocumentItem

class FileEventLanguageServerListener(
  private val project: Project,
  private val coroutineScope: CoroutineScope
) : FileEditorManagerListener, LanguageServerStartedListener {
  private val languageServerService by lazy { project.service<GitLabLanguageServerService>() }

  override fun fileOpened(source: FileEditorManager, file: VirtualFile) {
    coroutineScope.launch { sendDidOpenNotification(file) }
  }

  override fun fileClosed(source: FileEditorManager, file: VirtualFile) {
    coroutineScope.launch {
      languageServerService.lspServer?.textDocumentService?.didClose(
        DidCloseTextDocumentParams(
          TextDocumentIdentifier(file.uri)
        )
      )
    }
  }

  override fun selectionChanged(event: FileEditorManagerEvent) {
    val file = event.newFile
      ?: return

    coroutineScope.launch {
      languageServerService.lspServer?.didChangeDocumentInActiveEditor(file.uri)
    }
  }

  override fun onLanguageServerStarted() {
    val fileEditorManager = FileEditorManager.getInstance(project)

    fileEditorManager.openFiles.forEach { sendDidOpenNotification(it) }
    fileEditorManager.selectedTextEditor?.virtualFile?.let { activeFile ->
      languageServerService.lspServer?.didChangeDocumentInActiveEditor(activeFile.uri)
    }
  }

  private fun sendDidOpenNotification(file: VirtualFile) {
    val textDocumentItem = file.toTextDocumentItem()
      ?: return

    languageServerService.lspServer?.textDocumentService?.didOpen(
      DidOpenTextDocumentParams(textDocumentItem)
    )
  }

  @Suppress("ReturnCount")
  private fun VirtualFile.toTextDocumentItem(): TextDocumentItem? {
    val document = runReadAction { FileDocumentManager.getInstance().getDocument(this) }
      ?: return null

    return TextDocumentItem(
      uri,
      Language.getInstance(fileType).id,
      modificationStamp.toInt(),
      document.text
    )
  }
}
