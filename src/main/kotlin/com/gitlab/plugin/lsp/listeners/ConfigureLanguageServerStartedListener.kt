package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.lsp.params.*
import com.gitlab.plugin.lsp.params.DuoChatFeature
import com.gitlab.plugin.lsp.services.CaCertificateService
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.services.GitLabProjectService
import com.gitlab.plugin.util.project.workspaceFolder
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.util.application
import org.eclipse.lsp4j.DidChangeConfigurationParams

class ConfigureLanguageServerStartedListener(private val project: Project) : LanguageServerStartedListener {
  private val gitLabTokenProviderManager by lazy { application.service<GitLabTokenProviderManager>() }
  private val duoPersistentSettings by lazy { application.service<DuoPersistentSettings>() }
  private val gitLabProjectService by lazy { project.service<GitLabProjectService>() }

  private val languageServerSettings by lazy { project.service<LanguageServerSettings>() }
  private val languageServerService by lazy { project.service<GitLabLanguageServerService>() }

  override fun onLanguageServerStarted() {
    languageServerService.lspServer?.workspaceService?.didChangeConfiguration(
      DidChangeConfigurationParams(
        Settings(
          baseUrl = duoPersistentSettings.url,
          token = gitLabTokenProviderManager.getToken(),
          ignoreCertificateErrors = duoPersistentSettings.ignoreCertificateErrors,
          projectPath = gitLabProjectService.getCurrentProjectPath().orEmpty(),
          telemetry = Telemetry(
            duoPersistentSettings.telemetryEnabled,
            duoPersistentSettings.trackingUrl
          ),
          logLevel = languageServerSettings.state.workspaceSettings.logLevel,
          openTabsContext = languageServerSettings.state.workspaceSettings.codeCompletion.enableOpenTabsContext,
          codeCompletion = CodeCompletion(
            additionalLanguages = languageServerSettings.state.workspaceSettings.codeCompletion.additionalLanguages,
            disabledSupportedLanguages = languageServerSettings.state.workspaceSettings.codeCompletion.getDisabledLanguages(),
            enabled = duoPersistentSettings.codeSuggestionsEnabled
          ),
          httpAgentOptions = languageServerSettings.state.workspaceSettings.httpAgentOptions.toDto().also {
            if (languageServerSettings.state.workspaceSettings.httpAgentOptions.pass) {
              it.ca = application.service<CaCertificateService>().caCertificateFile?.absolutePath
            }
          },
          featureFlags = languageServerSettings.state.workspaceSettings.featureFlags.toDto(),
          workspaceFolders = listOfNotNull(project.workspaceFolder),
          duoChat = DuoChatFeature(enabled = duoPersistentSettings.duoChatEnabled)
        )
      )
    )

    if (BuildConfig.USE_LS_WEBVIEW) {
      project.service<CefChatBrowser>().loadLSWebView()
    }
  }
}
