package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.lsp.params.StreamingCompletionResponse
import com.gitlab.plugin.lsp.services.CodeCompletionRegistryService
import com.gitlab.plugin.util.code.CodeFormatter
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class StreamingCompletionListener(
  private val project: Project,
  private val coroutineScope: CoroutineScope
) : CompletionListener {
  private val logger = logger<StreamingCompletionListener>()
  private val codeFormatter = CodeFormatter()

  override fun update(params: StreamingCompletionResponse) {
    val codeCompletionRegistryService = project.service<CodeCompletionRegistryService>()
    if (!params.done) {
      codeCompletionRegistryService[params.id]?.run {
        coroutineScope.launch {
          val value = codeFormatter.format(params.completion, context.toFormattingContext())

          runInEdt {
            element.update(value)
          }
        }
      }
    } else {
      codeCompletionRegistryService.complete(params)
    }
  }

  override fun cancel(id: String) {
    logger.debug("Received streaming cancel. id=$id")

    project.service<CodeCompletionRegistryService>().cancel(id)
  }
}
