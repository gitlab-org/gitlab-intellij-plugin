package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.uri
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.event.DocumentEvent
import com.intellij.openapi.editor.event.DocumentListener
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.project.ProjectLocator
import com.intellij.openapi.vfs.VirtualFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.eclipse.lsp4j.DidChangeTextDocumentParams
import org.eclipse.lsp4j.TextDocumentContentChangeEvent
import org.eclipse.lsp4j.VersionedTextDocumentIdentifier

internal class FileChangedLanguageServerListener(
  private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
) : DocumentListener {
  var currentFile: VirtualFile? = null

  override fun documentChanged(event: DocumentEvent) {
    val virtualFile = FileDocumentManager.getInstance().getFile(event.document)
      ?: return

    val projects = ProjectLocator.getInstance()
      .getProjectsForFile(virtualFile)
      .filterNotNull()

    coroutineScope.launch {
      for (project in projects) {
        project.service<GitLabLanguageServerService>().lspServer?.textDocumentService?.didChange(
          DidChangeTextDocumentParams(
            VersionedTextDocumentIdentifier(virtualFile.uri, virtualFile.modificationStamp.toInt()),
            listOf(TextDocumentContentChangeEvent(event.document.text))
          )
        )

        if (currentFile != virtualFile) {
          project.service<GitLabLanguageServerService>().lspServer?.didChangeDocumentInActiveEditor(
            virtualFile.uri
          )
        }
      }
    }.invokeOnCompletion { currentFile = virtualFile }
  }
}
