package com.gitlab.plugin.lsp.params

data class StreamWithId(val id: String)
