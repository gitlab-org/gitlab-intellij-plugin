package com.gitlab.plugin.lsp.params

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class FeatureId {
  @SerialName("authentication")
  AUTHENTICATION,

  @SerialName("code_suggestions")
  CODE_SUGGESTIONS,

  @SerialName("chat")
  CHAT
}

data class FeatureStateParams(
  val featureId: FeatureId,
  val engagedChecks: List<FeatureStateCheckParams>
)

data class FeatureStateCheckParams(
  val checkId: String,
  val details: String? = null
)
