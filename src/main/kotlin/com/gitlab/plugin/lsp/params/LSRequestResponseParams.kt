package com.gitlab.plugin.lsp.params

import com.gitlab.plugin.chat.view.model.InsertCodeSnippetMessage
import kotlinx.serialization.Serializable

object LSRequestResponseParams {
  const val WEBVIEW_PLUGIN_ID = "duo-chat-v2"
}

data class CurrentFileContextRequestParams(val pluginId: String, val type: String, val payload: String)
data class InsertCodeSnippetParams(val pluginId: String, val type: String, val payload: InsertCodeSnippetMessage)

data class CurrentFileContextResponseParams(
  val selectedText: String,
  val fileName: String,
  val contentAboveCursor: String,
  val contentBelowCursor: String
)

enum class LSRequestType(val requestType: String) {
  CURRENT_FILE_CONTEXT("getCurrentFileContext"),
  INSERT_CODE_SNIPPET("insertCodeSnippet"),
  NEW_PROMPT("newPrompt"),
  APP_READY("appReady"),
  SHOW_MESSAGE("showMessage"),
}

// Outgoing requests/notifications

data class WebViewInfo(
  val id: String,
  val title: String,
  var uris: List<String>
)

data class NewPromptParams(
  val pluginId: String = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
  val type: String = LSRequestType.NEW_PROMPT.requestType,
  val payload: NewPromptPayload
)

data class NewPromptPayload(
  val prompt: String,
  val fileContext: CurrentFileContextResponseParams
)

data class LSNotificationParams(
  val pluginId: String,
  val type: String,
  val payload: Any?
)

enum class ViewMessageType(val messageType: String) {
  ERROR("error"),
  WARNING("warning"),
  INFO("info")
}

@Serializable
data class ViewMessagePayload(
  val type: String,
  val message: String,
)
