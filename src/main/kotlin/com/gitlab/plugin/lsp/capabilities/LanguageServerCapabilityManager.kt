package com.gitlab.plugin.lsp.capabilities

import com.google.gson.JsonObject
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project

@Service(Service.Level.PROJECT)
class LanguageServerCapabilityManager(
  private val project: Project
) {
  private val logger = logger<LanguageServerCapabilityManager>()
  private val registrar = HashMap<String, LanguageServerCapability>()

  fun register(id: String, method: String, options: JsonObject) {
    val registeredCapability = registrar[method]
    if (registeredCapability != null) {
      logger.info("Registering new options to capability. type=$method")
      return registeredCapability.register(id, options)
    }

    registrar[method] = when (method) {
      "workspace/didChangeWatchedFiles" -> DidChangeWatchedFileCapability(project)
      else -> return logger.info("Unknown capability: $method")
    }.apply { register(id, options) }

    logger.info("Capability $method registered.")
  }

  fun unregister(id: String, method: String) {
    registrar[method]?.unregister(id)
  }

  fun unregisterAll() {
    registrar.values.forEach { it.unregisterAll() }
  }
}
