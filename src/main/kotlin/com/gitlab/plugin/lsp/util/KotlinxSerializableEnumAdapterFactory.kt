package com.gitlab.plugin.lsp.util

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Custom adapter to allow lsp4j's LanguageServer service to handle annotations on enums that use the @Serializable
// annotation from the kotlinx.serialization library. The default adapter for enums only serializes enum values into
// their ordinal value.
class KotlinxSerializableEnumAdapterFactory : TypeAdapterFactory {
  override fun <T> create(gson: Gson, typeToken: TypeToken<T>): TypeAdapter<T>? {
    val rawType = typeToken.rawType
    // Let the other adapters handle this if it's not an enum class, or let the default enum adapter handle it if
    // it isn't annotated with kotlinx.serialization's @Serializable annotation
    if (
      !Enum::class.java.isAssignableFrom(rawType) ||
      rawType == Enum::class.java ||
      rawType.getAnnotation(Serializable::class.java) == null
    ) {
      return null
    }

    @Suppress("UNCHECKED_CAST")
    val enumClass = (if (!rawType.isEnum) rawType.superclass else rawType) as Class<out Enum<*>>

    @Suppress("UNCHECKED_CAST")
    return when {
      Enum::class.java.isAssignableFrom(enumClass) -> {
        KotlinxSerializableEnumAdapter(enumClass) as TypeAdapter<T>
      }
      else -> null
    }
  }

  class KotlinxSerializableEnumAdapter<T : Enum<T>>(private val enumClass: Class<T>) : TypeAdapter<T>() {
    private val nameToConstant = enumClass.enumConstants.associateBy { enum ->
      enumClass.getField(enum.name)
        .getAnnotation(SerialName::class.java)?.value
        ?: enum.name
    }

    override fun write(out: JsonWriter, value: T?) {
      if (value != null) {
        val serializedName = nameToConstant.entries.find { it.value == value }?.key
        out.value(serializedName)
      } else {
        out.nullValue()
      }
    }

    override fun read(input: JsonReader): T? {
      if (input.peek() == JsonToken.NULL) {
        input.nextNull()
        return null
      }

      val value = input.nextString()
      return nameToConstant[value]
    }
  }
}
