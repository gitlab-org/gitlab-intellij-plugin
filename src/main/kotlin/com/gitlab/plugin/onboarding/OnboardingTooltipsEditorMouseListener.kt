package com.gitlab.plugin.onboarding

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.LogicalPosition
import com.intellij.openapi.editor.event.EditorMouseEvent
import com.intellij.openapi.editor.event.EditorMouseListener
import com.intellij.openapi.ui.popup.JBPopupListener
import com.intellij.openapi.ui.popup.LightweightWindowEvent
import com.intellij.ui.GotItTooltip
import java.awt.Point

internal class OnboardingTooltipsEditorMouseListener : EditorMouseListener {
  private var hasShownCodeCompletionTooltip = false
  private var hasShownCodeGenerationTooltip = false

  private var isCodeCompletionTooltipShowing = false

  override fun mouseClicked(event: EditorMouseEvent) {
    super.mouseClicked(event)

    if (!DuoPersistentSettings.getInstance().codeSuggestionsEnabled) return

    if (!hasShownCodeCompletionTooltip) {
      showCodeCompletionTooltip(event.editor)
    } else if (!hasShownCodeGenerationTooltip && !isCodeCompletionTooltipShowing) {
      showCodeGenerationTooltip(event.editor)
    }
  }

  private fun showCodeCompletionTooltip(editor: Editor) {
    GotItTooltip(
      id = "code-completion-tooltip-id",
      textSupplier = {
        """
          To see a suggestion, try writing some code.
          Press ${code("Tab")} to accept, ${code("Esc")} to cancel, or continue typing to ignore.
        """.trimIndent()
      }
    )
      .withHeader("Code Completion")
      .show(editor.contentComponent) { editorContentComponent, balloon ->
        balloon.addListener(object : JBPopupListener {
          override fun beforeShown(event: LightweightWindowEvent) {
            super.beforeShown(event)
            isCodeCompletionTooltipShowing = true
          }

          override fun onClosed(event: LightweightWindowEvent) {
            super.onClosed(event)

            isCodeCompletionTooltipShowing = false

            if (!editorContentComponent.isShowing) return

            showCodeGenerationTooltip(editor)
          }
        })

        getCaretPosition(editor)
      }

    hasShownCodeCompletionTooltip = true
  }

  private fun showCodeGenerationTooltip(editor: Editor) {
    GotItTooltip(
      id = "code-generation-tooltip-id",
      textSupplier = {
        """
          To see code generation in action, write a comment, then press ${code("Enter")}, like this:
          <br>
          <br>
          // create a function that returns a random number between 1 and 100
        """.trimIndent()
      }
    )
      .withHeader("Code Generation")
      .show(editor.contentComponent) { _, _ -> getCaretPosition(editor) }

    hasShownCodeGenerationTooltip = true
  }

  private fun getCaretPosition(editor: Editor): Point {
    return editor.caretModel.logicalPosition
      .let { LogicalPosition(it.line + 1, it.column) } // +1 line to show the tooltip below the caret
      .let { editor.logicalPositionToXY(it) }
      .let { Point(it.x, it.y) }
  }
}
