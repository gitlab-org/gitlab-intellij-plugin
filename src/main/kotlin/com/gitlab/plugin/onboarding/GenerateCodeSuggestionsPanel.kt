package com.gitlab.plugin.onboarding

import com.gitlab.plugin.ktx.splitTextIntoJLabels
import com.gitlab.plugin.ui.components.Shortcut
import com.gitlab.plugin.ui.components.WrappableLabel
import com.intellij.util.ui.JBFont
import com.intellij.util.ui.WrapLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*

@Suppress("MagicNumber")
class GenerateCodeSuggestionsPanel(
  private val onBackButtonClicked: () -> Unit,
  private val onNextButtonClicked: () -> Unit,
) : JPanel() {

  private val title = WrappableLabel("Generate a Code Suggestion") {
    font = JBFont.h0().asBold()
    alignmentX = RIGHT_ALIGNMENT
  }

  private val instructions = JPanel(WrapLayout(FlowLayout.LEFT, 0, 0)).apply {
    alignmentX = RIGHT_ALIGNMENT

    splitTextIntoJLabels("Try it out: give an instruction in natural language while writing a comment, then press ")
    add(Shortcut("Enter"))
    add(JLabel("."))
  }

  private val process = JPanel(WrapLayout(FlowLayout.LEFT, 0, 0)).apply {
    alignmentX = RIGHT_ALIGNMENT
    splitTextIntoJLabels(
      "Duo will generate a code suggestion based on the context of your comment and the " +
        "rest of your code."
    )
  }

  private val buttons = JPanel(FlowLayout(FlowLayout.TRAILING)).apply {
    alignmentX = RIGHT_ALIGNMENT

    add(JButton("Back").apply { addActionListener { onBackButtonClicked() } })
    add(JButton("Next").apply { addActionListener { onNextButtonClicked() } })
  }

  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)

    add(title)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(instructions)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(process)
    add(Box.createRigidArea(Dimension(0, 16)))
    add(buttons)
  }
}
