package com.gitlab.plugin.onboarding

import com.intellij.openapi.components.service
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowManager
import java.time.Instant
import java.time.temporal.ChronoUnit

class OnboardingLifecycleManager(
  private val toolWindow: ToolWindow,
  private val toolWindowManager: ToolWindowManager
) {
  companion object {
    private const val ONBOARDING_EXPIRATION_DAYS = 10
  }

  fun manageOnboardingLifecycle() {
    val onboardingPersistentStateComponent = service<OnboardingPersistentStateComponent>().state
    val timeFirstLaunched = onboardingPersistentStateComponent.timeFirstLaunched

    when {
      timeFirstLaunched == null -> firstOnboardingLaunch(onboardingPersistentStateComponent)
      isOnboardingExpired(timeFirstLaunched) -> unregisterOnboardingToolWindow()
    }
  }

  private fun firstOnboardingLaunch(settings: OnboardingState) {
    settings.timeFirstLaunched = Instant.now()
    toolWindowManager.invokeLater { toolWindow.show() }
  }

  private fun unregisterOnboardingToolWindow() {
    toolWindowManager.invokeLater {
      toolWindowManager.unregisterToolWindow(toolWindow.id)
    }
  }

  private fun isOnboardingExpired(timeFirstLaunched: Instant): Boolean {
    return ChronoUnit.DAYS.between(timeFirstLaunched, Instant.now()) > ONBOARDING_EXPIRATION_DAYS
  }

  fun completeOnboarding() {
    toolWindow.hide()
    unregisterOnboardingToolWindow()
    service<OnboardingPersistentStateComponent>().state.hasCompletedOnboarding = true
  }
}
