package com.gitlab.plugin.onboarding

import com.gitlab.plugin.authentication.GitLabOAuthService
import com.gitlab.plugin.ui.components.HyperlinkTextPane
import com.gitlab.plugin.ui.components.WrappableLabel
import com.gitlab.plugin.util.Urls
import com.gitlab.plugin.util.launchUrl
import com.intellij.openapi.components.service
import com.intellij.util.application
import com.intellij.util.ui.JBFont
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JButton
import javax.swing.JPanel

@Suppress("MagicNumber")
class AuthenticatePanel(
  private val onVerifySetupButtonClicked: () -> Unit,
  private val onBackButtonClicked: () -> Unit,
  private val onNextButtonClicked: () -> Unit,
) : JPanel() {

  private val title = WrappableLabel("Authenticate with GitLab") {
    font = JBFont.h0().asBold()
    alignmentX = RIGHT_ALIGNMENT
  }

  private val gitLabComUserInfo = HyperlinkTextPane().apply {
    alignmentX = RIGHT_ALIGNMENT

    addText("If you're a GitLab.com user, ")
    addHyperlink("sign in") { application.service<GitLabOAuthService>().authorize() }
    addText(" with your GitLab account to get started.")
  }

  private val selfManagedUserInfo = HyperlinkTextPane().apply {
    alignmentX = RIGHT_ALIGNMENT

    addText("If you're on GitLab Self-Managed or GitLab Dedicated, ")
    addHyperlink("generate a personal access token") { launchUrl(Urls.GENERATE_TOKEN) }
    addText(" with the 'api' scope and add it to your account. Or, set up ")
    addHyperlink("1Password integration") { launchUrl(Urls.JETBRAINS_1PASSWORD_CLI_INTEGRATION) }
    addText(".")
  }

  private val troubleshootingInfo = HyperlinkTextPane().apply {
    alignmentX = RIGHT_ALIGNMENT

    addText("If you have any issues, review the ")
    addHyperlink("setup instructions") { launchUrl(Urls.PLUGIN_SETUP_INSTRUCTIONS) }
    addText(" and ")
    addHyperlink("verify your setup", onVerifySetupButtonClicked)
    addText(".")
  }

  private val buttons = JPanel(FlowLayout(FlowLayout.TRAILING)).apply {
    alignmentX = RIGHT_ALIGNMENT

    add(JButton("Back").apply { addActionListener { onBackButtonClicked() } })
    add(JButton("Next").apply { addActionListener { onNextButtonClicked() } })
  }

  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)

    add(title)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(gitLabComUserInfo)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(selfManagedUserInfo)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(troubleshootingInfo)
    add(Box.createRigidArea(Dimension(0, 16)))
    add(buttons)
  }
}
