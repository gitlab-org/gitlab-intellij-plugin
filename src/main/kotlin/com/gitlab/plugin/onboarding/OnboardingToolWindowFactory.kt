package com.gitlab.plugin.onboarding

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.intellij.openapi.components.service
import com.intellij.openapi.options.ShowSettingsUtil
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.ui.content.ContentFactory

internal class OnboardingToolWindowFactory : ToolWindowFactory, DumbAware {

  private lateinit var toolWindow: ToolWindow
  private lateinit var project: Project

  private val onboardingLifecycleManager by lazy {
    OnboardingLifecycleManager(toolWindow, ToolWindowManager.getInstance(project))
  }

  override fun init(toolWindow: ToolWindow) {
    this.toolWindow = toolWindow
    onboardingLifecycleManager.manageOnboardingLifecycle()
  }

  override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
    val component = OnboardingPanel.getInstance(
      onVerifySetupButtonClicked = {
        ShowSettingsUtil.getInstance().showSettingsDialog(project, GitLabBundle.message("settings.ui.group.name"))
      },

      onOnboardingCompleted = onboardingLifecycleManager::completeOnboarding,

      onOpenDuoChatClicked = {
        ToolWindowManager
          .getInstance(this.project)
          .getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)
          ?.show()
      }
    )

    val content = ContentFactory.getInstance().createContent(
      component,
      "",
      false
    )

    toolWindow.contentManager.addContent(content)
  }

  override suspend fun isApplicableAsync(project: Project): Boolean {
    return if (!service<OnboardingPersistentStateComponent>().state.hasCompletedOnboarding && !::project.isInitialized) {
      this.project = project
      true
    } else {
      false
    }
  }
}
