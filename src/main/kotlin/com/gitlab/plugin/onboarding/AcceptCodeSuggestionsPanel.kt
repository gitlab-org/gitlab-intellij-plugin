package com.gitlab.plugin.onboarding

import com.gitlab.plugin.ktx.splitTextIntoJLabels
import com.gitlab.plugin.ui.components.HyperlinkTextPane
import com.gitlab.plugin.ui.components.Shortcut
import com.gitlab.plugin.ui.components.WrappableLabel
import com.gitlab.plugin.util.Urls
import com.gitlab.plugin.util.launchUrl
import com.intellij.util.ui.JBFont
import com.intellij.util.ui.WrapLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*

@Suppress("MagicNumber")
class AcceptCodeSuggestionsPanel(
  private val onBackButtonClicked: () -> Unit,
  private val onNextButtonClicked: () -> Unit,
) : JPanel() {

  private val title = WrappableLabel("Accept a Code Suggestion") {
    font = JBFont.h0().asBold()
    alignmentX = RIGHT_ALIGNMENT
  }

  private val tryCodeSuggestions = HyperlinkTextPane().apply {
    alignmentX = RIGHT_ALIGNMENT

    addText("Try it out: open a ")
    addHyperlink("supported file type") { launchUrl(Urls.SUPPORTED_LANGUAGES_DOC) }
    addText(" and start writing code to receive AI code suggestions.")
  }

  private val instructions = JPanel(WrapLayout(FlowLayout.LEFT, 0, 0)).apply {
    alignmentX = RIGHT_ALIGNMENT

    add(JLabel("Press "))
    add(Shortcut("Tab"))
    add(JLabel(" to accept, "))
    add(Shortcut("Esc"))
    splitTextIntoJLabels(" to cancel, or continue typing to ignore.")
  }

  private val buttons = JPanel(FlowLayout(FlowLayout.TRAILING)).apply {
    alignmentX = RIGHT_ALIGNMENT

    add(JButton("Back").apply { addActionListener { onBackButtonClicked() } })
    add(JButton("Next").apply { addActionListener { onNextButtonClicked() } })
  }

  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)

    add(title)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(tryCodeSuggestions)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(instructions)
    add(Box.createRigidArea(Dimension(0, 16)))
    add(buttons)
  }
}
