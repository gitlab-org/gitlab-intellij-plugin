package com.gitlab.plugin.onboarding

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.xmlb.Converter
import com.intellij.util.xmlb.annotations.OptionTag
import java.time.Instant
import java.time.format.DateTimeParseException

@Service(Service.Level.APP)
@State(name = "Onboarding", storages = [Storage(value = "gitlab.xml")], reportStatistic = false)
class OnboardingPersistentStateComponent : PersistentStateComponent<OnboardingState> {

  private var state = OnboardingState()

  override fun getState(): OnboardingState = state

  override fun loadState(state: OnboardingState) {
    this.state = state
  }
}

data class OnboardingState(
  var hasCompletedOnboarding: Boolean = false,
  @OptionTag(converter = InstantConverter::class) var timeFirstLaunched: Instant? = null
)

class InstantConverter : Converter<Instant?>() {

  private val logger: Logger = logger<InstantConverter>()

  override fun fromString(value: String): Instant? {
    return try {
      Instant.parse(value)
    } catch (e: DateTimeParseException) {
      logger.error("Error converting $value to Instant", e)
      null
    }
  }

  override fun toString(value: Instant) = value.toString()
}
