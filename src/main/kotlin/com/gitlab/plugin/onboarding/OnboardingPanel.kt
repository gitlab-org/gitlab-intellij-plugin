package com.gitlab.plugin.onboarding

import com.intellij.ui.components.JBScrollPane
import com.intellij.ui.util.maximumWidth
import com.intellij.ui.util.preferredWidth
import java.awt.BorderLayout
import javax.swing.*
import javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
import javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED

@Suppress("MagicNumber")
class OnboardingPanel private constructor(
  onVerifySetupButtonClicked: () -> Unit,
  onOnboardingCompleted: () -> Unit,
  onOpenDuoChatClicked: () -> Unit
) : JPanel() {

  private val header: JComponent by lazy {
    val imageIcon = ImageIcon(javaClass.getResource("/assets/gitlabduo.jpeg"))
    JLabel(imageIcon).apply {
      alignmentX = LEFT_ALIGNMENT
    }
  }

  private val welcomePanel: JComponent by lazy {
    wrapPanelWithMargin(WelcomePanel(::navigateToAuthenticatePanel))
  }

  private val authenticatePanel: JComponent by lazy {
    wrapPanelWithMargin(
      AuthenticatePanel(onVerifySetupButtonClicked, ::navigateToWelcomePanel, ::navigateToAcceptCodeSuggestionsPanel)
    )
  }

  private val acceptCodeSuggestionsPanel: JComponent by lazy {
    wrapPanelWithMargin(
      AcceptCodeSuggestionsPanel(::navigateToAuthenticatePanel, ::navigateToGenerateCodeSuggestionsPanel)
    )
  }

  private val generateCodeSuggestionsPanel: JComponent by lazy {
    wrapPanelWithMargin(
      GenerateCodeSuggestionsPanel(::navigateToAcceptCodeSuggestionsPanel, ::navigateToDuoChatPanel)
    )
  }

  private val duoChatPanel: JComponent by lazy {
    wrapPanelWithMargin(
      DuoChatPanel(onOpenDuoChatClicked, ::navigateToGenerateCodeSuggestionsPanel, onOnboardingCompleted)
    )
  }

  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)
    border = BorderFactory.createEmptyBorder()

    add(header)
    add(welcomePanel)
  }

  private fun navigateToAuthenticatePanel() = switchPanel(authenticatePanel)
  private fun navigateToWelcomePanel() = switchPanel(welcomePanel)
  private fun navigateToAcceptCodeSuggestionsPanel() = switchPanel(acceptCodeSuggestionsPanel)
  private fun navigateToGenerateCodeSuggestionsPanel() = switchPanel(generateCodeSuggestionsPanel)
  private fun navigateToDuoChatPanel() = switchPanel(duoChatPanel)

  private fun switchPanel(newPanel: JComponent) {
    remove(1) // Remove the current body panel
    add(newPanel)
    revalidate()
    repaint()
  }

  private fun wrapPanelWithMargin(panel: JPanel): JPanel {
    return JPanel(BorderLayout()).apply {
      alignmentX = LEFT_ALIGNMENT
      maximumWidth = header.preferredWidth
      border = BorderFactory.createEmptyBorder(16, 16, 16, 16)
      add(panel, BorderLayout.PAGE_START)
    }
  }

  companion object {
    fun getInstance(
      onVerifySetupButtonClicked: () -> Unit,
      onOnboardingCompleted: () -> Unit,
      onOpenDuoChatClicked: () -> Unit
    ): JBScrollPane {
      return JBScrollPane(
        OnboardingPanel(onVerifySetupButtonClicked, onOnboardingCompleted, onOpenDuoChatClicked),
        VERTICAL_SCROLLBAR_AS_NEEDED,
        HORIZONTAL_SCROLLBAR_NEVER
      ).apply {
        border = BorderFactory.createEmptyBorder()
      }
    }
  }
}
