package com.gitlab.plugin.services.chat

import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.chat.ChatController
import com.gitlab.plugin.chat.context.managers.LanguageServerContextManager
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.chat.view.WebViewChatView
import com.gitlab.plugin.chat.view.model.ProjectStateChangedMessage
import com.gitlab.plugin.services.DuoChatStateService
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindowManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.swing.JComponent

@Service(Service.Level.PROJECT)
internal class ChatService(private val project: Project, val coroutineScope: CoroutineScope) : Disposable {
  private val chatView = WebViewChatView(
    chatBrowser = project.service<CefChatBrowser>(),
    toolWindowManager = ToolWindowManager.getInstance(project),
    duoChatStateService = project.service<DuoChatStateService>()
  )

  private val controller = ChatController(
    chatView = chatView,
    project = project,
    contextManager = LanguageServerContextManager(project),
    coroutineScope = coroutineScope,
  )

  val browserComponent: JComponent
    get() = project.service<CefChatBrowser>().browserComponent

  suspend fun processNewUserPrompt(prompt: NewUserPromptRequest) = controller.processNewUserPrompt(prompt)

  fun activateChatWithPrompt(prompt: NewUserPromptRequest) {
    ToolWindowManager.activateChat()

    coroutineScope.launch {
      processNewUserPrompt(prompt)
    }
  }

  fun updateProjectState() {
    chatView.projectStateChanged(
      ProjectStateChangedMessage(
        hasSelectedEditorInWindow = FileEditorManager.getInstance(project).selectedTextEditor != null
      )
    )
  }

  override fun dispose() {
    project.service<CefChatBrowser>().dispose()
  }

  private fun ToolWindowManager.Companion.activateChat() {
    getInstance(project)
      .getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)
      ?.takeIf { it.isVisible }
      ?.activate(null)
  }

  fun activateLSWebViewChat() {
    ToolWindowManager.getInstance(project)
      .getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)
      ?.activate(null)
  }
}
