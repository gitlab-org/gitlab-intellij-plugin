package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.requests.Metadata
import com.gitlab.plugin.di.DependencyInjectionAppService
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import io.ktor.client.call.*
import kotlinx.coroutines.runBlocking
import java.lang.module.ModuleDescriptor.Version
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.milliseconds

const val GITLAB_MINIMUM_VERSION = "16.8.0"
const val DUO_CHAT_MINIMUM_VERSION = "16.8.0"
const val CLEAN_SLASH_COMMAND_MINIMUM_VERSION = "17.5.0"
const val CODE_SUGGESTIONS_MINIMUM_VERSION = "16.8.0"

enum class VersionBasedFeature(private val minimumSupportedVersion: String) {
  DUO_CHAT(DUO_CHAT_MINIMUM_VERSION),
  REMOVE_CLEAN_COMMAND(CLEAN_SLASH_COMMAND_MINIMUM_VERSION),
  CODE_SUGGESTIONS(CODE_SUGGESTIONS_MINIMUM_VERSION),
  JETBRAINS_EXTENSION(GITLAB_MINIMUM_VERSION);

  val requiredVersion: Version
    get() = Version.parse(minimumSupportedVersion)
}

@Service(Service.Level.APP)
class GitLabServerService {
  private val logger = logger<GitLabServerService>()
  private var gitLabServer = GitLabServer(Version.parse("0.0.0"), lastUpdated = 0)

  private fun isVersionSupported(version: Version, minimumVersion: Version): Boolean {
    return version >= minimumVersion
  }

  fun isEnabled(feature: VersionBasedFeature): Boolean {
    return isVersionSupported(get().version, feature.requiredVersion)
  }

  fun get(): GitLabServer {
    if (isExpired()) runBlocking { update() }
    return gitLabServer
  }

  private fun isExpired() = (System.currentTimeMillis() - gitLabServer.lastUpdated).milliseconds > 2.hours

  private suspend fun update() {
    logger.info("Updating GitLab server version")

    val metadataResponse =
      service<DependencyInjectionAppService>()
        .getDuoClient()
        .get("api/v4/metadata")
        ?.body<Metadata.Response?>()
        ?: return logger.info("Failed to load GitLab server version")

    gitLabServer = GitLabServer(Version.parse(metadataResponse.version))
  }

  data class GitLabServer(var version: Version, var lastUpdated: Long = System.currentTimeMillis())
}
