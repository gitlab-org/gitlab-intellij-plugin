package com.gitlab.plugin.services

import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import kotlinx.coroutines.runBlocking

@Service(Service.Level.PROJECT)
class GitLabProjectService(project: Project) {
  private val projectService = project.service<GitRepositoryService>()

  fun getCurrentProjectPath(): String? {
    val currentRepository = runBlocking { projectService.fetchRepository() }
    val remoteRepositoryUrl = currentRepository?.remotes?.firstOrNull()?.urls?.first()
    val projectPath = remoteRepositoryUrl?.let { projectPathRegex.find(it)?.groups?.last()?.value }
    return projectPath?.split(".git")?.first()
  }

  companion object {
    // This regex is used for multiple URL formats: "ssh://username:password@host:port", "https://", "git@gitlab.com:"
    private val projectPathRegex = Regex("""(?:ssh://[^:]+:[^@]+@[^/]+/|https://[^/]+/|.*@.*?:)(.+)""")
  }
}
