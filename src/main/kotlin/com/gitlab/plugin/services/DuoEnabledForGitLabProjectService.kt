package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.GraphQLApi
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.util.application
import kotlinx.coroutines.runBlocking

@Service(Service.Level.PROJECT)
class DuoEnabledForGitLabProjectService(private val project: Project) {
  private val logger = logger<DuoEnabledForGitLabProjectService>()

  private var isDuoEnabledAtGitLabProjectLevel: Boolean? = null

  fun isDuoEnabledAtGitLabProjectLevel(forceRefresh: Boolean = false): Boolean {
    if (forceRefresh) isDuoEnabledAtGitLabProjectLevel = null

    isDuoEnabledAtGitLabProjectLevel?.let { return it }

    val projectPath = project.service<GitLabProjectService>().getCurrentProjectPath()
    if (projectPath != null) {
      val graphQLProject = runBlocking {
        application.service<GraphQLApi>().getProject(projectPath)
      }

      if (graphQLProject?.duoFeaturesEnabled != null) {
        isDuoEnabledAtGitLabProjectLevel = graphQLProject.duoFeaturesEnabled
      } else {
        logger.info("The project retrieved from GraphQL is null.")
      }
    } else {
      isDuoEnabledAtGitLabProjectLevel = true
    }

    return isDuoEnabledAtGitLabProjectLevel ?: true
  }
}
