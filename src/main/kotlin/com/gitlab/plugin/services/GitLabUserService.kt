package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.graphql.CurrentUserQuery
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.Service.Level.APP
import com.intellij.openapi.components.service
import com.intellij.util.application
import kotlinx.coroutines.runBlocking

@Service(APP)
class GitLabUserService(
  private val graphQLApi: GraphQLApi = application.service<GraphQLApi>()
) {
  private var currentUser: CurrentUserQuery.CurrentUser? = null

  // Current user ID format is expected to be `gid://gitlab/User/{id} where id is an integer
  fun getCurrentUserId(): Int? {
    return currentUser?.id?.substringAfterLast('/')?.toIntOrNull()
  }

  suspend fun invalidateAndFetchCurrentUser(): CurrentUserQuery.CurrentUser? {
    currentUser = graphQLApi.getCurrentUser()
    return currentUser
  }

  fun retrieveCurrentUser(): CurrentUserQuery.CurrentUser? {
    if (currentUser == null) {
      runBlocking {
        currentUser = graphQLApi.getCurrentUser()
      }
    }
    return currentUser
  }
}
