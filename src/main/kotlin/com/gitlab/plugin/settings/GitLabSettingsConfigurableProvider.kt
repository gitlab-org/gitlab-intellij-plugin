package com.gitlab.plugin.settings

import com.gitlab.plugin.BuildConfig
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurableProvider
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope

const val ALPHA_RELEASE_CHANNEL = "alpha"

internal class GitLabSettingsConfigurableProvider(
  private val project: Project,
  private val coroutineScope: CoroutineScope
) : ConfigurableProvider() {
  override fun createConfigurable(): Configurable? = when {
    canCreateConfigurable() -> GitLabSettingsConfigurable(project, coroutineScope).let {
      when (BuildConfig.RELEASE_CHANNEL) {
        ALPHA_RELEASE_CHANNEL -> it.asBeta()
        else -> it
      }
    }
    else -> null
  }
}
