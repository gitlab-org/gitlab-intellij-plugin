package com.gitlab.plugin.settings

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.gitlab.plugin.services.health.ConfigurationValidationRequest
import com.gitlab.plugin.services.health.ConfigurationValidationService
import com.intellij.icons.AllIcons
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import com.intellij.ui.dsl.builder.Row
import javax.swing.Icon
import javax.swing.JLabel

class SettingsHealthChecker(
  private val project: Project,
  private val errorReportingInstructions: Cell<BrowserLink>,
  private val healthCheckGroup: Row,
  private val healthCheckLabels: Map<FeatureId, Cell<JLabel>>,
  private val verifyLabel: Cell<JLabel>,
  private val cannotVerifyLabel: Cell<JLabel>
) {
  private var healthCheckVisible = false

  suspend fun check(request: ConfigurationValidationRequest) {
    cannotVerifyLabel.visible(false)
    errorReportingInstructions.visible(false)

    val healthCheckResult = project.service<ConfigurationValidationService>().validateConfiguration(request)

    if (!healthCheckVisible) {
      healthCheckVisible = true
      healthCheckGroup.visible(true)
    }

    if (healthCheckResult == null) {
      cannotVerifyLabel.visible(true)
      healthCheckLabels.forEach { (_, cell) -> cell.visible(false) }
    } else {
      healthCheckLabels.forEach { (name, cell) ->
        cell.updateTextAndIcon(healthCheckResult[name])
      }
    }

    verifyLabel.visible(false)
    errorReportingInstructions.visible(healthCheckResult.hasError())
  }

  private fun Cell<JLabel>.updateTextAndIcon(healthCheck: FeatureStateParams?) {
    if (healthCheck == null) {
      visible(false)
      return
    }

    visible(true)
    component.text = GitLabBundle.message("feature.${healthCheck.featureId.name.lowercase()}")
    component.icon = healthCheck.icon()

    comment?.text = healthCheck.engagedChecks.text
    comment?.isVisible = healthCheck.engagedChecks.isNotEmpty()
  }

  private val List<FeatureStateCheckParams>.text
    get() = when {
      isNotEmpty() -> joinToString(separator = "<br>") { it.details ?: it.checkId }
      else -> null
    }

  private fun FeatureStateParams.icon(): Icon {
    return when {
      engagedChecks.isEmpty() -> AllIcons.RunConfigurations.TestPassed
      else -> AllIcons.General.Warning
    }
  }

  private fun Map<FeatureId, FeatureStateParams>?.hasError(): Boolean {
    return this == null || values.any { it.engagedChecks.isNotEmpty() }
  }
}
