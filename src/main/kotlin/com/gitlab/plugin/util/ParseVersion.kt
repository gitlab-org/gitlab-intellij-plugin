package com.gitlab.plugin.util

fun parseVersion(input: String): String? {
  return input
    .split('.', limit = 4)
    .filter { !it.contains("null") && it != "+" }
    .joinToString(".")
    .ifEmpty { null }
}
