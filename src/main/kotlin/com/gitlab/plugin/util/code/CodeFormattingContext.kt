package com.gitlab.plugin.util.code

import com.intellij.lang.Language
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project

data class CodeFormattingContext(
  val project: Project,
  val editor: Editor,
  val prefix: String,
  val suffix: String,
  val language: Language
) {
  val isCurrentLineEmpty by lazy {
    editor
      .document
      .text
      .lines()
      .getOrNull(editor.caretModel.logicalPosition.line)
      .isNullOrBlank()
  }
}
