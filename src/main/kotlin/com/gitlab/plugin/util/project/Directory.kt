package com.gitlab.plugin.util.project

import com.gitlab.plugin.util.uri
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import org.eclipse.lsp4j.WorkspaceFolder

val Project.workspaceFolder
  get() = guessProjectDir()?.let {
    val urlWithTrailingSlash = if (it.uri.endsWith("/")) it.uri else "${it.uri}/"
    WorkspaceFolder(urlWithTrailingSlash, it.name)
  }
