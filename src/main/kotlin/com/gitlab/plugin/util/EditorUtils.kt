package com.gitlab.plugin.util

import com.intellij.openapi.editor.Editor

fun Editor.getCursorOffset() = when {
  selectionModel.hasSelection() -> selectionModel.selectionEnd
  else -> caretModel.offset
}
