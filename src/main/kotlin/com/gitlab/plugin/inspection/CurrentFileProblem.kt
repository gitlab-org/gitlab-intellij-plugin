package com.gitlab.plugin.inspection

import com.intellij.openapi.util.TextRange

data class CurrentFileProblem(
  val description: String,
  val textRange: TextRange,
  val lineNumber: Int
)
