package com.gitlab.plugin.authentication

import com.intellij.collaboration.auth.OAuthCallbackHandlerBase
import com.intellij.openapi.components.service
import com.intellij.util.application

internal class GitLabOAuthCallbackHandler : OAuthCallbackHandlerBase() {
  override fun oauthService() = application.service<GitLabOAuthService>()
  override fun getServiceName(): String = GitLabOAuthService.SERVICE_NAME

  private val oAuthCallbackPublisher: GitLabOAuthCallbackListener = application
    .messageBus
    .syncPublisher(GitLabOAuthCallbackListener.OAUTH_CALLBACK_TOPIC)

  override fun handleAcceptCode(isAccepted: Boolean): AcceptCodeHandleResult {
    oAuthCallbackPublisher.onOAuthCallback(isAccepted)
    val page = if (isAccepted) successPage else failurePage
    return AcceptCodeHandleResult.Page(page)
  }

  private val successPage = getPage(
    "Authorization successful",
    "You can close this page and return to your IDE.",
    "https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/illustrations/rocket-launch-md.svg",
    "Rocket illustration"
  )

  private val failurePage = getPage(
    "Authorization failed",
    "Something went wrong. Return to your IDE and try again, or use a different authentication method.",
    "https://gitlab.com/-/project/32950782/uploads/f1e7de7f83030fc06d3602153cabc7bd/fail.svg",
    "Failure illustration"
  )

  @Suppress("LongMethod")
  private fun getPage(
    title: String,
    subtitle: String,
    imgSrc: String,
    imgAlt: String,
  ) =
    """
      <html lang="en">
        <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>$title</title>
          <style>
            :root {
              --default-regular-font: "GitLab Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
              --header-height: 48px;
              --text-color-default: #3a383f;
            }
            body {
              margin: 0;
              font-family: var(--default-regular-font);
              color: var(--text-color-default);
            }
            .header {
              position: fixed;
              top: 0;
              right: 0;
              left: 0;
              height: var(--header-height);
              display: flex;
              justify-content: center;
              align-items: center;
              background: #fff;
              border-bottom: 1px solid #dcdcde;
              z-index: 1030;
            }
            .content {
              padding-top: calc(var(--header-height) + 64px);
              display: flex;
              flex-direction: column;
              align-items: center;
              text-align: center;
            }
            .title {
              font-size: 1.75rem;
              font-weight: 600;
              margin-bottom: 0;
            }
            .subtitle {
              font-size: 1rem;
              margin-top: 0.75rem;
            }
          </style>
        </head>
        <body>
          <header class="header">
            <img src="https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/illustrations/gitlab_logo.svg" alt="GitLab Logo" />
          </header>
          <main class="content">
            <img src="$imgSrc" alt="$imgAlt" />
            <h1 class="title">$title</h1>
            <p class="subtitle">$subtitle</p>
          </main>
        </body>
      </html>
    """.trimIndent()
}
