package com.gitlab.plugin.authentication

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.ui.NotificationGroupType
import com.gitlab.plugin.util.Urls
import com.intellij.execution.ExecutionException
import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.util.ExecUtil
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application

// Using "Copy Secret Reference" in the 1Password app includes quotes so use a capture group to get the reference
// op://<vault-name>/<item-name>[/<section-name>]/<field-name>
val OP_SECRET_REFERENCE_PATTERN = Regex(
  "^(?:" +
    "\"(op://[^/]+/[^/]+(?:/[^/]+)?/[^/\"]+)\"" +
    "|" +
    "(op://[^/]+/[^/]+(?:/[^/]+)?/[^/\"]+)" +
    ")$"
)

@Service(Service.Level.APP)
class OnePasswordTokenProvider : TokenProvider {
  private val logger = logger<OnePasswordTokenProvider>()

  private var cachedSecretReference: String = ""
  private var cachedToken: String = ""
  private var cachedAccount: String? = null

  override fun token(): String {
    val settings = application.service<DuoPersistentSettings>()
    val secretReference = settings
      .integrate1PasswordCLISecretReference
      .takeIf { settings.integrate1PasswordCLI }
      ?.let(::get1PasswordSecretReference)
      ?: return ""

    val account = settings.integrate1PasswordAccount
    val token = getTokenBySecretReference(secretReference, account)

    if (token != cachedToken || secretReference != cachedSecretReference || account != cachedAccount) {
      cachedSecretReference = secretReference
      cachedAccount = account
      cachedToken = token
    }

    return token
  }

  /*
   * It is important we do not cache the token in this method.
   * It is used for validation of temporary credentials which could be applied or not.
   */
  fun getTokenBySecretReference(secretReferenceInput: String, account: String? = null): String {
    val secretReference = get1PasswordSecretReference(secretReferenceInput)
      ?: return ""

    if (cachedSecretReference == secretReference && cachedAccount == account && cachedToken.isNotEmpty()) {
      return cachedToken
    }

    var token = ""
    try {
      val commandLine = GeneralCommandLine("op", "read", secretReference)

      if (account != null) {
        commandLine.addParameters("--account", account)
      }

      val output = ExecUtil.execAndGetOutput(commandLine)

      if (output.exitCode == 0) {
        token = output.stdoutLines.firstOrNull().orEmpty()
      } else {
        logger.warn("Unable to read secret reference 'op read $secretReference' had an exit of ${output.exitCode}")
        display1PasswordIntegrationNotification(
          "Unable to read 1Password CLI secret reference. See idea.log for more details."
        )
      }
    } catch (ex: ExecutionException) {
      logger.warn("Unable to run 1password CLI", ex)

      display1PasswordIntegrationNotification(
        "Failed to execute command: op read $secretReference\nSee idea.log for more details."
      )
    }

    return token
  }

  fun getAccounts(): List<Account> {
    try {
      val commandLine = GeneralCommandLine("op", "account", "list")
      val output = ExecUtil.execAndGetOutput(commandLine)

      if (output.exitCode == 0) {
        val lines = output.stdoutLines
        return lines
          .drop(1) // Skip labels line
          .map { it.split(Regex("\\s+")) }
          .map { (url, email) ->
            Account(
              url = url,
              email = email
            )
          }
      } else {
        logger.warn("Command 'op account list' exited code ${output.exitCode}. Error: ${output.stderr}")
      }
    } catch (ex: ExecutionException) {
      logger.warn("Unable to run 1password CLI", ex)
    }

    return emptyList()
  }

  private fun display1PasswordIntegrationNotification(message: String) {
    GitLabNotificationManager().sendNotification(
      Notification(
        title = GitLabBundle.message("notification.title.gitlab-duo"),
        message = message,
        actions = listOf(
          NotificationAction.link(
            title = GitLabBundle.message("settings.ui.gitlab.1password-cli-gitlab-docs-label"),
            url = Urls.JETBRAINS_1PASSWORD_CLI_INTEGRATION,
          )
        )
      ),
      NotificationGroupType.IMPORTANT,
      NotificationType.ERROR
    )
  }

  private fun get1PasswordSecretReference(reference: String): String? = OP_SECRET_REFERENCE_PATTERN.find(reference)
    ?.groups
    ?.filterNotNull()
    ?.lastOrNull()
    ?.value

  data class Account(val url: String, val email: String)
}
