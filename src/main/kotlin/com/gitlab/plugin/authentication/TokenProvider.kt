package com.gitlab.plugin.authentication

fun interface TokenProvider {
  fun token(): String
}
