package com.gitlab.plugin.authentication

import com.intellij.openapi.diagnostic.logger
import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.AppLevel

interface GitLabOAuthCallbackListener {
  companion object {
    @AppLevel
    val OAUTH_CALLBACK_TOPIC = Topic(
      GitLabOAuthCallbackListener::class.java.name,
      GitLabOAuthCallbackListener::class.java
    )
  }

  fun onOAuthCallback(isAccepted: Boolean)
}

internal class DefaultGitLabOAuthCallbackListener(
  val onSuccess: () -> Unit,
  val onFailure: () -> Unit = {}
) : GitLabOAuthCallbackListener {
  private val logger = logger<DefaultGitLabOAuthCallbackListener>()

  override fun onOAuthCallback(isAccepted: Boolean) {
    when {
      isAccepted -> { onSuccess() }
      else -> {
        logger.warn("OAuth login failed on callback")
        onFailure()
      }
    }
  }
}
