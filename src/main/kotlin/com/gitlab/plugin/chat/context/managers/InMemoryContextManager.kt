package com.gitlab.plugin.chat.context.managers

import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.context.AiContextItemManager
import com.gitlab.plugin.chat.context.AiContextSearchQuery

// Used by the Quick Chat feature as the language server does not support multiple context window to be open at once.
class InMemoryContextManager : AiContextItemManager {
  private val items = mutableMapOf<String, AiContextItem>()

  override suspend fun query(query: AiContextSearchQuery): List<AiContextItem> {
    // nothing do for now to in the context of in memory
    return emptyList()
  }

  override suspend fun add(item: AiContextItem) {
    items[item.id] = item
  }

  override suspend fun remove(item: AiContextItem) {
    items.remove(item.id)
  }

  override suspend fun retrieveSelectedContextItemsWithContent(): List<AiContextItem> {
    return items.values.toList()
  }

  override suspend fun getCurrentItems(): List<AiContextItem> {
    return items.values.toList()
  }

  override suspend fun getAvailableCategories(): List<AiContextCategory> {
    return listOf(AiContextCategory.SNIPPET)
  }

  override suspend fun clearSelectedContextItems() {
    items.clear()
  }

  // Sending additional context for Quick Chat is not required since the snippets are already included with the request
  // This also ensures backward compatibility for users that are not on the latest GitLab version
  override suspend fun shouldIncludeAdditionalContext(): Boolean {
    return false
  }
}
