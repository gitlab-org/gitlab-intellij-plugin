package com.gitlab.plugin.chat.context

interface AiContextItemManager {
  suspend fun query(query: AiContextSearchQuery): List<AiContextItem>
  suspend fun add(item: AiContextItem)
  suspend fun remove(item: AiContextItem)
  suspend fun retrieveSelectedContextItemsWithContent(): List<AiContextItem>
  suspend fun getCurrentItems(): List<AiContextItem>
  suspend fun getAvailableCategories(): List<AiContextCategory>
  suspend fun shouldIncludeAdditionalContext(): Boolean
  suspend fun clearSelectedContextItems()
}
