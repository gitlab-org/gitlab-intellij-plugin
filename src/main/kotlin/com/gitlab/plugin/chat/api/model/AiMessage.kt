package com.gitlab.plugin.chat.api.model

import com.apollographql.apollo3.api.Subscription
import com.gitlab.plugin.chat.context.*
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.ChatWithAdditionalContextSubscription
import com.intellij.util.asSafely
import io.ktor.util.*
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

data class AiMessage(
  val requestId: String,
  val role: String,
  val content: String,
  val contentHtml: String,
  val timestamp: String,
  val errors: List<String> = emptyList(),
  val chunkId: Int? = null,
  val extras: ChatRecord.Metadata? = null
)

internal fun Subscription.Data.toAiMessage(): AiMessage? = when (this) {
  is ChatSubscription.Data -> aiCompletionResponse?.let { response ->
    AiMessage(
      requestId = response.requestId.orEmpty(),
      role = response.role.name.toLowerCasePreservingASCIIRules(),
      content = response.content.orEmpty(),
      contentHtml = response.contentHtml.orEmpty(),
      timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString(),
      errors = response.errors.orEmpty(),
      chunkId = response.chunkId
    )
  }
  is ChatWithAdditionalContextSubscription.Data -> aiCompletionResponse?.let { response ->
    AiMessage(
      requestId = response.requestId.orEmpty(),
      role = response.role.name.toLowerCasePreservingASCIIRules(),
      content = response.content.orEmpty(),
      contentHtml = response.contentHtml.orEmpty(),
      timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString(),
      errors = response.errors.orEmpty(),
      chunkId = response.chunkId,
      extras = response.extras?.let { metadata ->
        ChatRecord.Metadata(
          sources = emptyList(), // sources is always empty in GraphQL responses
          contextItems = metadata.additionalContext?.map { item ->
            AiContextItem(
              id = item.id,
              category = AiContextCategory.valueOf(item.category.name),
              metadata = item.metadata?.asSafely<LinkedHashMap<*, *>>()?.toAiContextItemMetadata()
            )
          }
        )
      }
    )
  }
  else -> null
}

@Suppress("UseOrEmpty")
internal fun LinkedHashMap<*, *>.toAiContextItemMetadata(): AIContextItemMetadata {
  return AIContextItemMetadata(
    title = this["title"] as? String ?: "",
    enabled = this["enabled"] as? Boolean ?: false,
    disabledReasons = this["disabledReasons"] as? List<String> ?: emptyList(),
    subType = AIContextProviderType.valueOf(this["subType"] as? String ?: ""),
    relativePath = this["relativePath"] as? String?,
    icon = this["icon"] as? String?,
    subTypeLabel = this["subTypeLabel"] as? String?,
    secondaryText = this["secondaryText"] as? String?,
    iid = this["iid"] as? String?,
    project = this["project"] as? String?,
    webUrl = this["webUrl"] as? String?,
    workspaceFolder = this["workspaceFolder"]?.asSafely<LinkedHashMap<*, *>>()?.let { workspaceFolder ->
      AiContextItemWorkspaceFolder(
        uri = workspaceFolder["uri"] as? String ?: "",
        name = workspaceFolder["name"] as? String ?: ""
      )
    },
    repositoryUri = this["repositoryUri"] as? String?,
    repositoryName = this["repositoryName"] as? String?,
    selectedBranch = this["selectedBranch"] as? String?,
    gitType = this["gitType"] as? String?,
    libs = this["libs"]?.asSafely<List<LinkedHashMap<*, *>>>()?.map { lib ->
      AiContextItemDependencyLibrary(
        name = lib["name"] as? String ?: "",
        version = lib["version"] as? String ?: ""
      )
    }
  )
}
