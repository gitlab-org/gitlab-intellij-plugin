package com.gitlab.plugin.chat.api.model

data class AiAction(
  val requestId: String,
  val errors: List<String>
)
