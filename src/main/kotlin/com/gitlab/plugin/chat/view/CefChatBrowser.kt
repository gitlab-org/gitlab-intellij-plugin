package com.gitlab.plugin.chat.view

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.chat.view.abstraction.ChatBrowser
import com.gitlab.plugin.lsp.webview.LanguageServerDuoChatWebViewService
import com.gitlab.plugin.lsp.webview.ThemeProvider
import com.gitlab.plugin.util.webview.*
import com.intellij.ide.ui.LafManagerListener
import com.intellij.ide.ui.UISettingsListener
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.colors.EditorColorsListener
import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.openapi.project.Project
import com.intellij.ui.jcef.JBCefBrowser
import com.intellij.ui.jcef.JBCefBrowserBase
import com.intellij.ui.jcef.JBCefJSQuery
import com.intellij.ui.jcef.JCEFHtmlPanel
import com.intellij.util.application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

private const val HOSTNAME = "localhost"
private const val PROTOCOL = "http"
private const val CHAT_URL = "http://localhost/index.html"

@Suppress("TooManyFunctions")
@Service(Service.Level.PROJECT)
internal class CefChatBrowser(private val project: Project, val coroutineScope: CoroutineScope) : ChatBrowser {
  val browserComponent
    get() = browser.component

  private val browser = JCEFHtmlPanel(CHAT_URL)
  private val localRequestHandler = CefLocalRequestHandler(PROTOCOL, HOSTNAME)
  private var postMessageHandler: (String) -> Unit = {}
  private val logger: Logger = logger<CefChatBrowser>()

  init {
    if (BuildConfig.USE_LS_WEBVIEW) {
      loadLSWebView()
    } else {
      browser.installIdeApi()
      browser.jbCefClient.addRequestHandler(localRequestHandler, browser.cefBrowser)
      browser.setInitialThemeAndFont()
      subscribeToThemeAndFontChanges()
    }
  }

  fun loadLSWebView() {
    coroutineScope.launch {
      val url = project.service<LanguageServerDuoChatWebViewService>().getWebViewInfo()
      logger.info("Loading LS webview URL: $url")
      browser.loadURL(url)
    }
    browser.setInitialLSWebViewTheme()
    subscribeToLSWebViewThemeChanges()
  }

  override fun dispose() = browser.dispose()

  override fun addLocalResource(path: String, resourcePathPrefix: String, mimeType: String) {
    val resourceStream = javaClass.getResourceAsStream("/$resourcePathPrefix$path")
    checkNotNull(resourceStream) { "Failed to load $path in $resourcePathPrefix" }

    localRequestHandler.addResource("/$path") {
      CefStreamResourceHandler(resourceStream, mimeType, this@CefChatBrowser)
    }
  }

  @Serializable
  data class DetailPayload(val detail: String)

  override fun postMessage(payload: String) {
    // Serialize your payload object
    val detailPayload = DetailPayload(payload)
    val jsonPayload = Json.encodeToString(DetailPayload.serializer(), detailPayload)

    // Now jsonPayload contains the entire serialized object including the key 'detail'
    browser.executeJavaScript(
      """
        window.dispatchEvent(
          new CustomEvent('message', $jsonPayload)
        )
      """.trimIndent()
    )
  }

  override fun onPostMessageReceived(block: (String) -> Unit) {
    postMessageHandler = block
  }

  private fun onPostMessageReceived(payload: String) = postMessageHandler(payload)
  private fun setTheme() {
    val themeColors = ThemeInfoPayload.buildJson()
    browser.executeJavaScript(
      """
        window.dispatchEvent(
          new CustomEvent('GitLabDuoThemeChanged', {detail: {themeColors: $themeColors}}))
      """.trimMargin()
    )
  }

  private fun setLSWebViewThemeAndSendThemeNotification() {
    val theme = service<ThemeProvider>().getCurrentTheme()
    project.service<LanguageServerDuoChatWebViewService>().sendThemeChangeNotification(theme)
  }

  private fun setFont() {
    val fonts = FontInfoPayload.buildJson()
    browser.executeJavaScript(
      """
        window.dispatchEvent(
          new CustomEvent('GitLabDuoFontChanged', { detail: {fontPayload: $fonts}}))
      """.trimMargin()
    )
  }

  private fun subscribeToThemeAndFontChanges() {
    val connection = application.messageBus.connect()

    // Subscribe to Theme listener
    connection.subscribe(LafManagerListener.TOPIC, LafManagerListener { setTheme() })
    connection.subscribe(LafManagerListener.TOPIC, LafManagerListener { setFont() })

    // Subscribes to Editor and Console Font listeners
    connection.subscribe(UISettingsListener.TOPIC, UISettingsListener { setFont() })
    connection.subscribe(EditorColorsManager.TOPIC, EditorColorsListener { setFont() })
  }

  private fun JBCefBrowser.installIdeApi() {
    // Query must be created before the browser launches
    val query =
      JBCefJSQuery
        .create(browser as JBCefBrowserBase)
        .addHandler(::onPostMessageReceived)

    addLoadEndHandler {
      executeJavaScript(
        """
        window['intellij'] = {
          postMessage: (payload) => {
            ${query.inject("JSON.stringify(payload)")}
          }
        }
        """.trimIndent()
      )
    }
  }

  private fun JBCefBrowser.setInitialThemeAndFont() = addLoadEndHandler {
    setTheme()
    setFont()
  }

  private fun JBCefBrowser.setInitialLSWebViewTheme() = addLoadEndHandler {
    setLSWebViewThemeAndSendThemeNotification()
  }

  private fun subscribeToLSWebViewThemeChanges() {
    val connection = application.messageBus.connect()

    // Subscribe to Theme, Editor and Console Font listeners
    connection.subscribe(LafManagerListener.TOPIC, LafManagerListener { setLSWebViewThemeAndSendThemeNotification() })
    connection.subscribe(UISettingsListener.TOPIC, UISettingsListener { setLSWebViewThemeAndSendThemeNotification() })
    connection.subscribe(
      EditorColorsManager.TOPIC,
      EditorColorsListener { setLSWebViewThemeAndSendThemeNotification() }
    )
  }
}
