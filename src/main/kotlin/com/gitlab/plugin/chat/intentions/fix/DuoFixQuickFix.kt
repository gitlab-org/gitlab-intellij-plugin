package com.gitlab.plugin.chat.intentions.fix

import com.gitlab.plugin.BuildConfig
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile

internal class DuoFixQuickFix(
  private val element: PsiElement,
  private val isDuoFixQuickFixEnabled: Boolean = BuildConfig.DUO_FIX_QUICK_FIX_ENABLED
) : DuoFixIntentionActionBase() {

  override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean =
    isDuoFixQuickFixEnabled

  override fun invoke(project: Project, editor: Editor, file: PsiFile) {
    val primaryCaret = editor.caretModel.primaryCaret
    val selectionRange = TextRange(primaryCaret.selectionStart, primaryCaret.selectionEnd)

    val textRange = when {
      primaryCaret.hasSelection() && element.textRange.intersects(selectionRange) -> selectionRange
      else -> element.textRange
    }

    buildAndSendChatPrompt(
      textRange = textRange,
      editor = editor,
      chatService = project.service(),
      chatRecordContextService = project.service(),
      fileProblemsService = project.service()
    )
  }
}
