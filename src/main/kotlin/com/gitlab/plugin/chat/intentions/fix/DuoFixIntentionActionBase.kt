package com.gitlab.plugin.chat.intentions.fix

import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.inspection.FileProblemsService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.codeInsight.intention.IntentionAction
import com.intellij.codeInsight.intention.LowPriorityAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.util.TextRange

internal abstract class DuoFixIntentionActionBase : IntentionAction, LowPriorityAction {
  override fun getText(): String = "Fix with Duo"

  override fun getFamilyName(): String = text

  override fun startInWriteAction(): Boolean = false

  protected fun buildAndSendChatPrompt(
    textRange: TextRange,
    editor: Editor,
    chatService: ChatService,
    chatRecordContextService: ChatRecordContextService,
    fileProblemsService: FileProblemsService
  ) {
    val prompt = NewUserPromptRequest(
      content = buildPromptContent(textRange, fileProblemsService),
      type = ChatRecord.Type.FIX_CODE,
      context = chatRecordContextService.getChatRecordContextOrNull(textRange, editor) ?: return
    )

    chatService.activateChatWithPrompt(prompt)
  }

  private fun buildPromptContent(
    textRange: TextRange,
    fileProblemsService: FileProblemsService,
  ): String {
    val errorsInRange = fileProblemsService
      .getCurrentFileErrors()
      .filter { textRange.intersects(it.textRange) }
      .map { "`${it.description}` on line ${it.lineNumber}" }

    return when (errorsInRange.size) {
      1 -> "/fix ${errorsInRange.first()}"
      else -> {
        val errors = errorsInRange.joinToString("\n\n")
        "/fix multiple issues:\n\n$errors"
      }
    }
  }
}
