package com.gitlab.plugin.chat.intentions.fix

import com.intellij.codeInsight.daemon.QuickFixActionRegistrar
import com.intellij.codeInsight.quickfix.UnresolvedReferenceQuickFixProvider
import com.intellij.psi.PsiReference

class DuoUnresolvedReferenceQuickFixProvider : UnresolvedReferenceQuickFixProvider<PsiReference>() {
  override fun registerFixes(ref: PsiReference, registrar: QuickFixActionRegistrar) {
    registrar.register(DuoFixQuickFix(ref.element))
  }

  override fun getReferenceClass(): Class<PsiReference> {
    return PsiReference::class.java
  }
}
