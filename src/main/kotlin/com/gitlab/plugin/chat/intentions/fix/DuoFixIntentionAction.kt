package com.gitlab.plugin.chat.intentions.fix

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.inspection.FileProblemsService
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiFile
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset

internal class DuoFixIntentionAction(
  private val isDuoFixIntentionEnabled: Boolean = BuildConfig.DUO_FIX_INTENTION_ENABLED
) : DuoFixIntentionActionBase() {

  override fun isAvailable(project: Project, editor: Editor, file: PsiFile): Boolean {
    if (!isDuoFixIntentionEnabled) return false

    val currentFileErrors = project.service<FileProblemsService>().getCurrentFileErrors()
    if (currentFileErrors.isEmpty()) return false

    val document = editor.document
    val caretLine = document.getLineNumber(editor.caretModel.offset)

    return currentFileErrors.none { caretLine == it.lineNumber }
  }

  override fun invoke(project: Project, editor: Editor, file: PsiFile) {
    buildAndSendChatPrompt(
      textRange = TextRange(file.startOffset, file.endOffset),
      editor = editor,
      chatService = project.service(),
      chatRecordContextService = project.service(),
      fileProblemsService = project.service()
    )
  }
}
