package com.gitlab.plugin.chat.quickchat.markdown

import com.intellij.lang.Language

@Suppress("CyclomaticComplexMethod")
object MarkdownLanguageConverter {
  fun convert(language: String): Language? {
    return when (language.lowercase()) {
      "c" -> Language.findLanguageByID("C++")
      "cpp" -> Language.findLanguageByID("C++")
      "csharp" -> Language.findLanguageByID("C#")
      "go" -> Language.findLanguageByID("go")
      "java" -> Language.findLanguageByID("JAVA")
      "javascript" -> Language.findLanguageByID("JavaScript")
      "jsx" -> Language.findLanguageByID("TypeScript JSX")
      "kotlin" -> Language.findLanguageByID("kotlin")
      "python" -> Language.findLanguageByID("Python")
      "php" -> Language.findLanguageByID("InjectablePHP")
      "ruby" -> Language.findLanguageByID("Ruby")
      "rust" -> Language.findLanguageByID("Rust")
      "scala" -> Language.findLanguageByID("Scala")
      "bash" -> Language.findLanguageByID("Shell Script")
      "swift" -> Language.findLanguageByID("Swift")
      "typescript" -> Language.findLanguageByID("TypeScript")
      "tsx" -> Language.findLanguageByID("TypeScript JSX")
      "haml" -> Language.findLanguageByID("Haml")
      "svelte" -> Language.findLanguageByID("SvelteHTML")
      "vue" -> Language.findLanguageByID("Vue")
      "hcl" -> Language.findLanguageByID("HCL-Terraform")
      "css" -> Language.findLanguageByID("CSS")
      "html" -> Language.findLanguageByID("HTML")
      "xml" -> Language.findLanguageByID("XML")
      "sql" -> Language.findLanguageByID("SQL")
      "pgsql" -> Language.findLanguageByID("PostgreSQL")
      "json" -> Language.findLanguageByID("JSON")
      else -> Language.findLanguageByID("TEXT")
    } ?: Language.findLanguageByID("TEXT")
  }
}
