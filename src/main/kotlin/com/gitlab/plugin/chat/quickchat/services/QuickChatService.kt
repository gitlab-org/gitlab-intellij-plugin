package com.gitlab.plugin.chat.quickchat.services

import com.gitlab.plugin.chat.ChatController
import com.gitlab.plugin.chat.context.managers.InMemoryContextManager
import com.gitlab.plugin.chat.quickchat.QUICK_CHAT_SESSION_KEY
import com.gitlab.plugin.chat.quickchat.QuickChatOpenMethod
import com.gitlab.plugin.chat.quickchat.QuickChatSession
import com.gitlab.plugin.chat.quickchat.QuickChatView
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUIFactory
import com.gitlab.plugin.chat.quickchat.ui.components.QuickChatIcons
import com.gitlab.plugin.telemetry.tracked.actions.trackQuickChatOpen
import com.gitlab.plugin.util.getCursorOffset
import com.intellij.openapi.components.Service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.Inlay
import com.intellij.openapi.editor.InlayProperties
import com.intellij.openapi.editor.ScrollType
import com.intellij.openapi.editor.addComponentInlay
import com.intellij.openapi.editor.markup.HighlighterLayer
import com.intellij.openapi.editor.markup.RangeHighlighter
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope

@Service(Service.Level.PROJECT)
class QuickChatService(private val project: Project, private val coroutineScope: CoroutineScope) {
  private val logger = logger<QuickChatService>()

  fun openChat(editor: Editor, openMethod: QuickChatOpenMethod) {
    val oldSession = editor.getUserData(QUICK_CHAT_SESSION_KEY)
    if (oldSession != null) {
      val currentLine = editor.caretModel.visualPosition.line

      if (currentLine == oldSession.line()) {
        logger.info("Did not open new quick chat as it is already open on the requested line")
        return
      }

      oldSession.close()
      logger.info("Closed quick chat session")
    }

    val ui = QuickChatUIFactory.create()

    val inlay = editor.addComponentInlay(
      offset = editor.getCursorOffset(),
      properties = InlayProperties().apply { priority(1) },
      component = ui
    )

    if (inlay == null) {
      logger.error("Failed to add component inlay for quick chat")
      return
    }

    ui.requestFocus()

    val controller = ChatController(
      chatView = QuickChatView(editor, ui),
      project = project,
      contextManager = InMemoryContextManager(),
      coroutineScope = coroutineScope
    )

    val gutterIcon = createGutterIcon(editor, inlay)
    val session = QuickChatSession(inlay, gutterIcon, controller)
    session.selectionChanged(editor)

    editor.putUserData(QUICK_CHAT_SESSION_KEY, session)
    editor.scrollingModel.scrollTo(editor.visualToLogicalPosition(inlay.visualPosition), ScrollType.MAKE_VISIBLE)
    trackQuickChatOpen(openMethod)

    logger.info("Quick chat session created and associated with editor")
  }

  private fun createGutterIcon(editor: Editor, inlay: Inlay<*>): RangeHighlighter {
    val inlayLine = editor.visualToLogicalPosition(inlay.visualPosition).line

    return editor
      .markupModel
      .addLineHighlighter(inlayLine, HighlighterLayer.CARET_ROW, null)
      .apply { gutterIconRenderer = QuickChatIcons.Gutter.Logo }
  }
}
