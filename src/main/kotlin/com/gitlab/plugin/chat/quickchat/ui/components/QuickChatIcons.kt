package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.ui.BaseGutterIconRenderer
import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object QuickChatIcons {
  object Gutter {
    @JvmField
    val Logo = object : BaseGutterIconRenderer() {
      override fun getIcon(): Icon {
        return IconLoader.getIcon("/icons/quick-chat/gutter.svg", javaClass)
      }
    }
  }

  @JvmField
  val InsertCodeSnippet = IconLoader.getIcon("/icons/quick-chat/insert.svg", javaClass)

  @JvmField
  val CopyToClipboard = IconLoader.getIcon("/icons/quick-chat/copy-to-clipboard.svg", javaClass)
}
