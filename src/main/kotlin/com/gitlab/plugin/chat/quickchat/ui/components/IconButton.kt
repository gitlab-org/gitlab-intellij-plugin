package com.gitlab.plugin.chat.quickchat.ui.components

import com.intellij.ui.JBColor
import java.awt.Component
import java.awt.Cursor
import java.awt.Dimension
import java.awt.event.ActionEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.BorderFactory
import javax.swing.Icon
import javax.swing.JButton

@Suppress("MagicNumber")
class IconButton(
  icon: Icon,
  text: String,
  private val action: (ActionEvent) -> Unit
) : JButton(icon) {
  init {
    alignmentX = Component.LEFT_ALIGNMENT

    border = BorderFactory.createLineBorder(JBColor.LIGHT_GRAY, 1, true)
    background = JBColor.background()

    toolTipText = text

    minimumSize = Dimension(32, 32)
    preferredSize = Dimension(32, 32)
    maximumSize = Dimension(32, 32)

    addMouseListener(
      object : MouseAdapter() {
        override fun mouseEntered(e: MouseEvent) {
          cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
          border = BorderFactory.createLineBorder(JBColor.DARK_GRAY, 1, true)
        }

        override fun mouseExited(e: MouseEvent) {
          cursor = Cursor.getDefaultCursor()
          border = BorderFactory.createLineBorder(JBColor.LIGHT_GRAY, 1, true)
        }

        override fun mousePressed(e: MouseEvent) {
          background = JBColor.DARK_GRAY
        }

        override fun mouseReleased(e: MouseEvent) {
          background = JBColor.background()
        }
      }
    )

    addActionListener { action(it) }
  }
}
