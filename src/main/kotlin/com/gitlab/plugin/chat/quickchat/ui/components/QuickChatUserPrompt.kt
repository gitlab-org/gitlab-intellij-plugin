package com.gitlab.plugin.chat.quickchat.ui.components

import com.intellij.ui.JBColor
import javax.swing.JLabel

class QuickChatUserPrompt(prompt: String) : JLabel(prompt) {
  init {
    foreground = JBColor.DARK_GRAY
  }
}
