package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.quickchat.ui.*
import com.gitlab.plugin.chat.quickchat.ui.listeners.QuickChatExitKeyListener
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.NewRecordMessage
import com.gitlab.plugin.chat.view.model.UpdateRecordMessage
import com.intellij.ui.components.JBScrollBar
import com.intellij.ui.components.JBScrollPane
import java.awt.Adjustable
import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import javax.swing.BorderFactory
import javax.swing.JPanel
import javax.swing.JScrollBar
import javax.swing.SwingUtilities

@Suppress("MagicNumber")
class QuickChatConversation(
  private val messageHandler: QuickChatUIMessageHandler
) : JBScrollPane(VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_NEVER) {
  private val chatMessages = HashMap<String, QuickChatMessage>()

  init {
    border = BorderFactory.createEmptyBorder(5, 0, 5, 0)
    background = quickChatBackground

    viewport.view = UI()

    addKeyListener(QuickChatExitKeyListener(messageHandler))
  }

  override fun createVerticalScrollBar(): JScrollBar {
    return object : JBScrollBar() {
      override fun isThin(): Boolean = true
    }
  }

  override fun getPreferredSize(): Dimension {
    return Dimension(QUICK_CHAT_WIDTH, super.getPreferredSize().height.coerceAtMost(QUICK_CHAT_MAX_HEIGHT))
  }

  private inner class UI : JPanel() {
    private val gridConstraints: GridBagConstraints

    init {
      layout = GridBagLayout()

      gridConstraints = GridBagConstraints().growsHorizontally().apply {
        gridwidth = QUICK_CHAT_WIDTH
      }

      messageHandler.register(::onMessage)
    }

    private fun onMessage(message: ChatViewMessage) {
      when (message) {
        is NewRecordMessage -> newMessage(message)
        is UpdateRecordMessage -> updateMessage(message)
        else -> Unit
      }
    }

    private fun newMessage(message: NewRecordMessage) {
      val record = message.record

      if (record.role == ChatRecord.Role.USER && record.state == ChatRecord.State.READY) {
        val newMessageComponent = QuickChatMessage(messageHandler, record.content.orEmpty())

        add(newMessageComponent, gridConstraints)

        chatMessages[record.requestId] = newMessageComponent
      } else if (record.role == ChatRecord.Role.ASSISTANT && record.state == ChatRecord.State.PENDING) {
        val messageComponent = chatMessages[record.requestId]
          ?: return

        messageComponent.createPendingDuoResponse()
      }

      SwingUtilities.invokeLater { verticalScrollBar.scrollToBottom() }
    }

    private fun updateMessage(message: UpdateRecordMessage) {
      val record = message.record

      if (record.role == ChatRecord.Role.ASSISTANT && record.state == ChatRecord.State.READY) {
        val messageComponent = chatMessages[record.requestId]
          ?: return

        messageComponent.updateDuoResponse(message.record)
      }
    }
  }

  private fun Adjustable.scrollToBottom() {
    value = maximum - visibleAmount
  }
}
