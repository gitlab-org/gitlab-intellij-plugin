package com.gitlab.plugin.chat.quickchat.ui

import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.view.model.ChatViewMessage

private typealias SentMessageListener = (message: ChatViewMessage) -> Unit

class QuickChatUIMessageHandler {
  var onMessageReceived: (message: QuickChatUIMessage) -> Unit = {}
  private val onSentMessageListeners: MutableSet<SentMessageListener> = mutableSetOf()

  fun dispatch(message: ChatViewMessage) = onSentMessageListeners.forEach { it(message) }
  fun register(listener: SentMessageListener) = onSentMessageListeners.add(listener)
  fun receive(message: QuickChatUIMessage) = onMessageReceived(message)
}
