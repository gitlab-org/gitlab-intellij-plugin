package com.gitlab.plugin.chat.quickchat.messages

sealed interface QuickChatUIMessage {
  data object Close : QuickChatUIMessage
  data class NewUserPrompt(val prompt: String) : QuickChatUIMessage
  data class InsertCodeSnippet(val snippet: String) : QuickChatUIMessage
}
