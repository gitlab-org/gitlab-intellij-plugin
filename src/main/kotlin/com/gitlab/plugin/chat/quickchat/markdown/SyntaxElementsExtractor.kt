package com.gitlab.plugin.chat.quickchat.markdown

import org.commonmark.node.*

class SyntaxElementsExtractor(
  private val content: String,
  private val syntaxElements: MutableList<SyntaxElement>
) : AbstractVisitor() {
  var offset = 0
  val lines = content.lines()

  override fun visit(document: Document) {
    super.visit(document)

    if (offset < content.length) {
      syntaxElements.add(SyntaxElement.Text(text = content.substring(offset)))
    }
  }

  override fun visit(code: Code) {
    val (start, end) = code.sourceSpans.position()

    syntaxElements.add(SyntaxElement.Text(text = content.substring(offset, start)))
    syntaxElements.add(SyntaxElement.Code(code = code.literal))

    offset = end
  }

  override fun visit(fencedCodeBlock: FencedCodeBlock) {
    val (start, end) = fencedCodeBlock.sourceSpans.position()

    syntaxElements.add(SyntaxElement.Text(text = content.substring(offset, start).keepOneNewLine()))
    syntaxElements.add(SyntaxElement.FencedCodeBlock(fencedCodeBlock.literal.trim('\n'), fencedCodeBlock.info))

    offset = end + fencedCodeBlock.closingFenceLength + 1 // Skip over closing fence and new line
  }

  private fun List<SourceSpan>.position(): Pair<Int, Int> {
    var offset = 0
    var startOffset = 0
    var endOffset = 0

    val start = first()
    val end = last()

    for (i in 0..end.lineIndex) {
      val line = lines.getOrNull(i) ?: break

      when (i) {
        start.lineIndex -> startOffset = offset + start.columnIndex
        end.lineIndex -> endOffset = offset + end.columnIndex
      }

      offset += line.length + 1 // Skip over for the new line
    }

    // This is to handle the case where a node starts and ends on the same line
    if (start == end) {
      endOffset = startOffset + end.length
    }

    return startOffset to endOffset
  }

  private fun String.keepOneNewLine(): String {
    /**
     * Some responses can contain multiple new lines, tabs or spaces between the text and a FencedCodeBlock.
     * This method will ensure that the extra characters are removed, leaving only one new line character at the end.
     */
    return "${trimEnd('\n', '\t', '\r', ' ')}\n"
  }
}
