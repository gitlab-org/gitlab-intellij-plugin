package com.gitlab.plugin.chat.quickchat.markdown

sealed interface SyntaxElement {
  data class Text(val text: String) : SyntaxElement
  data class Code(val code: String) : SyntaxElement
  data class FencedCodeBlock(val snippet: String, val language: String) : SyntaxElement
}
