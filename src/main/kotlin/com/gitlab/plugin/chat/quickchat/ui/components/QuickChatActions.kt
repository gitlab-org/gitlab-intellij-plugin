package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.quickchat.ui.CAN_SUBMIT
import com.gitlab.plugin.chat.quickchat.ui.quickChatBackground
import com.gitlab.plugin.chat.quickchat.ui.quickChatPrimaryColor
import com.intellij.ui.DocumentAdapter
import com.intellij.ui.JBColor
import com.intellij.ui.table.JBTable
import com.intellij.util.ui.JBFont
import java.awt.BorderLayout
import java.awt.Component
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*
import javax.swing.event.DocumentEvent
import javax.swing.table.AbstractTableModel
import javax.swing.table.DefaultTableCellRenderer
import kotlin.math.max

@Suppress("MagicNumber")
class QuickChatActions(val chatBox: QuickChatBox) : JPanel(BorderLayout()) {
  private val actionsTable: ActionsTable

  private val availableActions = arrayListOf(
    ActionElement("/reset", "Reset conversation and ignore the previous messages."),
    ActionElement("/tests", "Write tests for the selected snippet."),
    ActionElement("/refactor", "Refactor the selected snippet."),
    ActionElement("/explain", "Explain the selected snippet."),
    ActionElement("/fix", "Fix the selected code snippet.")
  )

  private val actionsModel = DefaultListModel<ActionElement>().apply { addAll(availableActions) }

  init {
    border = BorderFactory.createEmptyBorder(5, 5, 0, 0)
    background = quickChatBackground

    isVisible = false

    actionsTable = ActionsTable().also { add(it, BorderLayout.WEST) }

    chatBox.onDocumentChange(DocumentChangeListener())
    chatBox.onKeyPress(KeyPressedListener())
  }

  private inner class ActionsTable : JBTable() {
    init {
      border = BorderFactory.createCompoundBorder(
        BorderFactory.createLineBorder(quickChatPrimaryColor, 1, true),
        BorderFactory.createEmptyBorder(4, 4, 4, 4)
      )
      background = quickChatBackground
      selectionBackground = JBColor.LIGHT_GRAY
      rowSelectionAllowed = true
      autoResizeMode = JTable.AUTO_RESIZE_OFF

      setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
      setShowGrid(false)

      model = object : AbstractTableModel() {
        override fun getRowCount(): Int = actionsModel.size

        override fun getColumnCount(): Int = 2

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
          return actionsModel.getElementAt(rowIndex).let {
            when (columnIndex) {
              0 -> it.name
              1 -> it.hint
              else -> throw IllegalArgumentException("Invalid column index")
            }
          }
        }
      }

      setDefaultRenderer(
        Object::class.java,
        object : DefaultTableCellRenderer() {
          override fun getTableCellRendererComponent(
            table: JTable,
            value: Any?,
            isSelected: Boolean,
            hasFocus: Boolean,
            row: Int,
            column: Int
          ): Component {
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column).apply {
              val topBorder = if (row == 0) 4 else 0
              val bottomBorder = if (row == actionsModel.size() - 1) 4 else 0
              val leftBorder = if (column == 0) 4 else 16
              val rightBorder = if (column == 1) 4 else 0

              border = BorderFactory.createEmptyBorder(
                topBorder,
                leftBorder,
                bottomBorder,
                rightBorder
              )

              background = when {
                isSelected -> table.selectionBackground
                else -> quickChatBackground
              }

              foreground = JBColor.DARK_GRAY
              text = value as String

              if (column == 0) {
                horizontalAlignment = LEFT
              } else {
                font = JBFont.small().asItalic()
                foreground = JBColor.GRAY
                horizontalAlignment = RIGHT
              }
            }
          }
        }
      )

      addMouseListener(object : MouseAdapter() {
        override fun mouseClicked(e: MouseEvent) {
          val row = rowAtPoint(e.point)

          if (row != -1) {
            val actionElement = actionsModel.getElementAt(row)
            chatBox.setText(actionElement.name)
          }
        }
      })

      resizeColumnWidth()
    }

    private fun resizeColumnWidth() {
      for (column in 0 until columnCount) {
        var maxWidth = 50 // minimum width

        for (row in 0 until rowCount) {
          val renderer = getCellRenderer(row, column)
          val comp = prepareRenderer(renderer, row, column)
          maxWidth = max(comp.preferredSize.width + 1, maxWidth)
        }

        columnModel.getColumn(column).preferredWidth = maxWidth
      }
    }
  }

  inner class DocumentChangeListener : DocumentAdapter() {
    override fun textChanged(e: DocumentEvent) {
      val newInput = e.document.getText(0, e.document.length)

      actionsModel.clear()
      availableActions
        .filter { it.name.startsWith(newInput, ignoreCase = true) && it.name != newInput && newInput.isNotBlank() }
        .forEach { actionsModel.addElement(it) }

      if (actionsModel.isEmpty) {
        isVisible = false
        e.document.putProperty(CAN_SUBMIT, true)
      } else {
        isVisible = true
        actionsTable.visibleRowCount = actionsModel.size

        if (actionsTable.selectedRow >= actionsModel.size) {
          actionsTable.setRowSelectionInterval(actionsModel.size - 1, actionsModel.size - 1)
        } else {
          actionsTable.setRowSelectionInterval(0, 0)
        }

        e.document.putProperty(CAN_SUBMIT, false)
      }

      // Needed for the menu height to resize properly
      revalidate()
    }
  }

  inner class KeyPressedListener : KeyAdapter() {
    override fun keyPressed(e: KeyEvent) {
      when (e.keyCode) {
        KeyEvent.VK_ENTER -> {
          val textField = e.component as? JTextField
            ?: return

          if (actionsTable.selectedRow !in 0..<actionsModel.size) return

          val selectedAction = actionsModel.get(actionsTable.selectedRow)
          textField.text = selectedAction.name
        }

        KeyEvent.VK_DOWN, KeyEvent.VK_UP -> {
          if (actionsModel.isEmpty) return

          val currentRow = actionsTable.selectedRow
          val rowCount = actionsModel.size()

          val newRow = when (e.keyCode) {
            KeyEvent.VK_DOWN -> (currentRow + 1) % rowCount
            KeyEvent.VK_UP -> (currentRow - 1 + rowCount) % rowCount
            else -> currentRow
          }

          actionsTable.setRowSelectionInterval(newRow, newRow)
          e.consume()
        }
      }
    }
  }
}

private data class ActionElement(val name: String, val hint: String)
