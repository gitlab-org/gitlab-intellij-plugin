package com.gitlab.plugin.chat.quickchat.ui.listeners

import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUIMessageHandler
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent

class QuickChatExitKeyListener(
  private val messageHandler: QuickChatUIMessageHandler
) : KeyAdapter() {
  override fun keyPressed(e: KeyEvent) {
    if (e.keyCode == KeyEvent.VK_ESCAPE) {
      messageHandler.receive(QuickChatUIMessage.Close)
    }
  }
}
