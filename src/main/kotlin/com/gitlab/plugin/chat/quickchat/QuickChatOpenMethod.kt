package com.gitlab.plugin.chat.quickchat

enum class QuickChatOpenMethod {
  SHORTCUT,
  CLICK_BUTTON
}
