package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.quickchat.markdown.SyntaxElement
import com.gitlab.plugin.chat.quickchat.markdown.SyntaxElementsExtractor
import com.gitlab.plugin.chat.quickchat.markdown.markdownParser
import com.gitlab.plugin.chat.quickchat.ui.*
import com.gitlab.plugin.chat.quickchat.ui.listeners.QuickChatExitKeyListener
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.diagnostic.logger
import com.intellij.ui.JBColor
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.util.*
import javax.swing.BorderFactory
import javax.swing.JPanel
import javax.swing.JTextPane
import javax.swing.UIManager
import javax.swing.text.DefaultCaret
import javax.swing.text.SimpleAttributeSet
import javax.swing.text.StyleConstants

private typealias ChunkId = Int

@Suppress("MagicNumber")
class QuickChatDuoResponse(
  private val messageHandler: QuickChatUIMessageHandler
) : JPanel(GridBagLayout()) {
  private val logger = logger<QuickChatDuoResponse>()
  private val markdownResponseRenderer = MarkdownResponseRenderer()

  var currentRenderTextPane: JTextPane?
  private val gridConstraints: GridBagConstraints

  private val chunks = TreeMap<ChunkId, String>()

  init {
    border = BorderFactory.createEmptyBorder()
    background = quickChatBackground

    gridConstraints = GridBagConstraints().growsHorizontally().apply {
      gridwidth = QUICK_CHAT_WIDTH
    }

    val textPane = ResponseTextPane(messageHandler).apply {
      text = "finding an answer ..."
    }

    add(textPane, gridConstraints)
    addKeyListener(QuickChatExitKeyListener(messageHandler))

    currentRenderTextPane = textPane
  }

  fun chunkReceived(record: ChatRecord) {
    val chunkId = record.chunkId
    val content = record.content.orEmpty()

    when {
      chunkId != null -> handleChunkReceived(chunkId, content)
      else -> handleFinalChunkReceived(content)
    }
  }

  private fun handleChunkReceived(chunkId: ChunkId, content: String) {
    chunks[chunkId] = content
    currentRenderTextPane?.text = chunks.values.joinToString("")
  }

  private fun handleFinalChunkReceived(content: String) {
    chunks.clear()
    logger.debug("Received final chunk of duo response. content=$content")

    val document = markdownParser.parse(content)

    val syntaxElements: MutableList<SyntaxElement> = mutableListOf()
    document.accept(SyntaxElementsExtractor(content, syntaxElements))

    logger.debug("Syntax elements extracted from content of final chunk. syntaxElements=$syntaxElements")
    markdownResponseRenderer.render(syntaxElements)
  }

  private inner class MarkdownResponseRenderer {
    fun render(syntaxElements: List<SyntaxElement>) = runInEdt {
      currentRenderTextPane?.text = ""

      syntaxElements.forEach { syntaxElement ->
        when (syntaxElement) {
          is SyntaxElement.Text -> getRenderTextPane().document.insertString(
            getRenderTextPane().document.length,
            syntaxElement.text,
            SimpleAttributeSet()
          )
          is SyntaxElement.Code -> getRenderTextPane().document.insertString(
            getRenderTextPane().document.length,
            syntaxElement.code,
            SimpleAttributeSet().apply {
              StyleConstants.setItalic(this, true)
              StyleConstants.setForeground(this, JBColor.BLUE)
              StyleConstants.setBackground(this, JBColor.background())
            }
          )
          is SyntaxElement.FencedCodeBlock -> {
            add(QuickChatCodeBlock(syntaxElement, messageHandler), gridConstraints)
            currentRenderTextPane = null
          }
        }
      }
    }

    private fun getRenderTextPane(): JTextPane {
      var textPane = currentRenderTextPane
      if (textPane != null) {
        return textPane
      }

      textPane = ResponseTextPane(messageHandler).also { add(it, gridConstraints) }

      currentRenderTextPane = textPane
      return textPane
    }
  }
}

private class ResponseTextPane(messageHandler: QuickChatUIMessageHandler) : JTextPane() {
  init {
    background = quickChatBackground

    font = UIManager.getFont("Label.font")
    foreground = quickChatPrimaryColor

    isEditable = false

    val defaultCaret = caret as DefaultCaret
    defaultCaret.updatePolicy = DefaultCaret.NEVER_UPDATE

    addKeyListener(QuickChatExitKeyListener(messageHandler))
  }
}
