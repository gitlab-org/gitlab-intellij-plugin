package com.gitlab.plugin.chat.quickchat.ui

import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.quickchat.ui.components.*
import com.gitlab.plugin.chat.quickchat.ui.listeners.QuickChatExitKeyListener
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.util.launchUrl
import com.intellij.ui.JBColor
import com.intellij.util.ui.UIUtil
import java.awt.BorderLayout
import java.awt.Cursor
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.*

@Suppress("MagicNumber")
class QuickChatUI : JPanel() {
  private val messageHandler = QuickChatUIMessageHandler()

  private var chatBox: QuickChatBox
  private var actions: QuickChatActions

  init {
    layout = BoxLayout(this, BoxLayout.Y_AXIS)

    border = BorderFactory.createEmptyBorder(15, 5, 15, 5)
    background = quickChatBackground

    putClientProperty(UIUtil.HIDE_EDITOR_FROM_DATA_CONTEXT_PROPERTY, true)

    fun add(component: JComponent) {
      component
        .apply { alignmentX = LEFT_ALIGNMENT }
        .let { super.add(it) }
    }

    add(header())
    add(JSeparator(JSeparator.HORIZONTAL))
    add(QuickChatConversation(messageHandler))
    chatBox = QuickChatBox(messageHandler).also { add(it) }
    actions = QuickChatActions(chatBox).also { add(it) }
    add(QuickChatContext(messageHandler))
    add(footer())

    addKeyListener(QuickChatExitKeyListener(messageHandler))

    // Ensure that quick chat focus can be regained after clicking outside the panel.
    addMouseListener(
      object : MouseAdapter() {
        override fun mouseClicked(e: MouseEvent) {
          requestFocus()
        }
      }
    )
  }

  private fun header() = JPanel(BorderLayout()).apply {
    border = BorderFactory.createEmptyBorder(0, 0, 3, 0)
    background = quickChatBackground

    add(
      JLabel("Duo Quick Chat", SwingConstants.LEFT).apply {
        iconTextGap = 5
        foreground = quickChatPrimaryColor
      },
      BorderLayout.WEST
    )

    add(
      ShortcutLabel("ESCAPE", "to close") { messageHandler.receive(QuickChatUIMessage.Close) },
      BorderLayout.EAST
    )
  }

  private fun footer() = JPanel(BorderLayout()).apply {
    border = BorderFactory.createEmptyBorder(3, 0, 0, 0)
    background = quickChatBackground

    add(
      JLabel("Give feedback", SwingConstants.LEFT).apply {
        font = font.deriveFont(10f)
        foreground = JBColor.BLUE
        cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)

        addMouseListener(object : MouseAdapter() {
          override fun mouseClicked(e: MouseEvent) {
            launchUrl("https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/648")
          }
        })
      },
      BorderLayout.WEST
    )
  }

  override fun requestFocus() {
    chatBox.requestFocus()
  }

  fun dispatch(message: ChatViewMessage) {
    messageHandler.dispatch(message)
  }

  fun onMessageReceived(block: (QuickChatUIMessage) -> Unit) {
    messageHandler.onMessageReceived = block
  }
}

// This is to allow unit tests to be run without creating a complete Swing UI.
object QuickChatUIFactory {
  fun create() = QuickChatUI()
}
