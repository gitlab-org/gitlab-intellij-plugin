package com.gitlab.plugin.chat.quickchat.ui

import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.ui.JBColor
import java.awt.Color
import java.awt.GridBagConstraints

const val QUICK_CHAT_WIDTH = 600
const val QUICK_CHAT_MAX_HEIGHT = 400

val quickChatPrimaryColor: Color = JBColor.MAGENTA
val quickChatBackground = JBColor.lazy {
  EditorColorsManager.getInstance().globalScheme.defaultBackground
}

fun GridBagConstraints.growsHorizontally(): GridBagConstraints = apply {
  gridx = 0
  gridy = GridBagConstraints.RELATIVE
  fill = GridBagConstraints.HORIZONTAL
  weightx = 1.0
  anchor = GridBagConstraints.NORTHWEST
}
