package com.gitlab.plugin.language

import com.intellij.openapi.fileTypes.FileType

data class Language(
  val id: String,
  val name: String,
  val fileExtensions: List<String>,
  val isJetBrainsSupported: Boolean = false
) {

  val displayName: String
    get() = "$name (${id.lowercase()})"

  /*
   * We're migrating the persistence values from displayName to ID to make it easier to cross-check IDs
   * between disabledSupportedLanguages and additionalLanguages. Checking both values below ensures pre-
   * and post-migration values are handled correctly.
   *
   * Once everyone is using version > 3.1.0, remove this and just compare using the ID.
   */
  fun matches(identifier: String): Boolean {
    return identifier.equals(id, ignoreCase = true) || identifier.equals(displayName, ignoreCase = true)
  }

  companion object {
    fun getInstance(fileType: FileType): Language {
      return languageList.find { fileType.defaultExtension in it.fileExtensions }
        ?: Language(fileType.name, fileType.displayName, listOf(fileType.defaultExtension))
    }

    /**
     * This list combines languages from two main sources:
     * 1. GitLab's supported languages for code suggestions:
     *    https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-languages
     * 2. Languages defined in the Language Server Protocol (LSP) specification:
     *    https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocumentItem
     *
     * For languages defined in the LSP specification, we use the identifiers specified there.
     * For other languages not in the LSP spec, we define identifiers arbitrarily,
     * often based on a common file extension for that language.
     */
    val languageList: List<Language> = listOf(
      Language("abap", "ABAP", listOf("abap")),
      Language("bat", "Windows Bat", listOf("bat", "cmd")),
      Language("bibtex", "BibTeX", listOf("bib")),
      Language("c", "C", listOf("c", "h")),
      Language("clojure", "Clojure", listOf("clj", "cljs", "cljc")),
      Language("coffeescript", "CoffeeScript", listOf("coffee")),
      Language("cpp", "C++", listOf("cpp", "cxx", "cc", "h", "hpp"), isJetBrainsSupported = true),
      Language("csharp", "C#", listOf("cs"), isJetBrainsSupported = true),
      Language("css", "CSS", listOf("css")),
      Language("dart", "Dart", listOf("dart")),
      Language("diff", "Diff", listOf("diff", "patch")),
      Language("dockerfile", "Dockerfile", listOf("Dockerfile")),
      Language("elixir", "Elixir", listOf("ex", "exs")),
      Language("erlang", "Erlang", listOf("erl", "hrl")),
      Language("fsharp", "F#", listOf("fs", "fsi", "fsx")),
      Language("git-commit", "Git Commit", listOf("COMMIT_EDITMSG", "MERGE_MSG")),
      Language("git-rebase", "Git Rebase", listOf("git-rebase-todo")),
      Language("go", "Go", listOf("go"), isJetBrainsSupported = true),
      Language("groovy", "Groovy", listOf("groovy", "gvy", "gy", "gsh")),
      Language("haml", "HAML", listOf("haml"), isJetBrainsSupported = true),
      Language("handlebars", "Handlebars", listOf("hbs", "handlebars")),
      Language("html", "HTML", listOf("html", "htm")),
      Language("ini", "Ini", listOf("ini")),
      Language("jade", "Pug", listOf("pug", "jade")),
      Language("java", "Java", listOf("java"), isJetBrainsSupported = true),
      Language("javascript", "JavaScript", listOf("js"), isJetBrainsSupported = true),
      Language("javascriptreact", "JavaScript XML", listOf("jsx")),
      Language("json", "JSON", listOf("json")),
      Language("kotlin", "Kotlin", listOf("kt", "kts"), isJetBrainsSupported = true),
      Language("latex", "LaTeX", listOf("tex")),
      Language("less", "Less", listOf("less")),
      Language("lua", "Lua", listOf("lua")),
      Language("makefile", "Makefile", listOf("Makefile", "makefile")),
      Language("markdown", "Markdown", listOf("md", "markdown")),
      Language("objective-c", "Objective-C", listOf("m")),
      Language("objective-cpp", "Objective-C++", listOf("mm")),
      Language("perl", "Perl", listOf("pl", "pm")),
      Language("perl6", "Perl 6", listOf("p6", "pl6", "pm6")),
      Language("php", "PHP", listOf("php", "phtml", "php3", "php4", "php5", "phps"), isJetBrainsSupported = true),
      Language("powershell", "Powershell", listOf("ps1", "psm1", "psd1")),
      Language("python", "Python", listOf("py", "pyw", "pyx"), isJetBrainsSupported = true),
      Language("r", "R", listOf("r", "R")),
      Language("razor", "Razor", listOf("cshtml")),
      Language("ruby", "Ruby", listOf("rb", "rbw"), isJetBrainsSupported = true),
      Language("rust", "Rust", listOf("rs"), isJetBrainsSupported = true),
      Language("sass", "SASS", listOf("sass")),
      Language("scala", "Scala", listOf("scala", "sc"), isJetBrainsSupported = true),
      Language("scss", "SCSS", listOf("scss")),
      Language("shaderlab", "ShaderLab", listOf("shader")),
      Language("shellscript", "Shell Script", listOf("sh", "bash"), isJetBrainsSupported = true),
      Language("sql", "SQL", listOf("sql"), isJetBrainsSupported = true),
      Language("svelte", "Svelte", listOf("svelte"), isJetBrainsSupported = true),
      Language("swift", "Swift", listOf("swift"), isJetBrainsSupported = true),
      Language("terraform", "Terraform", listOf("tf", "tfvars"), isJetBrainsSupported = true),
      Language("tex", "TeX", listOf("tex")),
      Language("typescript", "TypeScript", listOf("ts"), isJetBrainsSupported = true),
      Language("typescriptreact", "TypeScript JSX", listOf("tsx")),
      Language("vb", "Visual Basic", listOf("vb")),
      Language("vue", "Vue", listOf("vue"), isJetBrainsSupported = true),
      Language("xml", "XML", listOf("xml", "xsl", "xsd")),
      Language("xsl", "XSL", listOf("xsl")),
      Language("yaml", "YAML", listOf("yml", "yaml"))
    )
  }
}
