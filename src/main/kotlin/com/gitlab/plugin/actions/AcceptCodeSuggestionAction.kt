package com.gitlab.plugin.actions

import com.intellij.codeInsight.inline.completion.session.InlineCompletionContext
import com.intellij.openapi.actionSystem.*

class AcceptCodeSuggestionAction : AnAction() {
  override fun actionPerformed(event: AnActionEvent) {
    val action = event.actionManager.getAction(IdeActions.ACTION_INSERT_INLINE_COMPLETION)

    action.actionPerformed(event)
  }

  override fun update(event: AnActionEvent) {
    val editor = event.getData(CommonDataKeys.EDITOR)
    if (editor == null) {
      event.presentation.isVisible = false
      event.presentation.isEnabled = false
      return
    }

    val inlineCompletionVisible = InlineCompletionContext.getOrNull(editor)?.isCurrentlyDisplaying() ?: false
    event.presentation.isVisible = inlineCompletionVisible
    event.presentation.isEnabled = inlineCompletionVisible
  }

  override fun getActionUpdateThread(): ActionUpdateThread {
    return ActionUpdateThread.BGT
  }
}
