package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.lsp.params.CurrentFileContextResponseParams
import com.gitlab.plugin.lsp.params.NewPromptParams
import com.gitlab.plugin.lsp.params.NewPromptPayload
import com.gitlab.plugin.lsp.webview.LanguageServerDuoChatWebViewService
import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger

open class SelectedContextChatActionBase(
  private val content: String,
  private val recordType: ChatRecord.Type,
) : AnAction() {

  private val logger = logger<SelectedContextChatActionBase>()

  override fun getActionUpdateThread() = ActionUpdateThread.BGT

  override fun actionPerformed(event: AnActionEvent) {
    val project = event.project ?: run {
      logger.warn("No project found for event.")
      return
    }

    if (BuildConfig.USE_LS_WEBVIEW) {
      project.service<ChatService>().activateLSWebViewChat()
      val chatRecordContext = project.service<ChatRecordContextService>().getChatRecordContextOrNull()

      project.service<LanguageServerDuoChatWebViewService>().sendNewPromptLSNotification(
        NewPromptParams(
          payload = NewPromptPayload(
            prompt = ChatRecord.Type.getSerialName(recordType),
            fileContext = CurrentFileContextResponseParams(
              chatRecordContext?.currentFile?.selectedText.orEmpty(),
              chatRecordContext?.currentFile?.fileName.orEmpty(),
              chatRecordContext?.currentFile?.contentAboveCursor.orEmpty(),
              chatRecordContext?.currentFile?.contentBelowCursor.orEmpty()
            )
          )
        )
      )
    } else {
      project.service<ChatService>().activateChatWithPrompt(
        NewUserPromptRequest(
          content = content,
          type = recordType,
          context = project.service<ChatRecordContextService>().getChatRecordContextOrNull() ?: return
        )
      )
    }
  }

  override fun update(event: AnActionEvent) {
    event.presentation.isVisible = event.project?.service<DuoChatStateService>()?.duoChatEnabled == true

    val caret = event.getData(CommonDataKeys.CARET)
    val hasSelectedText = caret?.hasSelection() ?: false

    event.presentation.isEnabled = hasSelectedText
  }
}
