package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.chat.model.ChatRecord

/**
 * Action will refactor currently selected code with GitLab Duo Chat
 */
class RefactorCodeChatAction : SelectedContextChatActionBase(
  REFACTOR_CODE_CHAT_CONTENT,
  ChatRecord.Type.REFACTOR_CODE
) {
  companion object {
    const val REFACTOR_CODE_CHAT_CONTENT = "/refactor"
  }
}
