package com.gitlab.plugin.actions.console

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.execution.impl.ConsoleViewImpl
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.LangDataKeys
import com.intellij.openapi.components.service
import com.intellij.openapi.project.DumbAwareAction
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ExplainConsoleWindowAction(private val dispatcher: CoroutineDispatcher = Dispatchers.Default) :
  DumbAwareAction(
    GitLabBundle.messagePointer("action.terminal.explain-code"),
    GitLabBundle.messagePointer("action.terminal.explain-code.description"),
    com.gitlab.plugin.ui.GitLabIcons.Actions.DuoChatEnabled
  ) {
  override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT

  override fun update(e: AnActionEvent) {
    e.presentation.isEnabledAndVisible = getConsoleSelectedText(e).isNotBlank()
  }

  override fun actionPerformed(e: AnActionEvent) {
    val consoleSelectedText = getConsoleSelectedText(e)
    val chatService = e.project?.service<ChatService>()

    CoroutineScope(dispatcher).launch {
      chatService?.processNewUserPrompt(
        NewUserPromptRequest(
          "/explain",
          ChatRecord.Type.EXPLAIN_CODE,
          context = ChatRecordContext(
            ChatRecordFileContext(
              fileName = "",
              selectedText = consoleSelectedText
            )
          )
        )
      )
    }
  }

  private fun getConsoleSelectedText(e: AnActionEvent): String {
    val consoleView = e.getData(LangDataKeys.CONSOLE_VIEW) as? ConsoleViewImpl ?: return ""
    val editor = consoleView.editor ?: return ""
    val selectionModel = editor.selectionModel
    return selectionModel.selectedText.orEmpty()
  }
}
