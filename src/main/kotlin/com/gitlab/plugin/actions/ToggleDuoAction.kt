package com.gitlab.plugin.actions

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.ToggleAction
import com.intellij.openapi.components.service
import com.intellij.util.application

class ToggleDuoAction : ToggleAction() {
  override fun isSelected(e: AnActionEvent): Boolean {
    return application.service<DuoPersistentSettings>().codeSuggestionsEnabled
  }

  override fun setSelected(e: AnActionEvent, state: Boolean) {
    application.service<DuoPersistentSettings>().codeSuggestionsEnabled = state
  }

  override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT
}
