package com.gitlab.plugin.actions.status

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.editor.getLanguage
import com.gitlab.plugin.editor.isCodeSuggestionsEnabledForLanguage
import com.gitlab.plugin.lsp.listeners.LanguageServerSettingsChangedListener
import com.gitlab.plugin.lsp.settings.CodeCompletion
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project

class ToggleCodeSuggestionsPerLanguage : AnAction() {

  override fun actionPerformed(event: AnActionEvent) {
    val codeCompletionState = event.project.getCodeCompletionState() ?: return

    val language = event.getData(CommonDataKeys.EDITOR)?.getLanguage() ?: return
    val id = language.id

    if (language.isJetBrainsSupported) {
      if (codeCompletionState.disabledSupportedLanguages.contains(id)) {
        codeCompletionState.enableSupportedLanguage(id)
      } else {
        codeCompletionState.disableSupportedLanguage(id)
      }
    } else {
      if (codeCompletionState.additionalLanguages.contains(id)) {
        codeCompletionState.removeAdditionalLanguage(id)
      } else {
        codeCompletionState.addAdditionalLanguage(id)
      }
    }

    // This persists the above changes to disk instead of waiting for an IDE shutdown. This ensures the changes are
    // not lost due to unexpected failure e.g. IDE crashes
    event.project?.save()

    event.project?.messageBus
      ?.syncPublisher(LanguageServerSettingsChangedListener.LANGUAGE_SERVER_SETTINGS_CHANGED_TOPIC)
      ?.onLanguageServerSettingsChanged()
  }

  override fun getActionUpdateThread() = ActionUpdateThread.BGT

  override fun update(event: AnActionEvent) {
    if (!DuoPersistentSettings.getInstance().codeSuggestionsEnabled) {
      event.presentation.isEnabledAndVisible = false
      return
    }

    val editor = event.getData(CommonDataKeys.EDITOR)

    val language = editor?.getLanguage() ?: run {
      event.presentation.isEnabledAndVisible = false
      return
    }

    val isEnabled = editor.isCodeSuggestionsEnabledForLanguage() ?: run {
      event.presentation.isEnabledAndVisible = false
      return
    }

    event.presentation.apply {
      isEnabledAndVisible = true
      text =
        "${if (isEnabled) "Disable" else "Enable"} Code Suggestions for ${language.name}"
    }
  }

  private fun Project?.getCodeCompletionState(): CodeCompletion? {
    this ?: return null
    return service<LanguageServerSettings>().state.workspaceSettings.codeCompletion
  }
}
