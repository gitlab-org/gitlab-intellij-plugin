package com.gitlab.plugin.actions.terminal

import com.intellij.openapi.Disposable
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.openapi.wm.ex.ToolWindowManagerListener
import com.jediterm.terminal.ui.TerminalAction
import com.jediterm.terminal.ui.TerminalActionPresentation
import com.jediterm.terminal.ui.TerminalActionProvider
import org.jetbrains.plugins.terminal.ShellTerminalWidget
import org.jetbrains.plugins.terminal.TerminalToolWindowManager
import java.awt.event.InputEvent
import java.awt.event.KeyEvent
import javax.swing.KeyStroke

class TerminalOpenListener(private val project: Project) : ToolWindowManagerListener {
  private val disposable = Disposer.newDisposable()

  override fun toolWindowsRegistered(ids: MutableList<String>, toolWindowManager: ToolWindowManager) {
    if (ids.contains("Terminal")) {
      if (project.service<TerminalActionsService>().areTerminalActionsEnabled()) {
        addDuoActionToTerminalContextMenu(project, disposable)
      }
    }

    super.toolWindowsRegistered(ids, toolWindowManager)
  }

  private fun addDuoActionToTerminalContextMenu(project: Project, parentDisposable: Disposable) {
    TerminalToolWindowManager.getInstance(project).addNewTerminalSetupHandler({
      val widget = ShellTerminalWidget.asShellJediTermWidget(it)
      if (widget != null) {
        widget.nextProvider = object : TerminalActionProvider {
          override fun getActions(): List<TerminalAction> {
            val presentation =
              TerminalActionPresentation(
                "Explain with Duo",
                KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.META_DOWN_MASK)
              )
            return listOf(ExplainDuoTerminalContextMenuAction(presentation, widget.project))
          }

          override fun getNextProvider(): TerminalActionProvider? = null

          override fun setNextProvider(provider: TerminalActionProvider?) {
            nextProvider = widget.nextProvider
          }
        }
      }
    }, parentDisposable)
  }

  override fun toolWindowShown(toolWindow: ToolWindow) {
    super.toolWindowShown(toolWindow)
    addDuoActionToTerminalContextMenu(project, disposable)
  }
}
