package com.gitlab.plugin.graphql.scalars

import com.apollographql.apollo3.api.Adapter
import com.apollographql.apollo3.api.CustomScalarAdapters
import com.apollographql.apollo3.api.json.JsonReader
import com.apollographql.apollo3.api.json.JsonWriter

class GitLabScalarAdapter<T : GitLabScalar>(
  private val constructor: (String) -> T
) : Adapter<T> {
  /**
   * Deserializes a JSON string into a value object.
   */
  override fun fromJson(
    reader: JsonReader,
    customScalarAdapters: CustomScalarAdapters
  ): T = constructor(reader.nextString()!!)

  /**
   * Serializes a value object into a JSON string.
   */
  override fun toJson(
    writer: JsonWriter,
    customScalarAdapters: CustomScalarAdapters,
    value: T
  ) {
    writer.value(value.value)
  }
}
