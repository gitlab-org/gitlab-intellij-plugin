package com.gitlab.plugin.exceptions

import com.gitlab.plugin.BuildConfig

// BuildConfig cannot be mocked directly in tests
fun isSentryTrackingEnabled(): Boolean {
  return BuildConfig.SENTRY_TRACKING_ENABLED
}
