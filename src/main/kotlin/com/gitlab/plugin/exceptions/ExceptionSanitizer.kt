package com.gitlab.plugin.exceptions

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.openapi.components.service
import com.intellij.util.application

class ExceptionSanitizer {
  private val duoPersistentSettings by lazy { application.service<DuoPersistentSettings>() }

  /**
   * Naive implementation of basic sanitization for exception messages.
   *
   * - Sanitizes the configured gitlab url.
   * - Sanitizes local file paths (unix and windows).
   * - Sanitizes personal access tokens.
   * - Sanitizes ip addresses.
   */
  fun sanitize(exception: Throwable): SanitizedException {
    var message = exception.message

    if (duoPersistentSettings.url.isNotEmpty()) {
      val httpUrl = duoPersistentSettings.url
      val wsUrl = httpUrl.replace("https://", "wss://").replace("http://", "ws://")

      message = message
        ?.replace(Regex("\\b($httpUrl(/\\S*)?)"), "gitlab_url")
        ?.replace(Regex("\\b($wsUrl(/\\S*)?)"), "websocket_gitlab_url")
    }

    message = message
      ?.replace(Regex("([0-9]{1,3}\\.){3}[0-9]{1,3}(:[0-9]*)?"), "ip_address")
      ?.replace(Regex("glpat-[0-9a-zA-Z\\-_]*"), "gitlab_personal_access_token")
      ?.replace(
        Regex(
          """(?<=\s|\()((/[^/\s]+(?:/[^/\s]*)*)|((?:[a-zA-Z]:)?[\\/][^\\/\s]+(?:[\\/][^\\/\s]*)*))"""
        ),
        "local_file_path"
      )

    return SanitizedException(message, exception)
  }
}

class SanitizedException(
  override val message: String?,
  exception: Throwable
) : Throwable(message) {
  init {
    stackTrace = exception.stackTrace
  }
}
