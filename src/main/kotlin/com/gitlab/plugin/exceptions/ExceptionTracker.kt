package com.gitlab.plugin.exceptions

import com.gitlab.plugin.api.duo.ExceptionHandler
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application

@Service(Service.Level.APP)
class ExceptionTracker {
  private val logger = logger<ExceptionHandler>()
  private val sentryTracker: SentryTracker by lazy { application.service<SentryTracker>() }

  fun handle(exception: Exception) {
    logger.warn(exception)

    sentryTracker.trackException(exception)
  }
}
