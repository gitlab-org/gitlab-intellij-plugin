package com.gitlab.plugin.editor

import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.event.EditorFactoryEvent
import com.intellij.serviceContainer.AlreadyDisposedException
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.assertDoesNotThrow

class GitLabEditorFactoryListenerTest : DescribeSpec({
  val editor = mockk<Editor>()
  val editorRegistryService = mockk<EditorRegistryService>(relaxed = true)
  val event = mockk<EditorFactoryEvent>(relaxed = true)

  val gitLabEditorFactoryListener = GitLabEditorFactoryListener()

  beforeEach {
    every { event.editor } returns editor
    every { editor.project?.service<EditorRegistryService>() } returns editorRegistryService
  }

  afterEach {
    clearAllMocks()
  }

  it("should register an editor when it's created") {
    gitLabEditorFactoryListener.editorCreated(event)
    verify { editorRegistryService.registerEditor(editor) }
  }

  it("should unregister an editor when it's released") {
    gitLabEditorFactoryListener.editorReleased(event)
    verify { editorRegistryService.unregisterEditor(editor) }
  }

  it("should not unregister an editor when it's released and the project is disposed") {
    every { editor.project?.service<EditorRegistryService>() } throws AlreadyDisposedException("Container is disposed")

    assertDoesNotThrow { gitLabEditorFactoryListener.editorReleased(event) }

    verify(exactly = 0) { editorRegistryService.unregisterEditor(any()) }
  }
})
