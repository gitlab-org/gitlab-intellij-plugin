package com.gitlab.plugin.editor

import com.gitlab.plugin.chat.listeners.SelectionChangedDuoChatListener
import com.gitlab.plugin.codesuggestions.gutter.GutterIconInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.listeners.StreamingInlineCompletionEventListener.Companion.STREAMING_INLINE_COMPLETION_TOPIC
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener.Companion.SWITCH_INLINE_COMPLETION_TOPIC
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryService
import com.intellij.codeInsight.inline.completion.InlineCompletion
import com.intellij.codeInsight.inline.completion.InlineCompletionEventListener
import com.intellij.codeInsight.inline.completion.InlineCompletionHandler
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.EditorFactory
import com.intellij.openapi.project.Project
import com.intellij.util.messages.MessageBusConnection
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.types.shouldBeSameInstanceAs
import io.mockk.*

class EditorRegistryServiceTest : DescribeSpec({
  val editor = mockk<Editor>(relaxed = true)
  val project = mockk<Project>()
  val telemetryService = mockk<TelemetryService>(relaxed = true)
  val editorRegistryService = EditorRegistryService(project)
  val messageBusConnection = mockk<MessageBusConnection>(relaxed = true)
  val inlineCompletionHandler = mockk<InlineCompletionHandler>(relaxed = true)

  beforeEach {
    every { editor.project?.messageBus?.connect() } returns messageBusConnection
    every { editor.getUserData(EDITOR_LISTENERS_KEY) } returns null

    mockkObject(InlineCompletion)
    every { InlineCompletion.getHandlerOrNull(any()) } returns inlineCompletionHandler
    every { project.service<TelemetryService>() } returns telemetryService
  }

  it("should register editor to listeners and topics") {
    editorRegistryService.registerEditor(editor)

    val switchGutterIconListener = slot<GutterIconInlineCompletionEventListener>()
    val streamingGutterIconListener = slot<GutterIconInlineCompletionEventListener>()
    verify {
      inlineCompletionHandler.addEventListener(any<InlineCompletionEventListener>())
      inlineCompletionHandler.addEventListener(any<GutterIconInlineCompletionEventListener>())

      messageBusConnection.subscribe(SWITCH_INLINE_COMPLETION_TOPIC, capture(switchGutterIconListener))
      messageBusConnection.subscribe(STREAMING_INLINE_COMPLETION_TOPIC, capture(streamingGutterIconListener))

      editor.selectionModel.addSelectionListener(any<SelectionChangedDuoChatListener>())
      editor.putUserData(EDITOR_LISTENERS_KEY, any())
    }

    switchGutterIconListener.captured shouldBeSameInstanceAs streamingGutterIconListener.captured
  }

  it("should unregister editor to listeners and topics") {
    val gutterIconListener = mockk<GutterIconInlineCompletionEventListener>(relaxUnitFun = true)
    val telemetryListener = mockk<TelemetryInlineCompletionEventListener>()
    val selectionChangedListener = mockk<SelectionChangedDuoChatListener>()

    every { editor.getUserData(EDITOR_LISTENERS_KEY) } returns EditorListeners(
      gutterIconListener,
      telemetryListener,
      selectionChangedListener
    )

    editorRegistryService.unregisterEditor(editor)

    verify {
      inlineCompletionHandler.removeEventListener(telemetryListener)
      inlineCompletionHandler.removeEventListener(gutterIconListener)
      editor.selectionModel.removeSelectionListener(selectionChangedListener)

      gutterIconListener.cleanup()
      editor.putUserData(EDITOR_LISTENERS_KEY, null)
    }
  }

  it("should register all editors") {
    val editors = arrayOf(
      mockk<Editor>(relaxed = true) { every { getUserData(EDITOR_LISTENERS_KEY) } returns null },
      mockk<Editor>(relaxed = true) { every { getUserData(EDITOR_LISTENERS_KEY) } returns null },
      mockk<Editor>(relaxed = true) { every { getUserData(EDITOR_LISTENERS_KEY) } returns null }
    )

    mockkStatic(EditorFactory::class)
    every { EditorFactory.getInstance().allEditors } returns editors

    editorRegistryService.registerAllEditors()

    editors.forEach { editor ->
      verify { editor.putUserData(EDITOR_LISTENERS_KEY, any()) }
    }
  }
})
