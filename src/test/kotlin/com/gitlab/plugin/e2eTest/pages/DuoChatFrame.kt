package com.gitlab.plugin.e2eTest.pages

import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.data.RemoteComponent
import com.intellij.remoterobot.fixtures.*
import com.intellij.remoterobot.search.locators.byXpath
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.keyboard
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import java.awt.event.KeyEvent
import java.time.Duration

fun RemoteRobot.duoChatFrame(function: DuoChatFrame.() -> Unit) {
  find<DuoChatFrame>(timeout = Duration.ofSeconds(120)).apply(function)
}

private const val DUO_CHAT_MESSAGES_XPATH =
  "//*[@id='chat-component']//*[contains(@class, 'duo-chat-message') and not(contains(@class, 'duo-chat-message-feedback'))]"
private const val DUO_CHAT_INPUT_XPATH = "//*[@id='chat-component']//*[contains(@class, 'duo-chat-input')]"
private const val FILE_DROPDOWN_XPATH = "//*[contains(@data-testid, 'context-item-menu')]"
private const val FILE_DROPDOWN_CLOSE_BUTTON = "//*[contains(@data-testid, 'context-item-menu-close-button')]"
private const val FILE_SEARCH_ITEM_XPATH = "//*[contains(@class, 'context-item-result')]"
private const val SELECTED_FILE_XPATH = "//*[@data-testid='chat-prompt-form']//*[contains(@id, 'context-item-file')]"
private const val PINNED_FILES = "//*[@data-testid='chat-prompt-form']//*[contains(@data-testid, 'chat-context-tokens-wrapper')]"

@FixtureName("Duo Chat Frame")
@DefaultXpath("type", "//div[@class='JBCefOsrComponent']")
class DuoChatFrame(remoteRobot: RemoteRobot, remoteComponent: RemoteComponent) :
  CommonContainerFixture(remoteRobot, remoteComponent) {

  fun sendChat(request: String) {
    step("Send Duo Chat request") {
      print("Sending Duo Chat request: \"$request\"")

      waitFor(errorMessage = "No Duo Chat input found.") {
        duoChatBrowser()!!.exist(DUO_CHAT_INPUT_XPATH)
      }

      clickChatInput()

      keyboard { enterText(request) }
      keyboard { key(KeyEvent.VK_ENTER) }
    }
  }

  fun getLastChatMessage(): String {
    val chatMessages = duoChatBrowser()!!.findElements(DUO_CHAT_MESSAGES_XPATH)

    return if (chatMessages.isNotEmpty()) chatMessages.last().html else ""
  }

  fun getPinnedContextSearchResults(): List<DomElement> =
    duoChatBrowser()!!.findElements(FILE_SEARCH_ITEM_XPATH)

  fun getPinnedContextFiles(): List<DomElement> =
    duoChatBrowser()!!.findElements(SELECTED_FILE_XPATH)

  fun clickChatInput() {
    duoChatBrowser()!!.findElement(DUO_CHAT_INPUT_XPATH)
      .clickWithoutScroll()
  }

  fun clearChatInput() {
    clickChatInput()
    keyboard { selectAll() }
    keyboard { key(KeyEvent.VK_BACK_SPACE) }
  }

  fun waitForFileContextMenu() = step("Wait for file context menu to appear") {
    waitFor(Duration.ofSeconds(60), errorMessage = "File dropdown did not appear") {
      duoChatBrowser()!!.exist(FILE_DROPDOWN_XPATH)
    }
  }

  fun selectFile(fileName: String) = step("Add file $fileName to pinned context") {
    duoChatBrowser()!!
      .findElement(
        // Take the first 3 characters because the filename may be split across 2 elements.
        "//*[contains(text(), '${fileName.take(3)}')]"
      )
      .clickWithoutScroll()
  }

  fun removePinnedFile(fileName: String) = step("Remove file $fileName from pinned context") {
    duoChatBrowser()!!
      .findElement("$PINNED_FILES//button[@aria-label='Remove']")
      .clickWithoutScroll()
  }

  fun closeFileContextMenu() = step("Closes the file context menu") {
    duoChatBrowser()!!
      .findElement(FILE_DROPDOWN_CLOSE_BUTTON)
      .clickWithoutScroll()
  }

  // For some reason the regular DomElement::click method is throwing an error when DomElement::scroll is called in it.
  // This method does pretty much the same thing without the scroll.
  private fun DomElement.clickWithoutScroll() {
    val clickY = elementData.location.y + (elementData.location.height / 2)
    val clickX = elementData.location.x + (elementData.location.width / 2)

    duoChatBrowser()!!.runJs(
      """
        robot.click(component, new Point($clickX, $clickY))
        """
    )
  }

  private fun duoChatBrowser(): JCefBrowserFixture? {
    waitForIgnoringError(Duration.ofSeconds(60), errorMessage = "No Duo Chat browser found.") {
      browsers().isNotEmpty() && browsers().any { it.getDom().contains("GitLab Duo Chat") }
    }

    return browsers().find { it.getDom().contains("GitLab Duo Chat") }
  }

  private fun browsers(): List<JCefBrowserFixture> {
    return remoteRobot.findAll<JCefBrowserFixture>(byXpath("//div[@class='JBCefOsrComponent']"))
  }
}
