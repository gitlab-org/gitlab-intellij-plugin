package com.gitlab.plugin.e2eTest.pages

import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.data.RemoteComponent
import com.intellij.remoterobot.fixtures.*
import com.intellij.remoterobot.search.locators.byXpath
import com.intellij.remoterobot.steps.CommonSteps
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.WaitForConditionTimeoutException
import com.intellij.remoterobot.utils.keyboard
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import java.time.Duration
import java.util.concurrent.Executors

fun RemoteRobot.idea(function: IdeaFrame.() -> Unit) {
  find<IdeaFrame>(timeout = Duration.ofSeconds(120)).apply(function)
}

private const val DO_NOT_SAVE_PASSWORD_LABEL_TEXT = "Do not save, forget passwords after restart"

@FixtureName("Idea frame")
@DefaultXpath("IdeFrameImpl type", "//div[@class='IdeFrameImpl']")
class IdeaFrame(remoteRobot: RemoteRobot, remoteComponent: RemoteComponent) :
  CommonContainerFixture(remoteRobot, remoteComponent) {

  val projectViewTree
    get() = find<ContainerFixture>(
      byXpath("ProjectViewTree", "//div[@class='ProjectViewTree']"),
      Duration.ofSeconds(20)
    )

  val projectName
    get() = step("Get project name") { return@step callJs<String>("component.getProject().getName()") }

  val menuBar: JMenuBarFixture
    get() = step("Menu...") {
      return@step remoteRobot.find(JMenuBarFixture::class.java, JMenuBarFixture.byType(), Duration.ofSeconds(20))
    }

  // Dumb mode is when IntelliJ is updating its indexes
  fun isDumbMode(): Boolean {
    return callJs(
      """
            const frameHelper = com.intellij.openapi.wm.impl.ProjectFrameHelper.getFrameHelper(component)
            if (frameHelper) {
                const project = frameHelper.getProject()
                project ? com.intellij.openapi.project.DumbService.isDumb(project) : true
            } else {
                true
            }
        """,
      true
    )
  }

  fun isDuoEnabled(): Boolean {
    return find<ComponentFixture>(
      byXpath("//div[@icon='gitlab-code-suggestions-enabled.svg']"),
      Duration.ofSeconds(20)
    ).isShowing
  }

  fun isDuoDisabled(): Boolean {
    return find<ComponentFixture>(
      byXpath("//div[@icon='gitlab-code-suggestions-disabled.svg']"),
      Duration.ofSeconds(20)
    ).isShowing
  }

  fun waitForDuoSuggestionLoading() {
    val executor = Executors.newFixedThreadPool(2)

    executor.submit {
      waitFor(interval = Duration.ofMillis(10), errorMessage = "Duo suggestion loading gutter icon did not display") {
        val gutter = TextEditorFixture(remoteRobot, remoteComponent).gutter
        gutter.getIcons().any { it.description.contains("/icons/gitlab-code-suggestions-loading.svg") }
      }
    }.get()

    executor.shutdown()
  }

  fun waitForDuoSuggestionLoaded() {
    waitFor(Duration.ofSeconds(20), errorMessage = "Duo suggestion loading icon still visible after 20 seconds") {
      findAll<ComponentFixture>(byXpath("//div[@icon='gitlab-code-suggestions-loading.svg']")).size == 0
    }
  }

  fun waitForDuoSuggestionReady() {
    waitFor(duration = Duration.ofSeconds(20), errorMessage = "Duo suggestion is ready gutter icon did not display") {
      val gutter = TextEditorFixture(remoteRobot, remoteComponent).gutter
      gutter.getIcons().any { it.description.contains("/icons/gitlab-code-suggestions-enabled.svg") }
    }
  }

  fun waitForDuoSuggestionAccepted() {
    waitFor(errorMessage = "Duo suggestion indicator should have disappeared after suggestion is accepted") {
      val gutter = TextEditorFixture(remoteRobot, remoteComponent).gutter

      gutter.getIcons().none {
        it.description.contains("/icons/gitlab-code-suggestions-loading.svg") ||
          it.description.contains("/icons/gitlab-code-suggestions-enabled.svg")
      }
    }
  }

  fun openDuoSettings() {
    step("Open GitLab Duo settings") {
      openSettings()
      keyboard { enterText("Preferences | Tools | GitLab Duo") }

      // Wait until GitLab Duo settings are displayed
      waitForIgnoringError { jLabel("GitLab Personal Access Token").isVisible() }
    }
  }

  fun turnOffKeychainSettings() {
    step("Open Password settings") {
      openSettings()
      keyboard { enterText("native Keychain") }

      // Wait until keychain settings are displayed
      waitForIgnoringError { radioButton(DO_NOT_SAVE_PASSWORD_LABEL_TEXT).isShowing }

      if (!radioButton(DO_NOT_SAVE_PASSWORD_LABEL_TEXT).isSelected()) {
        radioButton(DO_NOT_SAVE_PASSWORD_LABEL_TEXT).select()
        button("Apply").clickWhenEnabled()
      }

      button("OK").clickWhenEnabled()
    }
  }

  fun openDuoChat() {
    step("Open GitLab Duo Chat") {
      button(byXpath("//div[@text='GitLab Duo Chat']")).clickWhenEnabled()

      waitFor(duration = Duration.ofSeconds(30)) {
        jLabel(
          JLabelFixture.byText("GitLab Duo Chat"),
          timeout = Duration.ofSeconds(30)
        ).isShowing
      }
    }
  }

  private fun openSettings() {
    step("Open settings") {
      waitForIgnoringError(Duration.ofSeconds(30)) {
        menuBar.select("Help", "Find Action...")
        find<ComponentFixture>(byXpath("//div[@class='SearchEverywhereUI']")).isShowing
      }
      keyboard { enterText("Settings") }

      // Wait for menu search to complete and Settings... to be present
      waitFor(duration = Duration.ofSeconds(10)) {
        find<ComponentFixture>(byXpath("//div[@class='JBList']")).hasText("Settings...")
      }

      keyboard { enter() }

      // The settings pane does not load if we enter text too quickly
      CommonSteps(remoteRobot).wait(5)
    }
  }

  @Suppress("SwallowedException")
  fun findNotification(text: String): JLabelFixture? {
    return try {
      remoteRobot.find<JLabelFixture>(
        byXpath(
          "//div[@class='balloon']//div[contains(@class, 'notification')]//div[contains(text(), '$text')]"
        )
      )
    } catch (e: WaitForConditionTimeoutException) {
      null
    }
  }
}
