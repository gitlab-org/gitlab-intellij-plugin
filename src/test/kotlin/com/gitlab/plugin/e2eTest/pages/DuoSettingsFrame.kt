package com.gitlab.plugin.e2eTest.pages

import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.data.RemoteComponent
import com.intellij.remoterobot.fixtures.*
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitForIgnoringError
import java.time.Duration

fun RemoteRobot.duoSettings(function: DuoSettingsFrame.() -> Unit) {
  find<DuoSettingsFrame>(timeout = Duration.ofSeconds(120)).apply(function)
}

private const val GITLAB_INSTANCE_LABEL_TEXT = "URL to GitLab instance"
private const val ACCESS_TOKEN_LABEL_TEXT = "GitLab Personal Access Token"
private const val ENABLE_TELEMETRY_LABEL_TEXT = "Enable telemetry"
private const val LANGUAGE_SERVER_SECTION_LABEL = "GitLab Language Server"
private const val LANGUAGE_SERVER_LOG_LEVEL = "Log Level"

@FixtureName("Duo Settings Frame")
@DefaultXpath("type", "//div[@class='MyDialog']")
class DuoSettingsFrame(remoteRobot: RemoteRobot, remoteComponent: RemoteComponent) :
  CommonContainerFixture(remoteRobot, remoteComponent) {

  fun authenticate(instance: String, token: String) {
    step("Authenticate plugin") {
      print("Authenticating plugin against $instance")

      // Wait until GitLab Duo settings are displayed
      waitForIgnoringError { jLabel(ACCESS_TOKEN_LABEL_TEXT).isVisible() }

      textField(GITLAB_INSTANCE_LABEL_TEXT).text = instance

      // directly set token value so token is not logged
      textField(ACCESS_TOKEN_LABEL_TEXT).runJs("component.setText('$token')")

      // disable telemetry in E2E tests
      disableTelemetry()

      button("Verify setup").clickWhenEnabled()
    }
  }

  fun setLanguageServerLogLevelToDebug() {
    waitForIgnoringError { jLabel(LANGUAGE_SERVER_SECTION_LABEL).isShowing }

    val sectionLabel = jLabel(LANGUAGE_SERVER_SECTION_LABEL)
    sectionLabel.click()

    textField(LANGUAGE_SERVER_LOG_LEVEL).text = "debug"
  }

  private fun disableTelemetry() {
    if (checkBox(ENABLE_TELEMETRY_LABEL_TEXT).isSelected()) checkBox(ENABLE_TELEMETRY_LABEL_TEXT).click()
  }
}
