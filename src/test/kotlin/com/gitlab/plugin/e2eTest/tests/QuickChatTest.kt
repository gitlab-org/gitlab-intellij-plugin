package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.e2eTest.pages.IdeaFrame
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.fixtures.ComponentFixture
import com.intellij.remoterobot.fixtures.JLabelFixture
import com.intellij.remoterobot.search.locators.byXpath
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.keyboard
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.awt.Toolkit
import java.awt.datatransfer.DataFlavor
import java.awt.event.KeyEvent
import java.time.Duration
import kotlin.test.assertTrue

@Suppress("Classname")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class QuickChatTest {
  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) {
    waitForIgnoringError(Duration.ofMinutes(3)) { remoteRobot.callJs("true") }

    ProjectSteps(remoteRobot).createNewProject()

    remoteRobot.idea {
      step("Ensure GitLab Duo is enabled") {
        assert(isDuoEnabled())
      }
    }

    ProjectSteps(remoteRobot).createNewFile("Java Class", "Prime.java")
  }

  @AfterEach
  fun closeChat(remoteRobot: RemoteRobot) = with(remoteRobot) {
    idea {
      closeQuickChat()
    }
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  // Temporarily disabled due to https://gitlab.com/gitlab-org/gitlab/-/issues/494140
  @Test
  @Video
  @Disabled
  fun `it should be able to send a message within the quick chat`(remoteRobot: RemoteRobot) = with(remoteRobot) {
    idea {
      openQuickChat()
      prompt("Hi")

      waitFor(duration = Duration.ofMinutes(1), errorMessage = "Response did not display") {
        find<ComponentFixture>(
          locator = byXpath("//div[contains(@visible_text, 'GitLab Duo Chat')]"),
          timeout = Duration.ofMinutes(1)
        ).isShowing
      }
    }
  }

  @Test
  @Video
  fun `it should be able to insert a code snippet from the quick chat`(remoteRobot: RemoteRobot) = with(remoteRobot) {
    idea {
      val textEditor = textEditor().editor

      openQuickChat()
      prompt("Write a Java static method called isPrime")
      waitForCompleteResponse("static boolean isPrime")

      step(text = "Click on insert code snippet button") {
        findAll<ComponentFixture>(
          locator = byXpath("//div[@defaulticon='insert.svg']"),
        ).first().click()
      }

      assertTrue(
        textEditor.text.contains("static boolean isPrime"),
        message = "Code snippet was not inserted"
      )
    }
  }

  // Temporarily disabled due to https://gitlab.com/gitlab-org/gitlab/-/issues/494140
  @Test
  @Video
  @Disabled
  fun `it should be able to copy a code snippet from the quick chat`(remoteRobot: RemoteRobot) = with(remoteRobot) {
    idea {
      openQuickChat()
      prompt("Write a Java static method called printPrimes")
      waitForCompleteResponse("void printPrimes")

      step(text = "Click on copy code snippet button") {
        findAll<ComponentFixture>(
          locator = byXpath("//div[@defaulticon='copy-to-clipboard.svg']"),
        ).first().click()
      }

      val clipboardContent = Toolkit.getDefaultToolkit().systemClipboard.getData(DataFlavor.stringFlavor).toString()
      assertTrue(
        clipboardContent.contains("static boolean isPrime"),
        message = "Code snippet was not copied to clipboard"
      )
    }
  }

  private fun IdeaFrame.openQuickChat() {
    keyboard {
      hotKey(KeyEvent.VK_ALT, KeyEvent.VK_C)
    }

    waitFor(duration = Duration.ofSeconds(1), errorMessage = "Quick Chat did not open") {
      find<JLabelFixture>(byXpath("//div[@text='Duo Quick Chat']")).isShowing
    }

    // reset the chat after opening to reduce randomness in responses
    keyboard {
      enterText("/reset")
      hotKey(KeyEvent.VK_ENTER)
    }

    waitFor(errorMessage = "Could not reset quick chat after opening it") {
      find<JLabelFixture>(byXpath("//div[@text='/reset']"), Duration.ofSeconds(5)).isShowing
    }
  }

  private fun IdeaFrame.closeQuickChat() {
    keyboard {
      escape()
    }

    waitFor(duration = Duration.ofSeconds(1), errorMessage = "Quick Chat did not close") {
      val xpath = byXpath("//div[@text='Duo Quick Chat']")
      findAll<JLabelFixture>(xpath).isEmpty()
    }
  }

  private fun IdeaFrame.prompt(message: String) {
    keyboard {
      enterText(message, delayBetweenCharsInMs = 10)
      hotKey(KeyEvent.VK_ENTER)
    }

    waitFor(
      duration = Duration.ofSeconds(10),
      interval = Duration.ofMillis(100),
      errorMessage = "Loading message did not display"
    ) {
      find<ComponentFixture>(
        locator = byXpath("//div[@visible_text='finding an answer ...']"),
        timeout = Duration.ofSeconds(10)
      ).isShowing
    }
  }

  private fun IdeaFrame.waitForCompleteResponse(expected: String) {
    waitFor(duration = Duration.ofSeconds(30), errorMessage = "Response did not display") {
      find<ComponentFixture>(
        locator = byXpath("//div[contains(@visible_text, '$expected')]"),
        timeout = Duration.ofMinutes(1)
      ).isShowing
    }

    waitFor(duration = Duration.ofMinutes(1), errorMessage = "Response did not fully display") {
      findAll<ComponentFixture>(
        locator = byXpath("//div[@defaulticon='copy-to-clipboard.svg']"),
      ).isNotEmpty()
    }
  }
}
