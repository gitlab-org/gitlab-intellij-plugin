package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.time.Duration.ofMinutes

@Suppress("ClassName")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PluginDefaultsTest {

  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) {
    waitForIgnoringError(ofMinutes(3)) { remoteRobot.callJs("true") }
    ProjectSteps(remoteRobot).createNewProject()
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  @Nested
  inner class `with no token set` {

    @Test
    @Video
    fun `has the correct default settings`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        step("Check the status icon") {
          idea { assert(isDuoDisabled()) }
        }

        openDuoSettings()

        step("Check plugin default settings") {
          assert(jLabel("URL to GitLab instance").isVisible())
          assert(textField("URL to GitLab instance").text.equals("https://gitlab.com"))
          assert(jLabel("GitLab Personal Access Token").isVisible())
          assert(button("Generate token on GitLab...").isEnabled())
          assert(button("Verify setup").isEnabled())
          assert(checkBox("Enable telemetry").isSelected())
        }

        button("Cancel").click()
      }
    }
  }
}
