package com.gitlab.plugin.actions

import com.gitlab.plugin.actions.chat.SelectedContextChatActionBase
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Caret
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class SelectedContextChatActionBaseTest : DescribeSpec({
  describe("SelectedContextChatActionBase") {
    beforeEach {
      mockDuoContextServicePersistentSettings()
      every { DuoPersistentSettings.getInstance().duoChatEnabled } returns true
    }

    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    describe("update") {
      val mockCaret = mockk<Caret>(relaxed = true)
      val mockActionEvent = mockk<AnActionEvent>(relaxed = true)
      val mockPresentation = mockk<Presentation>(relaxUnitFun = true)

      beforeEach {
        every { mockActionEvent.getData(CommonDataKeys.CARET) } returns mockCaret
        every { mockActionEvent.presentation } returns mockPresentation
        every { mockActionEvent.project?.service<DuoChatStateService>() } returns mockk()
      }

      describe("with selection") {
        beforeEach {
          every { mockCaret.hasSelection() } returns true
          every { mockActionEvent.project?.service<DuoChatStateService>()?.duoChatEnabled } returns true
        }

        it("sets enabled to true") {
          val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)
          action.update(mockActionEvent)

          verify(exactly = 1) { mockPresentation.setEnabled(true) }
        }
      }

      describe("with no selection") {
        beforeEach {
          every { mockCaret.hasSelection() } returns false
          every { mockActionEvent.project?.service<DuoChatStateService>()?.duoChatEnabled } returns true
        }

        it("sets enabled to false") {
          val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)
          action.update(mockActionEvent)

          verify(exactly = 1) { mockPresentation.setEnabled(false) }
        }
      }

      it("sets visible to true if duo features are enabled") {
        every { mockActionEvent.project?.service<DuoChatStateService>()?.duoChatEnabled } returns true
        val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)

        action.update(mockActionEvent)

        verify { mockPresentation.setVisible(true) }
      }

      it("sets visible to false if duo features are disabled") {
        every { mockActionEvent.project?.service<DuoChatStateService>()?.duoChatEnabled } returns false
        val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)

        action.update(mockActionEvent)

        verify { mockPresentation.setVisible(false) }
      }
    }

    describe("actionPerformed") {
      val mockProject = mockk<Project>()
      val mockActionEvent = mockk<AnActionEvent>()
      val mockChatService = mockk<ChatService>()
      val mockChatRecordContextService = mockk<ChatRecordContextService>()
      val mockChatRecordContext = mockk<ChatRecordContext>()

      beforeEach {
        every { mockActionEvent.project } returns mockProject
        every { mockProject.service<ChatService>() } returns mockChatService
        every { mockProject.service<ChatRecordContextService>() } returns mockChatRecordContextService
        every { mockChatService.activateChatWithPrompt(any()) } just Runs
      }

      it("activates chat with the correct NewUserPromptRequest when context is available") {
        val content = "Test content"
        val type = ChatRecord.Type.GENERAL
        val action = SelectedContextChatActionBase(content, type)

        every { mockChatRecordContextService.getChatRecordContextOrNull() } returns mockChatRecordContext

        action.actionPerformed(mockActionEvent)

        verify {
          mockChatService.activateChatWithPrompt(
            match<NewUserPromptRequest> {
              it.content == content &&
                it.type == type &&
                it.context == mockChatRecordContext
            }
          )
        }
      }

      it("does not activate chat when context is null") {
        val content = "Test content"
        val type = ChatRecord.Type.GENERAL
        val action = SelectedContextChatActionBase(content, type)

        every { mockChatRecordContextService.getChatRecordContextOrNull() } returns null

        action.actionPerformed(mockActionEvent)

        verify(exactly = 0) { mockChatService.activateChatWithPrompt(any()) }
      }

      it("handles null project gracefully") {
        val content = "Test content"
        val type = ChatRecord.Type.GENERAL
        val action = SelectedContextChatActionBase(content, type)

        every { mockActionEvent.project } returns null

        action.actionPerformed(mockActionEvent)

        verify(exactly = 0) { mockChatService.activateChatWithPrompt(any()) }
      }
    }
  }
})
