package com.gitlab.plugin.actions.terminal

import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.jediterm.terminal.ui.TerminalActionPresentation
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import java.awt.event.KeyEvent
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ExplainDuoTerminalContextMenuActionTest : DescribeSpec({
  val mockProject = mockk<Project>()
  val mockPresentation = mockk<TerminalActionPresentation>()
  val mockTerminalActionsService = mockk<TerminalActionsService>()
  val mockKeyEvent = mockk<KeyEvent>()

  beforeEach {
    every { mockProject.service<TerminalActionsService>() } returns mockTerminalActionsService
  }

  afterEach {
    clearAllMocks()
    unmockkAll()
  }

  describe("isEnabled") {
    it("returns true when text is selected in terminal") {
      every { mockTerminalActionsService.areTerminalActionsEnabled() } returns true
      every { mockTerminalActionsService.isTextSelectedInTerminal() } returns true
      val action = ExplainDuoTerminalContextMenuAction(mockPresentation, mockProject)

      val result = action.isEnabled(mockKeyEvent)

      assertTrue(result)
      verify(exactly = 1) { mockTerminalActionsService.isTextSelectedInTerminal() }
    }

    it("returns false when no text is selected in terminal") {
      every { mockTerminalActionsService.areTerminalActionsEnabled() } returns false
      val action = ExplainDuoTerminalContextMenuAction(mockPresentation, mockProject)

      val result = action.isEnabled(mockKeyEvent)

      assertFalse(result)
      verify(exactly = 0) { mockTerminalActionsService.isTextSelectedInTerminal() }
    }
  }

  describe("actionPerformed") {
    it("triggers Duo explain and returns the result") {
      every { mockTerminalActionsService.triggerDuoExplain() } returns true
      val action = ExplainDuoTerminalContextMenuAction(mockPresentation, mockProject)

      val result = action.actionPerformed(mockKeyEvent)

      assertTrue(result)
      verify(exactly = 1) { mockTerminalActionsService.triggerDuoExplain() }
    }

    it("returns false when Duo explain fails") {
      every { mockTerminalActionsService.triggerDuoExplain() } returns false
      val action = ExplainDuoTerminalContextMenuAction(mockPresentation, mockProject)

      val result = action.actionPerformed(mockKeyEvent)

      assertFalse(result)
      verify(exactly = 1) { mockTerminalActionsService.triggerDuoExplain() }
    }
  }
})
