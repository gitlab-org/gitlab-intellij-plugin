package com.gitlab.plugin.actions.terminal

import com.gitlab.plugin.chat.mockDuoChatToolWindowManager
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.terminal.JBTerminalWidget
import com.intellij.util.ui.UIUtil
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlin.test.assertTrue

@OptIn(ExperimentalCoroutinesApi::class)
class TerminalActionsServiceTest : DescribeSpec({
  val duoChatToolWindow: ToolWindow = mockk<ToolWindow>()

  describe("TerminalActionsService") {
    val mockProject = mockk<Project>()
    val mockToolWindowManager = mockk<ToolWindowManager>()
    val mockTerminalToolWindow = mockk<ToolWindow>()
    val mockTerminalWidget = mockk<JBTerminalWidget>(relaxed = true)
    val mockChatService = mockk<ChatService>()
    val mockDuoChatToolWindow = mockk<ToolWindow>()
    val testScope = CoroutineScope(UnconfinedTestDispatcher())
    val mockActionEvent = mockk<AnActionEvent>(relaxed = true)

    beforeEach {
      mockkStatic(UIUtil::class)

      every { mockProject.service<ToolWindowManager>() } returns mockToolWindowManager
      mockDuoChatToolWindowManager(duoChatToolWindow, mockActionEvent.project)
      every { mockToolWindowManager.getToolWindow("Terminal") } returns mockTerminalToolWindow
      every { mockTerminalToolWindow.component } returns mockTerminalWidget
      every { mockProject.service<ChatService>() } returns mockChatService
    }

    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    describe("isTextSelectedInTerminal") {
      it("returns true when text is selected") {
        every { mockTerminalWidget.selectedText } returns "Selected Text"

        val service = TerminalActionsService(mockProject, testScope)
        val result = service.isTextSelectedInTerminal()

        assert(result)
      }

      it("returns false when no text is selected") {
        every { mockTerminalWidget.selectedText } returns ""

        val service = TerminalActionsService(mockProject, testScope)
        val result = service.isTextSelectedInTerminal()

        assert(!result)
      }
    }

    describe("triggerDuoExplain") {
      beforeEach {
        every { mockToolWindowManager.getToolWindow("GitLab Duo Chat") } returns mockDuoChatToolWindow
        every { mockDuoChatToolWindow.isVisible } returns true
        every { mockDuoChatToolWindow.activate(null) } just Runs
        coEvery { mockChatService.processNewUserPrompt(any()) } just Runs
      }

      it("returns true and processes new user prompt when text is selected") {
        every { mockTerminalWidget.selectedText } returns "Selected Text"

        val service = TerminalActionsService(mockProject, testScope)
        val result = service.triggerDuoExplain()

        assertTrue(result)
        coVerify(exactly = 1) {
          mockChatService.processNewUserPrompt(
            match {
              it.content == "/explain" &&
                it.type == ChatRecord.Type.EXPLAIN_CODE &&
                it.context?.currentFile?.selectedText == "Selected Text"
            }
          )
        }
        verify(exactly = 1) { mockDuoChatToolWindow.activate(null) }
      }

      it("returns false when no text is selected") {
        every { mockTerminalWidget.selectedText } returns ""

        val service = TerminalActionsService(mockProject, testScope)
        val result = service.triggerDuoExplain()

        assert(!result)
        coVerify(exactly = 0) { mockChatService.processNewUserPrompt(any()) }
        verify(exactly = 0) { mockDuoChatToolWindow.activate(any()) }
      }
    }
  }
})
