package com.gitlab.plugin.actions.terminal

import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ExplainTerminalToolWindowActionTest : DescribeSpec({
  describe("ExplainTerminalToolWindowAction") {
    val mockTerminalActionsService = mockk<TerminalActionsService>()
    val mockEvent = mockk<AnActionEvent>()
    val mockProject = mockk<Project>()
    val actionPresentation = Presentation()

    beforeEach {
      every { mockProject.service<TerminalActionsService>() } returns mockTerminalActionsService
      every { mockEvent.project } returns mockProject
      every { mockEvent.presentation } returns actionPresentation
    }

    afterEach {
      clearAllMocks()
      unmockkAll()
    }

    describe("update") {
      it("enables presentation when text is selected") {
        every { mockTerminalActionsService.areTerminalActionsEnabled() } returns true
        every { mockTerminalActionsService.isTextSelectedInTerminal() } returns true

        val action = ExplainTerminalToolWindowAction()
        action.update(mockEvent)

        assertTrue(actionPresentation.isEnabled)
      }

      it("disables presentation when no text is selected") {
        every { mockTerminalActionsService.areTerminalActionsEnabled() } returns false

        val action = ExplainTerminalToolWindowAction()
        action.update(mockEvent)

        assertFalse(actionPresentation.isEnabled)
      }
    }

    describe("actionPerformed") {
      it("triggers Duo explain when project is available") {
        every { mockTerminalActionsService.triggerDuoExplain() } returns true

        val action = ExplainTerminalToolWindowAction()
        action.actionPerformed(mockEvent)

        verify(exactly = 1) { mockTerminalActionsService.triggerDuoExplain() }
      }

      it("does not trigger Duo explain when project is null") {
        every { mockEvent.project } returns null

        val action = ExplainTerminalToolWindowAction()
        action.actionPerformed(mockEvent)

        verify(exactly = 0) { mockTerminalActionsService.triggerDuoExplain() }
      }
    }
  }
})
