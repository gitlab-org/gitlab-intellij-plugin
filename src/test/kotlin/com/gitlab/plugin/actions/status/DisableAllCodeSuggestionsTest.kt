package com.gitlab.plugin.actions.status

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.mockk.*

class DisableAllCodeSuggestionsTest : DescribeSpec({
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)
  val project = mockk<Project>()

  lateinit var event: AnActionEvent
  lateinit var subject: DisableAllCodeSuggestions

  beforeSpec {
    mockApplication()
  }

  beforeEach {
    stubRunReadAction()
    stubRunWriteAction()

    event = mockAnActionEvent()
    subject = DisableAllCodeSuggestions()

    every { event.project } returns project
    every { project.service<GitLabLanguageServerService>() } returns languageServerService
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  context("with code suggestions disabled") {
    beforeEach {
      stubCodeSuggestionsEnabled(initial = false)
    }

    it("actionPerformed does not disable code suggestions") {
      subject.actionPerformed(event)

      verify(exactly = 0) {
        languageServerService.lspServer?.workspaceService?.didChangeConfiguration(any())
      }
    }

    it("update hides action when code suggestions are disabled") {
      subject.update(event)

      event.presentation.isVisible.shouldBeFalse()
    }
  }

  context("with code suggestions enabled") {
    beforeEach {
      stubCodeSuggestionsEnabled(initial = true)
    }

    it("actionPerformed disables code suggestions") {
      subject.actionPerformed(event)

      verify(exactly = 1) {
        languageServerService.lspServer?.workspaceService?.didChangeConfiguration(
          match { (it.settings as Settings).codeCompletion?.enabled == false }
        )
      }
    }

    it("update displays action when code suggestions are enabled") {
      subject.update(event)

      event.presentation.isVisible.shouldBeTrue()
    }
  }
})
