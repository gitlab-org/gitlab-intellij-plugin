package com.gitlab.plugin.actions.status

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.services.CodeSuggestionsStateService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.shouldBe
import io.mockk.*

class CodeSuggestionsStatusTest : DescribeSpec({
  val project = mockk<Project>()
  val codeSuggestionsStateService = mockk<CodeSuggestionsStateService>(relaxUnitFun = true)

  lateinit var status: CodeSuggestionsStatus
  lateinit var event: AnActionEvent

  beforeSpec {
    mockApplication()
    mockkObject(DuoPersistentSettings)
  }

  beforeEach {
    stubRunReadAction()
    event = mockAnActionEvent()
    status = CodeSuggestionsStatus()

    every { event.project } returns project
    every { project.service<CodeSuggestionsStateService>() } returns codeSuggestionsStateService
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("update") {
    describe("with code suggestions disabled in settings") {
      beforeEach {
        every { codeSuggestionsStateService.getEngagedCheck() } returns FeatureStateCheckParams("check-id")
        every { codeSuggestionsStateService.codeSuggestionsEnabled } returns false
      }

      it("displays as disabled state to user") {
        status.update(event)

        event.presentation.icon.shouldBe(GitLabIcons.Actions.CodeSuggestionsDisabled)
        event.presentation.isEnabled.shouldBeFalse()
        event.presentation.text.shouldBeEqual("Code Suggestions: Disabled (check-id)")
      }
    }

    describe("with code suggestions enabled in settings") {
      beforeEach {
        every { codeSuggestionsStateService.getEngagedCheck() } returns null
        every { codeSuggestionsStateService.codeSuggestionsEnabled } returns true
      }

      it("displays as enabled state to user") {
        status.update(event)

        event.presentation.icon.shouldBe(GitLabIcons.Actions.CodeSuggestionsEnabled)
        event.presentation.isEnabled.shouldBeFalse()
        event.presentation.text.shouldBeEqual("Code Suggestions: Enabled")
      }
    }
  }
})
