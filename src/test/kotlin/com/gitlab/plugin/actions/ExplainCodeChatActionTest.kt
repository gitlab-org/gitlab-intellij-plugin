package com.gitlab.plugin.actions

import com.gitlab.plugin.actions.chat.ExplainCodeChatAction
import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.core.test.testCoroutineScheduler
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalStdlibApi::class, ExperimentalCoroutinesApi::class)
class ExplainCodeChatActionTest : DescribeSpec({
  describe("ExplainCodeChatAction") {
    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    val mockActionEvent = mockk<AnActionEvent>(relaxed = true)
    val mockChatService = mockk<ChatService>(relaxed = true)
    val mockEditor = mockk<Editor>(relaxed = true)
    val chatBrowser = mockk<CefChatBrowser>(relaxed = true)

    beforeEach {
      every { mockActionEvent.getData(CommonDataKeys.EDITOR) } returns mockEditor
      every { mockActionEvent.project?.service<ChatService>() } returns mockChatService
      every { mockActionEvent.project?.service<ChatRecordContextService>() } returns mockk(relaxed = true)
      every { mockEditor.selectionModel.hasSelection() } returns true
      every { mockActionEvent.project?.service<CefChatBrowser>() } returns chatBrowser
      every { chatBrowser.browserComponent } returns mockk()
    }

    describe("actionPerformed") {
      it("calls processNewUserPrompt with the correct content and record type").config(coroutineTestScope = true) {
        val action = ExplainCodeChatAction()
        action.actionPerformed(mockActionEvent)
        testCoroutineScheduler.advanceUntilIdle()

        coVerify(exactly = 1) { mockChatService.activateChatWithPrompt(any()) }
        coVerify {
          mockChatService.activateChatWithPrompt(
            match {
              it.content == ExplainCodeChatAction.EXPLAIN_CODE_CHAT_CONTENT && it.type == ChatRecord.Type.EXPLAIN_CODE
            }
          )
        }
      }
    }
  }
})
