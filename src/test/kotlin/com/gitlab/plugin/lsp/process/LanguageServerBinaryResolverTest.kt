package com.gitlab.plugin.lsp.process

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class LanguageServerBinaryResolverTest : DescribeSpec({
  val defaultOSValue = System.getProperty("os.name")
  val defaultArchValue = System.getProperty("os.arch")

  afterSpec {
    System.setProperty("os.name", defaultOSValue)
    System.setProperty("os.arch", defaultArchValue)
  }

  listOf(
    TestSystemData(os = "Windows 10") to "gitlab-lsp-win-x64.exe",
    TestSystemData(os = "Linux") to "gitlab-lsp-linux-x64",
    TestSystemData(os = "Unix") to "gitlab-lsp-linux-x64",
    TestSystemData(os = "Aix") to "gitlab-lsp-linux-x64",
    TestSystemData(os = "macOS", arch = "aarch64") to "gitlab-lsp-macos-arm64",
    TestSystemData(os = "macOS") to "gitlab-lsp-macos-x64",
    TestSystemData(os = "Mac OS x") to "gitlab-lsp-macos-x64", // darwin
  ).forEach { (data, binary) ->
    it("should resolve $binary for $data") {
      System.setProperty("os.name", data.os)
      System.setProperty("os.arch", data.arch.orEmpty())

      LanguageServerBinaryUtil().resolveBinaryName() shouldBe binary
    }
  }

  it("should error when receiving non-supported os and arch combination") {
    System.setProperty("os.name", "what")

    shouldThrow<IllegalStateException> { LanguageServerBinaryUtil().resolveBinaryName() }
  }
})

private data class TestSystemData(val os: String, val arch: String? = null)
