package com.gitlab.plugin.lsp.codesuggestions

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.telemetry.TelemetryService
import com.gitlab.plugin.lsp.params.InlineCompletionParams
import com.gitlab.plugin.lsp.params.InlineCompletionTriggerKind
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.code.CodeFormatter
import com.gitlab.plugin.util.code.CodeFormattingContext
import com.google.gson.JsonPrimitive
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.eclipse.lsp4j.Command
import org.eclipse.lsp4j.CompletionItem
import org.eclipse.lsp4j.CompletionList
import org.eclipse.lsp4j.jsonrpc.messages.Either
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors

class LanguageServerCompletionStrategyTest : DescribeSpec({
  mockkConstructor(CodeFormatter::class)

  val context = mockk<SuggestionContext>()
  val formattingContext = mockk<CodeFormattingContext>()

  val project = mockk<Project>()
  val coroutineScope = CoroutineScope(Executors.newSingleThreadExecutor().asCoroutineDispatcher())

  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)

  val strategy = LanguageServerCompletionStrategy(project, coroutineScope)

  beforeEach {
    every { project.getService(GitLabLanguageServerService::class.java) } returns languageServerService
    every { project.getService(TelemetryService::class.java) } returns mockk(relaxed = true)

    every { context.toFormattingContext() } returns formattingContext

    coEvery {
      anyConstructed<CodeFormatter>().format(any<String>(), formattingContext)
    } answers { call -> call.invocation.args[0] as String }

    every { context.isInvoked } returns false
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should cancel previous code suggestions request if a new request is made") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
    }
    val firstRequest = CompletableFuture<Either<List<CompletionItem>, CompletionList>>()
    val secondRequest = CompletableFuture.completedFuture(
      Either.forLeft<List<CompletionItem>, CompletionList>(emptyList())
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returnsMany listOf(firstRequest, secondRequest)

    val job1 = launch { strategy.generateCompletions(context) }
    delay(100)
    val job2 = launch { strategy.generateCompletions(context) }
    job2.join()

    firstRequest.isCancelled shouldBe true
    job1.cancel()
  }

  it("return non-streaming code suggestions received from the language server") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
    }
    val completionItems = listOf(
      CompletionItem().apply {
        insertText = "Hello, World!"
        command = Command().apply {
          command = "gitlab.ls.codeSuggestionAccepted"
          arguments = listOf(JsonPrimitive("unique-tracking-id-1"))
        }
      },
      CompletionItem().apply {
        insertText = "Goodbye, World!"
        command = Command().apply {
          command = "gitlab.ls.codeSuggestionAccepted"
          arguments = listOf(JsonPrimitive("unique-tracking-id-2"))
        }
      },
    )
    val lsResponse = CompletableFuture.completedFuture(
      Either.forLeft<List<CompletionItem>, CompletionList>(completionItems)
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returns lsResponse

    val result = strategy.generateCompletions(context)

    val request = slot<InlineCompletionParams>()
    verify { languageServerService.lspServer?.inlineCompletion(capture(request)) }
    request.captured.position.line shouldBe 10
    request.captured.position.character shouldBe 20
    request.captured.textDocument.uri shouldBe "file///Users/plugins/completion.kt"
    request.captured.context.triggerKind shouldBe InlineCompletionTriggerKind.AUTOMATIC.ordinal

    coVerify { anyConstructed<CodeFormatter>().format("Hello, World!", formattingContext) }
    result?.choices?.get(0)?.text shouldBe "Hello, World!"
    result?.choices?.get(0)?.uniqueTrackingId shouldBe "unique-tracking-id-1"
    result?.choices?.get(0)?.streamId shouldBe null
    result?.choices?.get(0)?.isStreaming shouldBe false

    coVerify { anyConstructed<CodeFormatter>().format("Goodbye, World!", formattingContext) }
    result?.choices?.get(1)?.text shouldBe "Goodbye, World!"
    result?.choices?.get(1)?.uniqueTrackingId shouldBe "unique-tracking-id-2"
    result?.choices?.get(1)?.streamId shouldBe null
    result?.choices?.get(1)?.isStreaming shouldBe false
  }

  it("ignores duplicated code suggestions that are duplicated after formatting") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
    }
    val completionItems = listOf(
      CompletionItem().apply {
        insertText = "Hello, World!"
        command = Command().apply {
          command = "gitlab.ls.codeSuggestionAccepted"
          arguments = listOf(JsonPrimitive("unique-tracking-id-1"))
        }
      },
      CompletionItem().apply {
        insertText = "Hello, World!"
        command = Command().apply {
          command = "gitlab.ls.codeSuggestionAccepted"
          arguments = listOf(JsonPrimitive("unique-tracking-id-2"))
        }
      },
    )
    val lsResponse = CompletableFuture.completedFuture(
      Either.forLeft<List<CompletionItem>, CompletionList>(completionItems)
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returns lsResponse

    val result = strategy.generateCompletions(context)

    val request = slot<InlineCompletionParams>()
    verify { languageServerService.lspServer?.inlineCompletion(capture(request)) }
    request.captured.position.line shouldBe 10
    request.captured.position.character shouldBe 20
    request.captured.textDocument.uri shouldBe "file///Users/plugins/completion.kt"
    request.captured.context.triggerKind shouldBe InlineCompletionTriggerKind.AUTOMATIC.ordinal

    result?.choices?.get(0)?.text shouldBe "Hello, World!"
    result?.choices?.get(0)?.uniqueTrackingId shouldBe "unique-tracking-id-1"
    result?.choices?.get(0)?.streamId shouldBe null
    result?.choices?.get(0)?.isStreaming shouldBe false
    result?.choices?.size shouldBe 1
  }

  it("return streaming code suggestions received from the language server") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
    }
    val completionItems = listOf(
      CompletionItem().apply {
        insertText = ""
        command = Command().apply {
          command = "gitlab.ls.startStreaming"
          arguments = listOf(
            JsonPrimitive("unique-stream-id-1"),
            JsonPrimitive("unique-tracking-id-1")
          )
        }
      },
    )
    val lsResponse = CompletableFuture.completedFuture(
      Either.forLeft<List<CompletionItem>, CompletionList>(completionItems)
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returns lsResponse

    val result = strategy.generateCompletions(context)

    val request = slot<InlineCompletionParams>()
    verify { languageServerService.lspServer?.inlineCompletion(capture(request)) }
    request.captured.position.line shouldBe 10
    request.captured.position.character shouldBe 20
    request.captured.textDocument.uri shouldBe "file///Users/plugins/completion.kt"
    request.captured.context.triggerKind shouldBe InlineCompletionTriggerKind.AUTOMATIC.ordinal

    result?.choices?.get(0)?.text shouldBe ""
    result?.choices?.get(0)?.uniqueTrackingId shouldBe "unique-tracking-id-1"
    result?.choices?.get(0)?.streamId shouldBe "unique-stream-id-1"
    result?.choices?.get(0)?.isStreaming shouldBe true
  }

  it("return no code suggestions if received none from the language server") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
    }
    val completionItems = emptyList<CompletionItem>()
    val lsResponse = CompletableFuture.completedFuture(
      Either.forLeft<List<CompletionItem>, CompletionList>(completionItems)
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returns lsResponse

    val result = strategy.generateCompletions(context)

    result?.choices?.isEmpty() shouldBe true
  }

  it("should error if the language server responds in error") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
    }
    val lsResponse = CompletableFuture.failedFuture<Either<List<CompletionItem>, CompletionList>>(
      Exception("Request failed")
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returns lsResponse

    val result = strategy.generateCompletions(context)

    result shouldBe null
  }

  it("should request using invoked trigger kind if the code suggestion was manually request") {
    every { context.toRequestPayload() } returns mockk<Completion.Payload>(relaxed = true) {
      every { currentFile.fileUri } returns "file///Users/plugins/completion.kt"
      every { currentFile.cursorLine } returns 10
      every { currentFile.cursorColumn } returns 20
      every { context.isInvoked } returns true
    }
    val completionItems = listOf(
      CompletionItem().apply {
        insertText = ""
        command = Command().apply {
          command = "gitlab.ls.startStreaming"
          arguments = listOf(
            JsonPrimitive("unique-stream-id-1"),
            JsonPrimitive("unique-tracking-id-1")
          )
        }
      },
    )
    val lsResponse = CompletableFuture.completedFuture(
      Either.forLeft<List<CompletionItem>, CompletionList>(completionItems)
    )
    every { languageServerService.lspServer?.inlineCompletion(any()) } returns lsResponse

    strategy.generateCompletions(context)

    val request = slot<InlineCompletionParams>()
    verify { languageServerService.lspServer?.inlineCompletion(capture(request)) }
    request.captured.position.line shouldBe 10
    request.captured.position.character shouldBe 20
    request.captured.textDocument.uri shouldBe "file///Users/plugins/completion.kt"
    request.captured.context.triggerKind shouldBe InlineCompletionTriggerKind.INVOKED.ordinal
  }
})
