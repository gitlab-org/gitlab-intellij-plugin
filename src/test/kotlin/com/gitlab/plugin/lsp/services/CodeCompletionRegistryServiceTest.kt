package com.gitlab.plugin.lsp.services

import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.gutter.GutterIconInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.listeners.StreamingInlineCompletionEventListener.Companion.STREAMING_INLINE_COMPLETION_TOPIC
import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.lsp.params.StreamingCompletionResponse
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.junit.jupiter.api.Assertions.*

class CodeCompletionRegistryServiceTest : DescribeSpec({
  val context = mockk<SuggestionContext>()
  val project = mockk<Project>()

  val registry = CodeCompletionRegistryService(project)
  val listener = mockk<GutterIconInlineCompletionEventListener>(relaxUnitFun = true)
  val element = mockk<UpdatableInlineCompletionGrayElement>(relaxed = true)

  beforeEach {
    every { project.messageBus.syncPublisher(STREAMING_INLINE_COMPLETION_TOPIC) } returns listener
  }

  afterEach {
    clearAllMocks()
  }

  it("should register an element") {
    val id = "test-id"
    val tracker = SuggestionsTracker(context, element, streamId = "1234")

    registry.register(id, tracker)

    registry[id] shouldBe tracker
  }

  it("should complete and remove an element") {
    val id = "complete-id"
    val tracker = SuggestionsTracker(context, element, streamId = "123")
    registry.register(id, tracker)

    val response = StreamingCompletionResponse(id = id, completion = "", done = true)
    registry.complete(response)

    registry[id] shouldBe null
    verify(exactly = 1) { listener.onStreamingInlineCompletionCompletion() }
  }

  it("should cancel an element") {
    val id = "cancel-id"
    val tracker = SuggestionsTracker(context, element, streamId = "123")
    registry.register(id, tracker)

    registry.cancel(id)

    registry[id] shouldBe null
    tracker.isCancelled shouldBe true
    verify(exactly = 1) { element.cancel() }
  }

  it("should not throw when completing non-existent element") {
    val response = StreamingCompletionResponse(id = "non-existent", completion = "", done = true)
    assertDoesNotThrow { registry.complete(response) }
  }

  it("should not throw when cancelling non-existent element") {
    assertDoesNotThrow { registry.cancel("non-existent") }
  }

  it("should cancel registered element") {
    val id = "test-cancel-id"
    val tracker = spyk(SuggestionsTracker(context, element, streamId = id))
    registry.register(id, tracker)

    registry.cancel(tracker)

    registry[id] shouldBe null
    tracker.isCancelled shouldBe true
    verify(exactly = 1) { element.cancel() }
  }

  it("should not cancel element when not registered") {
    val tracker = spyk(SuggestionsTracker(context, element, streamId = "123"))

    registry.cancel(tracker)

    verify(exactly = 0) { tracker.element.cancel() }
  }
})
