package com.gitlab.plugin.lsp.params

import com.gitlab.plugin.codesuggestions.telemetry.Event
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class SettingsDtoMappingTest : DescribeSpec({
  it("should map Telemetry object correctly") {
    val telemetry = com.gitlab.plugin.workspace.Telemetry(
      enabled = true,
      trackingUrl = "https://telemetry.gtilab.com"
    )

    val telemetryDto = telemetry.toDto()

    telemetryDto.enabled shouldBe telemetry.enabled
    telemetryDto.trackingUrl shouldBe telemetry.trackingUrl
    telemetryDto.actions shouldBe Event.Type.entries.map { action -> mapOf("action" to action.action) }
  }

  it("should map CodeCompletion object correctly") {
    val codeCompletion = com.gitlab.plugin.lsp.settings.CodeCompletion().apply {
      this.disabledSupportedLanguages.add("kotlin")
      this.additionalLanguages.add("html")
    }

    val codeCompletionDto = codeCompletion.toDto()

    codeCompletionDto.disabledSupportedLanguages shouldBe codeCompletion.disabledSupportedLanguages
    codeCompletionDto.additionalLanguages shouldBe codeCompletion.additionalLanguages
    codeCompletionDto.enableSecretRedaction shouldBe true
  }

  it("should map HttpAgentOptions object correctly") {
    val httpAgentOptions = com.gitlab.plugin.lsp.settings.HttpAgentOptions().apply {
      this.ca = "test_ca"
      this.cert = "test_cert"
      this.certKey = "test_key"
    }

    val httpAgentOptionsDto = httpAgentOptions.toDto()

    httpAgentOptionsDto.ca shouldBe httpAgentOptions.ca
    httpAgentOptionsDto.cert shouldBe httpAgentOptions.cert
    httpAgentOptionsDto.certKey shouldBe httpAgentOptions.certKey
  }

  it("should map FeatureFlags object correctly") {
    val featureFlags = com.gitlab.plugin.lsp.settings.FeatureFlags().apply {
      this.streamCodeGenerations = true
    }

    val featureFlagsDto = featureFlags.toDto()

    featureFlagsDto.streamCodeGenerations shouldBe true
    featureFlagsDto.codeSuggestionsClientDirectToGateway shouldBe true
  }
})
