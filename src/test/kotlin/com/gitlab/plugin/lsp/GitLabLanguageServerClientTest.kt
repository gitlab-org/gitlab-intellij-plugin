package com.gitlab.plugin.lsp

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.codesnippets.InsertCodeSnippetService
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.chat.view.model.InsertCodeSnippetMessage
import com.gitlab.plugin.git.GitDiffProvider
import com.gitlab.plugin.lsp.capabilities.LanguageServerCapabilityManager
import com.gitlab.plugin.lsp.listeners.CompletionListener
import com.gitlab.plugin.lsp.listeners.CompletionListener.Companion.CODE_COMPLETION_TOPIC
import com.gitlab.plugin.lsp.params.*
import com.gitlab.plugin.lsp.webview.LanguageServerDuoChatWebViewService
import com.gitlab.plugin.services.AuthenticationStateService
import com.gitlab.plugin.services.CodeSuggestionsStateService
import com.gitlab.plugin.services.DuoChatStateService
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationGroupType
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.serialization.json.Json
import org.eclipse.lsp4j.*

@OptIn(ExperimentalCoroutinesApi::class)
class GitLabLanguageServerClientTest : DescribeSpec({
  val project = mockk<Project>()

  val completionListener = mockk<CompletionListener>(relaxUnitFun = true)
  val authenticationStateService = mockk<AuthenticationStateService>(relaxUnitFun = true)
  val codeSuggestionState = mockk<CodeSuggestionsStateService>(relaxUnitFun = true)
  val duoChatStateService = mockk<DuoChatStateService>(relaxUnitFun = true)
  val notificationManager = mockk<GitLabNotificationManager>(relaxUnitFun = true)
  val capabilityManager = mockk<LanguageServerCapabilityManager>(relaxUnitFun = true)
  val gitDiffProvider = mockk<GitDiffProvider>()
  val coroutineScope = TestScope(UnconfinedTestDispatcher())
  val chatRecordContextService = mockk<ChatRecordContextService>()
  val insertCodeSnippetService = mockk<InsertCodeSnippetService>(relaxUnitFun = true)
  val languageServerDuoChatWebViewService = mockk<LanguageServerDuoChatWebViewService>(relaxUnitFun = true)

  val client = GitLabLanguageServerClient(project, coroutineScope, notificationManager, capabilityManager)

  beforeSpec {
    mockApplication()
  }

  beforeEach {
    every { project.service<GitDiffProvider>() } returns gitDiffProvider
    every { project.service<CodeSuggestionsStateService>() } returns codeSuggestionState
    every { project.service<DuoChatStateService>() } returns duoChatStateService
    every { project.service<AuthenticationStateService>() } returns authenticationStateService
    every { project.service<ChatRecordContextService>() } returns chatRecordContextService
    every { project.messageBus.syncPublisher(CODE_COMPLETION_TOPIC) } returns completionListener
    every { project.service<InsertCodeSnippetService>() } returns insertCodeSnippetService
    every { project.service<LanguageServerDuoChatWebViewService>() } returns languageServerDuoChatWebViewService
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("showMessage") {
    it("should not display anything when receiving a null message") {
      client.showMessage(null)

      verify(exactly = 0) {
        notificationManager.sendNotification(any(), any(), any())
      }
    }

    it("should display an error message") {
      val errorMessage = "This is an error message."
      val errorMessageParams = MessageParams(MessageType.Error, errorMessage)

      client.showMessage(errorMessageParams)

      val notification = slot<Notification>()
      verify {
        notificationManager.sendNotification(
          capture(notification),
          NotificationGroupType.IMPORTANT,
          NotificationType.ERROR
        )
      }
      notification.captured.message shouldBe errorMessage
    }

    it("should display a warning message") {
      val warningMessage = "This is a warning message."
      val warningMessageParams = MessageParams(MessageType.Warning, warningMessage)

      client.showMessage(warningMessageParams)

      val notification = slot<Notification>()
      verify {
        notificationManager.sendNotification(
          capture(notification),
          NotificationGroupType.IMPORTANT,
          NotificationType.WARNING
        )
      }
      notification.captured.message shouldBe warningMessage
    }

    it("should display an info message") {
      val infoMessage = "This is an info message."
      val infoMessageParams = MessageParams(MessageType.Info, infoMessage)

      client.showMessage(infoMessageParams)

      val notification = slot<Notification>()
      verify {
        notificationManager.sendNotification(
          capture(notification),
          NotificationGroupType.GENERAL,
          NotificationType.INFORMATION
        )
      }
      notification.captured.message shouldBe infoMessage
    }
    it("should not display a log message") {
      val logMessage = "This is a log message."
      val logMessageParams = MessageParams(MessageType.Log, logMessage)

      client.showMessage(logMessageParams)

      verify(exactly = 0) {
        notificationManager.sendNotification(any(), any(), any())
      }
    }
  }

  describe("showMessageRequest") {
    it("should not display anything when receiving a null message") {
      client.showMessageRequest(null)

      verify(exactly = 0) {
        notificationManager.sendNotification(any(), any(), any())
      }
    }

    it("should display an error message") {
      val errorMessage = "This is an error message."
      val errorMessageParams = ShowMessageRequestParams().apply {
        message = errorMessage
        type = MessageType.Error
        actions = listOf(MessageActionItem("Go to settings"))
      }

      client.showMessageRequest(errorMessageParams)

      val notification = slot<Notification>()
      verify {
        notificationManager.sendNotification(
          capture(notification),
          NotificationGroupType.IMPORTANT,
          NotificationType.ERROR
        )
      }
      notification.captured.message shouldBe "This is an error message."
      notification.captured.actions[0].title shouldBe "Go to settings"
    }

    it("should display a warning message") {
      val warningMessage = "This is a warning message."
      val warningMessageParams = ShowMessageRequestParams().apply {
        message = warningMessage
        type = MessageType.Warning
        actions = listOf(MessageActionItem("Dismiss"))
      }

      client.showMessageRequest(warningMessageParams)

      val notification = slot<Notification>()
      verify {
        notificationManager.sendNotification(
          capture(notification),
          NotificationGroupType.IMPORTANT,
          NotificationType.WARNING
        )
      }
      notification.captured.message shouldBe warningMessage
      notification.captured.actions[0].title shouldBe "Dismiss"
    }

    it("should display an info message") {
      val infoMessage = "This is an info message."
      val infoMessageParams = ShowMessageRequestParams().apply {
        message = infoMessage
        type = MessageType.Info
        actions = emptyList()
      }

      client.showMessageRequest(infoMessageParams)

      val notification = slot<Notification>()
      verify {
        notificationManager.sendNotification(
          capture(notification),
          NotificationGroupType.GENERAL,
          NotificationType.INFORMATION
        )
      }
      notification.captured.message shouldBe infoMessage
      notification.captured.actions.shouldBeEmpty()
    }

    it("should not display a log message") {
      val logMessage = "This is a log message."
      val logMessageParams = ShowMessageRequestParams().apply {
        message = logMessage
        type = MessageType.Log
        actions = emptyList()
      }

      client.showMessageRequest(logMessageParams)

      verify(exactly = 0) {
        notificationManager.sendNotification(any(), any(), any())
      }
    }
  }

  describe("gitlabFeatureStateChange") {
    describe("code_suggestions") {
      it("should update code suggestions state based on the feature state") {
        val featureState = FeatureStateParams(
          featureId = FeatureId.CODE_SUGGESTIONS,
          engagedChecks = emptyList()
        )

        client.gitlabFeatureStateChange(arrayOf(featureState))

        verify { codeSuggestionState.update(featureState) }
      }
    }

    describe("chat") {
      it("should update chat state based on the feature state") {
        val featureState = FeatureStateParams(
          featureId = FeatureId.CHAT,
          engagedChecks = emptyList()
        )

        client.gitlabFeatureStateChange(arrayOf(featureState))

        verify { duoChatStateService.update(featureState) }
      }
    }

    describe("authentication") {
      it("should update authentication state based on the feature state") {
        val featureState = FeatureStateParams(
          featureId = FeatureId.AUTHENTICATION,
          engagedChecks = emptyList()
        )

        client.gitlabFeatureStateChange(arrayOf(featureState))

        verify { authenticationStateService.update(featureState) }
      }
    }
  }

  describe("cancelStreaming") {
    it("should cancel streaming when receiving a StreamWithId") {
      val streamId = "test-stream-id"
      val streamWithId = StreamWithId(streamId)

      client.cancelStreaming(streamWithId)

      verify { completionListener.cancel(streamId) }
    }
  }

  describe("streamingCompletionResponse") {
    it("should update completion listener with streaming completion response") {
      val streamingResponse = StreamingCompletionResponse("test-id", "test-content", false)

      client.streamingCompletionResponse(streamingResponse)

      verify { completionListener.update(streamingResponse) }
    }
  }

  describe("registerCapability") {
    it("should register capabilities") {
      val registrationParams = RegistrationParams(
        listOf(
          Registration(
            "unique-id-1",
            "workspace/didChangeWatchedFiles",
            JsonObject().apply {
              add("watchers", JsonArray())
            }
          ),
          Registration(
            "unique-id-2",
            "other/capability",
            JsonObject()
          ),
        ),
      )

      client.registerCapability(registrationParams).get()

      verify {
        capabilityManager.register("unique-id-1", "workspace/didChangeWatchedFiles", any<JsonObject>())
        capabilityManager.register("unique-id-2", "other/capability", any<JsonObject>())
      }
    }
  }

  describe("unregisterCapability") {
    it("should unregister capabilities") {
      val params = UnregistrationParams(
        listOf(
          Unregistration("unique-id-1", "workspace/didChangeWatchedFiles"),
          Unregistration("unique-id-2", "unknown-capability"),
        ),
      )

      client.unregisterCapability(params).get()

      verify { capabilityManager.unregister("unique-id-1", "workspace/didChangeWatchedFiles") }
      verify { capabilityManager.unregister("unique-id-2", "unknown-capability") }
    }
  }

  describe("getGitDiff") {
    it("should return git diff when branch is provided") {
      val request = GitDiffParams("repo/uri", "feature-branch")
      val expectedDiff = "diff --git a/file.txt b/file.txt\n..."
      every { gitDiffProvider.getDiffWithBranch("repo/uri", "feature-branch") } returns expectedDiff

      val result = client.getGitDiff(request).get()

      result shouldBe expectedDiff
      verify { gitDiffProvider.getDiffWithBranch("repo/uri", "feature-branch") }
    }

    it("should return git diff with HEAD when branch is not provided") {
      val request = GitDiffParams("repo/uri", null)
      val expectedDiff = "diff --git a/file.txt b/file.txt\n..."
      every { gitDiffProvider.getDiffWithHead("repo/uri") } returns expectedDiff

      val result = client.getGitDiff(request).get()

      result shouldBe expectedDiff
      verify { gitDiffProvider.getDiffWithHead("repo/uri") }
    }

    it("should return null when an exception occurs") {
      val request = GitDiffParams("repo/uri", "feature-branch")
      every {
        gitDiffProvider.getDiffWithBranch("repo/uri", "feature-branch")
      } throws RuntimeException("Git diff error")

      val result = client.getGitDiff(request).get()

      result shouldBe null
      verify { gitDiffProvider.getDiffWithBranch("repo/uri", "feature-branch") }
    }
  }

  describe("getCurrentFileContext") {
    it("should return correct file context when request is valid") {
      val request = CurrentFileContextRequestParams(
        LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
        "getCurrentFileContext",
        ""
      )
      val expectedContext = ChatRecordContext(
        currentFile = ChatRecordFileContext(
          selectedText = "selected text",
          fileName = "test.kt",
          contentAboveCursor = "content above",
          contentBelowCursor = "content below"
        )
      )

      coEvery { chatRecordContextService.getChatRecordContextOrNull() } returns expectedContext

      val result = client.getCurrentFileContext(request).getNow(null)

      result shouldBe CurrentFileContextResponseParams(
        "selected text",
        "test.kt",
        "content above",
        "content below"
      )
    }

    it("should return empty response when ChatRecordContext is null") {
      val request = CurrentFileContextRequestParams(
        LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
        "getCurrentFileContext",
        ""
      )
      coEvery { chatRecordContextService.getChatRecordContextOrNull() } returns null

      val result = client.getCurrentFileContext(request).get()
      result shouldBe CurrentFileContextResponseParams("", "", "", "")
    }

    it("should return null when request is invalid") {
      val request = CurrentFileContextRequestParams("invalid-plugin", "invalidType", "")

      val result = client.getCurrentFileContext(request).get()

      result shouldBe null
    }

    it("should return null when an exception occurs") {
      val request = CurrentFileContextRequestParams("gitlab-plugin", "getCurrentFileContext", "")
      coEvery { chatRecordContextService.getChatRecordContextOrNull() } throws RuntimeException("Test exception")

      val result = client.getCurrentFileContext(request).get()
      result shouldBe null
    }
  }

  describe("handleLSNotification") {
    it("should insert code snippet when request is valid") {
      val jsonSerializer = Json { ignoreUnknownKeys = true }
      val jsonPayload = """{"snippet":"println(\"Hello, World!\")"}"""

      val request = LSNotificationParams(
        pluginId = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
        type = LSRequestType.INSERT_CODE_SNIPPET.requestType,
        payload = jsonPayload
      )

      client.handleLSNotification(request).get()

      coVerify(exactly = 1) {
        insertCodeSnippetService.insertCodeSnippet(
          jsonSerializer.decodeFromString<InsertCodeSnippetMessage>(jsonPayload)
        )
      }
    }
  }

  it("should not insert code snippet when pluginId is incorrect") {
    val request = LSNotificationParams(
      pluginId = "invalid-plugin",
      type = "insertCodeSnippet",
      payload = InsertCodeSnippetMessage("println(\"Hello, World!\")")
    )

    client.handleLSNotification(request).get()

    coVerify(exactly = 0) {
      insertCodeSnippetService.insertCodeSnippet(any())
    }
  }

  it("should not insert code snippet when type is incorrect") {
    val request = LSNotificationParams(
      pluginId = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
      type = "invalidType",
      payload = InsertCodeSnippetMessage("println(\"Hello, World!\")")
    )

    client.handleLSNotification(request).get()

    coVerify(exactly = 0) {
      insertCodeSnippetService.insertCodeSnippet(any())
    }
  }

  it("should handle exceptions gracefully") {
    val request = LSNotificationParams(
      pluginId = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
      type = "insertCodeSnippet",
      payload = InsertCodeSnippetMessage("println(\"Hello, World!\")")
    )

    coEvery { insertCodeSnippetService.insertCodeSnippet(any()) } throws RuntimeException("Test exception")

    client.handleLSNotification(request).get()

    coVerify(exactly = 0) {
      insertCodeSnippetService.insertCodeSnippet(request.payload as InsertCodeSnippetMessage)
    }
  }

  it("should mark LS communication as ready when APP_READY notification is received") {
    val request = LSNotificationParams(
      pluginId = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
      type = LSRequestType.APP_READY.requestType,
      payload = null
    )

    client.handleLSNotification(request).get()

    coVerify {
      languageServerDuoChatWebViewService.markLSCommunicationAsReady()
    }
  }

  it("should show message when request type is SHOW_MESSAGE") {
    val jsonPayload = """{"type":"error", "message":"Test error message"}"""

    val request = LSNotificationParams(
      pluginId = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
      type = LSRequestType.SHOW_MESSAGE.requestType,
      payload = jsonPayload
    )

    client.handleLSNotification(request).get()

    val notification = slot<Notification>()
    verify {
      notificationManager.sendNotification(
        capture(notification),
        NotificationGroupType.IMPORTANT,
        NotificationType.ERROR
      )
    }
    notification.captured.message shouldBe "Test error message"
  }

  it("should not show message when plugin ID is incorrect") {
    val jsonPayload = """{"type":"info", "message":"Test info message"}"""

    val request = LSNotificationParams(
      pluginId = "incorrect-plugin-id",
      type = LSRequestType.SHOW_MESSAGE.requestType,
      payload = jsonPayload
    )

    client.handleLSNotification(request).get()

    verify(exactly = 0) {
      notificationManager.sendNotification(any(), any(), any())
    }
  }

  it("should handle exceptions gracefully when showing message") {
    val jsonPayload = """{"type":"info", "message":"Test info message"}"""

    val request = LSNotificationParams(
      pluginId = LSRequestResponseParams.WEBVIEW_PLUGIN_ID,
      type = LSRequestType.SHOW_MESSAGE.requestType,
      payload = jsonPayload
    )

    every {
      notificationManager.sendNotification(any(), any(), any())
    } throws RuntimeException("Test exception")

    client.handleLSNotification(request).get()

    verify(exactly = 1) {
      notificationManager.sendNotification(any(), any(), any())
    }
  }
})
