package com.gitlab.plugin.lsp.capabilities

import com.google.gson.JsonObject
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import org.junit.jupiter.api.assertDoesNotThrow

class LanguageServerCapabilityManagerTest : DescribeSpec({
  val project = mockk<Project>(relaxed = true)
  var languageServerCapabilityManager = LanguageServerCapabilityManager(project)

  beforeSpec {
    mockkConstructor(DidChangeWatchedFileCapability::class)
  }

  beforeEach {
    languageServerCapabilityManager = LanguageServerCapabilityManager(project)

    every { anyConstructed<DidChangeWatchedFileCapability>().register(any(), any()) } returns Unit
    every { anyConstructed<DidChangeWatchedFileCapability>().unregister(any()) } returns Unit
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should register and start a new didChangeWatchedFiles capability") {
    val id = "123"
    val method = "workspace/didChangeWatchedFiles"
    val options = mockk<JsonObject>(relaxed = true)

    languageServerCapabilityManager.register(id, method, options)

    verify { anyConstructed<DidChangeWatchedFileCapability>().register("123", options) }
  }

  it("should register and new didChangeWatchedFiles options") {
    val id1 = "123"
    val id2 = "456"
    val method = "workspace/didChangeWatchedFiles"
    val options = mockk<JsonObject>(relaxed = true)

    languageServerCapabilityManager.register(id1, method, options)
    languageServerCapabilityManager.register(id2, method, options)

    verify { anyConstructed<DidChangeWatchedFileCapability>().register("123", options) }
    verify { anyConstructed<DidChangeWatchedFileCapability>().register("456", options) }
  }

  it("should not register an unsupported capability") {
    val id = "456"
    val method = "unknown"
    val options = mockk<JsonObject>(relaxed = true)

    assertDoesNotThrow { languageServerCapabilityManager.register(id, method, options) }
  }

  it("should unregister capability") {
    val id = "123"
    val method = "workspace/didChangeWatchedFiles"
    val options = mockk<JsonObject>(relaxed = true)

    languageServerCapabilityManager.register(id, method, options)
    verify { anyConstructed<DidChangeWatchedFileCapability>().register(id, options) }

    languageServerCapabilityManager.unregister(id, method)
    verify { anyConstructed<DidChangeWatchedFileCapability>().unregister(id) }
  }

  it("should unregister all capabilities") {
    val id = "123"
    val method = "workspace/didChangeWatchedFiles"
    val options = mockk<JsonObject>(relaxed = true)
    languageServerCapabilityManager.register(id, method, options)

    languageServerCapabilityManager.unregisterAll()

    verify { anyConstructed<DidChangeWatchedFileCapability>().unregisterAll() }
  }
})
