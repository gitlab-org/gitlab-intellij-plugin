package com.gitlab.plugin.lsp.capabilities

import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.uri
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.openapi.vfs.newvfs.events.VFileContentChangeEvent
import com.intellij.openapi.vfs.newvfs.events.VFileCopyEvent
import com.intellij.openapi.vfs.newvfs.events.VFileCreateEvent
import com.intellij.openapi.vfs.newvfs.events.VFileDeleteEvent
import com.intellij.openapi.vfs.newvfs.events.VFileEvent
import com.intellij.openapi.vfs.newvfs.events.VFileMoveEvent
import com.intellij.openapi.vfs.newvfs.events.VFilePropertyChangeEvent
import com.intellij.util.messages.MessageBusConnection
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.eclipse.lsp4j.*
import org.junit.jupiter.api.assertDoesNotThrow
import kotlin.time.Duration.Companion.milliseconds

class DidChangeWatchedFileCapabilityTest : DescribeSpec({
  val project = mockk<Project>(relaxed = true)
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)

  val capability = DidChangeWatchedFileCapability(
    project,
    sendBatchDelayInMs = 10L // Send events instantly
  )

  beforeEach {
    every { project.getService(GitLabLanguageServerService::class.java) } returns languageServerService

    // Interested in Create events for Kotlin File Events
    // Interested in all events for Java File Events
    capability.register(
      id = "123",
      options = JsonObject().apply {
        add(
          "watchers",
          JsonArray().apply {
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.kt")
                addProperty("kind", WatchKind.Create)
              }
            )
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.java")
              }
            )
          }
        )
      }
    )
  }

  afterEach { clearAllMocks() }

  it("can be connected register and unregister new watchers") {
    val newCapability = DidChangeWatchedFileCapability(project)

    val connection = mockk<MessageBusConnection>(relaxed = true)
    every { project.messageBus.connect() } returns connection

    newCapability.register(
      id = "123",
      options = JsonObject().apply {
        add(
          "watchers",
          JsonArray().apply {
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.kt")
                addProperty("kind", WatchKind.Create)
              }
            )
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.java")
              }
            )
          }
        )
      }
    )
    newCapability.register(
      id = "456",
      options = JsonObject().apply {
        add("watchers", JsonArray())
      }
    )
    verify { connection.subscribe(VirtualFileManager.VFS_CHANGES, newCapability) }

    newCapability.unregister("123")
    verify(exactly = 0) { connection.dispose() }

    newCapability.unregister("456")
    verify(exactly = 1) { connection.dispose() }
  }

  it("can register the capability after it has been unregistered") {
    val newCapability = DidChangeWatchedFileCapability(project)

    val connection = mockk<MessageBusConnection>(relaxed = true)
    every { project.messageBus.connect() } returns connection

    newCapability.register(
      id = "123",
      options = JsonObject().apply {
        add("watchers", JsonArray())
      }
    )

    newCapability.unregister("123")

    assertDoesNotThrow {
      newCapability.register(
        id = "456",
        options = JsonObject().apply {
          add("watchers", JsonArray())
        }
      )
    }
  }

  it("can unregister all watchers") {
    val newCapability = DidChangeWatchedFileCapability(project)

    val connection = mockk<MessageBusConnection>(relaxed = true)
    every { project.messageBus.connect() } returns connection

    newCapability.register(
      id = "123",
      options = JsonObject().apply {
        add(
          "watchers",
          JsonArray().apply {
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.kt")
                addProperty("kind", WatchKind.Create)
              }
            )
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.java")
              }
            )
          }
        )
      }
    )
    newCapability.register(
      id = "456",
      options = JsonObject().apply {
        add("watchers", JsonArray())
      }
    )
    verify { connection.subscribe(VirtualFileManager.VFS_CHANGES, newCapability) }

    newCapability.unregisterAll()
  }

  describe("before") {
    it("should not send any message when no event is interested by the watcher") {
      val event = mockk<VFileEvent>(relaxed = true)
      every { event.path } returns "/path/to/file.txt"

      capability.before(mutableListOf(event))

      wait(25)
      verify(exactly = 0) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(any()) }
    }

    it("should create a Deleted FileEvent when a file is moved") {
      val event1 = mockk<VFileMoveEvent>(relaxed = true)
      val event2 = mockk<VFileMoveEvent>(relaxed = true)
      // Delete events are not watched for Kotlin files
      every { event1.path } returns "/path/to/fileKt.kt"
      every { event1.file.uri } returns "file:///path/to/fileKt.kt"

      // Delete events are watched for Java files
      every { event2.path } returns "/path/to/fileJava.java"
      every { event2.file.uri } returns "file:///path/to/fileJava.java"

      capability.before(mutableListOf(event1, event2))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileJava.java"
      params.captured.changes[0].type shouldBe FileChangeType.Deleted
    }

    it("should create a Deleted FileEvent when a file is renamed") {
      val event = mockk<VFilePropertyChangeEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileJava.java"
      every { event.propertyName } returns "name"
      every { event.file.uri } returns "file:///path/to/fileJava.java"

      capability.before(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileJava.java"
      params.captured.changes[0].type shouldBe FileChangeType.Deleted
    }

    it("should not create a FileEvent other event types") {
      val event1 = mockk<VFileCreateEvent>(relaxed = true)
      val event2 = mockk<VFileDeleteEvent>(relaxed = true)
      val event3 = mockk<VFileContentChangeEvent>(relaxed = true)
      val event4 = mockk<VFileCopyEvent>(relaxed = true)
      every { event1.path } returns "/path/to/file.java"
      every { event2.path } returns "/path/to/file.java"
      every { event3.path } returns "/path/to/file.java"
      every { event4.path } returns "/path/to/file.java"

      capability.before(mutableListOf(event1, event2, event3, event4))

      wait(25)
      verify(exactly = 0) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(any()) }
    }
  }

  describe("after") {
    it("should create a Created FileEvent when a file is created") {
      val event = mockk<VFileCreateEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileKt.kt"
      every { event.file?.uri } returns "file:///path/to/fileKt.kt"

      capability.after(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileKt.kt"
      params.captured.changes[0].type shouldBe FileChangeType.Created
    }

    it("should create a Deleted FileEvent when a file is deleted") {
      val event = mockk<VFileDeleteEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileJava.java"
      every { event.file.uri } returns "file:///path/to/fileJava.java"

      capability.after(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileJava.java"
      params.captured.changes[0].type shouldBe FileChangeType.Deleted
    }

    it("should create a Created FileEvent when a file is copied") {
      val event = mockk<VFileCopyEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileJava.java"
      every { event.findCreatedFile()?.uri } returns "file:///path/to/fileJava.java"

      capability.after(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileJava.java"
      params.captured.changes[0].type shouldBe FileChangeType.Created
    }

    it("should create a Created FileEvent when a file is moved") {
      val event = mockk<VFileMoveEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileKt.kt"
      every { event.file.uri } returns "file:///path/to/fileKt.kt"

      capability.after(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileKt.kt"
      params.captured.changes[0].type shouldBe FileChangeType.Created
    }

    it("should create a Created FileEvent when a file is renamed") {
      val event = mockk<VFilePropertyChangeEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileJava.java"
      every { event.propertyName } returns "name"
      every { event.file.uri } returns "file:///path/to/fileJava.java"

      capability.after(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileJava.java"
      params.captured.changes[0].type shouldBe FileChangeType.Created
    }

    it("should create a Changed FileEvent for other event types") {
      val event = mockk<VFileEvent>(relaxed = true)
      every { event.path } returns "/path/to/fileJava.java"
      every { event.file?.uri } returns "file:///path/to/fileJava.java"

      capability.after(mutableListOf(event))

      wait(25)
      val params = slot<DidChangeWatchedFilesParams>()
      verify(exactly = 1) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(capture(params)) }
      params.captured.changes shouldHaveSize 1
      params.captured.changes[0].uri shouldBe "file:///path/to/fileJava.java"
      params.captured.changes[0].type shouldBe FileChangeType.Changed
    }

    it("should not send any message when no event is interested by the watcher") {
      val event = mockk<VFileEvent>(relaxed = true)
      every { event.path } returns "/path/to/file.txt"

      capability.after(mutableListOf(event))

      wait(25)
      verify(exactly = 0) { languageServerService.lspServer?.workspaceService?.didChangeWatchedFiles(any()) }
    }
  }

  describe("toDidChangeWatchedFilesRegistrationOptions") {
    it("should convert JsonObject to DidChangeWatchedFilesRegistrationOptions") {
      val jsonObject = JsonObject().apply {
        add(
          "watchers",
          JsonArray().apply {
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.kt")
                addProperty("kind", 7)
              }
            )
            add(
              JsonObject().apply {
                addProperty("globPattern", "**/*.java")
              }
            )
          }
        )
      }

      val result = jsonObject.toDidChangeWatchedFilesRegistrationOptions()

      result.watchers shouldHaveSize 2
      result.watchers[0].globPattern.left shouldBe "**/*.kt"
      result.watchers[0].kind shouldBe 7

      result.watchers[1].globPattern.left shouldBe "**/*.java"
      result.watchers[1].kind shouldBe null
    }
  }
})

private fun wait(millis: Int) = runBlocking { delay(millis.milliseconds) }
