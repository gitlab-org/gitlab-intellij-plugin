package com.gitlab.plugin.lsp.webview

import com.gitlab.plugin.lsp.params.*
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CompletableFuture

class LanguageServerDuoChatWebViewServiceTest : DescribeSpec({
  val project = mockk<Project>()
  val languageServerService = mockk<GitLabLanguageServerService>()
  val lspServer = mockk<com.gitlab.plugin.lsp.GitLabLanguageServer>()

  beforeEach {
    every { project.service<GitLabLanguageServerService>() } returns languageServerService
    every { languageServerService.lspServer } returns lspServer
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("LanguageServerWebViewService") {
    val service = LanguageServerDuoChatWebViewService(project)

    describe("getWebViewInfo") {
      it("should return the correct URI when duo-chat WebViewInfo is available") {
        val webViewInfoList = listOf(
          WebViewInfo("other-id", "title1", listOf("other-uri")),
          WebViewInfo(LSRequestResponseParams.WEBVIEW_PLUGIN_ID, "title2", listOf("https://example.com/duo-chat-v2"))
        )
        val future = CompletableFuture.completedFuture(webViewInfoList)
        every { lspServer.getWebViewInfo() } returns future

        runBlocking {
          val result = service.getWebViewInfo()
          result shouldBe "https://example.com/${LSRequestResponseParams.WEBVIEW_PLUGIN_ID}"
        }
      }

      it("should return empty when duo-chat WebViewInfo is not available") {
        val webViewInfoList = listOf(
          WebViewInfo("other-id", "title3", listOf("other-uri"))
        )
        val future = CompletableFuture.completedFuture(webViewInfoList)
        every { lspServer.getWebViewInfo() } returns future

        runBlocking {
          val result = service.getWebViewInfo()
          result shouldBe ""
        }
      }

      it("should return an empty string if the server fails to start") {
        every { languageServerService.lspServer } returns null

        runBlocking {
          val result = service.getWebViewInfo()
          result shouldBe ""
        }
      }
    }

    describe("sendThemeChangeNotification") {
      it("should call didChangeTheme on the lspServer with the provided theme") {
        val theme = mockk<ThemeProvider.Theme>()
        every { lspServer.didChangeTheme(any()) } just Runs

        service.sendThemeChangeNotification(theme)

        verify { lspServer.didChangeTheme(theme) }
      }
    }

    describe("sendNewPromptLSNotification") {
      val fileContext = CurrentFileContextResponseParams(
        "selected text",
        "test.kt",
        "content above",
        "content below"
      )
      val newPromptParams = NewPromptParams(payload = NewPromptPayload("explainCode", fileContext))

      it("should call sendNewPrompt on the lspServer when ready") {

        every { lspServer.sendNewPrompt(newPromptParams) } just Runs

        service.markLSCommunicationAsReady()
        service.sendNewPromptLSNotification(newPromptParams)

        verify(exactly = 1) { lspServer.sendNewPrompt(newPromptParams) }
      }

      it("should send queued prompts when marked as ready") {
        every { lspServer.sendNewPrompt(any()) } just Runs

        service.sendNewPromptLSNotification(newPromptParams)
        service.markLSCommunicationAsReady()

        verify(exactly = 1) { lspServer.sendNewPrompt(newPromptParams) }
      }

      it("should not call sendNewPrompt when lspServer is null") {
        every { languageServerService.lspServer } returns null

        service.markLSCommunicationAsReady()
        service.sendNewPromptLSNotification(newPromptParams)

        verify(exactly = 0) { lspServer.sendNewPrompt(any()) }
      }
    }
  }
})
