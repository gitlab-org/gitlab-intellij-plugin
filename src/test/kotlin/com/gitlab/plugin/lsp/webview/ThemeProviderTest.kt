package com.gitlab.plugin.lsp.webview

import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.openapi.editor.colors.EditorColorsScheme
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import java.awt.Color

class ThemeProviderTest : DescribeSpec({
  describe("ThemeProvider") {
    val themeProvider = ThemeProvider()
    val mockEditorColorsManager = mockk<EditorColorsManager>()
    val mockGlobalScheme = mockk<EditorColorsScheme>()

    beforeTest {
      mockkStatic(EditorColorsManager::class)
      every { EditorColorsManager.getInstance() } returns mockEditorColorsManager
      every { mockEditorColorsManager.globalScheme } returns mockGlobalScheme
    }
    describe("getCurrentTheme") {
      it("correctly handles font values and color conversion CSS values") {
        every { mockGlobalScheme.editorFontName } returns "Arial, Helvetica, sans-serif"
        every { mockGlobalScheme.defaultBackground } returns Color(128, 128, 128)
        every { mockGlobalScheme.editorFontSize } returns 5

        val theme = themeProvider.getCurrentTheme()

        theme.styles["--editor-font-family"] shouldBe "Arial, Helvetica, sans-serif"
        theme.styles["--editor-textCodeBlock-background"] shouldBe "#808080"
        theme.styles["--editor-font-size"] shouldBe "5"
      }
    }
  }
})
