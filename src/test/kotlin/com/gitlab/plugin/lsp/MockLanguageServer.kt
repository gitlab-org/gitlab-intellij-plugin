package com.gitlab.plugin.lsp

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.jetbrains.rd.util.string.printToString
import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.unmockkConstructor
import java.io.*
import java.util.concurrent.CompletableFuture

class MockLanguageServer {
  private val receivedRequests = mutableListOf<String>()
  private val methodResponses = mutableMapOf<String, String>()
  private var testProcess: Process? = null
  private var serverIn: PipedInputStream? = null
  private var clientOut: PipedOutputStream? = null
  private var clientIn: PipedInputStream? = null
  private var serverOut: PipedOutputStream? = null
  private var serverThread: Thread? = null
  private var shouldContinue = true

  fun start() {
    shouldContinue = true

    serverIn = PipedInputStream()
    clientOut = PipedOutputStream(serverIn)
    clientIn = PipedInputStream()
    serverOut = PipedOutputStream(clientIn)

    serverThread = Thread {
      val reader = serverIn!!.bufferedReader()
      val writer = OutputStreamWriter(serverOut!!).buffered()
      this.main(reader, writer)
    }.apply {
      start()
    }

    testProcess = object : Process() {
      override fun getOutputStream() = clientOut!!
      override fun getInputStream() = clientIn!!
      override fun getErrorStream() = ByteArrayInputStream(ByteArray(0))
      override fun waitFor() = 0
      override fun exitValue() = 0
      override fun destroy() { stop() }
      override fun onExit() = CompletableFuture.completedFuture(this as Process)
      override fun toHandle() = ProcessHandle.current()
    }

    mockkConstructor(ProcessBuilder::class)
    every { anyConstructed<ProcessBuilder>().start() } returns testProcess
  }

  fun stop() {
    shouldContinue = false

    try {
      clientOut?.let {
        it.flush()
        it.close()
      }
      serverIn?.close()
      serverOut?.flush()
      serverOut?.close()
      clientIn?.close()

      serverThread?.interrupt()
    } finally {
      serverThread = null
      serverIn = null
      clientOut = null
      clientIn = null
      serverOut = null
      testProcess = null
      receivedRequests.clear()
      methodResponses.clear()
    }

    unmockkConstructor(ProcessBuilder::class)
  }

  fun setMethodResponse(method: String, response: String) {
    methodResponses[method] = response
  }

  fun requestsForMethod(method: String): List<String> {
    return receivedRequests.filter { it.contains("\"method\":\"$method\"") }
  }

  fun clearReceivedRequests() {
    receivedRequests.clear()
  }

  private fun main(input: BufferedReader, output: BufferedWriter) {
    while (shouldContinue) {
      val content = readNextRequest(input)

      when {
        content == null -> {
          // Don't exit on null content
          continue
        }
        content.isNotEmpty() -> {
          val response = handleRequest(content)
          if (response != null) output.write(response)
          output.flush()
        }
      }
    }
  }

  private fun readNextRequest(stdin: BufferedReader): String? {
    return try {
      while (shouldContinue) {
        val result = processNextLine(stdin) ?: continue
        return result
      }
      return null
    } catch (e: IOException) {
      e.printToString()
      null
    }
  }

  private fun processNextLine(stdin: BufferedReader): String? {
    val line = stdin.readLine() ?: return null
    if (!line.startsWith("Content-Length: ")) return null

    val contentLength = line.substring("Content-Length: ".length).toInt()
    stdin.readLine() // Skip empty line

    val buffer = CharArray(contentLength)
    return when (stdin.read(buffer, 0, contentLength)) {
      -1 -> null
      else -> String(buffer)
    }
  }

  private fun handleRequest(content: String): String? {
    receivedRequests.add(content)

    val request = JsonParser.parseString(content).asJsonObject
    val response = JsonObject()
    response.addProperty("jsonrpc", "2.0")

    request.get("id").also {
      when {
        it != null -> response.add("id", it)
        else -> return null // Do not respond to notifications
      }
    }

    val method = request.get("method")?.asString

    when {
      methodResponses.containsKey(method) -> {
        val methodResponseJson = JsonParser.parseString(methodResponses[method]).asJsonObject
        methodResponseJson.entrySet().forEach { (key, value) ->
          response.add(key, value)
        }
      }
      method == "initialize" -> {
        val result = JsonObject()
        val capabilities = JsonObject()
        val serverInfo = JsonObject()
        serverInfo.addProperty("name", "test-server")
        result.add("capabilities", capabilities)
        result.add("serverInfo", serverInfo)
        response.add("result", result)
      }
      else -> {
        response.addProperty("result", true)
      }
    }

    val responseString = response.toString()
    return buildString {
      append("Content-Length: ${responseString.length}\r\n")
      append("Content-Type: application/vscode-jsonrpc; charset=utf-8\r\n")
      append("\r\n")
      append(responseString)
    }
  }
}
