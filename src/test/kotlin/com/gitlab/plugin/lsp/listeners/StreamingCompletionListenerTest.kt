package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.lsp.params.StreamingCompletionResponse
import com.gitlab.plugin.lsp.services.CodeCompletionRegistryService
import com.gitlab.plugin.util.code.CodeFormatter
import com.gitlab.plugin.util.code.CodeFormattingContext
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher

@OptIn(ExperimentalCoroutinesApi::class)
class StreamingCompletionListenerTest : DescribeSpec({
  mockkStatic(::runInEdt)
  mockkConstructor(CodeFormatter::class)

  val context = mockk<SuggestionContext>()
  val formattingContext = mockk<CodeFormattingContext>()

  val project = mockk<Project>()
  val coroutineScope = TestScope(UnconfinedTestDispatcher())

  val codeCompletionRegistryService = mockk<CodeCompletionRegistryService>(relaxUnitFun = true)

  lateinit var listener: StreamingCompletionListener

  beforeEach {
    every { project.service<CodeCompletionRegistryService>() } returns codeCompletionRegistryService
    every { runInEdt(null, any<() -> Unit>()) } answers { secondArg<() -> Unit>()() }

    every { context.toFormattingContext() } returns formattingContext

    coEvery { anyConstructed<CodeFormatter>().format(any<String>(), formattingContext) } answers { call ->
      "${call.invocation.args[0]?.toString()} formatted"
    }

    listener = StreamingCompletionListener(project, coroutineScope)
  }

  afterSpec {
    unmockkAll()
  }

  it("should not update completion if it is missing") {
    val params = StreamingCompletionResponse(
      id = "test-id",
      completion = "partial completion",
      done = false
    )
    val displayedSuggestion = mockk<UpdatableInlineCompletionGrayElement>(relaxUnitFun = true)
    every { codeCompletionRegistryService[params.id] } returns null

    listener.update(params)

    verify(exactly = 0) { displayedSuggestion.update(params.completion) }
  }

  it("should update completion when not done") {
    val params = StreamingCompletionResponse(
      id = "test-id",
      completion = "partial completion",
      done = false
    )
    val element = mockk<UpdatableInlineCompletionGrayElement>(relaxed = true)
    val tracker = SuggestionsTracker(context, element, streamId = "123")
    every { codeCompletionRegistryService[params.id] } returns tracker

    listener.update(params)

    verify { element.update("partial completion formatted") }
  }

  it("should complete when done") {
    val params = StreamingCompletionResponse(
      id = "test-id",
      completion = "",
      done = true
    )

    listener.update(params)

    verify { codeCompletionRegistryService.complete(params) }
  }

  it("should cancel completion") {
    val id = "test-id"

    listener.cancel(id)

    verify { codeCompletionRegistryService.cancel(id) }
  }
})
