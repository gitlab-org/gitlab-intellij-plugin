package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.uri
import com.intellij.openapi.editor.Document
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.fileEditor.FileEditorManagerEvent
import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Computable
import com.intellij.openapi.vfs.VirtualFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.eclipse.lsp4j.DidCloseTextDocumentParams
import org.eclipse.lsp4j.DidOpenTextDocumentParams

class FileEventLanguageServerListenerTest : DescribeSpec({
  val project = mockk<Project>()

  val fileEditorManager = mockk<FileEditorManager>()
  val fileDocumentManager = mockk<FileDocumentManager>(relaxed = true)

  val coroutineScope = TestScope(UnconfinedTestDispatcher())
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)
  val listener = FileEventLanguageServerListener(project, coroutineScope)

  beforeSpec {
    mockkStatic(FileDocumentManager::getInstance)
    mockkStatic(FileEditorManager::getInstance)
  }

  beforeEach {
    val application = mockApplication()
    every { application.runReadAction(any<Computable<*>>()) } answers {
      (invocation.args[0] as Computable<*>).compute()
    }

    every { project.getService(GitLabLanguageServerService::class.java) } returns languageServerService
    every { FileDocumentManager.getInstance() } returns fileDocumentManager
    every { FileEditorManager.getInstance(project) } returns fileEditorManager
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("fileOpened") {
    it("should not send didOpen notification if file has no document") {
      val file = mockk<VirtualFile>()
      every { fileDocumentManager.getDocument(file) } returns null

      listener.fileOpened(fileEditorManager, file)

      verify(exactly = 0) {
        languageServerService.lspServer?.textDocumentService?.didOpen(any())
      }
    }

    it("should send didOpen notification") {
      val file = mockk<VirtualFile>(relaxed = true) {
        every { uri } returns "file:///Users/jetbrains-plugin/main.kt"
        every { modificationStamp } returns 1L
        every { fileType } returns mockk<FileType> {
          every { defaultExtension } returns "kt"
        }
      }

      val document = mockk<Document> {
        every { text } returns "LSP server opened"
      }

      every { fileDocumentManager.getDocument(file) } returns document

      listener.fileOpened(fileEditorManager, file)

      val params = slot<DidOpenTextDocumentParams>()
      verify {
        languageServerService.lspServer?.textDocumentService?.didOpen(capture(params))
      }
      params.captured.textDocument.text shouldBe "LSP server opened"
      params.captured.textDocument.languageId shouldBe "kotlin"
      params.captured.textDocument.version shouldBe 1
      params.captured.textDocument.uri shouldBe "file:///Users/jetbrains-plugin/main.kt"
    }
  }

  describe("fileClosed") {
    it("should send didClose notification for given file") {
      val file = mockk<VirtualFile> {
        every { uri } returns "file:///Users/jetbrains-plugin/main.kt"
      }

      listener.fileClosed(fileEditorManager, file)

      val params = slot<DidCloseTextDocumentParams>()
      verify { languageServerService.lspServer?.textDocumentService?.didClose(capture(params)) }
      params.captured.textDocument.uri shouldBe "file:///Users/jetbrains-plugin/main.kt"
    }
  }

  describe("onLanguageServerStarted") {
    it("should send open files and active files to the language server") {
      val file1 = mockk<VirtualFile> {
        every { uri } returns "file:///Users/jetbrains-plugin/main.kt"
        every { modificationStamp } returns 1L

        every { fileType } returns mockk<FileType> {
          every { defaultExtension } returns "kt"
        }
      }

      val file2 = mockk<VirtualFile> {
        every { uri } returns "file:///Users/jetbrains-plugin/completion.kt"
        every { modificationStamp } returns 1L

        every { fileType } returns mockk<FileType> {
          every { defaultExtension } returns "kt"
        }
      }

      val document = mockk<Document> {
        every { text } returns "LSP server opened"
      }

      every { fileDocumentManager.getDocument(file1) } returns document
      every { fileDocumentManager.getDocument(file2) } returns document
      every { fileEditorManager.openFiles } returns arrayOf(file1, file2)
      every { fileEditorManager.selectedTextEditor?.virtualFile } returns file1

      listener.onLanguageServerStarted()

      val didOpenParams = mutableListOf<DidOpenTextDocumentParams>()
      verify {
        languageServerService.lspServer?.textDocumentService?.didOpen(capture(didOpenParams))
      }
      didOpenParams[0].textDocument.text shouldBe "LSP server opened"
      didOpenParams[0].textDocument.languageId shouldBe "kotlin"
      didOpenParams[0].textDocument.version shouldBe 1
      didOpenParams[0].textDocument.uri shouldBe "file:///Users/jetbrains-plugin/main.kt"

      didOpenParams[1].textDocument.text shouldBe "LSP server opened"
      didOpenParams[1].textDocument.languageId shouldBe "kotlin"
      didOpenParams[1].textDocument.version shouldBe 1
      didOpenParams[1].textDocument.uri shouldBe "file:///Users/jetbrains-plugin/completion.kt"

      verify {
        languageServerService.lspServer?.didChangeDocumentInActiveEditor("file:///Users/jetbrains-plugin/main.kt")
      }
    }
  }

  describe("tab changed") {
    it("should send didChangeDocumentInActiveEditor on tab change") {
      val file = mockk<VirtualFile> {
        every { uri } returns "file:///Users/jetbrains-plugin/main.kt"
      }
      val event = mockk<FileEditorManagerEvent> {
        every { newFile } returns file
      }

      listener.selectionChanged(event)

      val uri = slot<String>()
      verify {
        languageServerService.lspServer?.didChangeDocumentInActiveEditor(capture(uri))
      }
      uri.captured shouldBe "file:///Users/jetbrains-plugin/main.kt"
    }

    it("should not send didChangeDocumentInActiveEditor if new file is null") {
      val event = mockk<FileEditorManagerEvent> {
        every { newFile } returns null
      }

      listener.selectionChanged(event)

      verify(exactly = 0) {
        languageServerService.lspServer?.didChangeDocumentInActiveEditor(any())
      }
    }
  }
})
