package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.lsp.GitLabLanguageServer
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.uri
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.event.DocumentEvent
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectLocator
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiManager
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.test.*
import org.eclipse.lsp4j.DidChangeTextDocumentParams

class FileChangedLanguageServerListenerTest : DescribeSpec({
  val project1 = mockk<Project>()
  val project2 = mockk<Project>()

  val event = mockk<DocumentEvent>()
  val document = mockk<Document>(relaxed = true)

  val otherFile = mockk<VirtualFile>()
  val file = mockk<VirtualFile>(relaxed = true)
  val psiFile = mockk<PsiFile>(relaxed = true)

  val fileDocumentManager = mockk<FileDocumentManager>()
  val projectLocator = mockk<ProjectLocator>()
  val psiManager = mockk<PsiManager>()

  val coroutineScope = TestScope(UnconfinedTestDispatcher())
  val languageServer = mockk<GitLabLanguageServer>(relaxed = true)
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)

  var listener = FileChangedLanguageServerListener(coroutineScope)

  beforeSpec {
    mockkStatic(FileDocumentManager::getInstance)
    mockkStatic(PsiManager::getInstance)
    mockkObject(ProjectLocator.Companion)
  }

  beforeEach {
    every { project1.getService(GitLabLanguageServerService::class.java) } returns languageServerService
    every { project2.getService(GitLabLanguageServerService::class.java) } returns languageServerService

    every { FileDocumentManager.getInstance() } returns fileDocumentManager
    every { ProjectLocator.getInstance() } returns projectLocator
    every { PsiManager.getInstance(project1) } returns psiManager
    every { PsiManager.getInstance(project2) } returns psiManager

    every { psiManager.findFile(file) } returns psiFile
    every { psiManager.findFile(otherFile) } returns psiFile

    every { projectLocator.getProjectsForFile(file) } returns listOf(project1, project2, null)
    every { projectLocator.getProjectsForFile(otherFile) } returns listOf(project1)

    every { fileDocumentManager.getFile(document) } returns file
    every { event.document } returns document

    every { languageServerService.lspServer } returns languageServer

    listener = FileChangedLanguageServerListener(coroutineScope)
  }

  afterEach { clearAllMocks(answers = false) }

  afterSpec { unmockkAll() }

  it("should send didChange notification for each projects related to document") {
    every { file.uri } returns "file:///Users/jetbrains-plugin/main.kt"
    every { file.modificationStamp } returns 1L
    every { document.text } returns "Hello, World!"

    listener.documentChanged(event)

    val params = mutableListOf<DidChangeTextDocumentParams>()
    verify(exactly = 2) { languageServer.textDocumentService.didChange(capture(params)) }
    params[0].textDocument.uri shouldBe "file:///Users/jetbrains-plugin/main.kt"
    params[0].textDocument.version shouldBe 1
    params[0].contentChanges[0].text shouldBe "Hello, World!"

    params[1].textDocument.uri shouldBe "file:///Users/jetbrains-plugin/main.kt"
    params[1].textDocument.version shouldBe 1
    params[1].contentChanges[0].text shouldBe "Hello, World!"
  }

  it("it should send didChangeDocumentInActiveEditor notification when a different file is updated") {
    // Active file is set on the first update
    every { file.uri } returns "file:///Users/jetbrains-plugin/main.kt"

    listener.documentChanged(event)

    val uris = mutableListOf<String>()
    verify(exactly = 2) { languageServer.didChangeDocumentInActiveEditor(capture(uris)) }
    uris[0] shouldBe "file:///Users/jetbrains-plugin/main.kt"
    uris[1] shouldBe "file:///Users/jetbrains-plugin/main.kt"

    // Updating the same file doesn't change the active file
    listener.documentChanged(event)
    verify(exactly = 2) { languageServer.didChangeDocumentInActiveEditor(any()) }

    // Updating a new file changes the active file to the new one
    every { otherFile.url } returns "file:///Users/jetbrains-plugin/completion.kt"
    every { otherFile.modificationStamp } returns 1L
    every { fileDocumentManager.getFile(document) } returns otherFile

    listener.documentChanged(event)

    val updatedURIs = mutableListOf<String>()
    verify(exactly = 3) { languageServer.didChangeDocumentInActiveEditor(capture(updatedURIs)) }
    updatedURIs[2] shouldBe "file:///Users/jetbrains-plugin/completion.kt"
  }
})
