package com.gitlab.plugin.exceptions

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.DuoPersistentSettings
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll

class ExceptionSanitizerTest : DescribeSpec({
  val settings = mockk<DuoPersistentSettings>()
  val sanitizer = ExceptionSanitizer()

  beforeEach {
    val application = mockApplication()
    every { application.getService(DuoPersistentSettings::class.java) } returns settings

    every { settings.url } returns "https://gitlab.com"
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("should remove windows file paths from exceptions") {
    val exception = Exception("Error occurred: C:\\Users\\JohnDoe\\Documents\\project\\file.kt not found")

    val sanitizedException = sanitizer.sanitize(exception)

    sanitizedException.message shouldBe "Error occurred: local_file_path not found"
    sanitizedException.cause shouldBe null
  }

  it("should remove unix file paths from exceptions") {
    val exception = Exception("Error occurred: /home/johndoe/project/file.kt not found")

    val sanitizedException = sanitizer.sanitize(exception)

    sanitizedException.message shouldBe "Error occurred: local_file_path not found"
    sanitizedException.cause shouldBe null
  }

  it("should remove configured gitlab url from exceptions") {
    every { settings.url } returns "https://my.gitlab.com"
    val exception1 = Exception("Could not connect to https://my.gitlab.com/api/v4/projects/123 (500)")
    val exception2 = Exception("Could not connect to https://my.gitlab.com (500)")

    val sanitizedException1 = sanitizer.sanitize(exception1)
    val sanitizedException2 = sanitizer.sanitize(exception2)

    sanitizedException1.message shouldBe "Could not connect to gitlab_url (500)"
    sanitizedException1.cause shouldBe null

    sanitizedException2.message shouldBe "Could not connect to gitlab_url (500)"
    sanitizedException2.cause shouldBe null
  }

  it("should remove configured gitlab url from websockets exceptions") {
    every { settings.url } returns "https://my.gitlab.com"
    val exception1 = Exception("Could not connect to wss://my.gitlab.com/api/v4/projects/123 (500)")
    val sanitizedException1 = sanitizer.sanitize(exception1)
    sanitizedException1.message shouldBe "Could not connect to websocket_gitlab_url (500)"
    sanitizedException1.cause shouldBe null

    every { settings.url } returns "http://my.gitlab.com"
    val exception2 = Exception("Could not connect to ws://my.gitlab.com (500)")
    val sanitizedException2 = sanitizer.sanitize(exception2)
    sanitizedException2.message shouldBe "Could not connect to websocket_gitlab_url (500)"
    sanitizedException2.cause shouldBe null
  }

  it("should remove personal access token from exceptions") {
    val exception = Exception("Could not substring glpat-89321798372189 index -1")

    val sanitizedException = sanitizer.sanitize(exception)

    sanitizedException.message shouldBe "Could not substring gitlab_personal_access_token index -1"
    sanitizedException.cause shouldBe null
  }

  it("should remove ip addresses from exceptions") {
    val exception = Exception("Connection failed: 192.168.1.100:8080 (500)")

    val sanitizedException = sanitizer.sanitize(exception)

    sanitizedException.message shouldBe "Connection failed: ip_address (500)"
    sanitizedException.cause shouldBe null
  }

  it("should ignore urls that are not the configured one") {
    val exception1 = Exception("Error occurred: https://example.com/todos (500)")
    val exception2 = Exception("Error occurred: wss://example.com/todos (500)")

    val sanitizedException1 = sanitizer.sanitize(exception1)
    val sanitizedException2 = sanitizer.sanitize(exception2)

    sanitizedException1.message shouldBe "Error occurred: https://example.com/todos (500)"
    sanitizedException1.cause shouldBe null

    sanitizedException2.message shouldBe "Error occurred: wss://example.com/todos (500)"
    sanitizedException2.cause shouldBe null
  }
})
