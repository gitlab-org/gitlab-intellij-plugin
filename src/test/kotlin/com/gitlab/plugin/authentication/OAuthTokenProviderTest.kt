package com.gitlab.plugin.authentication

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.util.TokenUtil
import com.intellij.collaboration.auth.services.OAuthCredentialsAcquirer
import com.intellij.openapi.components.service
import com.intellij.util.application
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import java.util.concurrent.Executors
import kotlin.test.assertEquals
import kotlin.time.Duration.Companion.seconds

class OAuthTokenProviderTest : DescribeSpec({
  lateinit var tokenProvider: OAuthTokenProvider
  val oAuthService = mockk<GitLabOAuthService>()
  val settings = mockk<DuoPersistentSettings>(relaxed = true)
  val timerRefreshInSeconds = 1

  beforeEach {
    mockkObject(TokenUtil)
    mockApplication()

    every { application.service<DuoPersistentSettings>() } returns settings
    val tokenUpdateLanguageServerService = mockk<TokenUpdateLanguageServerService>()
    every { application.service<TokenUpdateLanguageServerService>() } returns tokenUpdateLanguageServerService
    every { tokenUpdateLanguageServerService.updateTokenLSConfiguration(any()) } just Runs
    every { settings.oauthEnabled } returns true
    every { application.service<GitLabOAuthService>() } returns oAuthService
    every { oAuthService.authorize() } returns mockk()
    every { TokenUtil.setOAuthToken(any()) } just Runs

    tokenProvider = OAuthTokenProvider()
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("token") {
    it("token returns empty string when no token is set") {
      every { TokenUtil.getOAuthToken() } returns null
      assertEquals("", tokenProvider.token())
    }

    it("sets oauthEnabled to false when no token is set") {
      every { TokenUtil.getOAuthToken() } returns null
      tokenProvider.token()
      verify { settings.oauthEnabled = false }
    }

    it("token returns valid token when not expired") {
      val validToken = GitLabAuthorizationToken("valid_token", "refresh_token", 3600, Clock.System.now().epochSeconds)

      tokenProvider.updateToken(validToken)

      assertEquals("valid_token", tokenProvider.token())
    }

    it("token returns a valid token when token is already cached in PasswordSafe") {
      every { TokenUtil.getOAuthToken() } returns GitLabAuthorizationToken(
        "valid_token",
        "refresh_token",
        3600,
        Clock.System.now().epochSeconds
      )
      // We update the token with null to simulate an IDE restart; the token should be retrieved from the PasswordSafe cache using TokenUtil
      tokenProvider.updateToken(null)

      assertEquals("valid_token", tokenProvider.token())
      verify(exactly = 1) { TokenUtil.getOAuthToken() }
    }
  }

  describe("refreshToken") {
    it("token returns valid token when not expired") {
      val validToken = GitLabAuthorizationToken("valid_token", "refresh_token", 3600, Clock.System.now().epochSeconds)

      tokenProvider.updateToken(validToken)

      assertEquals("valid_token", tokenProvider.token())
    }

    context("when expired") {
      it("refreshes token") {
        val oldToken = GitLabAuthorizationToken(
          "old_token",
          "refresh_token",
          3600,
          Clock.System.now().epochSeconds - 3601
        )
        val newToken = GitLabAuthorizationToken(
          "new_token",
          "refresh_token",
          3600,
          Clock.System.now().epochSeconds
        )

        every {
          oAuthService.refreshToken(oldToken)
        } returns OAuthCredentialsAcquirer.AcquireCredentialsResult.Success(newToken)

        tokenProvider.updateToken(oldToken)

        assertEquals("new_token", tokenProvider.token())
      }

      it("checks PasswordSafe for a new token if refresh fails") {
        val oldToken = GitLabAuthorizationToken(
          "old_token",
          "refresh_token",
          3600,
          Clock.System.now().epochSeconds - 3601
        )
        val newToken = GitLabAuthorizationToken(
          "new_token",
          "refresh_token",
          3600,
          Clock.System.now().epochSeconds
        )

        every {
          oAuthService.refreshToken(oldToken)
        } returns OAuthCredentialsAcquirer.AcquireCredentialsResult.Error("Some error")
        every { TokenUtil.getOAuthToken() } returns newToken

        tokenProvider.updateToken(oldToken)

        assertEquals("new_token", tokenProvider.token())
      }
    }

    it("sets token to null if PasswordSafe doesn't have a newer one") {
      val oldToken = GitLabAuthorizationToken(
        "old_token",
        "refresh_token",
        3600,
        Clock.System.now().epochSeconds - 3601
      )

      every {
        oAuthService.refreshToken(oldToken)
      } returns OAuthCredentialsAcquirer.AcquireCredentialsResult.Error("Some error")
      every { TokenUtil.getOAuthToken() } returns oldToken
      every { settings.oauthEnabled } returns true andThen false

      tokenProvider.updateToken(oldToken)

      verify { TokenUtil.setOAuthToken(null) }
      verify { settings.oauthEnabled = false }
      verify(exactly = 0) { settings.oauthEnabled = true }

      assertEquals("", tokenProvider.token())
    }
  }

  describe("startTokenRefreshTimer") {
    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    it("timer shouldn't start when OAuth is not enabled") {
      every { settings.oauthEnabled } returns false

      tokenProvider.startTokenRefreshTimer(timerRefreshInSeconds)

      verify(exactly = 0) {
        oAuthService.refreshToken(any())
      }
    }

    it("token refreshes periodically when OAuth is enabled") {
      val realScheduler = Executors.newScheduledThreadPool(1)
      tokenProvider.scheduler = realScheduler

      val newToken =
        GitLabAuthorizationToken("new_token", "refresh_token", 3600, Clock.System.now().epochSeconds)

      every { oAuthService.refreshToken(any()) } returns OAuthCredentialsAcquirer.AcquireCredentialsResult.Success(newToken)

      val expiredToken = GitLabAuthorizationToken(
        "expired_token",
        "refresh_token",
        3600,
        Clock.System.now().minus(Instant.fromEpochSeconds(5000)).inWholeSeconds
      )

      // Make sure the token provider has an existing token that is expired
      tokenProvider.updateToken(expiredToken)

      tokenProvider.startTokenRefreshTimer(timerRefreshInSeconds)

      delay(2.seconds)

      assertEquals("new_token", tokenProvider.token())

      verify(exactly = 1) { oAuthService.refreshToken(any()) }

      realScheduler.shutdownNow()
    }
  }
})
