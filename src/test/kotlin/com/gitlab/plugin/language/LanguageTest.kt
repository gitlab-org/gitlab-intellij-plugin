package com.gitlab.plugin.language

import com.intellij.openapi.fileTypes.FileType
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk

class LanguageTest : DescribeSpec({
  describe("Language.getInstance") {
    it("returns correct Language for known file extension") {
      val mockFileType = mockk<FileType>()
      every { mockFileType.defaultExtension } returns "kt"

      val result = Language.getInstance(mockFileType)

      result.id shouldBe "kotlin"
      result.name shouldBe "Kotlin"
      result.fileExtensions shouldBe listOf("kt", "kts")
      result.isJetBrainsSupported shouldBe true
    }

    it("returns custom Language for unknown file extension") {
      val mockFileType = mockk<FileType>()
      every { mockFileType.defaultExtension } returns "unknown"
      every { mockFileType.name } returns "Unknown"
      every { mockFileType.displayName } returns "Unknown File Type"

      val result = Language.getInstance(mockFileType)

      result.id shouldBe "Unknown"
      result.name shouldBe "Unknown File Type"
      result.fileExtensions shouldBe listOf("unknown")
      result.isJetBrainsSupported shouldBe false
    }
  }

  describe("Language.matches") {
    val language = Language("kotlin", "Kotlin", listOf("kt", "kts"), true)

    it("matches with exact id") {
      language.matches("kotlin") shouldBe true
    }

    it("matches with case-insensitive id") {
      language.matches("KOTLIN") shouldBe true
    }

    it("matches with exact displayName") {
      language.matches("Kotlin (kotlin)") shouldBe true
    }

    it("matches with case-insensitive displayName") {
      language.matches("KOTLIN (KOTLIN)") shouldBe true
    }

    it("does not match with incorrect id") {
      language.matches("java") shouldBe false
    }

    it("does not match with incorrect displayName") {
      language.matches("Java (java)") shouldBe false
    }

    it("does not match with partial id") {
      language.matches("kot") shouldBe false
    }

    it("does not match with partial displayName") {
      language.matches("Kotlin (kot)") shouldBe false
    }
  }
})
