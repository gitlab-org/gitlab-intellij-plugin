package com.gitlab.plugin.settings

import com.gitlab.plugin.GitLabBundle.message
import com.gitlab.plugin.api.mockHttpClient
import com.gitlab.plugin.authentication.*
import com.gitlab.plugin.language.Language
import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.services.health.ConfigurationValidationService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.components.service
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.ui.ComboBox
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.registerOrReplaceServiceInstance
import com.intellij.ui.components.ActionLink
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBPasswordField
import com.intellij.ui.components.JBTextField
import com.intellij.util.application
import io.kotest.assertions.nondeterministic.eventually
import io.mockk.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.jetbrains.ide.BuiltInServerManager
import org.junit.jupiter.api.*
import java.awt.Component
import java.awt.Container
import java.net.URI
import java.net.URLDecoder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Instant
import java.util.regex.Pattern
import javax.swing.AbstractButton
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.JLabel
import javax.swing.event.ChangeEvent
import javax.swing.text.JTextComponent
import kotlin.random.Random
import kotlin.reflect.KMutableProperty0
import kotlin.test.assertNotEquals
import kotlin.time.Duration.Companion.seconds

const val OAUTH_TAB_INDEX = 0
const val PAT_TAB_INDEX = 1
const val ONE_PASSWORD_TAB_INDEX = 2

// Integration tests for settings configured through the settings configurable
class GitLabSettingsTest : BasePlatformTestCase() {

  //region class properties
  private lateinit var configurable: GitLabSettingsConfigurable
  private lateinit var languageServerSettings: LanguageServerSettings
  private lateinit var configurationValidationService: ConfigurationValidationService
  private lateinit var gitLabSettingsConfigurableComponent: JComponent
  private lateinit var settings: DuoPersistentSettings

  private lateinit var urlField: JBTextField
  private lateinit var codeSuggestionsEnabledCheckbox: JBCheckBox
  private lateinit var duoChatEnabledCheckbox: JBCheckBox
  private lateinit var verifyButton: JButton
  private lateinit var patField: JBPasswordField
  private lateinit var onePasswordCheckbox: JBCheckBox
  private lateinit var onePasswordSecretReferenceField: JBTextField
  private lateinit var onePasswordAccountsComboBox: ComboBox<*>
  private lateinit var jbSupportedLanguageCheckBoxes: List<JBCheckBox>
  private lateinit var additionalLanguagesTextField: JBTextField

  private val coroutineScope = TestScope(UnconfinedTestDispatcher())
  //endregion

  @BeforeEach
  public override fun setUp() {
    super.setUp()

    mockkConstructor(SettingsHealthChecker::class)
    coEvery { anyConstructed<SettingsHealthChecker>().check(any()) } returns Unit

    languageServerSettings = LanguageServerSettings()

    configurationValidationService = mockk<ConfigurationValidationService> {
      coEvery { validateConfiguration(any()) } returns mapOf(
        FeatureId.AUTHENTICATION to FeatureStateParams(
          featureId = FeatureId.AUTHENTICATION,
          engagedChecks = listOf(FeatureStateCheckParams(checkId = "authentication-failed-1"))
        )
      )
    }

    project.registerOrReplaceServiceInstance(
      LanguageServerSettings::class.java,
      languageServerSettings,
      testRootDisposable
    )

    project.registerOrReplaceServiceInstance(
      ConfigurationValidationService::class.java,
      configurationValidationService,
      testRootDisposable
    )

    configurable = GitLabSettingsConfigurable(project, coroutineScope)

    // The form will not validate if the url is not set
    settings = DuoPersistentSettings.getInstance()
      .apply { loadState(DuoPersistentSettings.State()) }

    gitLabSettingsConfigurableComponent = configurable.createComponent()

    urlField = gitLabSettingsConfigurableComponent.findComponent("URL to GitLab instance") as JBTextField

    codeSuggestionsEnabledCheckbox =
      gitLabSettingsConfigurableComponent.findComponent("Enable Code Suggestions") as JBCheckBox

    duoChatEnabledCheckbox = gitLabSettingsConfigurableComponent.findComponent("Enable GitLab Duo Chat") as JBCheckBox

    verifyButton = gitLabSettingsConfigurableComponent.findComponent("Verify setup") as JButton

    onePasswordCheckbox =
      gitLabSettingsConfigurableComponent.findComponent("Integrate with 1Password CLI") as JBCheckBox

    onePasswordSecretReferenceField =
      gitLabSettingsConfigurableComponent.findComponent("Secret reference:") as JBTextField

    onePasswordAccountsComboBox = gitLabSettingsConfigurableComponent.findComponent("1Password account:") as ComboBox<*>

    setUpPatToken()
  }

  @AfterEach
  override fun tearDown() {
    super.tearDown()
    unmockkAll()

    // Reset this, as it can persist from test to test
    application.service<OAuthTokenProvider>().updateToken(null)
  }

  @Test
  fun `stores PAT token on apply`() {
    val tabbedPane = configurable.authTabbedPane.component
    val token = setUpPatToken()

    assertNotEquals(token, application.service<GitLabTokenProviderManager>().getToken())
    assertEquals(tabbedPane.getIconAt(PAT_TAB_INDEX), null)

    configurable.apply()

    assertEquals(token, application.service<GitLabTokenProviderManager>().getToken())

    runBlocking {
      eventually(1.seconds) {
        assertEquals(tabbedPane.getIconAt(PAT_TAB_INDEX), GitLabIcons.Status.Success)
        assertEquals(tabbedPane.getIconAt(OAUTH_TAB_INDEX), null)
        assertEquals(tabbedPane.getIconAt(ONE_PASSWORD_TAB_INDEX), null)
      }
    }
  }

  @Nested
  inner class OnePasswordIntegrationTests {
    val onePasswordTokenProvider = mockk<OnePasswordTokenProvider>()
    lateinit var component: JComponent
    lateinit var onePasswordCheckbox: JBCheckBox
    lateinit var onePasswordSecretReferenceField: JBTextField
    lateinit var onePasswordAccountDropdown: ComboBox<*>

    @BeforeEach
    fun setUp1Password() {
      every { onePasswordTokenProvider.getTokenBySecretReference(any(), any()) } returns "1password-token"
      every { onePasswordTokenProvider.getAccounts() } returns emptyList()

      application.registerOrReplaceServiceInstance(
        OnePasswordTokenProvider::class.java,
        onePasswordTokenProvider,
        testRootDisposable
      )

      component = configurable.createComponent()
      onePasswordCheckbox = component.findComponent("Integrate with 1Password CLI") as JBCheckBox
      onePasswordSecretReferenceField = component.findComponent("Secret reference:") as JBTextField
      onePasswordAccountDropdown = component.findComponent("1Password account:") as ComboBox<*>
    }

    @Test
    fun `stores token from 1Password on apply`() {
      val tabbedPane = configurable.authTabbedPane.component

      configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)
      onePasswordCheckbox.doClick()
      onePasswordSecretReferenceField.text = "op://Private/pat/token"

      assertNotEquals("1password-token", application.service<GitLabTokenProviderManager>().getToken())
      assertEquals(tabbedPane.getIconAt(ONE_PASSWORD_TAB_INDEX), null)

      mockTokenAcceptance()

      configurable.apply()

      verify { onePasswordTokenProvider.getTokenBySecretReference("op://Private/pat/token") }

      assertEquals("1password-token", application.service<GitLabTokenProviderManager>().getToken())

      runBlocking {
        eventually(1.seconds) {
          assertEquals(GitLabIcons.Status.Success, tabbedPane.getIconAt(ONE_PASSWORD_TAB_INDEX))
          assertEquals(null, tabbedPane.getIconAt(OAUTH_TAB_INDEX))
          assertEquals(null, tabbedPane.getIconAt(PAT_TAB_INDEX))
        }
      }
    }

    @Test
    fun `stores token when user has multiple 1Password accounts`() {
      every { onePasswordTokenProvider.getAccounts() } returns listOf(
        OnePasswordTokenProvider.Account(
          url = "myfirstaccount.1password.com",
          email = "name@example.com"
        ),
        OnePasswordTokenProvider.Account(
          url = "mysecondaccount.1password.com",
          email = "name@example.com"
        )
      )

      configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)
      onePasswordCheckbox.doClick()

      val items = (0 until onePasswordAccountDropdown.itemCount).map {
        onePasswordAccountDropdown.model.getElementAt(it)
      }

      runBlocking {
        eventually(1.seconds) {
          assertEquals(listOf("myfirstaccount.1password.com", "mysecondaccount.1password.com"), items)
        }
      }

      onePasswordAccountDropdown.selectedItem = "mysecondaccount.1password.com"
      onePasswordSecretReferenceField.text = "op://Private/pat/token"
      configurable.apply()

      assertEquals("mysecondaccount.1password.com", DuoPersistentSettings.getInstance().integrate1PasswordAccount)

      application.service<GitLabTokenProviderManager>().getToken()

      verify(exactly = 1) {
        onePasswordTokenProvider.getTokenBySecretReference("op://Private/pat/token", "mysecondaccount.1password.com")
      }
    }

    @Test
    fun `fills in 1Password account from settings as selected item and maintains selected item`() {
      every { onePasswordTokenProvider.getAccounts() } returns listOf(
        OnePasswordTokenProvider.Account(
          url = "myfirstaccount.1password.com",
          email = "name@example.com"
        ),
        OnePasswordTokenProvider.Account(
          url = "mysecondaccount.1password.com",
          email = "name@example.com"
        ),
        OnePasswordTokenProvider.Account(
          url = "mythirdaccount.1password.com",
          email = "name@example.com"
        )
      )

      DuoPersistentSettings.getInstance().integrate1PasswordAccount = "mysecondaccount.1password.com"

      configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)

      runBlocking {
        eventually(1.seconds) {
          assertEquals("mysecondaccount.1password.com", onePasswordAccountDropdown.selectedItem)
        }
      }

      onePasswordAccountDropdown.selectedItem = "mythirdaccount.1password.com"

      // Maintains selectedItem after switch to another tab and back (which triggers a re-fetch)
      configurable.setActiveAuthTab(AuthTab.OAUTH)
      configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)

      runBlocking {
        eventually(1.seconds) {
          assertEquals("mythirdaccount.1password.com", onePasswordAccountDropdown.selectedItem)
        }
      }
    }

    @Test
    fun `shows error when no 1Password accounts found and refreshes on link click`() {
      every { onePasswordTokenProvider.getAccounts() } returns emptyList()

      configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)
      onePasswordCheckbox.doClick()

      val warning = component.findComponent(
        "No 1Password account found".toRegex()
      ) as JTextComponent

      runBlocking {
        eventually(1.seconds) {
          assertTrue(warning.isVisible)
          assertNull(onePasswordAccountDropdown.selectedItem)
        }
      }

      every { onePasswordTokenProvider.getAccounts() } returns listOf(
        OnePasswordTokenProvider.Account(
          url = "myaccount.1password.com",
          email = "name@example.com"
        )
      )

      val retryLink = component.findComponent("Refresh") as ActionLink

      retryLink.doClick()

      runBlocking {
        eventually(1.seconds) {
          assertFalse(warning.isVisible)

          val items = (0 until onePasswordAccountDropdown.itemCount).map {
            onePasswordAccountDropdown.model.getElementAt(
              it
            )
          }
          assertEquals(listOf("myaccount.1password.com"), items)
        }
      }
    }
  }

  @Nested
  inner class FeatureIntegrationTests {

    @Test
    fun `Enable Code Suggestions checkbox changes are saved properly`() {
      testCheckBox(
        "settings.ui.gitlab.enable-duo-code-suggestions",
        settings::codeSuggestionsEnabled
      )
    }

    @Test
    fun `Enable GitLab Duo Chat checkbox changes are saved properly`() {
      testCheckBox(
        "settings.ui.gitlab.enable-duo-chat",
        settings::duoChatEnabled
      )
    }
  }

  @Nested
  inner class ConnectionIntegrationTests {

    @Test
    fun `GitLab instance URL changes are saved accordingly`() {
      val expectedUrl = "https://test.com"
      urlField.text = expectedUrl
      configurable.apply()
      assertEquals(expectedUrl, settings.url)

      // Trailing slash should be removed on apply
      urlField.text = "$expectedUrl/"
      configurable.apply()
      assertEquals(expectedUrl, settings.url)
    }

    @Test
    fun `Ignore certificate errors checkbox changes are saved properly`() {
      testCheckBox(
        "settings.ui.gitlab.ignore-certificate-errors-label",
        settings::ignoreCertificateErrors
      )
    }
  }

  @Nested
  inner class AuthenticationIntegrationTests {

    @Test
    fun `Authentication reverts to PAT after OAuth revocation`() {
      // Set up PAT
      val patToken = setUpPatToken()
      configurable.apply()

      // Authenticate with OAuth
      settings.oauthEnabled = true

      val oauthProvider = application.service<OAuthTokenProvider>()
      val oauthToken = "oauth-token"
      oauthProvider.updateToken(
        GitLabAuthorizationToken(
          accessToken = oauthToken,
          refreshToken = "magna",
          expiresIn = 10000,
          createdAt = Instant.now().epochSecond
        )
      )

      // Recreate configurable to detect that OAuth has been enabled
      configurable = GitLabSettingsConfigurable(project, coroutineScope)
      configurable.createComponent()

      assertEquals(oauthToken, application.service<GitLabTokenProviderManager>().getToken())
      assertEquals(OAUTH_TAB_INDEX, configurable.authTabbedPane.component.selectedIndex)

      // Revoke OAuth
      oauthProvider.updateToken(null)

      // Recreate configurable to detect that OAuth has been revoked
      configurable = GitLabSettingsConfigurable(project, coroutineScope)
      configurable.createComponent()

      // Check if authentication reverted to PAT
      assertEquals(patToken, application.service<GitLabTokenProviderManager>().getToken())
      assertEquals(PAT_TAB_INDEX, configurable.authTabbedPane.component.selectedIndex)
    }

    @Test
    fun `apply() fails if no auth method is defined`() {
      settings.oauthEnabled = false
      settings.integrate1PasswordCLI = false

      configurable = GitLabSettingsConfigurable(project, coroutineScope)
      val component = configurable.createComponent()

      val patField = component.findComponent("GitLab Personal Access Token") as JBPasswordField
      patField.text = ""

      assertThrows<ConfigurationException> { configurable.apply() }
    }
  }

  @Nested
  inner class AdvancedIntegrationTests {

    @Test
    fun `Enable telemetry checkbox changes are saved properly`() {
      testCheckBox(
        checkBoxLabel = "settings.ui.gitlab.enable-telemetry",
        actualState = settings::telemetryEnabled
      )
    }

    @Test
    fun `Restart language server button triggers restart on click`() {
      val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)

      project.registerOrReplaceServiceInstance(
        GitLabLanguageServerService::class.java,
        languageServerService,
        testRootDisposable
      )

      languageServerService.serverRunning

      val restartButton = gitLabSettingsConfigurableComponent
        .findComponent(message("settings.ui.language-server.restart")) as JButton

      restartButton.doClick()

      verify(exactly = 1) { languageServerService.restart() }
    }

    @Test
    fun `Log level changes are saved properly`() {
      val textField = gitLabSettingsConfigurableComponent.findComponent("Log Level") as JBTextField
      val expectedLogLevel = "foo"

      textField.text = expectedLogLevel
      configurable.apply()

      assertEquals(expectedLogLevel, languageServerSettings.state.workspaceSettings.logLevel)
      assertEquals(expectedLogLevel, textField.text)
    }

    @Test
    fun `Stream code generations checkbox changes are saved properly`() {
      testCheckBox(
        "settings.ui.gitlab.advanced.stream-code-generations",
        languageServerSettings.state.workspaceSettings.featureFlags::streamCodeGenerations
      )
    }

    @Test
    fun `HTTP agent options field changes are saved properly`() {
      val passCaCheckBox = testCheckBox(
        "settings.ui.gitlab.advanced.pass-ca-cert",
        languageServerSettings.state.workspaceSettings.httpAgentOptions::pass
      )

      passCaCheckBox.isSelected = true

      var textField = gitLabSettingsConfigurableComponent
        .findComponent(message("settings.ui.gitlab.advanced.ca")) as JBTextField
      textField.text = "foo"
      configurable.apply()
      assertEquals("foo", textField.text)
      assertEquals("foo", languageServerSettings.state.workspaceSettings.httpAgentOptions.ca)

      textField = gitLabSettingsConfigurableComponent
        .findComponent(message("settings.ui.gitlab.advanced.cert")) as JBTextField
      textField.text = "foo"
      configurable.apply()
      assertEquals("foo", textField.text)
      assertEquals("foo", languageServerSettings.state.workspaceSettings.httpAgentOptions.cert)

      textField = gitLabSettingsConfigurableComponent
        .findComponent(message("settings.ui.gitlab.advanced.cert-key")) as JBTextField
      textField.text = "foo"
      configurable.apply()
      assertEquals("foo", textField.text)
      assertEquals("foo", languageServerSettings.state.workspaceSettings.httpAgentOptions.certKey)
    }
  }

  @Nested
  inner class CodeSuggestionsIntegrationTests {

    private val additionalLanguages = mutableSetOf("foo", "bar")
    private val jbSupportedLanguages = Language.languageList.filter { it.isJetBrainsSupported }

    @BeforeEach
    fun setUp() {
      Language
        .languageList
        .filter { it.isJetBrainsSupported }
        .map { language ->
          gitLabSettingsConfigurableComponent.findComponent(language.displayName) as JBCheckBox
        }
        .let { jbSupportedLanguageCheckBoxes = it }

      additionalLanguagesTextField =
        gitLabSettingsConfigurableComponent.findComponent("Additional languages") as JBTextField

      additionalLanguagesTextField.text = additionalLanguages.joinToString(", ")
    }

    @Test
    fun `All supported languages are enabled by default`() {
      jbSupportedLanguageCheckBoxes.forEach {
        assertTrue(it.isSelected)
      }
    }

    @Test
    fun `When all supported languages are disabled, language server settings state should reflect that accordingly`() {
      jbSupportedLanguageCheckBoxes.forEach {
        it.isSelected = false
      }

      configurable.apply()

      val disabledLanguages = languageServerSettings.state.workspaceSettings.codeCompletion.disabledSupportedLanguages

      jbSupportedLanguages.forEach { jbSupportedLanguage ->
        assertTrue(disabledLanguages.any { it == jbSupportedLanguage.id })
      }
    }

    @Test
    fun `When some supported languages are disabled, language server settings state should reflect that accordingly`() {
      val expectedDisabledLanguages = listOf(
        jbSupportedLanguages[1],
        jbSupportedLanguages[2],
        jbSupportedLanguages[3],
      )

      jbSupportedLanguageCheckBoxes[1].isSelected = false
      jbSupportedLanguageCheckBoxes[2].isSelected = false
      jbSupportedLanguageCheckBoxes[3].isSelected = false

      configurable.apply()

      val disabledLanguages = languageServerSettings.state.workspaceSettings.codeCompletion.disabledSupportedLanguages

      disabledLanguages.forEach { actual ->
        assertTrue(expectedDisabledLanguages.any { it.id == actual })
      }
    }

    @Test
    fun `Additional languages are loaded correctly`() {
      additionalLanguagesTextField.text.split(",").map { it.trim() }.forEach { additionalLanguage ->
        assertTrue(additionalLanguages.any { it == additionalLanguage })
      }
    }

    @Test
    fun `When additional languages are set, language server settings state should reflect that accordingly`() {
      configurable.apply()

      val actualAdditionalLanguages = languageServerSettings.state.workspaceSettings.codeCompletion.additionalLanguages

      actualAdditionalLanguages.forEach { actual ->
        assertTrue(additionalLanguages.any { it == actual })
      }
    }

    @Test
    fun `When send open tabs as context checkbox is set, language server settings state should reflect that accordingly`() {
      testCheckBox(
        "settings.ui.gitlab.verify-setup.code-suggestions.open-tabs",
        languageServerSettings.state.workspaceSettings.codeCompletion::enableOpenTabsContext
      )
    }
  }

  @Test
  @Suppress("LongMethod")
  fun `completes OAuth flow on link click`() {
    // Mock IntelliJ method for opening browser (used for opening tab where user confirms OAuth authentication)
    mockkStatic(BrowserUtil::class)
    every { BrowserUtil.browse(any<String>()) } just Runs

    configurable = GitLabSettingsConfigurable(project, coroutineScope)

    val component = configurable.createComponent()

    configurable.setActiveAuthTab(AuthTab.OAUTH)

    assertNotEquals("oauth-token", application.service<GitLabTokenProviderManager>().getToken())

    val oauthLoginLink = component.findComponent("Login with GitLab account") as ActionLink
    val oauthRevokeLink = component.findComponent("Revoke access to GitLab account") as ActionLink
    val oauthEnabledLabel = component.findComponent("Authenticated") as JLabel

    assertTrue(oauthLoginLink.isVisible)
    assertFalse(oauthRevokeLink.isVisible)
    assertFalse(oauthEnabledLabel.isVisible)
    assertEquals(configurable.authTabbedPane.component.getIconAt(OAUTH_TAB_INDEX), null)

    // Click the OAuth action link
    oauthLoginLink.doClick()

    val authPageUrlSlot = slot<String>()

    // Verify that the OAuth browser tab was opened with the correct URL
    runBlocking {
      eventually(3.seconds) {
        verify(exactly = 1) { BrowserUtil.browse(capture(authPageUrlSlot)) }
      }
    }

    val authPageUri = URI(authPageUrlSlot.captured)
    val queryParams = authPageUri.query.split("&").associate {
      val (key, value) = it.split("=")
      key to URLDecoder.decode(value, "UTF-8")
    }

    assertEquals(
      "https://gitlab.com/oauth/authorize",
      "${authPageUri.scheme}://${authPageUri.host}${authPageUri.path}"
    )
    assertTrue(
      queryParams["redirect_uri"]?.contains(
        "http://127.0.0.1:${BuiltInServerManager.getInstance().port}/api/oauth/gitlab/authorization"
      ) ?: false
    )

    val codeChallenge = queryParams["code_challenge"]
    val state = queryParams["state"]

    val realClient = HttpClient.newHttpClient()

    // Prepare to receive callback from by mocking the token request that we will subsequently make
    val mockedClient = mockHttpClient(
      """
        {
          "access_token":"oauth-token",
          "token_type":"Bearer",
          "expires_in":7200,
          "refresh_token":"abcabcabc",
          "scope":"api",
          "created_at":${System.currentTimeMillis()}
        }
      """.trimIndent()
    )

    // Make sure built-in server is running
    val builtInServerManager = BuiltInServerManager.getInstance()
    builtInServerManager.waitForStart()

    // Hit OAuth callback route on built-in server with mocked callback
    val request = HttpRequest.newBuilder()
      .uri(
        URI.create(
          "http://127.0.0.1:${BuiltInServerManager.getInstance().port}/api/oauth/gitlab/authorization?code=$codeChallenge&state=$state"
        )
      ).GET().build()
    realClient.send(request, HttpResponse.BodyHandlers.ofString())

    // Verify token request was made as expected
    val tokenRequestSlot = slot<HttpRequest>()

    runBlocking {
      eventually(3.seconds) {
        verify(exactly = 1) {
          mockedClient.send(capture(tokenRequestSlot), any<HttpResponse.BodyHandler<String>>())
        }
      }
    }

    val tokenRequest = tokenRequestSlot.captured
    val tokenRequestUri = tokenRequest.uri()
    val tokenRequestParams = tokenRequestUri.query.split("&").associate {
      val (key, value) = it.split("=")
      URLDecoder.decode(key, "UTF-8") to URLDecoder.decode(value, "UTF-8")
    }

    assertEquals(
      "https://gitlab.com/oauth/token",
      "${tokenRequestUri.scheme}://${tokenRequestUri.host}${tokenRequestUri.path}"
    )
    assertEquals(codeChallenge, tokenRequestParams["code"])
    assertEquals("authorization_code", tokenRequestParams["grant_type"])

    // ID of the actual OAuth application in GitLab
    val expectedClientId = "732efe7d78ba62b01d2a95cd3b02f3ca2a00c7cec32c01e060c1b7664a9aa367"
    assertEquals(expectedClientId, tokenRequestParams["client_id"])

    // Verify OAuth token was stored
    runBlocking {
      eventually(3.seconds) {
        assertEquals("oauth-token", application.service<GitLabTokenProviderManager>().getToken())
      }
    }

    assertFalse(oauthLoginLink.isVisible)
    assertTrue(oauthRevokeLink.isVisible)
    assertEquals(configurable.authTabbedPane.component.getIconAt(OAUTH_TAB_INDEX), GitLabIcons.Status.Success)
    assertEquals(configurable.authTabbedPane.component.getIconAt(PAT_TAB_INDEX), null)
    assertEquals(configurable.authTabbedPane.component.getIconAt(ONE_PASSWORD_TAB_INDEX), null)
    assertTrue(oauthEnabledLabel.isVisible)
    assertEquals(GitLabIcons.Status.Success, oauthEnabledLabel.icon)
  }

  @Test
  fun `Verify button is visible when OAuth tab is selected and OAuth is enabled`() {
    DuoPersistentSettings.getInstance().oauthEnabled = true
    configurable.setActiveAuthTab(AuthTab.OAUTH)

    configurable.authTabbedPane.component.changeListeners[0].stateChanged(mockk<ChangeEvent>())

    runBlocking {
      eventually(1.seconds) {
        assertEquals(verifyButton.isVisible, true)
      }
    }

    assertEquals(patField.isEnabled, false)
    assertEquals(onePasswordCheckbox.isEnabled, false)
    assertEquals(onePasswordAccountsComboBox.isEnabled, false)
    assertEquals(onePasswordSecretReferenceField.isEnabled, false)
  }

  @Test
  fun `Verify button is NOT visible when OAuth tab is selected and OAuth is disabled`() {
    DuoPersistentSettings.getInstance().oauthEnabled = false
    configurable.setActiveAuthTab(AuthTab.OAUTH)

    configurable.authTabbedPane.component.changeListeners[0].stateChanged(mockk<ChangeEvent>())

    assertEquals(verifyButton.isVisible, false)
  }

  @Test
  fun `Verify button is visible when PAT tab is selected and OAuth is disabled`() {
    DuoPersistentSettings.getInstance().oauthEnabled = false
    configurable.setActiveAuthTab(AuthTab.PAT)

    configurable.authTabbedPane.component.changeListeners[0].stateChanged(mockk<ChangeEvent>())

    assertEquals(verifyButton.isVisible, true)
    assertEquals(patField.isEnabled, true)
  }

  @Test
  fun `Verify button is NOT visible when 1Password tab is selected and OAuth is enabled`() {
    DuoPersistentSettings.getInstance().oauthEnabled = true
    configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)

    configurable.authTabbedPane.component.changeListeners[0].stateChanged(mockk<ChangeEvent>())

    assertEquals(verifyButton.isVisible, false)
    assertEquals(patField.isEnabled, false)
    assertEquals(onePasswordCheckbox.isEnabled, false)
    assertEquals(onePasswordAccountsComboBox.isEnabled, false)
    assertEquals(onePasswordSecretReferenceField.isEnabled, false)
  }

  @Test
  fun `Verify button is visible when 1Password tab is selected and OAuth is disabled`() {
    DuoPersistentSettings.getInstance().oauthEnabled = false
    configurable.setActiveAuthTab(AuthTab.ONE_PASSWORD)

    configurable.authTabbedPane.component.changeListeners[0].stateChanged(mockk<ChangeEvent>())

    assertEquals(verifyButton.isVisible, true)
    assertEquals(onePasswordCheckbox.isEnabled, true)
    assertEquals(onePasswordAccountsComboBox.isEnabled, true)
    assertEquals(onePasswordSecretReferenceField.isEnabled, true)
  }

  @Test
  fun `Verify button should use current field values and not saved ones`() {
    DuoPersistentSettings.getInstance().url = "https://gitlab.com"
    DuoPersistentSettings.getInstance().codeSuggestionsEnabled = false
    DuoPersistentSettings.getInstance().duoChatEnabled = false

    urlField.text = "https://staging.gitlab.com"
    duoChatEnabledCheckbox.isSelected = true
    codeSuggestionsEnabledCheckbox.isSelected = true

    verifyButton.doClick()

    coVerify(timeout = 5000L) {
      anyConstructed<SettingsHealthChecker>().check(
        match {
          it.baseUrl == "https://staging.gitlab.com" &&
            it.duoChatEnabled == true &&
            it.codeSuggestionsEnabled == true
        }
      )
    }
  }

  private fun setUpPatToken(): String {
    configurable.setActiveAuthTab(AuthTab.PAT)

    patField = gitLabSettingsConfigurableComponent.findComponent("GitLab Personal Access Token") as JBPasswordField

    val token = Random.nextInt().toString()
    patField.text = token

    mockTokenAcceptance()

    return token
  }

  private fun mockTokenAcceptance() {
    coEvery { configurationValidationService.validateConfiguration(any()) } returns mapOf(
      FeatureId.AUTHENTICATION to FeatureStateParams(
        featureId = FeatureId.AUTHENTICATION,
        engagedChecks = emptyList()
      )
    )
  }

  private fun testCheckBox(
    checkBoxLabel: String,
    actualState: KMutableProperty0<Boolean>,
  ): JBCheckBox {
    val checkBox = gitLabSettingsConfigurableComponent.findComponent(message(checkBoxLabel)) as JBCheckBox

    checkBox.isSelected = true
    configurable.apply()
    assertTrue(checkBox.isSelected)
    assertTrue(actualState.get())

    checkBox.isSelected = false
    configurable.apply()
    assertFalse(checkBox.isSelected)
    assertFalse(actualState.get())

    return checkBox
  }
}

private fun Container.findComponent(regex: Regex): Component? {
  for (component in components) {
    when {
      component is AbstractButton && regex in component.text -> {
        return component
      }

      component is JTextComponent && regex in component.text -> {
        return component
      }

      component is JLabel && component.text != null && regex in component.text -> {
        if (component.labelFor != null) {
          return component.labelFor
        }
        return component
      }

      component is Container -> {
        val foundComponent = component.findComponent(regex)
        if (foundComponent != null) return foundComponent
      }
    }
  }
  return null
}

private fun Container.findComponent(text: String): Component? {
  // Create a regex that matches the exact string from start to end
  val exactMatchRegex = "^${Pattern.quote(text)}$".toRegex()
  return findComponent(exactMatchRegex)
}

// Sets the active tab in the authentication tabbed interface to the PAT tab.
// Currently, the only token changes that get persisted from the auth panel are the ones for the active tab.
private fun GitLabSettingsConfigurable.setActiveAuthTab(authTab: AuthTab) {
  authTabbedPane.component.selectedIndex = when (authTab) {
    AuthTab.OAUTH -> OAUTH_TAB_INDEX
    AuthTab.PAT -> PAT_TAB_INDEX
    AuthTab.ONE_PASSWORD -> ONE_PASSWORD_TAB_INDEX
  }
}

private enum class AuthTab {
  OAUTH,
  PAT,
  ONE_PASSWORD
}
