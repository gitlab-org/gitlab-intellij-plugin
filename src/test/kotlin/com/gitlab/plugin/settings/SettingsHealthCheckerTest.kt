package com.gitlab.plugin.settings

import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.gitlab.plugin.services.health.*
import com.intellij.icons.AllIcons
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import com.intellij.ui.dsl.builder.Row
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import javax.swing.JEditorPane
import javax.swing.JLabel

class SettingsHealthCheckerTest : DescribeSpec({
  val healthyTokenCheck = FeatureStateParams(
    featureId = FeatureId.AUTHENTICATION,
    engagedChecks = emptyList()
  )

  val healthyCodeSuggestionsCheck = FeatureStateParams(
    featureId = FeatureId.CODE_SUGGESTIONS,
    engagedChecks = emptyList(),
  )

  val degradedCodeSuggestionCheck = FeatureStateParams(
    featureId = FeatureId.CODE_SUGGESTIONS,
    engagedChecks = listOf(
      FeatureStateCheckParams(
        checkId = "no-duo-license",
        details = "purchase duo license"
      )
    )
  )

  val healthyDuoChatCheck = FeatureStateParams(
    featureId = FeatureId.CHAT,
    engagedChecks = emptyList()
  )

  val project = mockk<Project>()

  val errorReportingInstructions: Cell<BrowserLink> = mockk(relaxed = true)
  val healthCheckGroup: Row = mockk(relaxed = true)
  val tokenHealth: Cell<JLabel> = mockLabelWithComment()
  val codeSuggestionHealth: Cell<JLabel> = mockLabelWithComment()
  val duoChatHealth: Cell<JLabel> = mockLabelWithComment()
  val verifyLabel: Cell<JLabel> = mockk(relaxed = true)
  val cannotVerifyLabel: Cell<JLabel> = mockk(relaxed = true)

  val configurationValidationResult = mutableMapOf(
    FeatureId.AUTHENTICATION to healthyTokenCheck,
    FeatureId.CODE_SUGGESTIONS to healthyCodeSuggestionsCheck,
    FeatureId.CHAT to healthyDuoChatCheck,
  )
  val configurationValidationRequest = ConfigurationValidationRequest(
    baseUrl = "https://gitlab.com",
    token = "glpat-12345676",
    codeSuggestionsEnabled = true,
    duoChatEnabled = false
  )
  val configurationValidationService = mockk<ConfigurationValidationService>()

  val settingsHealthChecker = SettingsHealthChecker(
    project = project,
    errorReportingInstructions = errorReportingInstructions,
    healthCheckGroup = healthCheckGroup,
    mapOf(
      FeatureId.AUTHENTICATION to tokenHealth,
      FeatureId.CODE_SUGGESTIONS to codeSuggestionHealth,
      FeatureId.CHAT to duoChatHealth
    ),
    verifyLabel = verifyLabel,
    cannotVerifyLabel = cannotVerifyLabel
  )

  beforeEach {
    every { project.service<ConfigurationValidationService>() } returns configurationValidationService

    configurationValidationResult[FeatureId.AUTHENTICATION] = healthyTokenCheck
    configurationValidationResult[FeatureId.CODE_SUGGESTIONS] = healthyCodeSuggestionsCheck
    configurationValidationResult[FeatureId.CHAT] = healthyDuoChatCheck

    coEvery {
      configurationValidationService.validateConfiguration(configurationValidationRequest)
    } returns configurationValidationResult

    every { verifyLabel.visible(any()) } returns mockk<Cell<JLabel>>()
    every { cannotVerifyLabel.visible(any()) } returns mockk<Cell<JLabel>>()
  }

  afterEach {
    clearAllMocks(answers = false)
  }

  it("should make error reporting instructions visible if is not healthy") {
    configurationValidationResult[FeatureId.CODE_SUGGESTIONS] = degradedCodeSuggestionCheck

    settingsHealthChecker.check(configurationValidationRequest)

    verify { errorReportingInstructions.visible(true) }
  }

  it("should make hide error reporting instructions if is healthy") {
    settingsHealthChecker.check(configurationValidationRequest)

    verify { errorReportingInstructions.visible(false) }
  }

  it("should hide cannot verify and error reporting label before configuration validation") {
    settingsHealthChecker.check(configurationValidationRequest)

    coEvery {
      cannotVerifyLabel.visible(false)
      errorReportingInstructions.visible(false)
      configurationValidationService.validateConfiguration(configurationValidationRequest)
    }
  }

  it("should hide verify label after configuration validation") {
    settingsHealthChecker.check(configurationValidationRequest)

    coEvery {
      configurationValidationService.validateConfiguration(configurationValidationRequest)
      verifyLabel.visible(false)
    }
  }

  it("should only display cannot verify and error reporting labels if the language server is not available") {
    coEvery { configurationValidationService.validateConfiguration(configurationValidationRequest) } returns null

    settingsHealthChecker.check(configurationValidationRequest)

    verify {
      cannotVerifyLabel.visible(true)
      errorReportingInstructions.visible(true)

      tokenHealth.visible(false)
      codeSuggestionHealth.visible(false)
      duoChatHealth.visible(false)
    }
  }

  it("should update labels with configuration validation result") {
    configurationValidationResult[FeatureId.AUTHENTICATION] = healthyTokenCheck
    configurationValidationResult[FeatureId.CODE_SUGGESTIONS] = degradedCodeSuggestionCheck
    configurationValidationResult[FeatureId.CHAT] = healthyDuoChatCheck

    settingsHealthChecker.check(configurationValidationRequest)

    verify { tokenHealth.visible(true) }
    tokenHealth.component.text shouldBe "Authentication"
    tokenHealth.component.icon shouldBe AllIcons.RunConfigurations.TestPassed
    tokenHealth.comment?.text shouldBe ""
    tokenHealth.comment?.isVisible shouldBe false

    codeSuggestionHealth.component.text shouldBe "Code Suggestions"
    codeSuggestionHealth.component.icon shouldBe AllIcons.General.Warning
    codeSuggestionHealth.comment?.text shouldBe degradedCodeSuggestionCheck.engagedChecks[0].details
    codeSuggestionHealth.comment?.isVisible shouldBe true

    verify { duoChatHealth.visible(true) }
    duoChatHealth.component.text shouldBe "Duo Chat"
    duoChatHealth.component.icon shouldBe AllIcons.RunConfigurations.TestPassed
    duoChatHealth.comment?.text shouldBe ""
    duoChatHealth.comment?.isVisible shouldBe false
  }

  it("should display health checks group on first health check") {
    val healthChecker = SettingsHealthChecker(
      project = project,
      errorReportingInstructions = errorReportingInstructions,
      healthCheckGroup = healthCheckGroup,
      healthCheckLabels = mapOf(),
      verifyLabel = verifyLabel,
      cannotVerifyLabel = cannotVerifyLabel
    )

    healthChecker.check(configurationValidationRequest)
    healthChecker.check(configurationValidationRequest)

    verify(exactly = 1) { healthCheckGroup.visible(true) }
  }

  it("should hide health check label if that health check was not performed") {
    configurationValidationResult.remove(FeatureId.AUTHENTICATION)

    settingsHealthChecker.check(configurationValidationRequest)

    verify { tokenHealth.visible(false) }
  }
})

private fun mockLabelWithComment(): Cell<JLabel> {
  return mockk<Cell<JLabel>> {
    every { component } returns JLabel("")
    every { comment } returns JEditorPane()
    every { visible(any()) } returns this
  }
}
