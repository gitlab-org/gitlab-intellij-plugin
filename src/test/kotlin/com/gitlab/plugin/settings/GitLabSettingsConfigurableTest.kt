package com.gitlab.plugin.settings

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.PatProvider
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.workspace.DuoConfigurationChangedListener
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.util.messages.MessageBus
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.CoroutineScope
import org.junit.jupiter.api.assertDoesNotThrow

class GitLabSettingsConfigurableTest : DescribeSpec({
  val project = mockk<Project>()
  val coroutineScope = mockk<CoroutineScope>()
  val application = mockApplication()
  val patProvider = mockk<PatProvider>()
  val languageServerService = mockk<GitLabLanguageServerService>()
  val lspSettings = mockk<LanguageServerSettings>()
  val settings = mockk<DuoPersistentSettings>()
  val messageBus = mockk<MessageBus>()
  val publisher = mockk<DuoConfigurationChangedListener>()

  beforeEach {
    every { application.service<PatProvider>() } returns patProvider
    every { application.service<DuoPersistentSettings>() } returns settings
    every { project.service<LanguageServerSettings>() } returns lspSettings
    every { project.service<GitLabLanguageServerService>() } returns languageServerService
    every { application.messageBus } returns messageBus
    every { messageBus.syncPublisher(DuoConfigurationChangedListener.DID_CHANGE_CONFIGURATION_TOPIC) } returns publisher
  }

  afterSpec { unmockkAll() }

  describe("initialization") {
    // On plugin unload, the DuoPersistentSettings service may not be available, but the configurable gets initialized.
    // This results in an error if the settings service is accessed during initialization.
    it("does not error when settings service unavailable") {
      every { application.service<DuoPersistentSettings>() } throws IllegalStateException()

      assertDoesNotThrow {
        GitLabSettingsConfigurable(project, coroutineScope)
      }
    }
  }
})
