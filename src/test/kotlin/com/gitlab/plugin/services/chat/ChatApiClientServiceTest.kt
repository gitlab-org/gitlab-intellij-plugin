package com.gitlab.plugin.services.chat

import com.apollographql.apollo3.api.ApolloResponse
import com.gitlab.plugin.api.GitLabChatRetryTimeoutException
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.ChatWithAdditionalContextSubscription
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiAdditionalContextCategory
import com.gitlab.plugin.graphql.type.AiMessageRole
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.intellij.openapi.components.service
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.ints.shouldBeExactly
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

@OptIn(ExperimentalCoroutinesApi::class)
class ChatApiClientServiceTest : DescribeSpec({
  lateinit var service: ChatApiClient

  val userId = "gid://gitlab/User/999"
  val userService = mockk<GitLabUserService>()
  val graphQLApi = mockk<GraphQLApi>()

  beforeContainer {
    mockkConstructor(ApolloClientFactory::class)
  }

  beforeEach {
    val application = mockApplication()

    mockkConstructor(CreateNotificationExceptionHandler::class)

    every { anyConstructed<CreateNotificationExceptionHandler>().handleException(any()) } just Runs
    every { application.service<GraphQLApi>() } returns graphQLApi
    every { application.service<GitLabUserService>() } returns userService
    every { userService.retrieveCurrentUser() } returns mockk {
      every { id } returns userId
    }

    service = ChatApiClient(CoroutineScope(UnconfinedTestDispatcher()))
  }

  afterEach {
    clearAllMocks()
  }

  afterContainer {
    unmockkAll()
  }

  describe("ChatApiClientService") {
    describe("processNewUserPrompt") {

      it("calls the chat mutation graphql api") {
        val currentFile = ChatRecordContext(
          currentFile = ChatRecordFileContext(fileName = "file.txt", selectedText = "some text")
        )
        val contextItems = listOf(mockk<AiContextItem>())
        coEvery {
          graphQLApi.chatMutation("random question", "123", currentFile, contextItems)
        } returns AiAction("abc", emptyList())

        val response = service.processNewUserPrompt(
          subscriptionId = "123",
          question = "random question",
          context = currentFile,
          aiContextItems = contextItems
        )

        response.shouldNotBeNull()
        response.aiAction.errors shouldBe emptyList()
        response.aiAction.requestId shouldBe "abc"
      }

      it("returns null when the api does not return anything") {
        coEvery {
          graphQLApi.chatMutation("random question", "123")
        } returns null

        val response = service.processNewUserPrompt("123", "random question")

        response shouldBe null
      }
    }

    describe("subscribeToUpdates") {
      val subscriptionId = "123"
      val requestId = "abc"

      beforeEach {
        coEvery {
          graphQLApi.chatMutation("random question", subscriptionId)
        } returns AiAction(requestId, emptyList())
      }

      afterEach {
        service.canceledPromptRequestIds.clear()
      }

      fun createChunk(
        chunkId: Int?,
        content: String,
        role: AiMessageRole = AiMessageRole.ASSISTANT
      ) = ChatSubscription.AiCompletionResponse(
        chunkId = chunkId,
        content = content,
        contentHtml = "<p>$content</p>",
        errors = emptyList(),
        extras = null,
        id = "123",
        requestId = requestId,
        role = role,
        timestamp = "",
        type = null,
      )

      fun createChunkWithAdditionalContext(
        chunkId: Int?,
        content: String
      ): ChatWithAdditionalContextSubscription.AiCompletionResponse {
        return ChatWithAdditionalContextSubscription.AiCompletionResponse(
          chunkId = chunkId,
          content = content,
          contentHtml = "<p>$content</p>",
          errors = emptyList(),
          id = "123",
          requestId = requestId,
          role = AiMessageRole.ASSISTANT,
          timestamp = "",
          type = null,
          extras = ChatWithAdditionalContextSubscription.Extras(
            sources = null,
            additionalContext = listOf(
              ChatWithAdditionalContextSubscription.AdditionalContext(
                id = "file-1",
                category = AiAdditionalContextCategory.FILE,
                metadata = LinkedHashMap<String, Any>().apply {
                  put("title", "file-1")
                  put("enabled", true)
                  put("subType", "LOCAL_FILE_SEARCH")
                  put("relativePath", "src/index.ts")
                }
              )
            )
          )
        )
      }

      it("stops calling onMessageReceived if requestId is in cancelledPromptRequestIds") {
        val chunk1 = ChatSubscription.Data(aiCompletionResponse = createChunk(1, "chunk 1"))
        val apolloResponse1 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = false,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(
          apolloResponse1,
        )

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = false, mockOnMessageReceived)

        service.canceledPromptRequestIds.add(requestId)

        coVerify(exactly = 1) { mockOnMessageReceived(any()) }

        val apolloResponse = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = false,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(
          apolloResponse
        )

        val mockOnMessageReceivedAfterCanceled: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = false, mockOnMessageReceived)

        coVerify(exactly = 0) { mockOnMessageReceivedAfterCanceled(any()) }
      }

      it("doesnt call onMessageReceived for system messages sent back from the stream") {
        val systemChunk = ChatSubscription.Data(
          aiCompletionResponse = createChunk(null, "GitLab Documentation", role = AiMessageRole.SYSTEM)
        )
        val chunk1 = ChatSubscription.Data(aiCompletionResponse = createChunk(1, "chunk 1"))
        val chunk2 = ChatSubscription.Data(aiCompletionResponse = createChunk(2, "chunk 2"))

        val apolloResponse1 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = systemChunk
        ).build()

        val apolloResponse2 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        val apolloResponse3 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk2
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = false,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(apolloResponse1, apolloResponse2, apolloResponse3)

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = false, mockOnMessageReceived)

        // Validate we never handle the TOOL chunk and do not stop receiving chunks.
        coVerify(exactly = 0) { mockOnMessageReceived(match { it.content == "GitLab Documentation" }) }
        coVerify(exactly = 2) { mockOnMessageReceived(any()) }
      }

      it("calls onMessageReceived for each message sent back from the stream") {
        val chunk1 = ChatSubscription.Data(aiCompletionResponse = createChunk(1, "chunk 1"))
        val chunk2 = ChatSubscription.Data(aiCompletionResponse = createChunk(2, "chunk 2"))

        val apolloResponse1 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        val apolloResponse2 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk2
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = false,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(
          apolloResponse1,
          apolloResponse2
        )

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = false, mockOnMessageReceived)

        coVerify(exactly = 2) { mockOnMessageReceived(any()) }
      }

      it("calls onMessageReceived for each message with additional context sent back from the stream") {
        val chunk1 = ChatWithAdditionalContextSubscription.Data(
          aiCompletionResponse = createChunkWithAdditionalContext(
            chunkId = 1,
            content = "chunk 1"
          )
        )
        val chunk2 = ChatWithAdditionalContextSubscription.Data(
          aiCompletionResponse = createChunkWithAdditionalContext(
            chunkId = 2,
            content = "chunk 2"
          )
        )

        val apolloResponse1 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        val apolloResponse2 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk2
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = true,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(
          apolloResponse1,
          apolloResponse2
        )

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = true, mockOnMessageReceived)

        coVerify(exactly = 2) { mockOnMessageReceived(any()) }
      }

      it("does not call onMessageReceived if an empty response is received") {
        val apolloResponse = ApolloResponse.Builder<ChatSubscription.Data>(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = null
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = false,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(
          apolloResponse
        )

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = false, mockOnMessageReceived)

        coVerify(exactly = 0) { mockOnMessageReceived(any()) }
      }

      it("does not call onMessageReceived if a chunk is received after the full message arrived") {
        val chunk1 = ChatSubscription.Data(aiCompletionResponse = createChunk(1, "Hello"))
        val chunk2 = ChatSubscription.Data(aiCompletionResponse = createChunk(2, "Hello, Wor"))
        val chunk3 = ChatSubscription.Data(aiCompletionResponse = createChunk(null, "Hello, World!"))

        val apolloResponse1 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk1
        ).build()

        val apolloResponse2 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk2
        ).build()

        val apolloResponse3 = ApolloResponse.Builder(
          operation = mockk(relaxed = true),
          requestUuid = mockk(relaxed = true),
          data = chunk3
        ).build()

        coEvery {
          graphQLApi.chatSubscription(
            clientSubscriptionId = subscriptionId,
            useAdditionalContext = false,
            userId = UserID("gid://gitlab/User/999")
          )
        } returns flowOf(apolloResponse1, apolloResponse3, apolloResponse2)

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, useAdditionalContext = false, mockOnMessageReceived)

        val messages = mutableListOf<AiMessage>()
        coVerify { mockOnMessageReceived.invoke(capture(messages)) }
        messages.size shouldBe 2
        messages[0].chunkId shouldBe 1
        messages[0].content shouldBe "Hello"

        messages[1].content shouldBe "Hello, World!"
        messages[1].chunkId shouldBe null
      }

      it("throws when userId is not found it fails") {
        every { userService.retrieveCurrentUser() } returns null

        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        shouldThrow<CurrentUserNotFoundException> {
          service.subscribeToUpdates(
            subscriptionId,
            useAdditionalContext = false,
            mockOnMessageReceived
          )
        }
      }
    }

    describe("cancelPrompt") {
      it("adds a cancelledPromptRequestId to canceledPromptRequestIds") {
        val canceledPromptRequestId = "abc"
        service.cancelPrompt(canceledPromptRequestId)

        service.canceledPromptRequestIds[0] shouldBe canceledPromptRequestId
        service.canceledPromptRequestIds.size shouldBeExactly 1
      }
    }

    describe("getMessages") {
      beforeEach {
        mockkObject(Clock.System)
      }

      afterEach {
        unmockkObject(Clock.System)
      }

      it("returns messages retrieved from api") {
        every { Clock.System.now() } returns Instant.parse("2024-04-24T00:00:00Z")

        val message1 = AiMessage(
          role = "user",
          content = "test1",
          contentHtml = "<html>test1</html>",
          requestId = "abc",
          timestamp = "2024-04-24T00:00",
          chunkId = null
        )

        val message2 = AiMessage(
          role = "assistant",
          content = "test2",
          contentHtml = "<html>test2</html>",
          requestId = "abc",
          timestamp = "2024-04-24T00:00",
          chunkId = null
        )

        coEvery {
          graphQLApi.chatQuery(requestId = "abc", useAdditionalContext = true)
        } returns listOf(message1, message2)

        val messages = service.getMessages(requestId = "abc", useAdditionalContext = true)

        messages shouldHaveSize 2
        messages[0] shouldBe message1
        messages[1] shouldBe message2

        coVerify(exactly = 1) { graphQLApi.chatQuery(any(), true) }
      }

      it("creates error notification when only user message retrieved") {
        val message1 = AiMessage(
          role = "user",
          content = "test1",
          contentHtml = "<html>test1</html>",
          requestId = "abc",
          timestamp = "2024-04-24T00:00",
          chunkId = null
        )

        coEvery {
          graphQLApi.chatQuery(requestId = "abc", useAdditionalContext = false)
        } returns listOf(message1)

        runBlocking {
          service.getMessages(requestId = "abc", useAdditionalContext = false)
        }

        coVerify(exactly = 1) {
          anyConstructed<CreateNotificationExceptionHandler>().handleException(
            match { it is GitLabChatRetryTimeoutException }
          )
        }
      }

      it("creates error notification when endpoint returns no messages") {
        coEvery {
          graphQLApi.chatQuery(requestId = "abc", useAdditionalContext = false)
        } returns emptyList()

        runBlocking {
          service.getMessages(requestId = "abc", useAdditionalContext = false)
        }

        coVerify(exactly = 1) {
          anyConstructed<CreateNotificationExceptionHandler>().handleException(
            match { it is GitLabChatRetryTimeoutException }
          )
        }
      }
    }
  }
})
