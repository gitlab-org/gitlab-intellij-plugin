package com.gitlab.plugin.services.health

import com.gitlab.plugin.lsp.GitLabLanguageServer
import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import com.intellij.openapi.vfs.VirtualFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CompletableFuture

class ConfigurationValidationServiceTest : DescribeSpec({
  val workspaceUrl = "file:///project/folder/"
  val name = "folder"

  val project = mockk<Project>()
  val virtualFile = mockk<VirtualFile>()

  val languageServerService = mockk<GitLabLanguageServerService>()
  val languageServer = mockk<GitLabLanguageServer>()

  val configurationValidationService = ConfigurationValidationService(project)

  beforeSpec {
    mockkStatic("com.intellij.openapi.project.ProjectUtil")
  }

  beforeEach {
    every { project.service<GitLabLanguageServerService>() } returns languageServerService
    every { project.guessProjectDir() } returns virtualFile
    every { languageServerService.lspServer } returns languageServer

    every { virtualFile.url } returns workspaceUrl
    every { virtualFile.name } returns name
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should not validate configuration when the language server is not available") {
    every { languageServerService.lspServer } returns null

    val result = runBlocking {
      configurationValidationService.validateConfiguration(
        request = ConfigurationValidationRequest(
          baseUrl = "https://gitlab.com",
          token = "**********",
          codeSuggestionsEnabled = true,
          duoChatEnabled = false
        )
      )
    }

    result shouldBe null
  }

  it("should return no result if the configuration validation fails") {
    every { languageServer.validateConfiguration(any()) } throws Exception("failed")

    val result = runBlocking {
      configurationValidationService.validateConfiguration(
        request = ConfigurationValidationRequest(
          baseUrl = "https://gitlab.com",
          token = "**********",
          codeSuggestionsEnabled = true,
          duoChatEnabled = false
        )
      )
    }

    result shouldBe null
  }

  it("should validate configuration using the language server") {
    val authenticationFeature = FeatureStateParams(FeatureId.AUTHENTICATION, engagedChecks = emptyList())
    val codeSuggestionsFeature = FeatureStateParams(FeatureId.CODE_SUGGESTIONS, engagedChecks = emptyList())
    val chatFeature = FeatureStateParams(FeatureId.CHAT, engagedChecks = emptyList())
    every { languageServer.validateConfiguration(any()) } returns CompletableFuture.completedFuture(
      listOf(authenticationFeature, codeSuggestionsFeature, chatFeature)
    )

    val result = runBlocking {
      configurationValidationService.validateConfiguration(
        request = ConfigurationValidationRequest(
          baseUrl = "https://gitlab.com",
          token = "test_token",
          codeSuggestionsEnabled = true,
          duoChatEnabled = false
        )
      )
    }

    result!! shouldHaveSize 3
    result[FeatureId.AUTHENTICATION] shouldBe authenticationFeature
    result[FeatureId.CODE_SUGGESTIONS] shouldBe codeSuggestionsFeature
    result[FeatureId.CHAT] shouldBe chatFeature

    verify {
      languageServer.validateConfiguration(
        match {
          it.baseUrl == "https://gitlab.com" &&
            it.token == "test_token" &&
            it.codeCompletion?.enabled == true &&
            it.duoChat?.enabled == false &&
            it.workspaceFolders?.size == 1 &&
            it.workspaceFolders?.get(0)?.uri == workspaceUrl &&
            it.workspaceFolders?.get(0)?.name == name
        }
      )
    }
  }
})
