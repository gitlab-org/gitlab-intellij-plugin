package com.gitlab.plugin.services

import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class CodeSuggestionsStateServiceTest : DescribeSpec({
  var codeSuggestionsStateService = CodeSuggestionsStateService()

  beforeEach {
    codeSuggestionsStateService = CodeSuggestionsStateService()
  }

  it("should be disabled by default") {
    codeSuggestionsStateService.codeSuggestionsEnabled shouldBe false
  }

  it("should be enabled when there are no engaged checks") {
    codeSuggestionsStateService.update(FeatureStateParams(FeatureId.CODE_SUGGESTIONS, engagedChecks = emptyList()))

    codeSuggestionsStateService.codeSuggestionsEnabled shouldBe true
  }

  it("should be disabled when there are engaged checks") {
    codeSuggestionsStateService.update(
      FeatureStateParams(
        featureId = FeatureId.CODE_SUGGESTIONS,
        engagedChecks = listOf(
          FeatureStateCheckParams(checkId = "check-id-1")
        )
      )
    )

    codeSuggestionsStateService.codeSuggestionsEnabled shouldBe false
  }

  it("should return the first engaged check") {
    codeSuggestionsStateService.update(
      FeatureStateParams(
        featureId = FeatureId.CODE_SUGGESTIONS,
        engagedChecks = listOf(
          FeatureStateCheckParams(checkId = "check-id-1"),
          FeatureStateCheckParams(checkId = "check-id-2"),
          FeatureStateCheckParams(checkId = "check-id-3")
        )
      )
    )

    codeSuggestionsStateService.getEngagedCheck()?.checkId shouldBe "check-id-1"
  }
})
