package com.gitlab.plugin.services

import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowManager
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class DuoChatStateServiceTest : DescribeSpec({
  val toolWindowManager = mockk<ToolWindowManager>()
  val toolWindow = mockk<ToolWindow>(relaxed = true)

  val project = mockk<Project>()
  var duoChatStateService = DuoChatStateService(project)

  beforeSpec {
    mockkStatic(ToolWindowManager::class)
    mockkStatic(::runInEdt)
  }

  beforeEach {
    duoChatStateService = DuoChatStateService(project)

    every { ToolWindowManager.getInstance(project) } returns toolWindowManager
    every { toolWindowManager.getToolWindow(DUO_CHAT_TOOL_WINDOW_ID) } returns toolWindow
    every { toolWindow.isDisposed } returns false

    every { runInEdt(null, any<() -> Unit>()) } answers { secondArg<() -> Unit>()() }
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should be disabled by default") {
    duoChatStateService.duoChatEnabled shouldBe false
  }

  it("should be enabled when there are no engaged checks") {
    duoChatStateService.update(FeatureStateParams(FeatureId.CHAT, engagedChecks = emptyList()))

    duoChatStateService.duoChatEnabled shouldBe true
  }

  it("should be disabled when there are engaged checks") {
    duoChatStateService.update(
      FeatureStateParams(
        featureId = FeatureId.CHAT,
        engagedChecks = listOf(
          FeatureStateCheckParams(checkId = "check-id-1")
        )
      )
    )

    duoChatStateService.duoChatEnabled shouldBe false
  }

  it("should return the first engaged check") {
    duoChatStateService.update(
      FeatureStateParams(
        featureId = FeatureId.CHAT,
        engagedChecks = listOf(
          FeatureStateCheckParams(checkId = "check-id-1"),
          FeatureStateCheckParams(checkId = "check-id-2"),
          FeatureStateCheckParams(checkId = "check-id-3")
        )
      )
    )

    duoChatStateService.getEngagedCheck()?.checkId shouldBe "check-id-1"
  }

  it("should update the toolwindow state on receive feature state") {
    duoChatStateService.update(
      FeatureStateParams(
        featureId = FeatureId.CHAT,
        engagedChecks = emptyList()
      )
    )
    verify { toolWindow.isAvailable = true }

    duoChatStateService.update(
      FeatureStateParams(
        featureId = FeatureId.CHAT,
        engagedChecks = listOf(FeatureStateCheckParams(checkId = "check-id-1"))
      )
    )
    verify { toolWindow.isAvailable = false }
  }
})
