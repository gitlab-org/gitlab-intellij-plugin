package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.graphql.CurrentUserQuery
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.mockk

class GitLabUserServiceTest : DescribeSpec({
  val graphQLApi = mockk<GraphQLApi>()
  val userService = GitLabUserService(graphQLApi)

  val userId1 = 123
  val currentUser1 = CurrentUserQuery.CurrentUser(
    id = "gid://gitlab/User/$userId1",
    duoChatAvailable = null,
    duoCodeSuggestionsAvailable = null,
    ide = null,
  )

  val userId2 = 456
  val currentUser2 = CurrentUserQuery.CurrentUser(
    id = "gid://gitlab/User/$userId2",
    duoChatAvailable = null,
    duoCodeSuggestionsAvailable = null,
    ide = null,
  )

  it("user id should initially be null") {
    userService.getCurrentUserId() shouldBe null
  }

  it("refreshes user id") {
    coEvery { graphQLApi.getCurrentUser() } returns currentUser1
    userService.retrieveCurrentUser()
    userService.getCurrentUserId() shouldBe userId1

    coEvery { graphQLApi.getCurrentUser() } returns currentUser2
    userService.invalidateAndFetchCurrentUser()
    userService.getCurrentUserId() shouldBe userId2
  }
})
