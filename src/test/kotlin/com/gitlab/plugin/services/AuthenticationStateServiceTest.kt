package com.gitlab.plugin.services

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationGroupType
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class AuthenticationStateServiceTest : DescribeSpec({
  val project = mockk<Project>()
  val statusService = mockk<GitLabStatusService>(relaxUnitFun = true)

  var authenticationStateService = AuthenticationStateService(project)

  beforeSpec {
    mockkConstructor(GitLabNotificationManager::class)
  }

  beforeEach {
    val application = mockApplication()
    every { application.service<GitLabStatusService>() } returns statusService
    every { anyConstructed<GitLabNotificationManager>().sendNotification(any(), any(), any(), any()) } returns Unit

    authenticationStateService = AuthenticationStateService(project)
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should be unauthenticated by default") {
    authenticationStateService.authenticated shouldBe false
  }

  it("should be authenticated when there are no engaged checks") {
    authenticationStateService.update(FeatureStateParams(featureId = FeatureId.AUTHENTICATION, emptyList()))

    authenticationStateService.authenticated shouldBe true
    verify { statusService.gitlabStatusEnabled() }
  }

  it("should be unauthenticated when there engaged checks") {
    authenticationStateService.update(
      FeatureStateParams(
        featureId = FeatureId.AUTHENTICATION,
        engagedChecks = listOf(
          FeatureStateCheckParams(checkId = "check-id")
        )
      )
    )

    authenticationStateService.authenticated shouldBe false
    verify { statusService.gitlabStatusDisabled() }
  }

  it("should display a notification when state switches from authenticated to unauthenticated") {
    authenticationStateService.update(FeatureStateParams(featureId = FeatureId.AUTHENTICATION, emptyList()))
    verify(exactly = 0) { anyConstructed<GitLabNotificationManager>().sendNotification(any(), any(), any(), any()) }

    authenticationStateService.update(
      FeatureStateParams(
        featureId = FeatureId.AUTHENTICATION,
        engagedChecks = listOf(
          FeatureStateCheckParams(checkId = "check-id", details = "details")
        )
      )
    )
    verify {
      anyConstructed<GitLabNotificationManager>().sendNotification(
        notificationData = Notification(title = "GitLab Duo", message = "details"),
        notificationGroupType = NotificationGroupType.IMPORTANT,
        notificationType = NotificationType.ERROR,
        project = project
      )
    }
  }
})
