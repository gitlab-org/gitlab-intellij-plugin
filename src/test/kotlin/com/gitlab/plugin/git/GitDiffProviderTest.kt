package com.gitlab.plugin.git

import com.intellij.openapi.GitRepositoryInitializer
import com.intellij.openapi.application.writeAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.testFramework.LightPlatformTestCase
import com.intellij.testFramework.PlatformTestUtil
import git4idea.GitVcs
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryManager
import io.kotest.matchers.string.shouldContain
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GitDiffProviderTest : LightPlatformTestCase() {
  private lateinit var git: Git
  private lateinit var repository: GitRepository
  private lateinit var projectRoot: VirtualFile
  private lateinit var gitDiffProvider: GitDiffProvider

  @BeforeEach
  fun setupGitRepository() {
    super.setUp()

    projectRoot = PlatformTestUtil.getOrCreateProjectBaseDir(project)
    git = Git.getInstance()

    GitVcs.getInstance(project).doActivate()
    GitRepositoryInitializer.getInstance()?.initRepository(project, projectRoot)

    repository = GitRepositoryManager.getInstance(project).getRepositoryForRoot(projectRoot)!!
    gitDiffProvider = GitDiffProvider(project)

    // Git cannot create a commit if it cannot determine the author.
    val handler = GitLineHandler(project, projectRoot, GitCommand.CONFIG)
    handler.addParameters("user.email", "test@gitlab.com")
    git.runCommand(handler)
  }

  @Test
  fun `it should get diff content from HEAD`() {
    val testFile = newFile("test.txt", "Initial content")

    git.add(project, projectRoot)
    git.commit(project, projectRoot, "Initial commit")

    updateFile(testFile, "Modified content")

    val diffContent = gitDiffProvider.getDiffWithHead(repository.root.url)

    diffContent shouldContain "-Initial content"
    diffContent shouldContain "+Modified content"
  }

  @Test
  fun `it should get diff content from branch`() {
    val testFile = newFile("test.txt", "Initial content")

    git.add(project, projectRoot)
    git.commit(project, projectRoot, "Initial commit")
    git.checkoutBranch("feature-branch", projectRoot)

    updateFile(testFile, "Modified feature-branch")

    git.add(project, projectRoot)
    git.commit(project, projectRoot, "Commit to feature-branch")
    git.checkoutBranch("master", projectRoot)

    updateFile(testFile, "Modified main")

    val diffContent = gitDiffProvider.getDiffWithBranch(repository.root.url, "feature-branch")

    diffContent shouldContain "-Modified feature-branch"
    diffContent shouldContain "+Modified main"
  }

  private fun newFile(fileName: String, content: String): VirtualFile {
    return runBlocking {
      writeAction {
        projectRoot
          .createChildData(this, fileName)
          .apply { setBinaryContent(content.toByteArray()) }
      }
    }
  }

  private fun updateFile(file: VirtualFile, newContent: String) {
    runBlocking {
      writeAction {
        file.setBinaryContent(newContent.toByteArray())
      }
    }
  }

  private fun Git.add(project: Project, projectRoot: VirtualFile) {
    val handler = GitLineHandler(project, projectRoot, GitCommand.ADD)
    handler.addParameters(".")
    runCommand(handler)
  }

  private fun Git.commit(project: Project, projectRoot: VirtualFile, message: String) {
    val handler = GitLineHandler(project, projectRoot, GitCommand.COMMIT)
    handler.addParameters("-m", message)
    runCommand(handler)
  }

  private fun Git.checkoutBranch(branch: String, projectRoot: VirtualFile) {
    val handler = GitLineHandler(project, projectRoot, GitCommand.CHECKOUT)
    handler.addParameters("-b", branch)
    runCommand(handler)
  }

  override fun getName(): String {
    return "GitDiffProviderTest"
  }
}
