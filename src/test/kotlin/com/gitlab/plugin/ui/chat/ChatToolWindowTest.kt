package com.gitlab.plugin.ui.chat

import com.gitlab.plugin.chat.ui.JCEFNotAvailablePanelFactory
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.ui.content.Content
import com.intellij.ui.content.ContentFactory
import com.intellij.ui.content.ContentManager
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import org.junit.jupiter.api.assertThrows
import javax.swing.JComponent
import javax.swing.JPanel

class ChatToolWindowTest : DescribeSpec({
  mockkObject(JCEFNotAvailablePanelFactory)

  val project = mockk<Project>()
  val toolWindow = mockk<ToolWindow>()
  val chatToolWindow = ChatToolWindow()

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should create content with chat browser component when JCEF is supported") {
    val contentManager = mockk<ContentManager>()
    val factory = mockk<ContentFactory>()
    val chatService = mockk<ChatService>()
    val browserComponent = mockk<JComponent>()
    val content = mockk<Content>()

    every { toolWindow.contentManager } returns contentManager
    every { contentManager.factory } returns factory
    every { project.service<ChatService>() } returns chatService
    every { chatService.browserComponent } returns browserComponent
    every { factory.createContent(browserComponent, null, false) } returns content
    every { content.setDisposer(chatService) } just Runs
    every { contentManager.addContent(content) } just Runs

    chatToolWindow.createToolWindowContent(project, toolWindow)

    verify {
      factory.createContent(browserComponent, null, false)
      content.setDisposer(chatService)
      contentManager.addContent(content)
    }
  }

  it("should create content with JCEFNotAvailablePanel when JCEF is not supported") {
    val contentManager = mockk<ContentManager>()
    val factory = mockk<ContentFactory>()
    val content = mockk<Content>()
    val jcefNotAvailablePanel = mockk<JPanel>()

    every { toolWindow.contentManager } returns contentManager
    every { contentManager.factory } returns factory
    every { project.service<ChatService>() } throws ExceptionInInitializerError(Throwable("JCEF is not supported"))
    every { JCEFNotAvailablePanelFactory.create() } returns jcefNotAvailablePanel
    every { factory.createContent(jcefNotAvailablePanel, null, false) } returns content
    every { contentManager.addContent(content) } just Runs

    chatToolWindow.createToolWindowContent(project, toolWindow)

    verify {
      factory.createContent(jcefNotAvailablePanel, null, false)
      contentManager.addContent(content)
    }
  }

  it("should rethrow exception when it's not related to JCEF support") {
    val contentManager = mockk<ContentManager>()
    val factory = mockk<ContentFactory>()
    every { toolWindow.contentManager } returns contentManager
    every { contentManager.factory } returns factory
    every { project.service<ChatService>() } throws ExceptionInInitializerError(Throwable("Some other error"))

    assertThrows<ExceptionInInitializerError> {
      chatToolWindow.createToolWindowContent(project, toolWindow)
    }
  }
})
