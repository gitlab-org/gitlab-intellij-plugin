package com.gitlab.plugin.inspection

import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer
import com.intellij.codeInsight.daemon.impl.DaemonCodeAnalyzerEx
import com.intellij.codeInsight.daemon.impl.HighlightInfo
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.util.Processor
import com.intellij.util.messages.MessageBusConnection
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.mockk.*

@Suppress("UsePropertyAccessSyntax")
class FileProblemsServiceTest : DescribeSpec({
  lateinit var project: Project
  lateinit var fileProblemsService: FileProblemsService
  lateinit var fileEditorManager: FileEditorManager
  lateinit var editor: Editor
  lateinit var document: Document
  lateinit var daemonListener: DaemonCodeAnalyzer.DaemonListener

  beforeEach {
    project = mockk()
    fileEditorManager = mockk()
    editor = mockk()
    document = mockk()

    every { project.messageBus.connect() } returns mockk(relaxed = true)
    every {
      project.messageBus.connect().subscribe(DaemonCodeAnalyzer.DAEMON_EVENT_TOPIC, any())
    } answers {
      daemonListener = secondArg()
    }

    every { FileEditorManager.getInstance(project) } returns fileEditorManager
    every { fileEditorManager.selectedTextEditor } returns editor
    every { editor.document } returns document

    fileProblemsService = FileProblemsService(project)
  }

  afterEach {
    clearAllMocks()
  }

  describe("FileProblemsService") {
    it("should refresh cache when daemonFinished is called") {
      // Initial setup with one error
      val highlightInfo1 = mockk<HighlightInfo>()
      every { document.textLength } returns 100
      every { document.getLineNumber(any()) } returns 0
      every { highlightInfo1.getDescription() } returns "Error 1"
      every { highlightInfo1.getStartOffset() } returns 10
      every { highlightInfo1.getEndOffset() } returns 20

      mockkStatic(DaemonCodeAnalyzerEx::class)

      // First call to processHighlights returns one error
      every {
        DaemonCodeAnalyzerEx.processHighlights(
          document,
          project,
          HighlightSeverity.ERROR,
          0,
          100,
          any<Processor<HighlightInfo>>()
        )
      } answers {
        val processor = lastArg<Processor<HighlightInfo>>()
        processor.process(highlightInfo1)
        true
      }

      // Initial check
      daemonListener.daemonFinished()
      fileProblemsService.getCurrentFileErrors() shouldContainExactly listOf(
        CurrentFileProblem("Error 1", TextRange(10, 20), 0)
      )

      // Update processHighlights to return no errors
      every {
        DaemonCodeAnalyzerEx.processHighlights(
          document,
          project,
          HighlightSeverity.ERROR,
          0,
          100,
          any<Processor<HighlightInfo>>()
        )
      } returns true

      // Trigger daemon finished again
      daemonListener.daemonFinished()

      // Verify cache was updated
      fileProblemsService.getCurrentFileErrors().shouldBeEmpty()
    }

    it("should handle null selected editor") {
      every { fileEditorManager.selectedTextEditor } returns null

      daemonListener.daemonFinished()

      fileProblemsService.getCurrentFileErrors().shouldBeEmpty()
    }

    it("should update currentFileErrors when there are highlights") {
      val highlightInfo1 = mockk<HighlightInfo>()
      val highlightInfo2 = mockk<HighlightInfo>()

      every { document.textLength } returns 100
      every { document.getLineNumber(any()) } returns 0

      every { highlightInfo1.getDescription() } returns "Error 1"
      every { highlightInfo1.getStartOffset() } returns 10
      every { highlightInfo1.getEndOffset() } returns 20

      every { highlightInfo2.getDescription() } returns "Error 2"
      every { highlightInfo2.getStartOffset() } returns 30
      every { highlightInfo2.getEndOffset() } returns 40

      mockkStatic(DaemonCodeAnalyzerEx::class)
      every {
        DaemonCodeAnalyzerEx.processHighlights(
          document,
          project,
          HighlightSeverity.ERROR,
          0,
          100,
          any<Processor<HighlightInfo>>()
        )
      } answers {
        val processor = lastArg<Processor<HighlightInfo>>()
        processor.process(highlightInfo1)
        processor.process(highlightInfo2)
        true
      }

      daemonListener.daemonFinished()

      fileProblemsService.getCurrentFileErrors() shouldContainExactly listOf(
        CurrentFileProblem("Error 1", TextRange(10, 20), 0),
        CurrentFileProblem("Error 2", TextRange(30, 40), 0)
      )
    }

    it("should clear currentFileErrors when there are no highlights") {
      every { document.textLength } returns 100
      mockkStatic(DaemonCodeAnalyzerEx::class)
      every {
        DaemonCodeAnalyzerEx.processHighlights(
          document,
          project,
          HighlightSeverity.ERROR,
          0,
          100,
          any()
        )
      } returns true

      fileProblemsService.getCurrentFileErrors().shouldBeEmpty()
    }

    it("should subscribe to DaemonCodeAnalyzer.DAEMON_EVENT_TOPIC") {
      val messageBusConnection = mockk<MessageBusConnection>(relaxed = true)
      every { project.messageBus.connect() } returns messageBusConnection

      FileProblemsService(project)

      verify {
        messageBusConnection.subscribe(DaemonCodeAnalyzer.DAEMON_EVENT_TOPIC, any())
      }
    }

    it("should not recompute errors if daemonFinished wasn't called between getCurrentFileErrors calls") {
      val highlightInfo = mockk<HighlightInfo>()
      every { document.textLength } returns 100
      every { document.getLineNumber(any()) } returns 0
      every { highlightInfo.getDescription() } returns "Test Error"
      every { highlightInfo.getStartOffset() } returns 10
      every { highlightInfo.getEndOffset() } returns 20

      mockkStatic(DaemonCodeAnalyzerEx::class)

      var processHighlightsCalls = 0

      every {
        DaemonCodeAnalyzerEx.processHighlights(
          document,
          project,
          HighlightSeverity.ERROR,
          0,
          100,
          any<Processor<HighlightInfo>>()
        )
      } answers {
        processHighlightsCalls++
        val processor = lastArg<Processor<HighlightInfo>>()
        processor.process(highlightInfo)
        true
      }

      // First call to getCurrentFileErrors
      daemonListener.daemonFinished() // Ensure cache is marked as outdated
      val firstResult = fileProblemsService.getCurrentFileErrors()

      // Second call to getCurrentFileErrors without calling daemonFinished
      val secondResult = fileProblemsService.getCurrentFileErrors()

      // Verify that processHighlights was only called once
      processHighlightsCalls shouldBe 1

      // Verify that both calls return the same result
      firstResult shouldContainExactly listOf(CurrentFileProblem("Test Error", TextRange(10, 20), 0))
      secondResult shouldBe firstResult
    }
  }
})
