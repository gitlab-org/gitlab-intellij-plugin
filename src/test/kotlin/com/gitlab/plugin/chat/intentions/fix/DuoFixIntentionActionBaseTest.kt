package com.gitlab.plugin.chat.intentions.fix

import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.inspection.CurrentFileProblem
import com.gitlab.plugin.inspection.FileProblemsService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class DuoFixIntentionActionBaseTest : DescribeSpec({
  describe("buildAndSendChatPrompt") {
    val editor = mockk<Editor>()
    val chatService = mockk<ChatService>()
    val chatRecordContextService = mockk<ChatRecordContextService>()
    val fileProblemsService = mockk<FileProblemsService>()
    val chatRecordContext = mockk<ChatRecordContext>()

    beforeEach {
      clearAllMocks()

      every {
        chatRecordContextService.getChatRecordContextOrNull(any(), any())
      } returns chatRecordContext

      every { chatService.activateChatWithPrompt(any()) } just Runs
    }

    it("should create correct prompt for single error") {
      val textRange = TextRange(0, 10)
      val error = CurrentFileProblem("Test error", textRange, 1)
      every { fileProblemsService.getCurrentFileErrors() } returns setOf(error)

      val testAction = object : DuoFixIntentionActionBase() {
        fun testBuildAndSendChatPrompt() {
          buildAndSendChatPrompt(
            textRange = textRange,
            editor = editor,
            chatService = chatService,
            chatRecordContextService = chatRecordContextService,
            fileProblemsService = fileProblemsService
          )
        }

        override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?) = true

        override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
          // no-op
        }
      }

      val expectedPrompt = NewUserPromptRequest(
        content = "/fix `Test error` on line 1",
        type = ChatRecord.Type.FIX_CODE,
        context = chatRecordContext
      )

      val promptSlot = slot<NewUserPromptRequest>()
      testAction.testBuildAndSendChatPrompt()

      verify { chatService.activateChatWithPrompt(capture(promptSlot)) }
      promptSlot.captured shouldBe expectedPrompt
    }

    it("should create correct prompt for multiple errors") {
      val textRange = TextRange(0, 20)
      val errors = setOf(
        CurrentFileProblem("Error 1", TextRange(0, 10), 1),
        CurrentFileProblem("Error 2", TextRange(10, 20), 2)
      )
      every { fileProblemsService.getCurrentFileErrors() } returns errors

      val testAction = object : DuoFixIntentionActionBase() {
        fun testBuildAndSendChatPrompt() {
          buildAndSendChatPrompt(
            textRange = textRange,
            editor = editor,
            chatService = chatService,
            chatRecordContextService = chatRecordContextService,
            fileProblemsService = fileProblemsService
          )
        }

        override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?) = true

        override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
          // no-op
        }
      }

      val expectedContent = """
        /fix multiple issues:

        `Error 1` on line 1

        `Error 2` on line 2
      """.trimIndent()

      val expectedPrompt = NewUserPromptRequest(
        content = expectedContent,
        type = ChatRecord.Type.FIX_CODE,
        context = chatRecordContext
      )

      val promptSlot = slot<NewUserPromptRequest>()
      testAction.testBuildAndSendChatPrompt()

      verify { chatService.activateChatWithPrompt(capture(promptSlot)) }
      promptSlot.captured shouldBe expectedPrompt
    }
  }
})
