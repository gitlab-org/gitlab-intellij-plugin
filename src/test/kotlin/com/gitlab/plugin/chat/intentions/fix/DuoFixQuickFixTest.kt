package com.gitlab.plugin.chat.intentions.fix

import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.inspection.CurrentFileProblem
import com.gitlab.plugin.inspection.FileProblemsService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Caret
import com.intellij.openapi.editor.CaretModel
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class DuoFixQuickFixTest : DescribeSpec({
  lateinit var element: PsiElement
  val project = mockk<Project>(relaxed = true)
  val fileProblemsService = mockk<FileProblemsService>(relaxed = true)

  beforeEach {
    element = mockk<PsiElement>()
    every { project.service<FileProblemsService>() } returns fileProblemsService
  }

  afterEach {
    clearAllMocks()
  }

  describe("isAvailable") {
    it("should be true if the feature flag is on") {
      val result = DuoFixQuickFix(element, true)
        .isAvailable(project, null, null)

      result shouldBe true
    }

    it("should be false if the feature flag is off") {
      val result = DuoFixQuickFix(element, false)
        .isAvailable(project, null, null)

      result shouldBe false
    }
  }

  describe("invoke") {
    lateinit var elementText: String
    lateinit var elementErrorDescription: String

    val document = mockk<Document>(relaxed = true)
    val file = mockk<PsiFile>(relaxed = true)
    val chatService = mockk<ChatService>(relaxed = true)
    val editor = mockk<Editor>(relaxed = true)
    val chatRecordContextService = mockk<ChatRecordContextService>(relaxed = true)
    val caretModel = mockk<CaretModel>(relaxed = true)
    val caret = mockk<Caret>(relaxed = true)
    val chatRecordContext = mockk<ChatRecordContext>(relaxed = true)

    val fullTextLength = 1000
    val lineNumber = 3

    var elementStartOffset = 42

    fun getElementEndOffset(): Int = elementStartOffset + elementText.length

    beforeEach {
      elementErrorDescription = "[UNRESOLVED_REFERENCE] Unresolved reference: printThisLine"
      elementText = """printThisLine("Hello world!")"""

      every {
        chatRecordContextService.getChatRecordContextOrNull(
          textRange = any(),
          selectedEditor = editor
        )
      } returns chatRecordContext

      every { project.service<ChatRecordContextService>() } returns chatRecordContextService
      every { project.service<ChatService>() } returns chatService

      every { chatService.activateChatWithPrompt(any()) } just Runs
      every { editor.document } returns document
      every { editor.caretModel } returns caretModel
      every { caretModel.primaryCaret } returns caret

      every { element.text } answers { elementText }
      every { element.textRange } answers { TextRange(elementStartOffset, elementStartOffset + elementText.length) }
      every { document.textLength } answers { fullTextLength }
      every { caret.hasSelection() } returns false
      every { document.getLineNumber(any()) } returns (lineNumber - 1)

      every {
        chatRecordContextService.getChatRecordContextOrNull(
          textRange = any(),
          selectedEditor = editor
        )
      } answers { chatRecordContext }
    }

    describe("with no text selected by the user in the editor") {
      beforeEach {
        every { editor.selectionModel.selectedText } returns null
        every { caret.hasSelection() } returns false
      }

      it("uses element as selected text range and builds expected prompt") {
        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = "/fix `$elementErrorDescription` on line $lineNumber",
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(elementStartOffset, getElementEndOffset()),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }

      it("uses first line number for multi-line error element text") {
        elementText = """
        someMethod(
          "param1",
          "param2"
        )
        """.trimIndent()

        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = "/fix `$elementErrorDescription` on line $lineNumber",
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(elementStartOffset, getElementEndOffset()),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }
    }

    describe("with text selected by the user in the editor") {
      var selectionStartOffset = 40
      var selectionEndOffset = 1200

      beforeEach {
        selectionStartOffset = 40
        selectionEndOffset = 1200
        every { caret.hasSelection() } returns true
        every { caret.selectionStart } answers { selectionStartOffset }
        every { caret.selectionEnd } answers { selectionEndOffset }
      }

      @Suppress("ArgumentListWrapping")
      it(
        "uses selection as selected text range and includes all issues in selection when selection includes triggering element and other errors"
      ) {
        val otherLineNumber = 10
        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          ),
          CurrentFileProblem(
            "[TYPE_MISMATCH] Type mismatch: inferred type is String but Int was expected",
            TextRange(elementStartOffset + 400, elementStartOffset + 410),
            otherLineNumber
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = """
            /fix multiple issues:

            `$elementErrorDescription` on line $lineNumber

            `[TYPE_MISMATCH] Type mismatch: inferred type is String but Int was expected` on line $otherLineNumber
          """.trimIndent(),
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(selectionStartOffset, selectionEndOffset),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }

      it("uses selection as selected text range when selection only includes triggering element") {
        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = "/fix `$elementErrorDescription` on line $lineNumber",
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(selectionStartOffset, selectionEndOffset),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }

      it("produces same prompt as no selection when selection does not include triggering element") {
        selectionStartOffset = 200
        selectionEndOffset = 300
        elementStartOffset = selectionStartOffset - 100

        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          ),
          CurrentFileProblem(
            "[TYPE_MISMATCH] Type mismatch: inferred type is String but Int was expected",
            TextRange(selectionStartOffset + 10, selectionStartOffset + 20),
            8
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = "/fix `$elementErrorDescription` on line $lineNumber",
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(elementStartOffset, getElementEndOffset()),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }

      it("includes partly overlapped errors") {
        elementStartOffset = selectionStartOffset - 10

        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          ),
          CurrentFileProblem(
            "[SYNTAX_ERROR] Missing semicolon",
            TextRange(selectionEndOffset - 10, selectionEndOffset),
            8
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = """
            /fix multiple issues:

            `$elementErrorDescription` on line $lineNumber

            `[SYNTAX_ERROR] Missing semicolon` on line 8
          """.trimIndent(),
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(selectionStartOffset, selectionEndOffset),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }

      it("does not include duplicate lines in prompt content") {
        val otherLineNumber = 10
        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem(
            elementErrorDescription,
            TextRange(elementStartOffset, getElementEndOffset()),
            lineNumber
          ),
          CurrentFileProblem(
            "[TYPE_MISMATCH] Type mismatch: inferred type is String but Int was expected",
            TextRange(elementStartOffset + 400, elementStartOffset + 410),
            otherLineNumber
          ),
          CurrentFileProblem(
            "[TYPE_MISMATCH] Type mismatch: inferred type is String but Int was expected",
            TextRange(elementStartOffset + 400, elementStartOffset + 410),
            otherLineNumber
          )
        )

        val expectedPrompt = NewUserPromptRequest(
          content = """
            /fix multiple issues:

            `$elementErrorDescription` on line $lineNumber

            `[TYPE_MISMATCH] Type mismatch: inferred type is String but Int was expected` on line $otherLineNumber
          """.trimIndent(),
          type = ChatRecord.Type.FIX_CODE,
          context = chatRecordContext
        )

        val quickFix = DuoFixQuickFix(element, true)
        val promptSlot = slot<NewUserPromptRequest>()
        quickFix.invoke(project, editor, file)

        verify {
          chatRecordContextService.getChatRecordContextOrNull(
            textRange = TextRange(selectionStartOffset, selectionEndOffset),
            selectedEditor = editor
          )
        }
        verify { chatService.activateChatWithPrompt(capture(promptSlot)) }

        val actualPrompt = promptSlot.captured
        actualPrompt shouldBe (expectedPrompt)
      }
    }
  }
})
