package com.gitlab.plugin.chat.quickchat

import com.gitlab.plugin.chat.ChatController
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUI
import com.intellij.openapi.editor.*
import com.intellij.openapi.editor.ComponentInlayRenderer
import com.intellij.openapi.editor.Inlay
import com.intellij.openapi.editor.markup.RangeHighlighter
import com.intellij.openapi.util.Disposer
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class QuickChatSessionTest : DescribeSpec({
  val inlay = mockk<Inlay<ComponentInlayRenderer<QuickChatUI>>>()
  val gutterIcon = mockk<RangeHighlighter>(relaxUnitFun = true)
  val controller = mockk<ChatController>(relaxUnitFun = true)

  val session = QuickChatSession(inlay, gutterIcon, controller)

  afterEach {
    clearAllMocks()
  }

  it("closing the session should dispose the inlay and the gutter icon") {
    mockkStatic(Disposer::class) {
      every { Disposer.dispose(any()) } returns Unit

      session.close()

      verify { Disposer.dispose(inlay) }
      verify { gutterIcon.dispose() }
    }
  }

  describe("selectionChanged") {
    val editor = mockk<Editor>()
    val selectionModel = mockk<SelectionModel>()

    beforeEach {
      every { editor.selectionModel } returns selectionModel
    }

    it("should update selection to include no lines when there is no selection") {
      every { selectionModel.hasSelection() } returns false

      session.selectionChanged(editor)

      verify { controller.selectionChanged(null, null) }
    }

    it("should update selection to include selected lines there is a selection") {
      // Set LogicalPosition as one line further and the VisualPosition
      every { editor.visualToLogicalPosition(any()) } answers {
        val position = firstArg<VisualPosition>()
        LogicalPosition(position.line + 1, position.column)
      }

      every { selectionModel.hasSelection() } returns true
      every { selectionModel.selectionStartPosition } returns VisualPosition(0, 0)
      every { selectionModel.selectionEndPosition } returns VisualPosition(1, 0)

      session.selectionChanged(editor)

      // Positions are 0-indexed and lines are displayed as 1-indexed
      verify { controller.selectionChanged(2, 3) }
    }
  }
})
