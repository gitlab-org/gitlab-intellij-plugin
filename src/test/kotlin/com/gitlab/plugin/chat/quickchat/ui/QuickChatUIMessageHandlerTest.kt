package com.gitlab.plugin.chat.quickchat.ui

import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.mockk.mockk

class QuickChatUIMessageHandlerTest : DescribeSpec({
  val chatViewMessage = mockk<ChatViewMessage>()
  val quickChatUIMessage = mockk<QuickChatUIMessage>()

  it("should dispatch event to all registered listeners") {
    val calls = mutableListOf<String>()
    val listener1: (ChatViewMessage) -> Unit = { calls += "listener1" }
    val listener2: (ChatViewMessage) -> Unit = { calls += "listener2" }
    val messageHandler = QuickChatUIMessageHandler()
    messageHandler.register(listener1)
    messageHandler.register(listener2)

    messageHandler.dispatch(chatViewMessage)

    calls shouldContainExactly listOf("listener1", "listener2")
  }

  it("should notify ui message received") {
    var calls = 0
    val onMessageReceived: (QuickChatUIMessage) -> Unit = { calls += 1 }
    val messageHandler = QuickChatUIMessageHandler()
    messageHandler.onMessageReceived = onMessageReceived

    messageHandler.receive(quickChatUIMessage)

    calls shouldBe 1
  }
})
