package com.gitlab.plugin.chat

import com.apollographql.apollo3.exception.ApolloException
import com.apollographql.apollo3.exception.ApolloNetworkException
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.codesnippets.InsertCodeSnippetService
import com.gitlab.plugin.chat.context.*
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.telemetry.trackFeedback
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.*
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.chat.ChatApiClient
import com.gitlab.plugin.telemetry.StandardContext
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import com.intellij.openapi.vfs.VirtualFile
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.constants.Parameter
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.eclipse.lsp4j.WorkspaceFolder
import java.io.EOFException
import java.net.ProtocolException
import java.util.*

@Suppress("LargeClass")
@OptIn(ExperimentalCoroutinesApi::class)
class ChatControllerTest : DescribeSpec({
  lateinit var chatController: ChatController
  lateinit var chatHistory: ChatHistory
  lateinit var viewOnMessageCallback: ((message: ChatViewMessage) -> Unit)

  val chatApiClient = mockk<ChatApiClient>(relaxed = true)
  val view = mockk<ChatView>(relaxed = true)
  val project = mockk<Project>(relaxed = true)
  val logger = mockk<Logger>(relaxed = true)
  val userService = mockk<GitLabUserService>(relaxed = true)
  val serverService = mockk<GitLabServerService>(relaxed = true)
  val context = mockk<ChatRecordContext>(relaxed = true)
  val contextManager = mockk<AiContextItemManager>(relaxed = true)
  val coroutineScope = CoroutineScope(UnconfinedTestDispatcher())
  val chatRecordContextService = mockk<ChatRecordContextService>()

  beforeSpec {
    mockkStatic(Project::guessProjectDir)
  }

  beforeAny {
    val application = mockApplication()

    every { view.onMessage(captureLambda()) } answers {
      viewOnMessageCallback = lambda<(message: ChatViewMessage) -> Unit>().captured
    }
    every { project.getService(ChatApiClient::class.java) } returns chatApiClient
    every { project.service<ChatRecordContextService>() } returns chatRecordContextService
    every { application.service<GitLabUserService>() } returns userService
    every { application.service<GitLabServerService>() } returns serverService

    chatHistory = ChatHistory()
    chatController = ChatController(
      chatView = view,
      chatHistory = chatHistory,
      project = project,
      logger = logger,
      contextManager = contextManager,
      coroutineScope = coroutineScope
    )

    every { userService.getCurrentUserId() } returns 123
    coEvery { contextManager.shouldIncludeAdditionalContext() } returns false
  }

  afterAny { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("init") {
    describe("chatView") {
      describe("onMessage") {
        describe("NewPromptMessage") {
          beforeEach {
            coEvery { chatRecordContextService.getChatRecordContextOrNull() } returns context
          }

          it("shows the view") {
            viewOnMessageCallback(NewPromptMessage("ping"))

            verify(exactly = 1) { view.show() }
          }

          it("adds an assistant record to chat history") {
            viewOnMessageCallback(NewPromptMessage("ping"))

            verify {
              view.addRecord(
                match {
                  it.record == chatHistory.records[1] &&
                    it.record.role == ChatRecord.Role.ASSISTANT
                }
              )
            }
          }

          it("clears the context items on new prompt message") {
            viewOnMessageCallback(NewPromptMessage("ping"))

            coVerify { contextManager.clearSelectedContextItems() }
            verify { view.setCurrentContextItems(any()) }
          }

          context("when editor context is available") {
            it("adds a user record with context to chat history") {
              viewOnMessageCallback(NewPromptMessage("ping"))

              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[0] &&
                      it.record.role == ChatRecord.Role.USER &&
                      it.record.context == context
                  }
                )
              }
            }
          }

          context("when additional context is available") {
            it("adds a user record with that context to chat history") {
              coEvery { contextManager.shouldIncludeAdditionalContext() } returns true
              coEvery { contextManager.retrieveSelectedContextItemsWithContent() } returns listOf(
                AiContextItem(
                  id = "unique-id-1",
                  category = AiContextCategory.FILE,
                  content = "content",
                  metadata = AIContextItemMetadata(
                    title = "file",
                    enabled = true,
                    subType = AIContextProviderType.LOCAL_FILE_SEARCH
                  )
                )
              )

              viewOnMessageCallback(NewPromptMessage("ping"))

              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[0] &&
                      it.record.role == ChatRecord.Role.USER &&
                      it.record.extras?.contextItems?.size == 1 &&
                      it.record.extras?.contextItems?.get(0)?.id == "unique-id-1"
                  }
                )
              }
            }
          }

          context("when editor context is not available") {
            beforeEach {
              coEvery { chatRecordContextService.getChatRecordContextOrNull() } returns null
            }

            it("adds a user record without context to chat history") {
              viewOnMessageCallback(NewPromptMessage("ping"))

              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[0] &&
                      it.record.role == ChatRecord.Role.USER &&
                      it.record.context == null
                  }
                )
              }
            }
          }
        }

        describe("appReadyMessage") {
          it("restores history to view") {
            // arrange
            val chatRecords = listOf(
              ChatRecord(
                role = ChatRecord.Role.USER,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "User message"
              ),
              ChatRecord(
                role = ChatRecord.Role.ASSISTANT,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "Assistant response"
              )
            )

            chatRecords.forEach { chatHistory.addRecord(it) }

            // act
            viewOnMessageCallback.invoke(AppReadyMessage)

            // assert
            // Verify each record in chat history is added to the view
            chatRecords.forEach { record ->
              verify {
                view.addRecord(
                  match {
                    it.record == record
                  }
                )
              }
            }
          }

          it("refreshes current context items and categories") {
            viewOnMessageCallback.invoke(AppReadyMessage)

            verify { view.setContextItemCategories(any()) }
            verify { view.setCurrentContextItems(any()) }
          }
        }

        describe("ContextItemAddedMessage") {
          it("calls the appropriate context manager method and refreshes current context items") {
            val contextItem = AiContextItem(
              id = "abc123",
              category = AiContextCategory.SNIPPET,
              content = "Some content",
              metadata = null
            )
            coEvery { contextManager.add(any()) } just Runs
            coEvery { contextManager.retrieveSelectedContextItemsWithContent() } returns listOf(contextItem)

            viewOnMessageCallback.invoke(ContextItemAddedMessage(contextItem))

            coVerify { contextManager.add(contextItem) }
            verify(exactly = 1) { view.setCurrentContextItems(ContextCurrentItemsResultMessage(listOf(contextItem))) }
          }
        }

        describe("ContextItemRemovedMessage") {
          it("calls the appropriate context manager method and refreshes current context items") {
            val contextItem = AiContextItem(
              id = "abc123",
              category = AiContextCategory.SNIPPET,
              content = "Some content",
              metadata = null
            )
            coEvery { contextManager.remove(any()) } just Runs
            coEvery { contextManager.retrieveSelectedContextItemsWithContent() } returns emptyList()

            viewOnMessageCallback.invoke(ContextItemRemovedMessage(contextItem))

            coVerify { contextManager.remove(contextItem) }
            verify(exactly = 1) { view.setCurrentContextItems(ContextCurrentItemsResultMessage(emptyList())) }
          }
        }

        describe("ContextItemSearchQueryMessage") {
          it("sets context item search results") {
            val workspaceFolder = WorkspaceFolder("some/path/", "gitlab-jetbrains-plugin")
            val settings = mockk<LanguageServerSettings>()
            val projectDir = mockk<VirtualFile> {
              every { url } returns workspaceFolder.uri
              every { name } returns workspaceFolder.name
            }
            every { project.service<LanguageServerSettings>() } returns settings
            every { project.guessProjectDir() } returns projectDir

            val expectedContextItems = listOf(
              AiContextItem(
                id = "abc123",
                category = AiContextCategory.SNIPPET,
                content = "Some content",
                metadata = null
              )
            )
            coEvery { contextManager.query(any()) } returns expectedContextItems

            viewOnMessageCallback.invoke(ContextItemSearchQueryMessage("query", AiContextCategory.SNIPPET))

            coVerify {
              contextManager.query(
                AiContextSearchQuery(
                  category = AiContextCategory.SNIPPET,
                  query = "query",
                  workspaceFolders = listOf(
                    AiContextItemWorkspaceFolder(
                      uri = workspaceFolder.uri,
                      name = workspaceFolder.name
                    )
                  )
                )
              )
            }
            verify {
              view.setContextItemSearchResults(
                ContextItemsSearchResultMessage(expectedContextItems)
              )
            }
          }
        }

        describe("SlashCommandsOpenedMessage") {
          it("triggers correct language server calls and updates context categories") {
            val expectedCategories = listOf(AiContextCategory.FILE, AiContextCategory.SNIPPET)

            coEvery { contextManager.getAvailableCategories() } returns expectedCategories

            viewOnMessageCallback.invoke(SlashCommandsOpenedMessage)

            coVerify { contextManager.getAvailableCategories() }
            verify { view.setContextItemCategories(ContextCategoriesResultMessage(expectedCategories)) }
          }
        }
      }
    }

    describe("process TrackFeedbackMessage") {
      mockkObject(StandardContext, GitLabApplicationService)
      mockApplicationInfo()
      mockPlugin()

      val tracker: Tracker = mockk()

      beforeEach {

        every { StandardContext.build(any()) } returns SelfDescribingJson(
          "standard_context",
          mapOf("extra" to "extra-val")
        )

        every { tracker.track(any()) } returns emptyList()

        every { GitLabApplicationService.getInstance().snowplowTracker } returns tracker
      }
      afterEach { unmockkAll() }

      it("tracks a message") {
        val message = slot<Structured>()
        trackFeedback(TrackFeedbackMessage("", TrackFeedbackMessage.Data("", "", emptyList())))

        verify(exactly = 1) { tracker.track(capture(message)) }
        message.captured.payload.map[Parameter.SE_CATEGORY] shouldBe "ask_gitlab_chat"
        message.captured.payload.map[Parameter.SE_LABEL] shouldBe "response_feedback"
        message.captured.payload.map[Parameter.SE_ACTION] shouldBe "click_button"
        message.captured.payload.map[Parameter.SE_PROPERTY] shouldBe null
        message.captured.context.size shouldBe 2
      }
    }

    it("processes InsertCodeSnippetMessage") {
      val message = InsertCodeSnippetMessage(snippet = "return 10;")

      val insertCodeSnippetService: InsertCodeSnippetService = mockk(relaxed = true)
      every { project.service<InsertCodeSnippetService>() } returns insertCodeSnippetService
      coEvery { insertCodeSnippetService.insertCodeSnippet(message) } returns Unit

      viewOnMessageCallback.invoke(message)

      coVerify(exactly = 1) { insertCodeSnippetService.insertCodeSnippet(message) }
    }

    describe("processNewUserPrompt") {
      describe("with GENERAL type and valid input") {
        it("add user and assistant record") {
          // arrange
          val content = "ping"
          val requestId = "00000000-00000000-00000000-00000000"

          coEvery {
            chatApiClient.processNewUserPrompt(any(), any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          chatController.processNewUserPrompt(
            NewUserPromptRequest(
              content,
              ChatRecord.Type.fromContent(
                content,
              )
            )
          )

          // assert

          // assert chat history
          chatHistory.records shouldHaveSize 2

          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.content shouldBe content
          }

          chatHistory.records[1].role shouldBe ChatRecord.Role.ASSISTANT

          val subscriptionId = chatHistory.records[0].id

          // assert view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[1]
              }
            )
          }

          // assert api client
          coVerify { chatApiClient.processNewUserPrompt(subscriptionId, content) }
          coVerify { chatApiClient.subscribeToUpdates(subscriptionId, any(), any()) }
        }

        it("sets up subscription and handles updates correctly") {
          // arrange
          val content = "ping"
          val requestId = UUID.randomUUID().toString()
          val chunkId = 0
          val aiActionResponse = AiActionResponse(AiAction(requestId, emptyList()))

          coEvery { chatApiClient.processNewUserPrompt(any(), any(), any()) } returns aiActionResponse

          val assistantContent = "pong"
          val aiContextItems = listOf(
            AiContextItem(
              id = "file-1",
              category = AiContextCategory.FILE,
              metadata = null
            )
          )
          val aiMessage = AiMessage(
            requestId,
            chunkId = chunkId,
            role = "assistant",
            content = assistantContent,
            contentHtml = "",
            timestamp = "",
            extras = ChatRecord.Metadata(
              contextItems = aiContextItems
            )
          )

          val newUserPromptRequest = NewUserPromptRequest(
            content,
            ChatRecord.Type.fromContent(content),
            aiContextItems = aiContextItems
          )

          val channel = Channel<AiMessage>()

          coEvery { contextManager.shouldIncludeAdditionalContext() } returns true
          coEvery { chatApiClient.subscribeToUpdates(any(), true, any()) } coAnswers {
            coroutineScope.launch {
              thirdArg<suspend (message: AiMessage) -> Unit>().invoke(channel.receive())
            }
          }
          coEvery { chatApiClient.processNewUserPrompt(any(), any(), any(), aiContextItems) } coAnswers {
            aiActionResponse
          }

          // act
          chatController.processNewUserPrompt(newUserPromptRequest)
          channel.send(aiMessage)

          // assert
          // Verify that subscribeToUpdates is called with correct parameters
          coVerify { chatApiClient.subscribeToUpdates(any(), true, any()) }

          // Verify that the chat history and view are updated with the new information from the subscription
          chatHistory.records.last().let {
            it.role shouldBe ChatRecord.Role.ASSISTANT
            it.content shouldBe assistantContent
            it.chunkId shouldBe chunkId
            it.extras?.contextItems?.get(0)?.id shouldBe "file-1"

            // Verify that the view is updated with the new assistant record
            verify {
              view.updateRecord(
                match { message ->
                  message.record == it
                }
              )
            }
          }
        }

        describe("with a streaming subscription error") {
          withData(
            ApolloException("Test error"),
            ApolloNetworkException("Network error", ProtocolException("Test error")),
            ApolloNetworkException("Network error", EOFException("Test error"))
          ) { exception ->
            val content = "ping"
            val requestId = UUID.randomUUID().toString()
            val expectedMessage = AiMessage(
              requestId,
              role = "assistant",
              content = "assistantContent",
              contentHtml = "",
              timestamp = ""
            )

            coEvery { contextManager.shouldIncludeAdditionalContext() } returns false
            coEvery { chatApiClient.subscribeToUpdates(any(), false, any()) } returns null
            coEvery {
              chatApiClient.processNewUserPrompt(any(), any(), any())
            } returns AiActionResponse(AiAction(requestId, emptyList()))
            coEvery { chatApiClient.getMessages(requestId, useAdditionalContext = false) } returns listOf(
              expectedMessage
            )

            chatController.processNewUserPrompt(
              NewUserPromptRequest(
                content,
                ChatRecord.Type.fromContent(
                  content,
                )
              )
            )

            chatHistory.records shouldHaveSize 2

            chatHistory.records[0].let {
              it.role shouldBe ChatRecord.Role.USER
              it.content shouldBe content
            }

            chatHistory.records[1].role shouldBe ChatRecord.Role.ASSISTANT
            chatHistory.records[1].state shouldBe ChatRecord.State.READY
            chatHistory.records[1].content shouldBe expectedMessage.content

            val subscriptionId = chatHistory.records[0].id

            verify(exactly = 1) { view.show() }
            verify {
              view.addRecord(
                match {
                  it.record == chatHistory.records[0]
                }
              )
            }

            verify {
              view.addRecord(
                match {
                  it.record == chatHistory.records[1]
                }
              )
            }

            coVerify { chatApiClient.processNewUserPrompt(subscriptionId, content) }
            coVerify { chatApiClient.subscribeToUpdates(subscriptionId, false, any()) }
            coVerify { chatApiClient.getMessages(requestId, useAdditionalContext = false) }
          }
        }
      }

      describe("with NEW_CONVERSATION type") {
        it("adds user record while not adding new assistant record") {
          // arrange
          val content = "Start New Conversation"
          val requestId = "newConvRequestId"
          val newUserPromptRequest = NewUserPromptRequest(content, ChatRecord.Type.NEW_CONVERSATION)
          val subscription = Job()

          coEvery {
            chatApiClient.subscribeToUpdates(any(), any(), any())
          } returns subscription

          coEvery {
            chatApiClient.processNewUserPrompt(any(), any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          chatController.processNewUserPrompt(newUserPromptRequest)

          // assert

          // Assert that a new conversation record is added to chat history
          chatHistory.records shouldHaveSize 1
          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.type shouldBe ChatRecord.Type.NEW_CONVERSATION
            it.content shouldBe content
          }

          // Assert that the view is shown and the record is added to the view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }
          subscription.isCancelled shouldBe true
        }
      }

      it("should include additional context items if enabled") {
        val contextItems = listOf(
          AiContextItem(
            id = "test-item",
            category = AiContextCategory.SNIPPET,
            content = "Test content",
            metadata = AIContextItemMetadata(
              title = "Test Item",
              enabled = true,
              subType = AIContextProviderType.SNIPPET
            )
          )
        )

        coEvery { contextManager.shouldIncludeAdditionalContext() } returns true
        coEvery { contextManager.retrieveSelectedContextItemsWithContent() } returns contextItems
        val requestId = UUID.randomUUID().toString()
        val content = "ping"
        val newUserPromptRequest = NewUserPromptRequest(
          content = content,
          type = ChatRecord.Type.fromContent(content),
          aiContextItems = contextItems
        )

        coEvery {
          chatApiClient.processNewUserPrompt(any(), "ping", null, contextItems)
        } returns AiActionResponse(AiAction(requestId, emptyList()))

        chatController.processNewUserPrompt(newUserPromptRequest)

        coVerify {
          chatApiClient.processNewUserPrompt(
            any(),
            content,
            any(),
            match { items ->
              items.isNotEmpty() && items.first().id == "test-item"
            }
          )
        }
      }

      it("should not include additional context items if disabled") {
        coEvery { contextManager.shouldIncludeAdditionalContext() } returns false
        val requestId = UUID.randomUUID().toString()
        val content = "ping"
        val newUserPromptRequest = NewUserPromptRequest(
          content = content,
          type = ChatRecord.Type.fromContent(content)
        )

        coEvery {
          chatApiClient.processNewUserPrompt(any(), content, null, null)
        } returns AiActionResponse(AiAction(requestId, emptyList()))

        chatController.processNewUserPrompt(newUserPromptRequest)

        coVerify {
          chatApiClient.processNewUserPrompt(
            any(),
            content,
            null,
            null
          )
        }
      }
    }

    describe("clearChat") {
      val requestId = "00000000-00000000-00000000-00000000"
      val subscriptionId = UUID.randomUUID().toString()
      describe("with successful API response") {
        it("clears history and the view") {
          coEvery {
            chatApiClient.clearChat(subscriptionId)
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          val initialChatRecords = listOf(
            ChatRecord(
              role = ChatRecord.Role.USER,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            ),
            ChatRecord(
              role = ChatRecord.Role.ASSISTANT,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            )
          )
          initialChatRecords.forEach { chatHistory.addRecord(it) }

          chatController.clearChatWindow(subscriptionId)

          verify(exactly = 1) { view.clear() }
          chatHistory.records shouldHaveSize 0
        }
      }

      describe("with exception") {
        it("does not clear the history or view") {
          val exception = mockk<RuntimeException>()
          coEvery { exception.message } returns "Error Message"

          coEvery {
            chatApiClient.clearChat(subscriptionId)
          } throws exception

          val initialChatRecords = listOf(
            ChatRecord(
              role = ChatRecord.Role.USER,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            ),
            ChatRecord(
              role = ChatRecord.Role.ASSISTANT,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            )
          )
          initialChatRecords.forEach { chatHistory.addRecord(it) }

          shouldThrow<RuntimeException> {
            chatController.clearChatWindow(subscriptionId)
          }

          verify(exactly = 0) { view.clear() }
          chatHistory.records shouldBe initialChatRecords
        }
      }
      describe("with API response errors") {
        it("logs the errors and does not clear history or view") {
          val aiAction = AiAction(requestId, listOf("Test error 1", "Test error 2"))

          coEvery {
            chatApiClient.clearChat(subscriptionId)
          } returns AiActionResponse(aiAction)

          val initialChatRecords = listOf(
            ChatRecord(
              role = ChatRecord.Role.USER,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            ),
            ChatRecord(
              role = ChatRecord.Role.ASSISTANT,
              requestId = UUID.randomUUID().toString(),
              state = ChatRecord.State.READY
            )
          )
          initialChatRecords.forEach { chatHistory.addRecord(it) }

          chatController.clearChatWindow(subscriptionId)

          verify(exactly = 0) { view.clear() }
          chatHistory.records shouldBe initialChatRecords
        }
      }
    }

    describe("selectionChanged") {
      it("should include no code if the lines are null") {
        chatController.selectionChanged(startLine = null, endLine = null)

        val item = slot<AiContextItem>()
        coVerify { contextManager.add(capture(item)) }
        item.captured.id shouldBe "selection"
        item.captured.content shouldBe "None"
        item.captured.category shouldBe AiContextCategory.SNIPPET

        verify { view.setCurrentContextItems(any()) }
      }

      it("should include single line if the start and end line are the same") {
        chatController.selectionChanged(startLine = 2, endLine = 2)

        val item = slot<AiContextItem>()
        coVerify { contextManager.add(capture(item)) }
        item.captured.id shouldBe "selection"
        item.captured.content shouldBe "2"
        item.captured.category shouldBe AiContextCategory.SNIPPET

        verify { view.setCurrentContextItems(any()) }
      }

      it("should include both lines if the start and end line are different") {
        chatController.selectionChanged(startLine = 0, endLine = 2)

        val item = slot<AiContextItem>()
        coVerify { contextManager.add(capture(item)) }
        item.captured.id shouldBe "selection"
        item.captured.content shouldBe "0-2"
        item.captured.category shouldBe AiContextCategory.SNIPPET

        verify { view.setCurrentContextItems(any()) }
      }

      it("should remove existing snippet context item") {
        val contextItem = AiContextItem(
          id = "snippet",
          category = AiContextCategory.SNIPPET,
          content = "0-2",
          metadata = null
        )

        coEvery { contextManager.getCurrentItems() } returns listOf(contextItem)
        coEvery { contextManager.remove(any()) } just Runs

        chatController.selectionChanged(startLine = 4, endLine = 6)

        coVerify { contextManager.remove(contextItem) }
      }
    }
  }
})
