package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.chat.view.WebViewChatView
import com.gitlab.plugin.chat.view.model.*
import com.gitlab.plugin.services.DuoChatStateService
import com.intellij.openapi.wm.ToolWindowManager
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class WebViewChatViewTest : DescribeSpec({
  val toolWindowManager: ToolWindowManager = mockk()
  val chatBrowser: CefChatBrowser = mockk(relaxed = true)
  val duoChatStateService: DuoChatStateService = mockk(relaxed = true)

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("init") {
    describe("adding local resources") {
      it("adds all asset files") {
        WebViewChatView(chatBrowser, toolWindowManager, duoChatStateService = duoChatStateService)

        verify(exactly = 4) { chatBrowser.addLocalResource(any(), any(), any()) }
      }

      it("uses the correct resource path prefix") {
        WebViewChatView(chatBrowser, toolWindowManager, duoChatStateService = duoChatStateService)

        verify { chatBrowser.addLocalResource(any(), WebViewChatView.RESOURCE_PATH_PREFIX, any()) }
      }
    }
  }

  describe("addRecord") {
    val chatRecord = buildChatRecord()

    it("posts a newRecord message to the browser") {
      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(
        chatBrowser,
        toolWindowManager,
        duoChatStateService = duoChatStateService
      ).addRecord(NewRecordMessage(chatRecord))

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.NEW_RECORD
    }
  }

  describe("updateRecord") {
    val chatRecord = buildChatRecord()

    it("posts an updateRecord message to the browser") {
      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(
        chatBrowser,
        toolWindowManager,
        duoChatStateService = duoChatStateService
      ).updateRecord(UpdateRecordMessage(chatRecord))

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.UPDATE_RECORD
    }
  }

  describe("projectStateChanged") {
    it("posts an updateRecord message to the browser") {
      val projectStateChangedRecord = ProjectStateChangedMessage(
        hasSelectedEditorInWindow = true
      )

      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(
        chatBrowser,
        toolWindowManager,
        duoChatStateService = duoChatStateService
      ).projectStateChanged(projectStateChangedRecord)

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.PROJECT_STATE_CHANGED
    }
  }

  describe("onMessage") {
    it("registers a handler with ChatBrowser") {
      WebViewChatView(
        chatBrowser,
        toolWindowManager,
        duoChatStateService = duoChatStateService
      ).onMessage(mockk())

      verify(exactly = 1) { chatBrowser.onPostMessageReceived(any()) }
    }

    it("calls the given block with the deserialized ChatViewMessage") {
      val onPostMessageReceivedSlot = slot<(String) -> Unit>()
      every { chatBrowser.onPostMessageReceived(capture(onPostMessageReceivedSlot)) } just runs

      val blockParam: (ChatViewMessage) -> Unit = mockk(relaxed = true)
      WebViewChatView(
        chatBrowser,
        toolWindowManager,
        duoChatStateService = duoChatStateService
      ).onMessage(blockParam)

      NewPromptMessage("test content").let {
        onPostMessageReceivedSlot.captured(it.toJson())

        verify { blockParam(it) }
      }
    }
  }

  describe("clear") {
    it("sends a clear message to the browser") {
      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(chatBrowser, toolWindowManager, duoChatStateService = duoChatStateService).clear()

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.CLEAR
    }
  }
})
