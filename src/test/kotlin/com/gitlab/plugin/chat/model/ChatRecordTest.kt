package com.gitlab.plugin.chat.model

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.VersionBasedFeature
import com.intellij.openapi.components.service
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe
import io.mockk.*

class ChatRecordTest : DescribeSpec({

  val gitlabServerService = mockk<GitLabServerService>()

  beforeAny {
    val application = mockApplication()
    every { application.service<GitLabServerService>() } returns gitlabServerService
  }

  afterEach {
    clearAllMocks()
  }

  describe("ChatRecord.Type") {
    describe("fromContent") {
      describe("returns the type for a known command for after 17.5.0") {
        every { gitlabServerService.isEnabled(VersionBasedFeature.REMOVE_CLEAN_COMMAND) } returns true

        withData(
          listOf(
            Pair("/explain", ChatRecord.Type.EXPLAIN_CODE),
            Pair("/tests", ChatRecord.Type.GENERATE_TESTS),
            Pair("/refactor", ChatRecord.Type.REFACTOR_CODE),
            Pair("/clear", ChatRecord.Type.CLEAR_CHAT),
            Pair("/clean", ChatRecord.Type.GENERAL),
            Pair("/reset", ChatRecord.Type.NEW_CONVERSATION),
          )
        ) {
          ChatRecord.Type.fromContent(it.first) shouldBe it.second
        }
      }
      describe("returns the type for a known command for before 17.5.0") {
        every { gitlabServerService.isEnabled(VersionBasedFeature.REMOVE_CLEAN_COMMAND) } returns false

        withData(
          listOf(
            Pair("/clear", ChatRecord.Type.CLEAR_CHAT),
            Pair("/clean", ChatRecord.Type.CLEAR_CHAT),
          )
        ) {
          ChatRecord.Type.fromContent(it.first) shouldBe it.second
        }
      }

      describe("recognizes commands that include whitespace") {
        withData(
          listOf(
            Pair("/explain ", ChatRecord.Type.EXPLAIN_CODE),
            Pair(" /explain ", ChatRecord.Type.EXPLAIN_CODE),
            Pair(" /explain", ChatRecord.Type.EXPLAIN_CODE),
          )
        ) {
          ChatRecord.Type.fromContent(it.first) shouldBe it.second
        }
      }

      it("returns general for an unknown command") {
        ChatRecord.Type.fromContent("/unknown") shouldBe ChatRecord.Type.GENERAL
      }
    }
  }
})
