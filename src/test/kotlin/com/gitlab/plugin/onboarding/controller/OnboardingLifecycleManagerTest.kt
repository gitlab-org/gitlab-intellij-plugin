package com.gitlab.plugin.onboarding.controller

import com.gitlab.plugin.onboarding.OnboardingLifecycleManager
import com.gitlab.plugin.onboarding.OnboardingPersistentStateComponent
import com.gitlab.plugin.onboarding.OnboardingState
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import com.intellij.testFramework.registerOrReplaceServiceInstance
import com.intellij.util.application
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant

class OnboardingLifecycleManagerTest : BasePlatformTestCase() {

  private val toolWindow = mockk<ToolWindow>(relaxed = true)
  private val toolWindowManager = mockk<ToolWindowManager>(relaxed = true)
  private val onboardingState = mockk<OnboardingState>(relaxed = true)

  private lateinit var onboardingLifecycleManager: OnboardingLifecycleManager

  @BeforeEach
  override fun setUp() {
    super.setUp()

    application.registerOrReplaceServiceInstance(
      OnboardingPersistentStateComponent::class.java,
      mockk<OnboardingPersistentStateComponent> {
        every { state } returns onboardingState
      },
      testRootDisposable
    )

    onboardingLifecycleManager = OnboardingLifecycleManager(toolWindow, toolWindowManager)
  }

  @AfterEach
  override fun tearDown() {
    disposeRootDisposable()
    super.tearDown()
  }

  @Test
  fun `Tool window should be shown when it's the first time launching the plugin`() {
    every { onboardingState.timeFirstLaunched } returns null

    val slot = slot<Runnable>()
    every { toolWindowManager.invokeLater(capture(slot)) } just Runs

    onboardingLifecycleManager.manageOnboardingLifecycle()

    verify { onboardingState.timeFirstLaunched = any() }

    slot.captured.run()
    verify { toolWindow.show() }
  }

  @Test
  fun `manageOnboardingLifecycle should unregister tool window when onboarding is expired`() {
    onboardingLifecycleManager =
      spyk(OnboardingLifecycleManager(toolWindow, toolWindowManager), recordPrivateCalls = true)
    every { onboardingLifecycleManager["isOnboardingExpired"](any<Instant>()) } returns true

    every { onboardingState.timeFirstLaunched } returns Instant.now()

    val slot = slot<Runnable>()
    every { toolWindowManager.invokeLater(capture(slot)) } just Runs

    onboardingLifecycleManager.manageOnboardingLifecycle()

    slot.captured.run()
    verify { toolWindowManager.unregisterToolWindow(any()) }
  }

  @Test
  fun `completeOnboarding should hide tool window, unregister it, and update settings`() {
    val slot = slot<Runnable>()
    every { toolWindowManager.invokeLater(capture(slot)) } just Runs

    onboardingLifecycleManager.completeOnboarding()

    slot.captured.run()
    verify { toolWindow.hide() }
    verify { toolWindowManager.unregisterToolWindow(any()) }
    verify { onboardingState.hasCompletedOnboarding = true }
  }
}
