package com.gitlab.plugin.codesuggestions.telemetry.destination

import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.intellij.openapi.diagnostic.Logger
import io.kotest.matchers.string.shouldContain
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Test

class LogTelemetryServiceDestinationTest {
  private val logMessageSlot = slot<String>()
  private val mockLogger: Logger = mockk { every { info(capture(logMessageSlot)) } returns Unit }
  private val logTelemetryDestination = LogTelemetryDestination(mockLogger)

  @Test
  fun `event logs the event and model`() {
    logTelemetryDestination.receive(
      Event(
        Event.Type.ACCEPTED,
        Event.Context(trackingId = "test-uuid", modelEngine = "test-engine")
      )
    )

    verify(exactly = 1) { mockLogger.info(any<String>()) }
    logMessageSlot.captured.let {
      it shouldContain "Action: suggestion_accepted, Context: Context("
      it shouldContain "modelEngine=test-engine"
      it shouldContain "trackingId=test-uuid"
    }
  }
}
