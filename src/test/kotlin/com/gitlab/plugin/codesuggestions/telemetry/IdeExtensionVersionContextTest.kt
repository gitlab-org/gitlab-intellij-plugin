package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.*

class IdeExtensionVersionContextTest : DescribeSpec({
  val ideVersion = "IU-232.9921.47"
  val ideName = "IntelliJ IDEA"
  val ideVendor = "JetBrains"
  val extensionName = "GitLab Duo"
  val extensionVersion = "0.4.1"

  beforeEach {
    mockApplicationInfo(buildNumber = ideVersion, applicationName = ideName, companyName = ideVendor)
    mockPlugin(name = extensionName, version = extensionVersion)
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("returns a self-describing JSON with the correct keys and values") {
    IdeExtensionVersionContext.build().let {
      it.map shouldContainAll mapOf(
        "schema" to IdeExtensionVersionContext.SCHEMA,
        "data" to mapOf(
          "ide_name" to ideName,
          "ide_version" to ideVersion,
          "ide_vendor" to ideVendor,
          "extension_name" to extensionName,
          "extension_version" to extensionVersion
        )
      )
    }
  }
})
