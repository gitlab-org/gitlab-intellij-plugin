package com.gitlab.plugin.codesuggestions

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.types.shouldNotBeSameInstanceAs

class SuggestionsProviderTest : DescribeSpec({
  it("should create a new tooltip presentation each request") {
    val provider = SuggestionsProvider()
    val initialPresentation = provider.providerPresentation
    val subsequentPresentation = provider.providerPresentation

    initialPresentation shouldNotBeSameInstanceAs subsequentPresentation
  }
})
