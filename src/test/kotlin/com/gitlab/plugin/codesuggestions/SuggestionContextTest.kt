package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.util.uri
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.editor.LogicalPosition
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk

@Suppress("UnstableApiUsage")
class SuggestionContextTest : DescribeSpec({
  it("should extract prefix and suffix from document") {
    val request = mockk<InlineCompletionRequest> {
      every { document } returns mockk {
        every { charsSequence } returns "prefix__suffix"
      }
      every { endOffset } returns 7
    }

    val context = SuggestionContext(request)

    context.prefix shouldBe "prefix_"
    context.suffix shouldBe "_suffix"
  }

  it("should create request payload based on request information") {
    val request = mockk<InlineCompletionRequest>(relaxed = true) {
      every { document.charsSequence } returns "prefix__suffix"
      every { file.virtualFile.path } returns "src/main.kt"
      every { file.virtualFile.uri } returns "file:///Users/src/main.kt"
      every { editor.project?.basePath } returns "src/"
      every { editor.caretModel.logicalPosition } returns LogicalPosition(10, 20)
      every { endOffset } returns 7
    }

    val payload = SuggestionContext(request).toRequestPayload()

    payload.currentFile.fileName shouldBe "main.kt"
    payload.currentFile.fileUri shouldBe "file:///Users/src/main.kt"
    payload.currentFile.contentAboveCursor shouldBe "prefix_"
    payload.currentFile.contentBelowCursor shouldBe "_suffix"
    payload.currentFile.cursorLine shouldBe 10
    payload.currentFile.cursorColumn shouldBe 20
    payload.telemetry.isEmpty() shouldBe true
  }

  it("isInvoked should initially be false") {
    val context = SuggestionContext(request = mockk<InlineCompletionRequest>(relaxed = true))

    context.isInvoked shouldBe false
  }
})
