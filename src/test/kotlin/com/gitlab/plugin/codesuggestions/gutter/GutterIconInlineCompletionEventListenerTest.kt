package com.gitlab.plugin.codesuggestions.gutter

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.markup.GutterIconRenderer
import com.intellij.openapi.editor.markup.RangeHighlighter
import io.kotest.core.spec.style.DescribeSpec
import io.ktor.util.*
import io.mockk.*
import org.junit.jupiter.api.Assertions.*

class GutterIconInlineCompletionEventListenerTest : DescribeSpec({
  val editor = mockk<Editor>(relaxed = true)
  val exceptionTracker: ExceptionTracker = mockk(relaxUnitFun = true)
  val listener = GutterIconInlineCompletionEventListener(editor)

  beforeEach {
    mockApplication().apply {
      every { getService(ExceptionTracker::class.java) } returns exceptionTracker
    }
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("onShow") {
    val event = mockk<InlineCompletionEventType.Show>()

    it("should not re-throw exceptions") {
      val exception = RuntimeException("Test exception")
      every { event.element } throws exception

      assertDoesNotThrow { listener.onShow(event) }

      verify { exceptionTracker.handle(exception) }
    }
  }

  describe("onRequest") {
    val event = mockk<InlineCompletionEventType.Request>()

    it("should not re-throw exceptions") {
      // Will throw an exception for application not being initialized
      assertDoesNotThrow { listener.onRequest(event) }

      verify { exceptionTracker.handle(any()) }
    }
  }

  describe("cleanup") {
    it("should remove all code suggestions icons from the gutter") {
      val highlighter1 = mockk<RangeHighlighter>(relaxed = true) {
        every { gutterIconRenderer } returns GutterIcons.Loading
      }
      val highlighter2 = mockk<RangeHighlighter>(relaxed = true) {
        every { gutterIconRenderer } returns GutterIcons.Ready
      }
      val highlighter3 = mockk<RangeHighlighter>(relaxed = true) {
        every { gutterIconRenderer } returns mockk<GutterIconRenderer>()
      }
      val highlighter4 = mockk<RangeHighlighter>(relaxed = true) {
        every { gutterIconRenderer } returns null
      }

      every { editor.getUserData(CODE_SUGGESTIONS_GUTTER_ICON_KEY) } returns highlighter1
      every { editor.markupModel.allHighlighters } returns arrayOf(
        highlighter2,
        highlighter3,
        highlighter4
      )

      listener.cleanup()

      verify { highlighter1.dispose() }
      verify { highlighter2.dispose() }
      verify(exactly = 0) { highlighter3.dispose() }
      verify(exactly = 0) { highlighter4.dispose() }
      verify { editor.putUserData(CODE_SUGGESTIONS_GUTTER_ICON_KEY, null) }
    }
  }

  describe("onStreamingInlineCompletionCompletion") {
    it("should update gutter icon to Ready when streaming completion is finished") {
      val highlighter = mockk<RangeHighlighter>(relaxed = true) {
        every { gutterIconRenderer } returns GutterIcons.Loading
      }
      every { editor.getUserData(CODE_SUGGESTIONS_GUTTER_ICON_KEY) } returns highlighter

      listener.onStreamingInlineCompletionCompletion()

      verify { highlighter.gutterIconRenderer = GutterIcons.Ready }
    }
  }
})
