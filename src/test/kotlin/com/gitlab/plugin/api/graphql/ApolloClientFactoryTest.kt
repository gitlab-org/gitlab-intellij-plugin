package com.gitlab.plugin.api.graphql

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import java.net.URI

class ApolloClientFactoryTest : DescribeSpec({
  describe("createSubscriptionUrl") {
    it("returns the correct url for a base url") {
      val baseUrl = "https://example.com"

      ApolloClientFactory().createSubscriptionUrl(URI(baseUrl)) shouldBe "wss://example.com/-/cable"
    }

    it("returns the correct url for a base url with a trailing slash") {
      val baseUrl = "https://example.com/"

      ApolloClientFactory().createSubscriptionUrl(URI(baseUrl)) shouldBe "wss://example.com/-/cable"
    }

    it("returns the correct url for a base url with a port") {
      val baseUrl = "https://example.com:8443"

      ApolloClientFactory().createSubscriptionUrl(URI(baseUrl)) shouldBe "wss://example.com:8443/-/cable"
    }

    it("returns the correct url for a base url with a port and trailing slash") {
      val baseUrl = "https://example.com:8443/"

      ApolloClientFactory().createSubscriptionUrl(URI(baseUrl)) shouldBe "wss://example.com:8443/-/cable"
    }

    it("includes the path when the base url has a relative path") {
      val baseUrl = "https://example.com/gitlab"

      ApolloClientFactory().createSubscriptionUrl(URI(baseUrl)) shouldBe "wss://example.com/gitlab/-/cable"
    }
  }

  describe("createServerUrl") {
    it("returns the correct url for a base url") {
      val baseUrl = "https://example.com"

      ApolloClientFactory().createServerUrl(URI(baseUrl)) shouldBe "https://example.com/api/graphql"
    }

    it("returns the correct url for a base url with a trailing slash") {
      val baseUrl = "https://example.com/"

      ApolloClientFactory().createServerUrl(URI(baseUrl)) shouldBe "https://example.com/api/graphql"
    }

    it("returns the correct url for a base url with a port") {
      val baseUrl = "https://example.com:8443"

      ApolloClientFactory().createServerUrl(URI(baseUrl)) shouldBe "https://example.com:8443/api/graphql"
    }

    it("returns the correct url for a base url with a port and trailing slash") {
      val baseUrl = "https://example.com:8443/"

      ApolloClientFactory().createServerUrl(URI(baseUrl)) shouldBe "https://example.com:8443/api/graphql"
    }

    it("includes the path when the base url has a relative path") {
      val baseUrl = "https://example.com/gitlab"

      ApolloClientFactory().createServerUrl(URI(baseUrl)) shouldBe "https://example.com/gitlab/api/graphql"
    }
  }

  describe("getWebSocketEngine") {
    it("should return null when WebSocket engine is not initialized") {
      val factory = ApolloClientFactory()

      factory.getWebSocketEngine() shouldBe null
    }
  }
})
