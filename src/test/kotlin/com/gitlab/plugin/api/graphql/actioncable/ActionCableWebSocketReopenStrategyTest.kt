package com.gitlab.plugin.api.graphql.actioncable

import com.apollographql.apollo3.exception.ApolloNetworkException
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

class ActionCableWebSocketReopenStrategyTest : DescribeSpec({
  describe("WebSocketReopenBackoff") {
    beforeTest {
      mockkStatic("kotlinx.coroutines.DelayKt")
      coEvery { delay(any<Long>()) } just Runs
    }

    afterTest {
      unmockkAll()
    }

    describe("handleNetworkError") {
      it("should return false if timeout has been exceeded") {
        val result = ActionCableWebSocketReopenStrategy(
          ActionCableWebSocketReopenStrategy.Config(
            initialWebSocketReopenDelayMilliseconds = 500L,
            maxWebSocketReopenDelayMilliseconds = 10_000L,
          )
        ).handleNetworkError(
          ApolloNetworkException("network error"),
          5
        )
        result shouldBe false
      }

      it("should return true if exception timeout has not been exceeded") {
        val result = ActionCableWebSocketReopenStrategy(
          ActionCableWebSocketReopenStrategy.Config(
            initialWebSocketReopenDelayMilliseconds = 100L,
            maxWebSocketReopenDelayMilliseconds = 30_000L,
          )
        ).handleNetworkError(
          ApolloNetworkException("network error"),
          0
        )
        result shouldBe true
      }

      it("should delay an exponentially increasing amount of time until the max is reached") {
        val delays = mutableListOf<Long>()
        coEvery { delay(capture(delays)) } just Runs

        val results = runBlocking {
          (0..4).map { attempt ->
            ActionCableWebSocketReopenStrategy(
              ActionCableWebSocketReopenStrategy.Config(
                initialWebSocketReopenDelayMilliseconds = 500L,
                maxWebSocketReopenDelayMilliseconds = 5_000L,
              )
            ).handleNetworkError(
              ApolloNetworkException("network error"),
              attempt.toLong()
            )
          }
        }

        results shouldBe listOf(
          true, // First attempt
          true, // Second attempt
          true, // Third attempt
          true, // Fourth attempt
          false // Fifth attempt, should exceed max
        )
        delays shouldBe listOf(
          500L,
          1000L,
          2000L,
          1500L, // Total elapsed time + next exponential step would exceed max, so this is the difference between elapsed time and max
        ) // delay should only be called 4 times, as the fifth attempt would exceed max
      }
    }
  }
})
