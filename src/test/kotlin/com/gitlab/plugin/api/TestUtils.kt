package com.gitlab.plugin.api

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.duo.DefaultTrustManagerChain
import com.gitlab.plugin.api.duo.TrustManagerChain
import com.gitlab.plugin.api.duo.UserConfigurableTrustManager
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.ide.plugins.IdeaPluginDescriptor
import com.intellij.openapi.application.Application
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.util.BuildNumber
import com.intellij.util.net.HttpConfigurable
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.mockkStatic
import java.net.http.HttpClient
import java.security.KeyStore
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

fun buildMockEngine(
  statusCode: HttpStatusCode = HttpStatusCode.OK,
  content: String = """{"some_property": "abc"}""",
  block: (HttpRequestData) -> Unit = {}
): HttpClientEngine = MockEngine { request ->
  block.invoke(request)

  respond(
    content = ByteReadChannel(content),
    status = statusCode,
    headers = headersOf(HttpHeaders.ContentType, "application/json")
  )
}

fun mockDuoContextServicePersistentSettings() {
  val duoPersistentSettings: DuoPersistentSettings = mockk()
  mockkObject(DuoPersistentSettings.Companion)
  every { DuoPersistentSettings.getInstance() } returns duoPersistentSettings
  every { DuoPersistentSettings.getInstance().url } returns "https://localhost:8443"

  mockkObject(GitLabUtil)
  every { GitLabUtil.userAgent } returns "gitlab-plugin-test-agent"

  mockkStatic(HttpConfigurable::class)
  every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>()
}

fun OkHttpConfig.configureTestClient(ignoreCertificateErrors: Boolean = false) {
  config {
    val trustManager = UserConfigurableTrustManager(
      mockk<DuoPersistentSettings>().also {
        every { it.ignoreCertificateErrors } returns ignoreCertificateErrors
      },
      mockk<TrustManagerChain>().also {
        every { it.getChain() } returns defaultTrustManagerList()
      }
    )
    val sslContext = SSLContext.getInstance("TLS")
    sslContext.init(null, arrayOf(trustManager), null)
    sslSocketFactory(sslContext.socketFactory, trustManager)
  }
}

fun mockApplication(): Application {
  mockkStatic(ApplicationManager::getApplication)

  val application = mockk<Application>(relaxed = true)
  every { ApplicationManager.getApplication() } returns application

  return application
}

fun mockApplicationInfo(
  applicationName: String = "IntelliJ IDEA",
  buildNumber: String = "IU-232.9921.47",
  companyName: String = "JetBrains",
) {
  val applicationInfoMock: ApplicationInfo = mockk(relaxed = true)
  val buildNumberMock: BuildNumber = mockk(relaxed = true)

  every { applicationInfoMock.build } returns buildNumberMock
  every { buildNumberMock.asString() } returns buildNumber
  every { applicationInfoMock.companyName } returns companyName
  every { applicationInfoMock.shortCompanyName } returns companyName
  every { applicationInfoMock.fullApplicationName } returns applicationName

  mockkStatic(ApplicationInfo::getInstance)
  every { ApplicationInfo.getInstance() } returns applicationInfoMock
}

fun mockPlugin(name: String = "GitLab Duo", version: String = "0.4.1") {
  val pluginMock: IdeaPluginDescriptor = mockk(relaxed = true)

  every { pluginMock.name } returns name
  every { pluginMock.version } returns version

  mockkObject(GitLabBundle)
  every { GitLabBundle.plugin() } returns pluginMock
}

fun mockDefaultTrustManagerChain() {
  mockkObject(DefaultTrustManagerChain)
  every { DefaultTrustManagerChain.getChain() } returns defaultTrustManagerList()
}

fun mockHttpClient(response: String): HttpClient {
  val mockHttpClient = mockk<HttpClient>(relaxed = true)
  val mockBuilder = mockk<HttpClient.Builder>(relaxed = true)

  mockkStatic(HttpClient::class)
  every { HttpClient.newBuilder() } returns mockBuilder
  every { mockBuilder.version(any()) } returns mockBuilder
  every { mockBuilder.build() } returns mockHttpClient

  val mockResponse = mockk<java.net.http.HttpResponse<String>>()
  every { mockResponse.statusCode() } returns 200
  every { mockResponse.body() } returns response

  every {
    mockHttpClient.send(any(), any<java.net.http.HttpResponse.BodyHandler<String>>())
  } returns mockResponse

  return mockHttpClient
}

private fun defaultTrustManagerList(): List<X509TrustManager> {
  val trustManager = TrustManagerFactory.getInstance(
    TrustManagerFactory.getDefaultAlgorithm()
  ).apply { init(null as KeyStore?) }.trustManagers.first() as X509TrustManager
  return listOf(trustManager)
}
