package com.gitlab.plugin.api.okhttp

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import okhttp3.Interceptor
import okhttp3.Request

class OkHttpAuthorizationInterceptorTest : DescribeSpec({
  val token = "glpat-xxxxxxxx"

  val tokenManager = mockk<GitLabTokenProviderManager> {
    every { getToken() } returns token
  }

  val interceptor = OkHttpAuthorizationInterceptor()

  beforeEach {
    val application = mockApplication()

    every { application.getService(GitLabTokenProviderManager::class.java) } returns tokenManager
  }

  afterEach {
    clearAllMocks(answers = false)
  }

  afterSpec {
    unmockkAll()
  }

  it("should add Authorization header when it's not present") {
    val request = Request.Builder().url("https://example.com").build()
    val chain = mockk<Interceptor.Chain>(relaxed = true) {
      every { request() } returns request
    }

    interceptor.intercept(chain)

    verify {
      chain.proceed(
        match {
          it.header("Authorization") == "Bearer glpat-xxxxxxxx"
        }
      )
    }
  }

  it("should not modify the request when Authorization header is already present") {
    val request = Request.Builder()
      .url("https://example.com")
      .header("Authorization", "Bearer glpat-yyyyyyyy")
      .build()
    val chain = mockk<Interceptor.Chain>(relaxed = true) {
      every { request() } returns request
    }

    interceptor.intercept(chain)

    verify {
      chain.proceed(
        match {
          it.header("Authorization") == "Bearer glpat-yyyyyyyy"
        }
      )
    }
    verify(exactly = 0) { tokenManager.getToken() }
  }
})
