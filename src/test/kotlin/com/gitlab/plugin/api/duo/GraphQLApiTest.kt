package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.annotations.ApolloExperimental
import com.apollographql.apollo3.exception.ApolloHttpException
import com.apollographql.apollo3.mockserver.MockResponse
import com.apollographql.apollo3.mockserver.MockServer
import com.apollographql.apollo3.testing.MapTestNetworkTransport
import com.apollographql.apollo3.testing.registerTestResponse
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWebSocketEngine
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockDefaultTrustManagerChain
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.chat.context.*
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.ChatWithAdditionalContextSubscription
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiAdditionalContextCategory
import com.gitlab.plugin.graphql.type.AiMessageRole
import com.gitlab.plugin.graphql.type.AiMessageType
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.intellij.openapi.components.service
import com.intellij.util.asSafely
import io.kotest.common.runBlocking
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldContainOnly
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.flow.reduce
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.net.URI

private val json = Json { ignoreUnknownKeys = true }

@OptIn(ApolloExperimental::class)
@Suppress("LargeClass")
class GraphQLApiTest : DescribeSpec({
  val tokenProviderManager = mockk<GitLabTokenProviderManager>()
  val exceptionHandlerMock = mockk<CreateNotificationExceptionHandler>()
  val exceptionTracker = mockk<ExceptionTracker>(relaxUnitFun = true)

  lateinit var graphqlApi: GraphQLApi
  lateinit var mockServer: MockServer

  beforeSpec {
    mockkConstructor(ActionCableWebSocketEngine::class)
  }

  beforeEach {
    val application = mockApplication()
    mockDefaultTrustManagerChain()
    mockDuoContextServicePersistentSettings()

    mockServer = MockServer()
    every { DuoPersistentSettings.getInstance().url } returns mockServer.url()
    every { application.service<DuoPersistentSettings>() } returns DuoPersistentSettings.getInstance()

    every { application.service<GitLabTokenProviderManager>() } returns tokenProviderManager
    every { tokenProviderManager.getToken() } returns "some_token"

    val apolloClientFactory = ApolloClientFactory()
    every { anyConstructed<ActionCableWebSocketEngine>().isOpen } returns true
    every { application.service<ApolloClientFactory>() } returns apolloClientFactory
    every { application.service<ExceptionTracker>() } returns exceptionTracker

    graphqlApi = GraphQLApi.create(apolloClientFactory.create(), exceptionHandlerMock)

    every { exceptionHandlerMock.handleException(any()) } returns Unit
  }

  afterEach {
    runBlocking { mockServer.stop() }
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  describe("getCurrentUser") {
    @Suppress("DEPRECATION")
    it("returns a valid user") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "currentUser": {
                  "id": "gid://gitlab/User/1",
                  "duoChatAvailable": true,
                  "duoCodeSuggestionsAvailable": true,
                  "id": "gid://gitlab/User/1",
                  "ide": { "codeSuggestionsEnabled": true }
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }

      actual.shouldNotBeNull()
      actual.duoChatAvailable.shouldNotBeNull().shouldBeTrue()
      actual.duoCodeSuggestionsAvailable.shouldNotBeNull().shouldBeTrue()
      actual.id.shouldBeEqual("gid://gitlab/User/1")
      actual.ide.let { ide ->
        ide.shouldNotBeNull()
        ide.codeSuggestionsEnabled.shouldBeTrue()
      }
    }

    @Suppress("DEPRECATION")
    it("returns null for fields absent from old schema versions") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "currentUser": {
                  "id": "gid://gitlab/User/1",
                  "ide": { "codeSuggestionsEnabled": true }
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }

      actual.shouldNotBeNull()
      actual.duoChatAvailable.shouldBeNull()
      actual.duoCodeSuggestionsAvailable.shouldBeNull()
      actual.id.shouldBeEqual("gid://gitlab/User/1")
      actual.ide.let { ide ->
        ide.shouldNotBeNull()
        ide.codeSuggestionsEnabled.shouldBeTrue()
      }
    }
  }

  describe("chatMutation") {
    it("requests a ChatMutation and returns a request id") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "aiAction": {
                  "requestId": "abc",
                  "errors": []
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.chatMutation("question", "123") }

      val request = mockServer.takeRequest()
      val requestBody = json.decodeFromString<GraphQlRequestBody>(request.body.utf8())
      requestBody.variables.question shouldBe "question"

      actual.shouldNotBeNull()
      actual.requestId.shouldNotBeNull().shouldBe("abc")
    }

    it("includes current file when present") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "aiAction": {
                  "requestId": "abc",
                  "errors": []
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val currentFile = ChatRecordFileContext(
        fileName = "the-file.txt",
        selectedText = "some selected text",
        contentAboveCursor = "some content above",
        contentBelowCursor = "some content below"
      )
      val ctx = ChatRecordContext(currentFile)
      val actual = runBlocking { graphqlApi.chatMutation("question", "123", context = ctx) }

      val request = mockServer.takeRequest()
      val requestBody = json.decodeFromString<GraphQlRequestBody>(request.body.utf8())
      requestBody.variables.question shouldBe "question"
      requestBody.variables.currentFileContext?.fileName shouldBe "the-file.txt"
      requestBody.variables.currentFileContext?.selectedText shouldBe "some selected text"
      requestBody.variables.currentFileContext?.contentAboveCursor shouldBe "some content above"
      requestBody.variables.currentFileContext?.contentBelowCursor shouldBe "some content below"

      actual.shouldNotBeNull()
      actual.requestId.shouldNotBeNull().shouldBe("abc")
    }

    it("includes ai context items when present") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
      {
        "data": {
          "aiAction": {
            "requestId": "abc",
            "errors": []
          }
        }
      }
      """
          )
          .statusCode(200)
          .build()
      )

      val aiContextItem1 = AiContextItem(
        id = "context-item-id-1",
        category = AiContextCategory.FILE,
        content = "Some content",
        metadata = AIContextItemMetadata(
          title = "Context Item",
          enabled = true,
          disabledReasons = listOf("test"),
          subType = AIContextProviderType.LOCAL_FILE_SEARCH,
          relativePath = "path/to/file",
          iid = "12345",
          project = "project",
          webUrl = "webUrl",
          workspaceFolder = AiContextItemWorkspaceFolder("uri", "name"),
          secondaryText = "secondaryText",
          subTypeLabel = "subTypeLabel",
          icon = "icons/icon",
          repositoryUri = "repositoryUri",
          repositoryName = "repositoryName",
          selectedBranch = "selectedBranch",
          gitType = "gitType",
          libs = listOf(
            AiContextItemDependencyLibrary(
              name = "name",
              version = "1.0.0"
            )
          )
        )
      )
      val aiContextItem2 = aiContextItem1.copy(id = "context-item-id-2")

      val actual = runBlocking {
        graphqlApi.chatMutation(
          content = "question",
          clientSubscriptionId = "123",
          aiContextItems = listOf(aiContextItem1, aiContextItem2)
        )
      }

      val request = mockServer.takeRequest()
      val requestBody = json.decodeFromString<GraphQlRequestBody>(request.body.utf8())
      requestBody.variables.question shouldBe "question"
      requestBody.variables.additionalContext?.shouldHaveSize(2)

      requestBody.variables.additionalContext!![0].id shouldBe "context-item-id-1"
      requestBody.variables.additionalContext[0].metadata.title shouldBe "Context Item"
      requestBody.variables.additionalContext[0].metadata.enabled shouldBe true
      requestBody.variables.additionalContext[0].metadata.disabledReasons shouldBe listOf("test")
      requestBody.variables.additionalContext[0].metadata.subType shouldBe "LOCAL_FILE_SEARCH"
      requestBody.variables.additionalContext[0].metadata.relativePath shouldBe "path/to/file"
      requestBody.variables.additionalContext[0].metadata.iid shouldBe "12345"
      requestBody.variables.additionalContext[0].metadata.project shouldBe "project"
      requestBody.variables.additionalContext[0].metadata.webUrl shouldBe "webUrl"
      requestBody.variables.additionalContext[0].metadata.workspaceFolder?.uri shouldBe "uri"
      requestBody.variables.additionalContext[0].metadata.workspaceFolder?.name shouldBe "name"
      requestBody.variables.additionalContext[0].metadata.secondaryText shouldBe "secondaryText"
      requestBody.variables.additionalContext[0].metadata.subTypeLabel shouldBe "subTypeLabel"
      requestBody.variables.additionalContext[0].metadata.icon shouldBe "icons/icon"
      requestBody.variables.additionalContext[0].metadata.repositoryUri shouldBe "repositoryUri"
      requestBody.variables.additionalContext[0].metadata.repositoryName shouldBe "repositoryName"
      requestBody.variables.additionalContext[0].metadata.selectedBranch shouldBe "selectedBranch"
      requestBody.variables.additionalContext[0].metadata.gitType shouldBe "gitType"
      requestBody.variables.additionalContext[0].metadata.libs!![0].name shouldBe "name"
      requestBody.variables.additionalContext[0].metadata.libs!![0].version shouldBe "1.0.0"

      requestBody.variables.additionalContext[1].id shouldBe "context-item-id-2"

      actual.shouldNotBeNull()
      actual.requestId.shouldNotBeNull().shouldBe("abc")
    }

    it("should timeout a chat mutation request if websocket is not able to establish a connection in time") {
      every { anyConstructed<ActionCableWebSocketEngine>().isOpen } returns false

      val result = runBlocking {
        graphqlApi.chatMutation(content = "question", clientSubscriptionId = "123")
      }

      result shouldBe null
      verify { exceptionTracker.handle(any<TimeoutCancellationException>()) }
    }
  }

  describe("chatQuery") {
    it("responds with messages including additional context") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "messages": {
                  "nodes": [{
                    "role": "USER",
                    "content": "test",
                    "contentHtml": "<html>test</html>",
                    "requestId": "requestId",
                    "type": "TOOL",
                    "extras": {
                      "sources": null,
                      "additionalContext": [
                        {
                          "id": "context-item-1",
                          "category": "FILE",
                          "metadata": {
                            "title": "Context Item 1",
                            "subType": "LOCAL_FILE_SEARCH",
                            "relativePath": "path/to/file1",
                            "icon": "icons/icon",
                            "secondaryText": "secondaryText",
                            "subTypeLabel": "subTypeLabel",
                            "enabled": true,
                            "disabledReasons": ["test"],
                            "iid": "1234",
                            "workspaceFolder": { "uri": "uri", "name": "name" },
                            "project": "project",
                            "webUrl": "url",
                            "repositoryUri": "repositoryUri",
                            "repositoryName": "repositoryName",
                            "selectedBranch": "selectedBranch",
                            "gitType": "gitType",
                            "libs": [
                              { "name": "name", "version": "1" }
                            ]
                          }
                        }
                      ]
                    }
                  }]
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.chatQuery("abc", useAdditionalContext = true) }

      actual.let { messages ->
        messages.shouldNotBeNull()
        messages shouldHaveSize 1

        messages.first().let { message ->
          message.shouldNotBeNull()

          message.role shouldBe "user"
          message.content shouldBe "test"
          message.errors shouldHaveSize 0
          message.contentHtml shouldBe "<html>test</html>"
          message.requestId shouldBe "abc"
          message.chunkId shouldBe null

          message.extras!!.contextItems!! shouldHaveSize 1
          message.extras!!.contextItems!![0].id shouldBe "context-item-1"
          message.extras!!.contextItems!![0].category shouldBe AiContextCategory.FILE
          message.extras!!.contextItems!![0].metadata!!.title shouldBe "Context Item 1"
          message.extras!!.contextItems!![0].metadata!!.subType shouldBe AIContextProviderType.LOCAL_FILE_SEARCH
          message.extras!!.contextItems!![0].metadata!!.enabled shouldBe true
          message.extras!!.contextItems!![0].metadata!!.relativePath shouldBe "path/to/file1"
          message.extras!!.contextItems!![0].metadata!!.disabledReasons shouldContainOnly listOf("test")
          message.extras!!.contextItems!![0].metadata!!.iid shouldBe "1234"
          message.extras!!.contextItems!![0].metadata!!.workspaceFolder!!.uri shouldBe "uri"
          message.extras!!.contextItems!![0].metadata!!.workspaceFolder!!.name shouldBe "name"
          message.extras!!.contextItems!![0].metadata!!.project shouldBe "project"
          message.extras!!.contextItems!![0].metadata!!.webUrl shouldBe "url"
          message.extras!!.contextItems!![0].metadata!!.icon shouldBe "icons/icon"
          message.extras!!.contextItems!![0].metadata!!.secondaryText shouldBe "secondaryText"
          message.extras!!.contextItems!![0].metadata!!.subTypeLabel shouldBe "subTypeLabel"
          message.extras!!.contextItems!![0].metadata!!.repositoryUri shouldBe "repositoryUri"
          message.extras!!.contextItems!![0].metadata!!.repositoryName shouldBe "repositoryName"
          message.extras!!.contextItems!![0].metadata!!.selectedBranch shouldBe "selectedBranch"
          message.extras!!.contextItems!![0].metadata!!.gitType shouldBe "gitType"
          message.extras!!.contextItems!![0].metadata!!.libs!![0].name shouldBe "name"
          message.extras!!.contextItems!![0].metadata!!.libs!![0].version shouldBe "1"
        }
      }
    }

    it("responds with messages without additional context") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "messages": {
                  "nodes": [{
                    "role": "USER",
                    "content": "test"
                  }]
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.chatQuery("abc", useAdditionalContext = false) }

      actual.let { messages ->
        messages.shouldNotBeNull()
        messages shouldHaveSize 1

        messages.first().let { message ->
          message.shouldNotBeNull()

          message.role shouldBe "user"
          message.content shouldBe "test"
        }
      }
    }

    it("handles server error") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
                {
                  "errors": [{
                    "message": "An error occurred"
                  }]
                }
                """
          )
          .statusCode(500)
          .build()
      )

      coEvery { exceptionHandlerMock.handleException(any()) } returns Unit

      runBlocking { graphqlApi.chatQuery("abc", useAdditionalContext = false) }

      coVerify { exceptionHandlerMock.handleException(ofType(ApolloHttpException::class)) }
    }
  }

  describe("server errors") {
    it("should throw for a 500 internal server error") {
      mockServer.enqueue(
        MockResponse.Builder().body("Internal server error").statusCode(500).build()
      )

      val actual = runBlocking { graphqlApi.getCurrentUser() }
      actual.shouldBeNull()

      verify(exactly = 1) { exceptionHandlerMock.handleException(any()) }
    }
  }

  describe("getProject") {
    it("returns a project with duoFeatures disabled") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "project": {
                  "duoFeaturesEnabled": false
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getProject("test/test-project") }

      actual.shouldNotBeNull()
      actual.duoFeaturesEnabled.shouldNotBeNull().shouldBeFalse()
    }

    it("returns a null project when projectPath is empty") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "project": null
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getProject("") }
      actual.shouldBeNull()
    }

    it("throws an error that is handled by the exceptionHandler if the queried field doesn't exist") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "errors": [
                {
                  "message": "Field 'duoFeaturesEnabled' doesn't exist on type 'Project'",
                  "locations": [
                    {
                      "line": 5,
                      "column": 5
                    }
                  ],
                  "path": [
                    "query ProjectQuery",
                    "project",
                    "duoFeaturesEnabled"
                  ],
                  "extensions": {
                    "code": "undefinedField",
                    "typeName": "Project",
                    "fieldName": "duoFeaturesEnabled"
                  }
                }
              ]
            }
            """
          )
          .statusCode(200)
          .build()
      )

      val actual = runBlocking { graphqlApi.getProject("test/test-project") }
      actual.shouldBeNull()

      verify(exactly = 1) { exceptionHandlerMock.handleException(any()) }
    }
    it("throws an error that is handled by the exceptionHandler if the user is not authorized") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "errors": [
                {
                  "message": "Http request failed with status code `401`"
                }
              ]
            }
            """
          )
          .statusCode(401)
          .build()
      )

      val actual = runBlocking { graphqlApi.getProject("test/test-project") }
      actual.shouldBeNull()

      verify(exactly = 1) { exceptionHandlerMock.handleException(any()) }
    }
  }

  describe("base url changed") {
    beforeEach {
      runBlocking { mockServer.stop() }
      mockServer = MockServer()

      every { DuoPersistentSettings.getInstance().url } returns mockServer.url()
      every { tokenProviderManager.getToken() } returns "super_secure_token"
    }

    it("is able to make queries to the new base url") {
      mockServer.enqueue(
        MockResponse.Builder()
          .body(
            """
            {
              "data": {
                "project": {
                  "duoFeaturesEnabled": false
                }
              }
            }
            """
          )
          .statusCode(200)
          .build()
      )
      graphqlApi.reload()

      val actual = runBlocking { graphqlApi.getProject("test/test-project") }

      val request = mockServer.takeRequest()
      request.headers["Authorization"] shouldBe "Bearer super_secure_token"
      request.headers["Host"] shouldBe URI(mockServer.url()).authority

      actual.shouldNotBeNull()
    }
  }

  describe("create chat subscription without additional context") {
    it("returns a flow with the expected response") {
      val apolloClient: ApolloClient by lazy {
        ApolloClient.Builder()
          .networkTransport(MapTestNetworkTransport())
          .build()
      }
      val subject = GraphQLApi.create(apolloClient, exceptionHandlerMock)
      val expected = ChatSubscription.AiCompletionResponse(
        id = "id",
        errors = emptyList(),
        requestId = "requestId",
        timestamp = "",
        chunkId = 1,
        content = "content plain",
        extras = ChatSubscription.Extras(emptyList()),
        role = AiMessageRole.ASSISTANT,
        type = AiMessageType.TOOL,
        contentHtml = "content html"
      )

      val clientSubscriptionId = "clientSubscriptionId"
      val userId = UserID("gid://gitlab/User/1")
      apolloClient.registerTestResponse(
        operation = ChatSubscription(
          clientSubscriptionId = clientSubscriptionId,
          userId = userId,
        ),
        data = ChatSubscription.Data(expected),
      )

      runBlocking {
        subject.chatSubscription(
          clientSubscriptionId,
          useAdditionalContext = false,
          userId
        )
      }
        ?.reduce { _, value -> value }
        .let {
          it.shouldNotBeNull()
          it.dataAssertNoErrors.asSafely<ChatSubscription.Data>()?.aiCompletionResponse?.shouldBeEqual(expected)
        }
    }
  }

  describe("create chat subscription with additional context") {
    it("returns a flow with the expected response") {
      val apolloClient: ApolloClient by lazy {
        ApolloClient.Builder()
          .networkTransport(MapTestNetworkTransport())
          .build()
      }
      val subject = GraphQLApi.create(apolloClient, exceptionHandlerMock)
      val expected = ChatWithAdditionalContextSubscription.AiCompletionResponse(
        id = "id",
        errors = emptyList(),
        requestId = "requestId",
        timestamp = "",
        chunkId = 1,
        content = "content plain",
        role = AiMessageRole.ASSISTANT,
        type = AiMessageType.TOOL,
        contentHtml = "content html",
        extras = ChatWithAdditionalContextSubscription.Extras(
          sources = emptyList<String>(),
          additionalContext = listOf(
            ChatWithAdditionalContextSubscription.AdditionalContext(
              id = "file-1",
              category = AiAdditionalContextCategory.FILE,
              metadata = LinkedHashMap<String, Any>().apply {
                put("title", "file-1")
                put("enabled", true)
                put("subType", "LOCAL_FILE_SEARCH")
                put("relativePath", "src/index.ts")
              }
            )
          )
        )
      )

      val clientSubscriptionId = "clientSubscriptionId"
      val userId = UserID("gid://gitlab/User/1")
      apolloClient.registerTestResponse(
        operation = ChatWithAdditionalContextSubscription(
          clientSubscriptionId = clientSubscriptionId,
          userId = userId,
        ),
        data = ChatWithAdditionalContextSubscription.Data(expected),
      )

      runBlocking {
        subject.chatSubscription(
          clientSubscriptionId,
          useAdditionalContext = true,
          userId
        )
      }
        ?.reduce { _, value -> value }
        .let {
          it.shouldNotBeNull()
          it.dataAssertNoErrors.asSafely<ChatSubscription.Data>()?.aiCompletionResponse?.shouldBeEqual(expected)
        }
    }
  }
})

@Serializable
data class GraphQlRequestBody(val variables: GraphQlVariables) {
  @Serializable
  data class GraphQlVariables(
    val question: String,
    val resourceId: String? = null,
    val currentFileContext: CurrentFile? = null,
    val platformOrigin: String? = null,
    val additionalContext: List<AiAdditionalContextInput>? = null
  ) {
    @Serializable
    data class CurrentFile(
      val fileName: String,
      val selectedText: String? = null,
      val contentAboveCursor: String? = null,
      val contentBelowCursor: String? = null
    )

    @Serializable
    data class AiAdditionalContextInput(
      val id: String,
      val metadata: Metadata
    ) {
      @Serializable
      data class Metadata(
        val title: String,
        val enabled: Boolean,
        val disabledReasons: List<String>? = null,
        val subType: String,
        val relativePath: String? = null,
        val iid: String? = null,
        val workspaceFolder: WorkspaceFolder? = null,
        val project: String? = null,
        val webUrl: String? = null,
        val icon: String? = null,
        val secondaryText: String? = null,
        val subTypeLabel: String? = null,
        val repositoryUri: String? = null,
        val repositoryName: String? = null,
        val selectedBranch: String? = null,
        val gitType: String? = null,
        val libs: List<DependencyLibrary>? = null
      ) {
        @Serializable
        data class DependencyLibrary(
          val name: String,
          val version: String? = null
        )

        @Serializable
        data class WorkspaceFolder(
          val uri: String,
          val name: String
        )
      }
    }
  }
}
