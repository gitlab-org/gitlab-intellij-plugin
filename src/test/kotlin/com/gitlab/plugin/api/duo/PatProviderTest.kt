package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.OnePasswordTokenProvider
import com.gitlab.plugin.authentication.PatProvider
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class PatProviderTest : DescribeSpec({
  describe("token") {
    val onePasswordTokenProvider = mockk<OnePasswordTokenProvider>(relaxed = true)
    val duoPersistentSettings = mockk<DuoPersistentSettings>()

    beforeEach {
      val application = mockApplication()
      every { application.getService(OnePasswordTokenProvider::class.java) } returns onePasswordTokenProvider
      every { application.getService(DuoPersistentSettings::class.java) } returns duoPersistentSettings

      mockDuoContextServicePersistentSettings()
      mockkObject(TokenUtil)
    }

    afterEach {
      clearAllMocks(answers = false)
    }

    afterSpec { unmockkAll() }

    context("with default token settings") {
      context("without a personal access token configured") {
        beforeEach {
          every { duoPersistentSettings.integrate1PasswordCLI } returns false
          every { TokenUtil.getToken() } returns null
        }

        it("returns the previously persisted token") {
          PatProvider().token() shouldBe ""
        }

        it("returns the previously persisted token without a keychain lookup when slowOperationsPermitted is false") {
          PatProvider().token(slowOperationsPermitted = false) shouldBe ""
          verify(exactly = 0) { TokenUtil.getToken() }
        }
      }

      context("without the 1password CLI integration configured") {
        beforeEach {
          every { duoPersistentSettings.integrate1PasswordCLI } returns false
          every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
        }

        it("returns the previously persisted token") {
          PatProvider().token() shouldBe "glpat-xxxxxxxx"
        }

        it("returns an empty string when slowOperationsPermitted is false") {
          PatProvider().token(slowOperationsPermitted = false) shouldBe ""
          verify(exactly = 0) { TokenUtil.getToken() }
        }
      }

      context("with the 1password CLI integration configured") {
        it("returns the personal access token stored in 1Password") {
          every { duoPersistentSettings.integrate1PasswordCLI } returns true
          every { onePasswordTokenProvider.token() } returns "glpat-xxxxxxxx"

          PatProvider().token() shouldBe "glpat-xxxxxxxx"
        }

        it("returns an empty string when slowOperationsPermitted is false") {
          PatProvider().token(slowOperationsPermitted = false) shouldBe ""
          verify(exactly = 0) { TokenUtil.getToken() }
        }
      }
    }

    context("with cached personal access token") {
      it("should not get keychain token again") {
        every { duoPersistentSettings.integrate1PasswordCLI } returns false
        every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"
        patProvider.token() shouldBe "glpat-xxxxxxxx"

        verify(exactly = 1) { TokenUtil.getToken() }
      }

      it("should not get 1password token again") {
        every { duoPersistentSettings.integrate1PasswordCLI } returns true
        every { onePasswordTokenProvider.token() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"
        patProvider.token() shouldBe "glpat-xxxxxxxx"

        verify(exactly = 1) { onePasswordTokenProvider.token() }
      }

      it("should update cached token with updated token settings") {
        every { duoPersistentSettings.integrate1PasswordCLI } returns false
        every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"

        val updatedSettings = WorkspaceSettingsBuilder { token("glpat-v2xxxxxx") }
        patProvider.cacheToken(updatedSettings.token)

        patProvider.token() shouldBe "glpat-v2xxxxxx"
        verify(exactly = 1) { TokenUtil.getToken() }
      }

      it("returns cached token when slowOperationsPermitted is false") {
        every { duoPersistentSettings.integrate1PasswordCLI } returns false
        every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"
        patProvider.token(slowOperationsPermitted = false) shouldBe "glpat-xxxxxxxx"
        verify(exactly = 1) { TokenUtil.getToken() }
      }
    }
  }
})
