package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.configureTestClient
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.gitlab.plugin.integrationtest.IntegrationTestEnvironment
import com.gitlab.plugin.integrationtest.IntegrationTestEnvironment.Configured
import com.intellij.openapi.components.service
import io.kotest.core.annotation.EnabledIf
import io.kotest.core.spec.style.DescribeSpec
import io.ktor.client.engine.okhttp.*
import io.mockk.*
import org.junit.jupiter.api.assertDoesNotThrow
import kotlin.test.assertNotNull

@EnabledIf(Configured::class)
class DuoClientIT : DescribeSpec({
  val env = IntegrationTestEnvironment.forSpec(
    DuoClientIT::class,
    IntegrationTestEnvironment(
      personalAccessToken = System.getenv("TEST_ACCESS_TOKEN"),
      gitlabHost = System.getenv("TEST_GITLAB_PROXY_HOST") ?: "https://localhost:8443",
      kclass = DuoClientIT::class,
    )
  )

  fun subject(ignoreCertificateErrors: Boolean) = DuoClient(
    userAgent = "gitlab-duo-plugin-integration-test",
    shouldRetry = false,
    httpClientEngine = OkHttp.create {
      configureTestClient(ignoreCertificateErrors = ignoreCertificateErrors)
    },
    host = env.gitlabHost,
  )

  describe("a non-trusted certificate chain") {
    val duoPersistentSettings = mockk<DuoPersistentSettings>()
    val exceptionTracker = mockk<ExceptionTracker>(relaxed = true)

    beforeEach {
      assertNotNull(env.personalAccessToken, "personalAccessToken must not be null")
      mockDuoContextServicePersistentSettings()
      val application = mockApplication()
      val tokenProviderManager = mockk<GitLabTokenProviderManager>()
      every { application.service<GitLabTokenProviderManager>() } returns tokenProviderManager
      every { application.service<DuoPersistentSettings>() } returns duoPersistentSettings
      every { service<ExceptionTracker>() } returns exceptionTracker
      every { tokenProviderManager.getToken() } returns env.personalAccessToken.toString()
    }

    afterEach {
      clearAllMocks()
    }

    afterSpec {
      unmockkAll()
    }

    it("ignoring certificate errors").config(enabledIf = env.running()) {
      every { duoPersistentSettings.ignoreCertificateErrors } returns true
      subject(ignoreCertificateErrors = true).let {
        assertDoesNotThrow { it.get("/api/v4/metadata") }
      }
    }

    it("without ignoring certificate errors").config(enabledIf = env.running()) {
      every { duoPersistentSettings.ignoreCertificateErrors } returns false
      subject(ignoreCertificateErrors = false).let {
        it.get("/api/v4/metadata")
        verify(exactly = 1) { exceptionTracker.handle(any()) }
      }
    }
  }
})
