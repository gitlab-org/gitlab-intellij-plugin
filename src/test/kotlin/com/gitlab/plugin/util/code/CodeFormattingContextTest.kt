package com.gitlab.plugin.util.code

import com.intellij.lang.Language
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.LogicalPosition
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk

class CodeFormattingContextTest : DescribeSpec({
  val project = mockk<Project>()
  val editor = mockk<Editor>(relaxed = true)
  val document = mockk<Document>()
  val language = mockk<Language>()

  beforeEach {
    every { editor.document } returns document
  }

  afterEach {
    clearAllMocks()
  }

  it("should have current as empty if it is empty") {
    every { document.text } returns "package main\n         "
    every { editor.caretModel.logicalPosition } returns LogicalPosition(1, 0)

    val context = CodeFormattingContext(
      project,
      editor,
      prefix = "package main\n         ",
      suffix = "",
      language
    )

    context.isCurrentLineEmpty shouldBe true
  }

  it("should have current as empty if the line is out of bounds") {
    every { document.text } returns "package main\nfunc main"
    every { editor.caretModel.logicalPosition } returns LogicalPosition(3, 0)

    val context = CodeFormattingContext(
      project,
      editor,
      prefix = "package main\nfunc main",
      suffix = "",
      language
    )

    context.isCurrentLineEmpty shouldBe true
  }

  it("should not have current as empty if it is not empty") {
    every { document.text } returns "package main\nfunc main"
    every { editor.caretModel.logicalPosition } returns LogicalPosition(1, 0)

    val context = CodeFormattingContext(
      project,
      editor,
      prefix = "package main\nfunc main",
      suffix = "",
      language
    )

    context.isCurrentLineEmpty shouldBe false
  }
})
