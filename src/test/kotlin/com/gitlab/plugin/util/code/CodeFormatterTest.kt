package com.gitlab.plugin.util.code

import com.intellij.lang.Language
import com.intellij.testFramework.HeavyPlatformTestCase
import com.intellij.testFramework.runInEdtAndWait
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

/*
 * HTML is used for the tests as it is a language supported by
 * GitLab code suggestions and installed by default in the platform.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CodeFormatterTest : HeavyPlatformTestCase() {
  private val codeFormatter = CodeFormatter()

  @BeforeAll
  override fun setUp() {
    super.setUp()
  }

  @AfterEach
  fun clear() {
    clearAllMocks()
  }

  @AfterAll
  override fun tearDown() {
    runInEdtAndWait { super.tearDown() }
  }

  @Test
  fun `it should not format code if it is inline`() {
    val prefix = "<html>\n"
    val code = "<h1>Hello</h1>"
    val suffix = "\n</html>"

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe code
  }

  @Test
  fun `it should throw when on empty code`() {
    val prefix = "<html>\n"
    val code = ""
    val suffix = "\n</html>"

    val result = shouldNotThrowAny {
      runBlocking {
        codeFormatter.format(
          code = code,
          context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
        )
      }
    }

    result shouldBe code
  }

  @Test
  fun `given code is inline after being trimmed, it should not be formatted`() {
    val prefix = "<html>\n"
    val code = "\n     <h1>Hello</h1>\n"
    val suffix = "\n</html>"

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe "<h1>Hello</h1>"
  }

  @Test
  fun `it should still attempt to format incomplete code`() {
    val prefix = "\n"
    val code = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>"
    val suffix = ""

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>"
  }

  /*
   * The CodeStyleManager will do it's best to indent the code using the AST even if the code is invalid.
   * The quality of the result will depend on how broken the code is.
   */
  @Test
  fun `it should still attempt to format invalid code`() {
    val prefix = "\n"
    // Invalid h1 closing tag and no p closing tag.
    val code = "<div>\n<h1>Hello</h2>\n<p>Tests are running...\n</div>"
    val suffix = ""

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h2>\n" +
      "        <p>Tests are running...\n" +
      "</div>"
  }

  @Test
  fun `it should trim start and end of code if it starts on empty line`() {
    val prefix = ""
    val code = "\n\t<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n\t    "
    val suffix = ""

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  @Test
  fun `it should only trim the end of code if it starts on a non-empty line`() {
    val prefix = "<html>\n<bo"
    val code = "dy>\n<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n</body>\n\n\n\n\t    "
    val suffix = "\n</html>"

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = false),
      )
    }

    result shouldBe "dy>\n" +
      "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>\n" +
      "</body>"
  }

  @Test
  fun `given prefix and suffix, it should indent all code lines except the current one`() {
    val prefix = "<html>\n<bo"
    val code = "dy>\n<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>\n</body>"
    val suffix = "\n</html>"

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = false)
      )
    }

    result shouldBe "dy>\n" +
      "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>\n" +
      "</body>"
  }

  @Test
  fun `given no prefix and no suffix, it should indent all code lines except the current one`() {
    val prefix = ""
    val code = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = ""

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  @Test
  fun `given prefix and no suffix, it should indent all code lines except the current one`() {
    val prefix = "<div>\n<h1>"
    val code = "Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = ""

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = false)
      )
    }

    result shouldBe "Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  @Test
  fun `given no prefix and suffix, it should indent all code lines except the current one`() {
    val prefix = ""
    val code = "<div>\n<h1>Hello</h1>\n<p>Tests are running...</p>\n</div>"
    val suffix = "\n<!-- trailing comment -->"

    val result = runBlocking {
      codeFormatter.format(
        code = code,
        context = formattingContext(prefix, suffix, isCurrentLineEmpty = true)
      )
    }

    result shouldBe "<div>\n" +
      "    <h1>Hello</h1>\n" +
      "    <p>Tests are running...</p>\n" +
      "</div>"
  }

  private fun formattingContext(prefix: String, suffix: String, isCurrentLineEmpty: Boolean) = mockk<CodeFormattingContext> {
    every { project } returns getProject()
    every { this@mockk.prefix } returns prefix
    every { this@mockk.suffix } returns suffix
    every { this@mockk.isCurrentLineEmpty } returns isCurrentLineEmpty
    every { language } returns Language.findLanguageByID("HTML")!!
  }

  override fun getName(): String {
    return javaClass.simpleName
  }
}
