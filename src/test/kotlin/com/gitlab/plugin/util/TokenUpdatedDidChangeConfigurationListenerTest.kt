package com.gitlab.plugin.util

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.OnePasswordTokenProvider
import com.gitlab.plugin.authentication.PatProvider
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class TokenUpdatedDidChangeConfigurationListenerTest : DescribeSpec({
  val userService = mockk<GitLabUserService>(relaxed = true)

  val patProvider = mockk<PatProvider>(relaxUnitFun = true)
  val onePasswordProvider = mockk<OnePasswordTokenProvider>(relaxUnitFun = true)

  val listener = TokenUpdatedDuoConfigurationChangedListener()

  beforeEach {
    val application = mockApplication()

    every { application.service<PatProvider>() } returns patProvider
    every { application.service<OnePasswordTokenProvider>() } returns onePasswordProvider
    every { application.service<GitLabUserService>() } returns userService
    every { onePasswordProvider.getTokenBySecretReference(any()) } returns "keychain_token"

    mockkObject(TokenUtil)
    every { TokenUtil.setToken(any()) } returns Unit

    mockProgressManager()
  }

  afterEach {
    clearAllMocks()
    unmockkAll()
  }

  it("updates TokenUtil token when keychain token is configured") {
    val settings = WorkspaceSettingsBuilder { token("keychain_token") }

    listener.onConfigurationChange(settings)

    verify { TokenUtil.setToken("keychain_token") }
  }

  it("clears TokenUtil when other token type is configured") {
    val settings = WorkspaceSettingsBuilder { onePassword("keychain_token") }

    listener.onConfigurationChange(settings)

    verify { TokenUtil.setToken("") }
  }

  it("updates patProvider cache when token is updated") {
    val settings = WorkspaceSettingsBuilder { onePassword("keychain_token") }

    listener.onConfigurationChange(settings)

    coVerify { patProvider.cacheToken("keychain_token") }
  }

  it("invalidates current GitLab user when token is updated") {
    val settings = WorkspaceSettingsBuilder { onePassword("keychain_token") }

    listener.onConfigurationChange(settings)

    coVerify { userService.invalidateAndFetchCurrentUser() }
  }
})

/**
 * Mocks the singleton ProgressManager to runs tasks synchronously.
 */
private fun mockProgressManager() {
  mockkStatic(ProgressManager::getInstance)

  val progressManager = mockk<ProgressManager>()
  every { ProgressManager.getInstance() } returns progressManager

  val taskSlot = slot<Task>()
  every { progressManager.run(capture(taskSlot)) } answers {
    val progressIndicator = mockk<ProgressIndicator>(relaxed = true)
    taskSlot.captured.run(progressIndicator)
  }
}
