package com.gitlab.plugin.util.webview

import com.intellij.ide.BrowserUtil
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class CefLocalRequestHandlerTest : DescribeSpec({
  describe("CefLocalRequestHandler") {
    val handler = CefLocalRequestHandler("local", "myapp")

    beforeTest {
      mockkStatic(BrowserUtil::class)
    }

    afterTest {
      clearAllMocks()
    }

    describe("shouldOpenInExternalBrowser") {
      context("when handling external links") {
        it("should open https URLs in system browser") {
          val result = handler.shouldOpenInExternalBrowser(
            "https://gitlab.com/some/repo",
            true
          )
          result shouldBe true
        }

        it("should open http URLs in system browser") {
          val result = handler.shouldOpenInExternalBrowser(
            "http://example.com",
            true
          )
          result shouldBe true
        }
      }

      context("when handling localhost URLs") {
        it("should not intercept localhost URLs") {
          val result = handler.shouldOpenInExternalBrowser(
            "http://localhost:3000/api",
            true
          )
          result shouldBe false
        }

        it("should not intercept 127.0.0.1 URLs") {
          val result = handler.shouldOpenInExternalBrowser(
            "http://127.0.0.1:8080/test",
            true
          )
          result shouldBe false
        }
      }

      context("when handling non-user initiated navigation") {
        it("should allow navigation without interception") {
          val result = handler.shouldOpenInExternalBrowser(
            "https://example.com",
            false
          )
          result shouldBe false
        }
      }

      context("when handling invalid URLs") {
        it("should handle malformed URLs gracefully") {
          val result = handler.shouldOpenInExternalBrowser(
            "not-a-valid-url",
            true
          )
          result shouldBe false
        }

        it("should handle null URL gracefully") {
          val result = handler.shouldOpenInExternalBrowser(
            null,
            true
          )
          result shouldBe false
        }
      }

      context("when handling local resources") {
        it("should allow navigation to local resources") {
          val result = handler.shouldOpenInExternalBrowser(
            "local://myapp/index.html",
            true
          )
          result shouldBe false
        }
      }
    }
  }
})
