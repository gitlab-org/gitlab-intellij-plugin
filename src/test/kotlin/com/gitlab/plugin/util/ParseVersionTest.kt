package com.gitlab.plugin.util

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class ParseVersionTest : DescribeSpec({
  describe("parseVersion") {
    it("parses pre-release version with null metadata") {
      parseVersion("2.3.0-alpha.null+null") shouldBe "2.3.0-alpha"
    }

    it("parses release version with null metadata") {
      parseVersion("2.3.0.null+null") shouldBe "2.3.0"
    }

    it("parses pre-release version without any metadata") {
      parseVersion("2.3.0-alpha") shouldBe "2.3.0-alpha"
    }

    it("parses release without any metadata") {
      parseVersion("2.3.0") shouldBe "2.3.0"
    }

    it("parses pre-release version with metadata = '+' only") {
      parseVersion("2.3.0-alpha.+") shouldBe "2.3.0-alpha"
    }

    it("parses empty string") {
      parseVersion("") shouldBe null
    }
  }
})
