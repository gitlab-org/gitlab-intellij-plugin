package com.gitlab.plugin.util

import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TokenUtilTest : BasePlatformTestCase() {
  @BeforeEach
  public override fun setUp() {
    super.setUp()
  }

  @AfterEach
  override fun tearDown() {
    super.tearDown()
  }

  @Test
  fun `it updates token to given value`() {
    val token = "dummyToken"

    TokenUtil.setToken(token)

    assertEquals(token, TokenUtil.getToken())
  }
}
