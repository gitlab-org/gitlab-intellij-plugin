#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
Bundler.require(:default)

PLUGIN_NAME="GitLab Duo Plugin"
BASEPATH=Pathname.new(File.expand_path(File.join('..')))
CHANGELOG_PATH=File.join(BASEPATH, 'build/changelog.txt')
PACKAGE_NAME="gitlab_duo_plugin"
SEMVER_PATTERN=/\A(\d|[1-9]\d+)\.(\d|[1-9]\d+)\.(\d|[1-9]\d+)\z/

# Return the GitLab::Client to interact with REST API v4
#
# @return [Gitlab::Client] client with endpoints and token configured
def api
  @client ||= begin
    Gitlab.configure do |config|
      config.endpoint = ENV.fetch("CI_API_V4_URL", "https://gitlab.com/api/v4")
      config.private_token = gitlab_token
    end

    Gitlab.client
  end
end

# A multi-dimensional hash of parsed properties
#
# @return [JavaProperties::Properties]
def properties
  @properties ||= JavaProperties.load(BASEPATH.join('gradle.properties').to_s)
end

# Return current version number parsed from gradle.properties
#
# @return [String] containing the version
# @raise [StandardError] when format doesnt comply with semantic versioning
def find_current_version
  properties[:'plugin.version'].tap do |version|
    unless version.match?(SEMVER_PATTERN)
      raise StandardError, "Invalid version: #{version}. Must follow semantic versioning format: <MAJOR>.<MINOR>.<PATCH>"
    end
  end
end

# Return a list of generated files
#
# @return [Array<Pathname>] list of files
def local_artifacts
  @artifacts ||= BASEPATH.glob('build/distributions/*.zip')
end

# Retrieve the project ID from the CI
#
# @return [String] containing the project ID
def project_id
  ENV.fetch('CI_PROJECT_ID')
end

# Retrieve the changes for the current release version from the Changelog
#
# @return [<String>] array with current changes
def read_changes
  @fetch_changes ||= if File.file?(CHANGELOG_PATH)
                       File.read(CHANGELOG_PATH)
                     else
                       ''
                     end
end

# Return the release data associated with a provided version
#
# @param [String] version
# @return [nil, Gitlab::ObjectifiedHash]
def find_release(version)
  tag_name = "v#{version}"
  api.project_release(project_id, tag_name)
rescue Gitlab::Error::NotFound
  return nil
end

# Return the package associated to the provided version
#
# @param [String] version
# @return [Gitlab::ObjectifiedHash, nil]
def package(version)
  packages = api.project_packages(project_id, {type: 'generic', package_name: PACKAGE_NAME, sort: 'desc', order_by: 'version'}, )
  packages.find {|pkg| pkg.version == version}
end

def package_artifact_exist?(artifact, package_id)
  files = api.project_package_files(project_id, package_id)

  files.any? {|f| f.file_name == File.basename(artifact)}
end

def publish_artifact!(artifact, version)
  package_filename = File.basename(artifact)
  package_registry_url = Addressable::URI.parse("#{api.endpoint}/projects/#{project_id}/packages/generic/#{PACKAGE_NAME}/#{version}/#{package_filename}")

  auth_header = if ENV['CI_JOB_TOKEN']
                  "JOB-TOKEN: ${CI_JOB_TOKEN}"
                else
                  "Authorization: Bearer #{gitlab_token}"
                end

  cmd = %Q(curl --header "#{auth_header}" --upload-file "#{artifact}" "#{package_registry_url.normalize!}")

  run_shell(cmd)
end

def find_published_artifacts(artifacts, remote_package)
  files = api.project_package_files(project_id, remote_package.id)
  files.select do |f|
    artifacts.any? { |artifact| File.basename(artifact) == f.file_name }
  end
end

def create_release!(version, artifacts = [], changelog: '')
  tag_name = "v#{version}"

  data = {
    name: "#{PLUGIN_NAME} - #{version}",
    tag_name: tag_name,
    assets: {
      links: []
    }
  }

  unless changelog.empty?
    data[:description] = changelog
  end

  begin
    artifacts_base_url = Addressable::URI.parse("#{api.endpoint}/projects/#{project_id}/packages/generic/#{PACKAGE_NAME}/#{version}/")
    artifacts.each do |artifact|
      artifact_url = (artifacts_base_url + artifact.file_name).to_s

      data[:assets][:links] << { name: artifact.file_name, url: artifact_url, link_type: 'package' }
    end

    Gitlab.create_project_release(project_id, data)
  rescue Gitlab::Error::Unprocessable => exception
    if exception.message.include? 'Ref is not specified'
      puts "You need to tag the release and ensure it is pushed to the remote git repository (tag: #{tag_name})"
    else
      raise exception
    end
  end
end

private

# Run a command on shell and print its output
#
# @return [Array<String>] with output content
def run_shell(*args, print_output: true)
  output = []
  cmd = args.join(' ')

  Dir.chdir(BASEPATH.to_s) do
    IO.popen(cmd).each do |line|
      p line.chomp if print_output
      output << line.chomp
    end
  end

  output
end

# Retrieve the GitLab Token from ENV variables
#
# One of the following variables are expected, in order of priority:
# - GITLAB_API_PRIVATE_TOKEN
# - GITLAB_TOKEN
# - CI_JOB_TOKEN
# @raise [KeyError] when none of the ENV variables are set
# @return [String] a PAT or JOB-TOKEN
def gitlab_token
  ENV['GITLAB_API_PRIVATE_TOKEN'] || ENV['GITLAB_TOKEN'] || ENV.fetch('CI_JOB_TOKEN')
end
