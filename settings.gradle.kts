rootProject.name = "gitlab-jetbrains-plugin"

pluginManagement {
  repositories {
    mavenCentral()
    gradlePluginPortal()
  }
}

dependencyResolutionManagement {
  versionCatalogs {
    create("libs") {
      // Plugins
      plugin("apollographql-apollo3", "com.apollographql.apollo3").version("3.8.5")
      plugin("github-gmazzo-buildconfig", "com.github.gmazzo.buildconfig").version("5.5.1")
      plugin("github-gradle-node", "com.github.node-gradle.node").version("7.1.0")
      plugin("gitlab-arturbosch-detekt", "io.gitlab.arturbosch.detekt").version("1.23.8")
      plugin("jetbrains-changelog", "org.jetbrains.changelog").version("2.2.1")
      plugin("jetbrains-intellij", "org.jetbrains.intellij").version("1.17.4")
      plugin("jetbrains-kotlin-jvm", "org.jetbrains.kotlin.jvm").version("1.9.25")
      plugin("jetbrains-kotlin-serialization", "org.jetbrains.kotlin.plugin.serialization").version("1.9.25")
      plugin("sentry", "io.sentry.jvm.gradle").version("5.3.0")

      // Dependencies
      library("apollo.graphql.runtime", "com.apollographql.apollo3", "apollo-runtime").version("3.8.5")
      library("commons.net", "commons-net", "commons-net").version("3.11.1")
      library("jackson.databind", "com.fasterxml.jackson.core", "jackson-databind").version("2.18.3")
      library("jackson.datatype", "com.fasterxml.jackson.datatype", "jackson-datatype-jsr310").version("2.18.3")
      library("kotlinx-datetime", "org.jetbrains.kotlinx", "kotlinx-datetime").version("0.6.2")
      library("ktor.client.auth", "io.ktor", "ktor-client-auth").version("2.3.13")
      library("ktor.client.contentNegotiation", "io.ktor", "ktor-client-content-negotiation").version("2.3.13")
      library("ktor.client.logging", "io.ktor", "ktor-client-logging").version("2.3.13")
      library("ktor.client.okhttp", "io.ktor", "ktor-client-okhttp").version("2.3.13")
      library("ktor.serialization.gson", "io.ktor", "ktor-serialization-gson").version("2.3.13")
      library("ktor.serialization.kotlinxJson", "io.ktor", "ktor-serialization-kotlinx-json").version("2.3.13")
      library("logback.classic", "ch.qos.logback", "logback-classic").version("1.5.17")
      library("snowplow.java.tracker", "com.snowplowanalytics", "snowplow-java-tracker").version("2.1.0")
      library("kotlinResult", "com.michael-bull.kotlin-result", "kotlin-result").version("2.0.1")
      library("commonmark", "org.commonmark", "commonmark").version("0.24.0")

      // Test dependencies
      library("apollo.graphql.mockserver", "com.apollographql.apollo3", "apollo-mockserver").version("3.8.5")
      library(
        "apollo.graphql.testingSupport",
        "com.apollographql.apollo3",
        "apollo-testing-support"
      ).version("3.8.5")
      library("detekt.formatting", "io.gitlab.arturbosch.detekt", "detekt-formatting").version("1.23.8")
      library("junit.jupiter", "org.junit.jupiter", "junit-jupiter").version("5.12.0")
      library("junit.jupiter.api", "org.junit.jupiter", "junit-jupiter-api").version("5.12.0")
      library("junit.jupiter.engine", "org.junit.jupiter", "junit-jupiter-engine").version("5.12.0")
      library("junit.jupiter.params", "org.junit.jupiter", "junit-jupiter-params").version("5.12.0")
      library("junit.platform.launcher", "org.junit.platform", "junit-platform-launcher").version("1.12.0")
      library("junit.platform.suite.engine", "org.junit.platform", "junit-platform-suite-engine").version("1.12.0")
      library("kotest.assertions.core", "io.kotest", "kotest-assertions-core").version("5.9.1")
      library("kotest.framework.datatest", "io.kotest", "kotest-framework-datatest").version("5.9.1")
      library("kotest.runner.junit5", "io.kotest", "kotest-runner-junit5").version("5.9.1")
      library("kotlin.test.junit", "org.jetbrains.kotlin", "kotlin-test-junit").version("1.9.25")
      library("kotlinx.coroutines.test", "org.jetbrains.kotlinx", "kotlinx-coroutines-test").version("1.9.0")
      library("ktor.client.mock", "io.ktor", "ktor-client-mock").version("2.3.13")
      library("mockk", "io.mockk", "mockk").version("1.13.17")
      library(
        "mockserver.junit.jupiter",
        "org.mock-server",
        "mockserver-junit-jupiter-no-dependencies"
      ).version("5.15.0")
      library("okhttp3.logging.interceptor", "com.squareup.okhttp3", "logging-interceptor").version("4.12.0")
      library("lsp4j", "org.eclipse.lsp4j", "org.eclipse.lsp4j").version("0.24.0")
      library("remote.fixtures", "com.intellij.remoterobot", "remote-fixtures").version("0.11.23")
      library("remote.robot", "com.intellij.remoterobot", "remote-robot").version("0.11.23")
      library("turbine", "app.cash.turbine", "turbine").version("1.2.0")
      library("video.recorder.junit5", "com.automation-remarks", "video-recorder-junit5").version("2.0")
    }
  }
}
